﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgottenPassword.aspx.cs" Inherits="SUDB.ForgottenPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Students' Union Database</title>

    <link rel="stylesheet" href="Styles/LoginPage.css" />
    <meta name="viewport" content="width=device-width, user-scalable=no">
</head>
<body>
    <form id="form1" runat="server">
    <div id="Wrapper">

          <div id="SiteContainer">

              <div id="ContentArea">

                  <h1>Students' Union Management Hub</h1>

                 <p class="center"><img src="Images/fsu.png" alt="Footsqueek for Students' Unions" /></p>
                  
                  <p class="center">If you are having difficulty loging on you can reset your password by entering your email address below.</p>

                     <div id="loginbox">

                         <asp:Literal ID="litError" Visible="false" runat="server"></asp:Literal>


                   <p"><asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Email Address:</asp:Label><br />
                                              
                     <asp:TextBox CssClass="txtbox" ID="UserName" runat="server"></asp:TextBox>

                                                  <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="You must enter your username." ToolTip="User Name is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator></p>
                                             
                      <p class="center"><asp:Button ID="LoginButton" runat="server" CssClass="submitbutton" CommandName="Login" Text="Reset Password" ValidationGroup="Login1" OnClick="LoginButton_Click" /></p>
                                          </div>  
                 


              </div>

          </div>
    
    </div>


            <div id="Footer">

                <div id="FooterContent">
           
                    </div>

        </div>

               <div id="BottomMenu">

                <div id="BottomMenuContent">
                    <p class="FootsqueekCredit">System Developed By <a href="http://www.footsqueek.co.uk">Footsqueek</a></p>
            
                    
                    </div>

        </div>
    </form>
</body>
</html>

