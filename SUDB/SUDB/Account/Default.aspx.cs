﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Account
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Common tools = new Common();
                var permissions = tools.getPermissions((Guid)Membership.GetUser().ProviderUserKey);

                if (permissions.roomBookingEmails == 1)
                {
                    chkRoomBooking.Checked = true;
                }
                if (permissions.caseEmails == 1)
                {
                    chkCaseManagement.Checked = true;
                }
                if (permissions.transportBookingEmails == 1)
                {
                    chkTransportSystem.Checked = true;
                }

                if (permissions.tripRegEmails == 1)
                {
                    chkTripReg.Checked = true;
                }

                if (tools.checkAccess("DutyOfCare") == "Full Access" || (tools.checkAccess("DutyOfCare") == "Read Only"))
                {
                    triptr.Visible = true;
                    roomtr.Visible = true;
                    transporttr.Visible = true;
                }
                else
                {
                    triptr.Visible = false;
                    roomtr.Visible = false;
                    transporttr.Visible = false;
                }
            }

            if(Roles.IsUserInRole("Staff"))
            {
                emailpreferences.Visible = true;
            }
            else
            {
                emailpreferences.Visible = false;
            }



        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Common tools = new Common();
            var changepass = tools.ChangePassword(txtOld.Text.Trim(), txtPass.Text.Trim());
            if(changepass)
            {
                litError.Text = "<p class=\"correct\">Your password change was successful</p>";
                
            }
            else
            {
                litError.Text = "<p class=\"error\">Your password change failed, please try again</p>";
            }

            txtPass.Text = "";
            txtConfirm.Text = "";
            txtOld.Text = "";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Common tools = new Common();

            int rooms = 0;
            int transport = 0;
            int cases = 0;
            int tripreg = 0;

            if(chkCaseManagement.Checked)
            {
                cases = 1;
            }
            if (chkTransportSystem.Checked)
            {
               transport = 1;
            }
            if (chkRoomBooking.Checked)
            {
                rooms = 1;
            }
            if (chkTripReg.Checked)
            {
                tripreg = 1;
            }

            tools.updateEmailPrefs(cases, transport, rooms, tripreg);
            
        }
    }
}