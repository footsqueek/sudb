﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Account.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Manage Account</h1>

    <p>Use this page to make changes to your account.</p>
      <asp:Literal ID="litError" runat="server"></asp:Literal>


    <p>
        <asp:Label ID="Label1" runat="server" CssClass="label" Text="Old Password"></asp:Label><br />
        <asp:TextBox CssClass="txtbox" ID="txtOld" TextMode="Password" runat="server"></asp:TextBox></p>

     <p>
        <asp:Label ID="Label2" runat="server" CssClass="label" Text="New Password"></asp:Label><br />
        <asp:TextBox CssClass="txtbox" ID="txtPass" TextMode="Password" runat="server"></asp:TextBox></p>

     <p>
        <asp:Label ID="Label3" runat="server" CssClass="label" Text="Confirm New Password"></asp:Label><br />
        <asp:TextBox CssClass="txtbox" ID="txtConfirm" TextMode="Password" runat="server"></asp:TextBox></p>

    <p class="clear"><asp:CompareValidator ID="CompareValidator1" runat="server" CssClass="error" ErrorMessage="Your new and confirm passwords do not match" ControlToCompare="txtConfirm" ControlToValidate="txtPass"></asp:CompareValidator><br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="error" ErrorMessage="You must enter your old password" ControlToValidate="txtOld"></asp:RequiredFieldValidator></p>

  
    <asp:Button CssClass="submitbutton" ID="btnSubmit" runat="server" CausesValidation="true" Text="Change Password" OnClick="btnSubmit_Click" />

    <div id="emailpreferences" runat="server" visible="false">
    <h2>Email Preferences</h2>

        <p>Please select the systems you would like to receive emails from:</p>

        <table class="nicetable">

                <tr runat="server" id="casetr">

                <td>Case Management System</td>
                <td>
                    <asp:CheckBox ID="chkCaseManagement" runat="server" /></td>

            </tr>

            <tr runat="server" id="triptr">

                <td>Trip Registration System</td>
                <td>
                    <asp:CheckBox ID="chkTripReg" runat="server" /></td>

            </tr>

              <tr runat="server" id="roomtr">

                <td>Room Booking System</td>
                <td>
                    <asp:CheckBox ID="chkRoomBooking" runat="server" /></td>

            </tr>

                <tr runat="server" id="transporttr">

                <td>Transport Booking System</td>
                <td>
                    <asp:CheckBox ID="chkTransportSystem" runat="server" /></td>

            </tr>


        </table>

        <asp:Button ID="btnSave" ValidationGroup="email" CssClass="submitbutton" runat="server" Text="Save Email Permissions" OnClick="btnSave_Click" />
        </div>

</asp:Content>
