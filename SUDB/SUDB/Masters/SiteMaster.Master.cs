﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.Profile;

namespace SUDatabase.Masters
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           

          //Redact Menu
            //Check Access
            Common tools = new Common();
            if (!tools.checkAccess("SystemManager").Equals("Full Access") && !tools.checkAccess("SystemManager").Equals("Read Only"))
               menuSystemManager.Visible = false;
            else
               menuSystemManager.Visible = true;

            if (!tools.checkAccess("Reports").Equals("Full Access") && !tools.checkAccess("Reports").Equals("Read Only"))
                menuReports.Visible = false;
            else
                menuReports.Visible = true;

            if (!tools.checkAccess("DutyOfCare").Equals("Full Access") && !tools.checkAccess("DutyOfCare").Equals("Read Only"))
                menuDutyOfCare.Visible = false;
            else
                menuDutyOfCare.Visible = true;

            if (!tools.checkAccess("Elections").Equals("Full Access") && !tools.checkAccess("Elections").Equals("Read Only"))
                menuElections.Visible = false;
            else
                menuElections.Visible = true;

            if (!tools.checkAccess("TripRegistration").Equals("Full Access"))
                menuTripReg.Visible = false;
            else
                menuTripReg.Visible = true;
            if (!tools.checkAccess("StudentManager").Equals("Full Access") && !tools.checkAccess("StudentManager").Equals("Read Only"))
                menuStudentManager.Visible = false;
            else
                menuStudentManager.Visible = true;
            if (!tools.checkAccess("Bar").Equals("Full Access") && !tools.checkAccess("Bar").Equals("Read Only"))
                menuBar.Visible = false;
            else
                menuBar.Visible = true;


            var su = tools.getSU();
            imgLogo.ImageUrl = su.BigLogoURL;


            var userdetails = Membership.GetUser();

            ProfileBase profile = ProfileBase.Create(userdetails.UserName);

            double CurrentTimestamp = (userdetails.CreationDate - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalSeconds;
            double SUCurrentTimestamp = ((DateTime)tools.getSU().CreatedDate - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalSeconds;

            litIntercom.Text =  "<script>window.intercomSettings = {app_id: \"p8qhcfsc\",name: \"" + (string)profile["FirstName"]  + " " + (string)profile["LastName"] + "\", email: \""+userdetails.Email+"\",created_at:" +CurrentTimestamp.ToString()+ ",company: {id: '"+tools.getSU().Id.ToString()+"',name: '"+tools.getSU().SUShortName+"', created_at: "+SUCurrentTimestamp.ToString()+"}};</script>";

            if(tools.returnDBName() != "footsque_sudb")
            {
                devdb.Visible = true;
            }
            else
            {
                devdb.Visible = false;
            }
            

        }
    }
}