﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using SUDatabase.Classes;
using SUDB;

namespace SUDatabase
{
    public partial class StudentUploader : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                DataClasses1DataContext db = new DataClasses1DataContext();

                int countin = 0;
                int countout = 0;
                int errorcount = 0;
                StreamReader reader = new StreamReader(FileUpload1.FileContent);
                while(!reader.EndOfStream)
                {
                    try
                    {
                        string textLine = reader.ReadLine();

                        string[] splitstring = textLine.Split(new string[] { "," }, StringSplitOptions.None);

                        Common tools = new Common();


                        var student = db.tblStudents.Where(p => p.StudentNumber == splitstring[0].Trim()).Where(p=>p.StudentsUnion==1).FirstOrDefault();

            
                       

                        if (student != null)
                        {
                            countin++;

                            student.FirstName = splitstring[1].Trim();
                            student.Surname = splitstring[2].Trim();
                            student.DateOfBirth = DateTime.Parse(splitstring[3].Trim());
                            student.Campus = int.Parse(splitstring[6]);
                            student.Faculty = int.Parse(splitstring[5]);
                            student.Course = splitstring[4].Trim();
                            student.Level = splitstring[7].Trim();
                            student.Gender = splitstring[8].Trim();
                            student.MembershipType = 1;
                            student.ModeOfStudy = splitstring[9].Trim();
                            /*student.UniversityEmail = splitstring[10].Trim();
                            student.FeeStatus = splitstring[11].Trim();
                            student.PersonalEmail = splitstring[10].Trim();*/

                            db.SubmitChanges();

                        }
                        else
                        {

                         
                            tblStudent newstudent = new tblStudent
                            {
                                StudentNumber = splitstring[0].Trim(),
                                FirstName = splitstring[1].Trim(),
                                Surname = splitstring[2].Trim(),
                                DateOfBirth = DateTime.Parse(splitstring[3].Trim()),
                                Faculty = int.Parse(splitstring[5]),
                                Campus = int.Parse(splitstring[6]),

                                Course = splitstring[4].Trim(),


                                Level = splitstring[7].Trim(),
                                Gender = splitstring[8].Trim(),
                                MembershipType = 1,
                                StudentsUnion = 1,
                                ModeOfStudy= splitstring[9].Trim(),
                                //UniversityEmail = splitstring[10].Trim(),
                                //FeeStatus= splitstring[11].Trim(),
                                //PersonalEmail= splitstring[10].Trim()
                            };

                            db.tblStudents.InsertOnSubmit(newstudent);
                            db.SubmitChanges();
                            countout++;
                        }

                    }
                    catch(Exception r)
                    {
                        Literal1.Text = Literal1.Text + "<p>Error In: " + reader.ReadLine() + "<br/>" + r.ToString() + "</p>";

                        errorcount++;
                    }
                }

                Literal1.Text = Literal1.Text + "<p>" + countin.ToString() + "</p>";
                Literal1.Text = Literal1.Text + "<p>" + countout.ToString() + "</p>";
                Literal1.Text = Literal1.Text + "<p>" + errorcount.ToString() + "</p>";

            }
        }
    }
}