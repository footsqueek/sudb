﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDB
{
    public partial class SportsMems : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();

            var members = (from p in db.tblMemberships join u in db.tblStudents on p.StudentId equals u.Id select u).Distinct();

            rptMems.DataSource = members;
            rptMems.DataBind();
            int i = 0;

            foreach(var member in members)
            {
                ((Label)rptMems.Items[i].FindControl("lblStudentNo")).Text = member.StudentNumber;
                i++;
            }
        }
    }
}