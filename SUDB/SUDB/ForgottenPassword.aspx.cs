﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace SUDB
{
    public partial class ForgottenPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (LoginButton.Text == "Return to Login Page")
                {
                    Response.Redirect("Default.aspx");
                }
                else
                {

                    MembershipUser useris = Membership.GetUser(UserName.Text);

                    Common tools = new Common();

                    if (tools.resetPassword(Guid.Parse(useris.ProviderUserKey.ToString())))
                    {
                        litError.Visible = true;
                        litError.Text = "<p class=\"error\">Your password has been reset and an e-mail has been sent to your e-mail address. If you continue to experience problems please contact your Students' Union.</p>";

                        UserName.Visible = false;

                        UserNameLabel.Visible = false;


                        LoginButton.Text = "Return to Login Page";

                    }
                    else
                    {
                        litError.Visible = true;
                        litError.Text = "<p class=\"error\">The e-mail address you have entered cannot be found on the system. Please try again</p>";
                    }

                }

            }
            catch
            {
                litError.Visible = true;
                litError.Text = "<p class=\"error\">There has been an error, Please try again.</p>";
              
            }

        
           
        }
    }
}