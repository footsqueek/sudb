﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDB
{
    public partial class Stats : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
        
            var range10 = from p in db.tblRegisters join x in db.tblSessions on p.SessionId equals x.Id where x.StartDateTime.Value.Month == 10 where p.AttendanceStatus==1 select p;
            lblOutput.Text = range10.Count().ToString();

            var range11 = from p in db.tblRegisters join x in db.tblSessions on p.SessionId equals x.Id where x.StartDateTime.Value.Month == 11 where p.AttendanceStatus == 1 select p;
            lblOutput.Text = lblOutput.Text + " " + range11.Count().ToString();

            var range12 = from p in db.tblRegisters join x in db.tblSessions on p.SessionId equals x.Id where x.StartDateTime.Value.Month == 12 where p.AttendanceStatus == 1 select p;
            lblOutput.Text = lblOutput.Text + " " + range12.Count().ToString();

            var range1 = from p in db.tblRegisters join x in db.tblSessions on p.SessionId equals x.Id where x.StartDateTime.Value.Month == 1 where p.AttendanceStatus == 1 select p;
            lblOutput.Text = lblOutput.Text + " " + range1.Count().ToString();

            var range2 = from p in db.tblRegisters join x in db.tblSessions on p.SessionId equals x.Id where x.StartDateTime.Value.Month == 2 where p.AttendanceStatus == 1 select p;
            lblOutput.Text = lblOutput.Text + " " + range2.Count().ToString();

            var range3 = from p in db.tblRegisters join x in db.tblSessions on p.SessionId equals x.Id where x.StartDateTime.Value.Month == 3 where p.AttendanceStatus == 1 select p;
            lblOutput.Text = lblOutput.Text + " " + range3.Count().ToString();

            var range4 = from p in db.tblRegisters join x in db.tblSessions on p.SessionId equals x.Id where x.StartDateTime.Value.Month == 4 where p.AttendanceStatus == 1 select p;
            lblOutput.Text = lblOutput.Text + " " + range4.Count().ToString();

            var range5 = from p in db.tblRegisters join x in db.tblSessions on p.SessionId equals x.Id where x.StartDateTime.Value.Month == 5 where p.AttendanceStatus == 1 select p;
            lblOutput.Text = lblOutput.Text + " " + range5.Count().ToString();

        }
    }
}