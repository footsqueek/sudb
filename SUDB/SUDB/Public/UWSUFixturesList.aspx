﻿<%@ Page Title="Fixtures" Language="C#" MasterPageFile="~/Masters/UWSUShop.Master" AutoEventWireup="true" CodeBehind="UWSUFixturesList.aspx.cs" Inherits="SUDatabase.Public.UWSUFixturesList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    
    <h2>BUCS Fixtures - <asp:Label ID="lblFixturesDate" runat="server" Text="Label"></asp:Label></h2>

    <table class="nicetable">

        <tr>
            <th>Club</th>
            <th>Team</th>
            <th>H/A</th>
            <th>v</th>
            <th>Time</th>
            <th>Venue</th>
            <th>Postcode</th>
            <th>Transport</th>
            <th>Depart</th>
            <th>Meet</th>
          
            <th>Teas</th>
            <th>Time</th>
               <th>Our Score</th>
               <th>Their Score</th>
            <th></th>

        </tr>

        <asp:Repeater ID="rptFixtures" runat="server">

            <ItemTemplate>

                <tr>

                     <td><asp:Label ID="lblTeam" runat="server" Text="Label"></asp:Label></td>
                      <td><asp:Label ID="lblActualTeam" runat="server" Text=""></asp:Label></td>
                     <td><asp:Label ID="lblHomeAway" runat="server" Text="Label"></asp:Label></td>
                     <td><asp:Label ID="lblv" runat="server" Text="Label"></asp:Label></td>
                     <td><asp:Label ID="lblTime" runat="server" Text="Label"></asp:Label></td>
                     <td><asp:Label ID="lblVenue" runat="server" Text="Label"></asp:Label></td>
                     <td><asp:Label ID="lblPostCode" runat="server" Text="Label"></asp:Label></td>
                     <td><asp:Label ID="lblTransport" runat="server" Text="Label"></asp:Label></td>
                     <td><asp:Label ID="lblDepart" runat="server" Text="Label"></asp:Label></td>
                     <td><asp:Label ID="lblMeet" runat="server" Text="Label"></asp:Label></td>
                   
                     <td><asp:Label ID="lblTeas" runat="server" Text="Label"></asp:Label></td>
                     <td><asp:Label ID="lblTeasTime" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblOurScore" runat="server" Text=""></asp:Label></td>
                                   <td><asp:Label ID="lblTheirScore" runat="server" Text=""></asp:Label></td>
               
                        

                </tr>

            </ItemTemplate>


        </asp:Repeater>
    </table>

</asp:Content>
