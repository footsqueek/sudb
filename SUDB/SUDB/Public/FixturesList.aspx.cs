﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Public
{
    public partial class FixturesList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bindFixtures();
        }

        protected void bindFixtures()
        {

            DateTime today = DateTime.Today;
            // The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
            int daysUntilWednesday = ((int)DayOfWeek.Wednesday - (int)today.DayOfWeek + 7) % 7;
            DateTime nextWednesday = today.AddDays(daysUntilWednesday);

            lblFixturesDate.Text = nextWednesday.ToString("dd MMM yyyy");

            try
            {
                DateTime fixdate = DateTime.Parse(Request.QueryString["date"]);

                if (fixdate != null)
                {
                  
                    lblFixturesDate.Text = fixdate.ToString("dd MMM yyyy");
                }
            }
            catch
            {

            }

            try
            {
                Common tools = new Common();

                var fixtures = tools.returnFixtures(DateTime.Parse(lblFixturesDate.Text));

                rptFixtures.DataSource = fixtures;
                rptFixtures.DataBind();

                int i = 0;

                foreach (var fixture in fixtures)
                {

                    var sportsoc = tools.returnSportSociety((int)fixture.SportSocietyId);

                    if (fixture.teamId > 0)
                    {
                        try
                        {
                            var team = tools.returnTeam((int)fixture.teamId);
                            ((Label)rptFixtures.Items[i].FindControl("lblActualTeam")).Text = team.TeamName;
                        }
                        catch
                        {
                            ((Label)rptFixtures.Items[i].FindControl("lblActualTeam")).Text = "";
                        }

                    }
                    else
                    {



                    }

                    ((Label)rptFixtures.Items[i].FindControl("lblTeam")).Text = sportsoc.Name;

                    if (fixture.TripRegId > 0)
                    {
                        ((Label)rptFixtures.Items[i].FindControl("lblHomeAway")).Text = "A";
                        ((Label)rptFixtures.Items[i].FindControl("lblDepart")).Text = ((DateTime)fixture.Depart).ToString("HH:mm");
                        ((Label)rptFixtures.Items[i].FindControl("lblMeet")).Text = ((DateTime)fixture.Depart.Value.AddMinutes(-15)).ToString("HH:mm");

                    }
                    else
                    {
                        ((Label)rptFixtures.Items[i].FindControl("lblHomeAway")).Text = "H";
                        ((Label)rptFixtures.Items[i].FindControl("lblDepart")).Text = "";
                        ((Label)rptFixtures.Items[i].FindControl("lblMeet")).Text = "";

                    }

                    ((Label)rptFixtures.Items[i].FindControl("lblv")).Text = fixture.SessionName;
                    ((Label)rptFixtures.Items[i].FindControl("lblVenue")).Text = fixture.Location;
                    ((Label)rptFixtures.Items[i].FindControl("lblPostCode")).Text = fixture.PostCode;
                    ((Label)rptFixtures.Items[i].FindControl("lblTime")).Text = ((DateTime)fixture.StartDateTime).ToString("HH:mm");
                    ((Label)rptFixtures.Items[i].FindControl("lblTeasTime")).Text = ((DateTime)fixture.TeasTime).ToString("HH:mm");
                    ((Label)rptFixtures.Items[i].FindControl("lblTeas")).Text = fixture.NoTeas.ToString();
                    ((Label)rptFixtures.Items[i].FindControl("lblTransport")).Text = fixture.Transport;
                    ((Label)rptFixtures.Items[i].FindControl("lblOurScore")).Text = fixture.ourScore.ToString();
                    ((Label)rptFixtures.Items[i].FindControl("lblTheirScore")).Text = fixture.theirScore.ToString();

                    if(fixture.CupLeague !=null)
                    {
                        ((Label)rptFixtures.Items[i].FindControl("lblCup")).Text = fixture.CupLeague;

                    }

                    if (fixture.NoTeas == 0)
                    {
                        ((Label)rptFixtures.Items[i].FindControl("lblTeasTime")).Text = "";
                        ((Label)rptFixtures.Items[i].FindControl("lblTeas")).Text = "";

                    }


                    i++;
                }
            }
            catch
            {

            }
        }
    }
}