﻿using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.Cases
{
    public partial class StaffCases : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();

            litNoOpen.Visible = false;

            if (!IsPostBack)
            {

                var users = Roles.GetUsersInRole("Staff");

                foreach (var user in users)
                {
                    var memuser = Membership.GetUser(user);

                    var permissions = tools.getPermissions((Guid)memuser.ProviderUserKey);

                    if (permissions.StudentsUnion == tools.getSU().Id)
                    {

                        ProfileBase profile = ProfileBase.Create(memuser.UserName);

                        if (profile["FirstName"].ToString().Equals(""))
                        {
                            cboStaff.Items.Add(new ListItem(memuser.UserName, memuser.ProviderUserKey.ToString()));

                        }
                        else
                        {
                            cboStaff.Items.Add(new ListItem(profile["FirstName"] + " " + profile["LastName"], memuser.ProviderUserKey.ToString()));
                        }
                    }
                }

                var curentuser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                cboStaff.SelectedValue = curentuser.ProviderUserKey.ToString();
            }

            var cases = tools.ReturnOpenStaffCases(Guid.Parse(cboStaff.SelectedValue));

            string closed = Request.QueryString["closed"];
            if(closed=="true")
            {
                cases = tools.ReturnClosedStaffCases(Guid.Parse(cboStaff.SelectedValue));
                litNoOpen.Text = "<p class=\"error\">This staff member has no closed cases that were assigned to them.</p>";
                lblPageHeading.Text = "View Closed Cases By Staff Member";
            }

            

            rptOpenCases.DataSource = cases;
            rptOpenCases.DataBind();

            int i = 0;

            foreach (tblCase curcase in cases)
            {
                //Lookup Student
                var student = tools.returnStudent((int)curcase.StudentId);

                IQueryable<tblCaseLog> notes = tools.ReturnCaseNotes(curcase.caseId);
                notes = notes.OrderByDescending(p => p.DateTime);
                var lastnote = notes.First();

                ((Label)rptOpenCases.Items[i].FindControl("lblCaseType")).Text = ((tblCaseType)tools.returnCaseType((int)curcase.caseType)).CaseType;
                ((Label)rptOpenCases.Items[i].FindControl("lblDateOpened")).Text = ((DateTime)curcase.dateOpened).ToString("dd MMM yyyy HH:mm");
                ((Label)rptOpenCases.Items[i].FindControl("lblLastActivity")).Text = ((DateTime)lastnote.DateTime).ToString("dd MMM yyyy HH:mm");
                ((Label)rptOpenCases.Items[i].FindControl("lblStudentName")).Text = student.FirstName + " " + student.Surname + " (" + student.StudentNumber + ")";
              
                ((Button)rptOpenCases.Items[i].FindControl("btnView")).CommandArgument = curcase.caseId.ToString();




                i++;
            }


            if (i == 0)
            {
                litNoOpen.Visible = true;
            }


            String access = tools.checkAccess("CaseLog");
            var currentUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

            if (access == "Full Access" || access == "Read Only" )
            {
                cboStaff.Enabled = true;

            }
            else
            {
                cboStaff.Enabled = false;
            }

            lblTotalCases.Text = tools.returnNoStaffCases(Guid.Parse(cboStaff.SelectedValue)).ToString();
            lblTotalCasesClosed.Text = tools.returnNoStaffCasesClosed(Guid.Parse(cboStaff.SelectedValue)).ToString();
        }

        public void btnView_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            Response.Redirect("../Cases/ViewCase.aspx?caseId=" + btnView.CommandArgument);
        }

        protected void btnDone_Click(object sender, EventArgs e)
        {
            Response.Redirect("../StudentManager/Default.aspx");
        }
    }
}