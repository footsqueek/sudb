﻿<%@ Page Title="Cases By Type" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="CasesByType.aspx.cs" Inherits="SUDatabase.Management.Cases.CasesByType" %>
<%@ Register src="../../UserControls/CaseMenu.ascx" tagname="CaseMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script>
         $(document).ready(function () {
             $("#tablesorted").tablesorter();
            
         }
 );

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CaseMenu ID="CaseMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Cases By Type</h1>

    <table id="tablesorted" class="nicetable">
        <thead>
        <tr>
            <th>Case Type</th>
            <th>Open Cases</th>
             <th>Total Cases</th>
             <th>Average Age</th>
            <th>Total Time</th>
        </tr>
            </thead>
        <tbody>

        <asp:Repeater ID="rptTypes" runat="server">
            <ItemTemplate>
            <tr>

                <td><asp:Label ID="lblType" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblTotalOpen" runat="server" Text="Label"></asp:Label></td>
                    <td><asp:Label ID="lblTotal" runat="server" Text="Label"></asp:Label></td>
                 <td><asp:Label ID="lblAge" runat="server" Text="Label"></asp:Label></td>
                         <td><asp:Label ID="lblTime" runat="server" Text="Label"></asp:Label></td>
            </tr>
            </ItemTemplate>

        </asp:Repeater>

        <tr>
            <th>Totals</th>
               <th><asp:Label ID="lblTotOpen" runat="server" Text="Label"></asp:Label></th>
                 <th><asp:Label ID="lblTotCases" runat="server" Text="Label"></asp:Label></th>
                 <th><asp:Label ID="lblAverageAge" runat="server" Text="Label"></asp:Label></th>
            <th><asp:Label ID="lblTotalTime" runat="server" Text="Label"></asp:Label></th>

        </tr>
</tbody>
    </table>

    <div id="addnew" runat="server">
    <asp:TextBox CssClass="txtbox" ID="txtNewMenuItem" runat="server"></asp:TextBox> <asp:Button CssClass="submitbutton" ID="btnAddItem" runat="server" Text="Add Case Type" OnClick="btnAddItem_Click" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNewMenuItem" ErrorMessage="<p class='error'>You must enter a new case type</p>"></asp:RequiredFieldValidator>
        </div>
</asp:Content>
