﻿<%@ Page Async="true" Title="View Case" ValidateRequest="false" MaintainScrollPositionOnPostback="true" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ViewCase.aspx.cs" Inherits="SUDatabase.Management.Cases.ViewCase" %>
<%@ Register src="../../UserControls/CaseMenu.ascx" tagname="CaseMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!--
     <script>
jQuery(function(){

var minimized_elements = $('.minimize');
var maxLines = 1;

minimized_elements.each(function(){    
    // var textArr = $(this).text().split(/\n/); // Not supported in IE < 9
    var textArr = $(this).html().replace(/\n?<br>/gi,"<br>").split(/<br>/);
    var countLines = textArr.length;

    if (countLines > maxLines) {
        text_less = textArr.slice(0, maxLines).join("<br>");
        text_more = textArr.slice(maxLines, countLines).join("<br>");
    }
    else return;

    $(this).html(
        text_less + '<span>... </span><a href="#" class="more">More</a>'+
        '<span style="display:none;">'+ text_more +' <a href="#" class="less">Less</a></span>'
    );
}); 

$('a.more', minimized_elements).click(function(event){
    event.preventDefault();
    $(this).hide().prev().hide();
    $(this).next().show();        
});

$('a.less', minimized_elements).click(function(event){
    event.preventDefault();
    $(this).parent().hide().prev().show().prev().show();    
});

});

</script>-->


    <script type="text/javascript" src="/Java/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link charmap anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime contextmenu paste textcolor colorpicker"
    ],
    toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | forecolor backcolor emoticons"
});
</script>

   
    <script>
         $(document).ready(function () {
          

             $('#txtActionDate').datetimepicker({
                 timepicker: false,
                 format: 'd M Y',
                 lang: 'en',
                 minDate: '0'

             });
         }
 );

    </script>


    <script> 
    function toggle(divid) {
	var ele = document.getElementById("Toggle"+divid);
	var text = document.getElementById("Expand" + divid);

	if (ele.style.display == "block") {
    		ele.style.display = "none";
    		text.innerHTML = "Show Note";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "Hide Note";
	}
} 
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CaseMenu ID="CaseMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>View Case</h1>
    <div id="casecontent">
  <p><asp:Label CssClass="label" ID="Label6" runat="server" Text="Student Name"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtName" Text="" Enabled="false" runat="server"></asp:TextBox></p>

        <h2>Student Details</h2>

        <table class="lefttable nicetable">

                <tr><td><strong>Date of Birth: </strong></td><td><asp:Label ID="lblDOB" runat="server" Text="Label"></asp:Label></td></tr>
        <tr><td><strong>Faculty: </strong></td><td><asp:Label ID="lblFaculty" runat="server" Text="Label"></asp:Label></td></tr>
               <tr><td><strong>Course: </strong></td><td><asp:Label ID="lblCourse" runat="server" Text=""></asp:Label></td></tr>
 
              <tr><td><strong>Level: </strong></td><td><asp:Label ID="lblLevel" runat="server" Text="Label"></asp:Label></td></tr>
               <tr><td><strong>Campus: </strong></td><td><asp:Label ID="lblcampus" runat="server" Text="Label"></asp:Label></td></tr>
             <tr><td><strong>Mobile Number: </strong></td><td><asp:Label ID="lblMobile" runat="server" Text="Label"></asp:Label></td></tr>
  <tr><td><strong>Email Address: </strong></td><td><asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label></td></tr>
 
            </table>

        <h2>Case Details</h2>


          <p><asp:Label  CssClass="label" ID="Label3" runat="server" Text="Case Type"></asp:Label><br /><asp:DropDownList CssClass="combobox" ID="cboCaseType" runat="server"></asp:DropDownList></p>


     <p class="leftpara"><asp:Label  CssClass="label" ID="Label4" runat="server" Text="Date Created"></asp:Label><br />
         <asp:TextBox ID="txtDateCreated" CssClass="txtbox" runat="server"></asp:TextBox></p>

     <p class="rightpara"><asp:Label  CssClass="label" Enabled="false" Visible="false" ID="lblClosed" runat="server" Text="Date Closed"></asp:Label><br />
         <asp:TextBox ID="txtClosed" Enabled="false"  CssClass="txtbox" Visible="false" runat="server"></asp:TextBox></p>

   
      <div class="clear"></div>
      <p class="leftpara"><asp:Label  CssClass="label" ID="Label1" runat="server" Text="Created By"></asp:Label><br /><asp:DropDownList CssClass="combobox" ID="cboCreatedBy" runat="server"></asp:DropDownList></p>


      <p class="rightpara"><asp:Label  CssClass="label" ID="Label2" runat="server" Text="Assigned To"></asp:Label><br /><asp:DropDownList CssClass="combobox" ID="cboAssignedTo" runat="server"></asp:DropDownList></p>


    <p class="leftpara"><asp:Label  CssClass="labelsmall" ID="Label5" runat="server" Text="Total Time: "></asp:Label><asp:Label ID="lblTime" runat="server" Text="15 minutes"></asp:Label></p>
        <p class="rightpara"><asp:Label  CssClass="labelsmall" ID="Label8" runat="server" Text="Case Age: "></asp:Label><asp:Label ID="lblAge" runat="server" Text="5 days"></asp:Label></p>


           <h2>Tags</h2>
        <table class="lefttable nicetable">

         

            <asp:Repeater ID="rptTags" runat="server">

                <ItemTemplate>

                     
            <tr>

                <td>
                    <asp:Label ID="lblTag"  runat="server" Text="Label"></asp:Label></td>

                <td>
                    <asp:Button ValidationGroup="new" ID="btnActive" CssClass="standardbutton" OnClick="btnActive_Click" runat="server" Text="Remove Tag" /></td>
            </tr>

                </ItemTemplate>

            </asp:Repeater>

            <tr>

                <th><asp:DropDownList ID="cboTags" CssClass="combobox" runat="server"></asp:DropDownList></th>
                <th><asp:Button ID="btnAdd" CssClass="standardbutton" runat="server" Text="Add Tag" OnClick="btnAdd_Click" /></th>
            </tr>

        </table>
        
                  

        </div>

    <div class="clear"></div>

<h2>Case Notes</h2>
    <table class="lefttable nicetable">

        <tr>

            <th>Date / Time</th>
            <th>Staff Member</th>
            <th>Time</th>

        </tr>
        
        <tr id="createnotes1" runat="server">
            <td><asp:Label ID="lblDateTime" runat="server" Text="Label"></asp:Label></td>
                 <td><asp:Label ID="lblStaffMember" runat="server" Text="Label"></asp:Label></td>
            <td><asp:DropDownList ID="cboTime" runat="server"></asp:DropDownList></td>

        </tr>

        <tr  id="createnotes2" runat="server">

            <td class="notes" colspan="3">

                <asp:TextBox Rows="2" CssClass="txtbox" TextMode="MultiLine" ID="txtNotes" runat="server"></asp:TextBox>

     
                <br /><br />
                <asp:fileupload id="attachment" runat="server"></asp:fileupload>
                <br />
                <asp:fileupload id="attachment1" runat="server"></asp:fileupload>
                <br />
                <asp:fileupload id="attachment2" runat="server"></asp:fileupload>
                
                <br /><br />

                <strong>Next Action:</strong> <asp:DropDownList ID="cboNextAction" runat="server"></asp:DropDownList>  <strong>Action By:</strong> <asp:DropDownList ID="cboActionBy" runat="server"></asp:DropDownList> <strong>Action Date:</strong> <asp:TextBox ID="txtActionDate" runat="server"></asp:TextBox>

                <br />

                 <asp:Button ValidationGroup="notesgroup" CausesValidation="true"  CssClass="submitbutton" ID="btnSaveClose" runat="server" Text="Add Notes" OnClick="btnSaveClose_Click" /> 


            </td>

        </tr>
    <asp:Repeater ID="rptCaseNotes" runat="server">

        <ItemTemplate>
            
           
            <tr>
                 <td><asp:Label ID="lblDateTime" runat="server" Text="Label"></asp:Label></td>
                 <td><asp:Label ID="lblStaffMember" runat="server" Text="Label"></asp:Label></td>
                 <td>
                     <asp:DropDownList ID="cboTime" runat="server"></asp:DropDownList></td>
                </tr>
            <tr>
                 <td class="notes" colspan="3"><asp:Label ID="lblNotes" style="display:none" CssClass="minimize" runat="server" Text="Label"></asp:Label> <a id="expand" runat="server">Show Note</a><br /><br /><asp:LinkButton OnClick="lblAttachments_Click" runat="server" id="lblAttachments" Text=""></asp:LinkButton> <asp:LinkButton OnClick="lblAttachments_Click" runat="server" id="lblAttachment1" Text=""></asp:LinkButton> <asp:LinkButton OnClick="lblAttachments_Click" runat="server" id="lblAttachment2" Text=""></asp:LinkButton></td>
            </tr>
           

        </ItemTemplate>

    </asp:Repeater>

        </table>

        <p><asp:Button  CssClass="submitbutton" ID="Button1" runat="server" Text="Save Changes" OnClick="Button1_Click" /> 
                         <asp:Button  CssClass="submitbutton" ID="Button2" runat="server" Text="Save and Close Case" OnClick="Button2_Click" />

  <asp:Button  CssClass="submitbutton" ID="Button3" runat="server" Text="View Students' Cases" OnClick="Button3_Click" /><asp:Button Visible="false"  CssClass="submitbutton" ID="btnReopen" runat="server" Text="Reopen Case" OnClick="btnReopen_Click" /> </p>

</asp:Content>
