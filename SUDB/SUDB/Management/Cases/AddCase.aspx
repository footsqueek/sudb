﻿<%@ Page Title="Add Case" ValidateRequest="false" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="AddCase.aspx.cs" Inherits="SUDatabase.Management.Cases.AddCase" %>
<%@ Register src="../../UserControls/CaseMenu.ascx" tagname="CaseMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

     <script type="text/javascript" src="/Java/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link charmap anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime contextmenu paste textcolor colorpicker"
    ],
    toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | forecolor backcolor emoticons"
});
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CaseMenu ID="CaseMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Create New Case</h1>

    <p>To add a new case please complete the details below. Please note that the system will automatically record the date and time that the case is added and will also record that you have opened the case.</p>

           <p><asp:Label CssClass="label" ID="Label6" runat="server" Text="Student Name"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtName" Text="Steve Westgarth (0400328)" Enabled="false" runat="server"></asp:TextBox></p>
     <table class="nicetable">

                <tr><td><strong>Date of Birth: </strong></td><td><asp:Label ID="lblDOB" runat="server" Text="Label"></asp:Label></td></tr>
        <tr><td><strong>Faculty: </strong></td><td><asp:Label ID="lblFaculty" runat="server" Text="Label"></asp:Label></td></tr>
           <tr><td><strong>Course: </strong></td><td><asp:Label ID="lblCourse" runat="server" Text=""></asp:Label></td></tr>
        
               <tr><td><strong>Level: </strong></td><td><asp:Label ID="lblLevel" runat="server" Text="Label"></asp:Label></td></tr>
               <tr><td><strong>Campus: </strong></td><td><asp:Label ID="lblcampus" runat="server" Text="Label"></asp:Label></td></tr>
             <tr><td><strong>Mobile Number: </strong></td><td>
                 <asp:TextBox ID="txtMobile" CssClass="txtbox" runat="server"></asp:TextBox>  </td></tr>
               <tr><td><strong>Email Address: </strong></td><td>
                 <asp:TextBox ID="txtEmail" CssClass="txtbox" runat="server"></asp:TextBox>  </td></tr>
            </table>

    <asp:Button ID="Button1" runat="server" CssClass="submitbutton" Text="Save Details" OnClick="btnSave_Click" />

   
        <p><asp:Label  CssClass="label" ID="Label3" runat="server" Text="Case Type"></asp:Label><br /><asp:DropDownList CssClass="combobox" ID="cboCaseType" runat="server"></asp:DropDownList></p>
    <asp:RequiredFieldValidator CssClass="error" ID="RequiredFieldValidator1" ControlToValidate="cboCaseType" runat="server" ErrorMessage="You must select a case type"></asp:RequiredFieldValidator>

  
      <p><asp:Label  CssClass="label" ID="Label4" runat="server" Text="Initial Case Notes"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" TextMode="MultiLine" Rows="5" ID="txtCaseNotes" runat="server"></asp:TextBox></p>

    <p>
        <asp:FileUpload ID="attachment" runat="server" /></p>

        <p><asp:Label  CssClass="label" ID="Label2" runat="server" Text="Assigned To"></asp:Label><br /><asp:DropDownList CssClass="combobox" ID="cboAssignedTo" runat="server"></asp:DropDownList></p>


      <p><asp:Label  CssClass="label" ID="Label5" runat="server" Text="Time Units"></asp:Label><br /><asp:DropDownList CssClass="combobox" ID="cboTime" runat="server"></asp:DropDownList></p>


    <asp:Button  CssClass="submitbutton" ID="btnSaveClose" runat="server" Text="Save and Close Case" OnClick="btnSaveClose_Click" />    <asp:Button CssClass="submitbutton" ID="btnSave" runat="server" Text="Save Case" OnClick="btnSave_Click" />

</asp:Content>
