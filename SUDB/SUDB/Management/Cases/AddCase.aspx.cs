﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security;
using System.Web.Security;
using System.Web.Profile;
using SUDB;
using System.Configuration;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage;

namespace SUDatabase.Management.Cases
{
    public partial class AddCase : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Common tools = new Common();
                var student = tools.returnStudentToEdit();

                if (student != null)
                {
                    txtName.Text = student.FirstName + " " + student.Surname + " (" + student.StudentNumber + ")";
                }
                else
                {
                    Response.Redirect("/StudentManager/");
                }

                if (student != null)
                {
                    txtName.Text = student.FirstName + " " + student.Surname + " (" + student.StudentNumber + ")";
                    try
                    {
                        lblDOB.Text = ((DateTime)student.DateOfBirth).ToString("dd MMM yyyy");
                    }
                    catch
                    {
                        lblDOB.Text = "Unknown Date of Birth";
                    }
                    try
                    {
                        lblLevel.Text = student.Level.ToString();
                    }
                    catch
                    {
                        lblLevel.Text = "Unknown Level";
                    }
                    try
                    {
                        lblCourse.Text = student.Course.ToString();
                    }
                    catch
                    {
                        lblCourse.Text = "Unknown Course";
                    }
                    try
                    {
                        lblFaculty.Text = tools.returnFaculty((int)student.Faculty).FacultyName;
                    }
                    catch { lblFaculty.Text = "Unknown Faculty"; }
                    try
                    {
                        lblcampus.Text = tools.returnCampus((int)student.Campus).Campus;
                    }
                    catch
                    {
                        lblcampus.Text = "Unknown Campus";
                    }

                    try
                    {
                        txtMobile.Text = student.MobileNumber;
                    }
                    catch
                    {
                        txtMobile.Text = "Unknown Mobile";
                    }

                    try
                    {
                        if (student.PersonalEmail != "")
                            txtEmail.Text = student.PersonalEmail;
                        else
                        {
                            var su = tools.getSU();
                            txtEmail.Text = student.StudentNumber + su.emailextension;
                        }
                    }
                    catch
                    {
                        txtEmail.Text = "";
                    }

                }

                cboCaseType.Items.Add(new ListItem("", ""));


                //Bind Fields

                var casetypes = tools.ReturnCaseTypes();

                foreach (tblCaseType type in casetypes)
                {
                    cboCaseType.Items.Add(new ListItem(type.CaseType, type.Id.ToString()));
                }



                var users = Roles.GetUsersInRole("Staff");

                foreach (var user in users)
                {
                    var memuser = Membership.GetUser(user);
                    var permissions = tools.getPermissions((Guid)memuser.ProviderUserKey);

                    if (permissions.StudentsUnion==tools.getSU().Id)
                    {

                        ProfileBase profile = ProfileBase.Create(memuser.UserName);

                        if (profile["FirstName"].ToString().Equals(""))
                        {
                            cboAssignedTo.Items.Add(new ListItem(memuser.UserName, memuser.ProviderUserKey.ToString()));

                        }
                        else
                        {
                            cboAssignedTo.Items.Add(new ListItem(profile["FirstName"] + " " + profile["LastName"], memuser.ProviderUserKey.ToString()));
                        }
                    }
                }

                var timeunits = tools.ReturnTimeUnits();

                foreach (tblTimeUnit time in timeunits)
                {
                    cboTime.Items.Add(new ListItem(time.TimeUnit, time.Id.ToString()));
                }


                var currentuser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                cboAssignedTo.SelectedValue = currentuser.ProviderUserKey.ToString();

            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Common tools = new Common();
            var student = tools.returnStudentToEdit();

              var currentuser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
              tools.updateStudentFromWelfare(txtMobile.Text, txtEmail.Text);
            int newcase =   tools.createNewCase(student.Id, int.Parse(cboCaseType.SelectedValue), Guid.Parse(cboAssignedTo.SelectedValue), (Guid)currentuser.ProviderUserKey, Server.HtmlEncode(txtCaseNotes.Text), int.Parse(cboTime.SelectedValue));

            if(newcase>0)
            {

                if (attachment.HasFile)
                {
                    // Retrieve storage account from connection string.

                    var storageAccount = CloudStorageAccount.Parse(
        ConfigurationManager.AppSettings["StorageConnectionString"]);

                    // Create the blob client.
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    // Retrieve reference to a previously created container.
                    CloudBlobContainer container = blobClient.GetContainerReference("caseattachments");

                    String filename = Guid.NewGuid().ToString() + attachment.FileName;

                    // Retrieve reference to a blob named "myblob".
                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

                    // Create or overwrite the "myblob" blob with contents from a local file.
                    using (attachment.PostedFile.InputStream)
                    {
                        blockBlob.UploadFromStream(attachment.PostedFile.InputStream);
                    }


                    //Save Attachment in DB
                    tools.saveAttachment(tools.ReturnCaseNotes(newcase).First().Id, attachment.FileName, filename);
                }


                Response.Redirect("ViewCase.aspx?caseId="+newcase.ToString());
            }
            else
            {
                //There has been an error

            }
           
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            Common tools = new Common();
            var student = tools.returnStudentToEdit();

            var currentuser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            tools.updateStudentFromWelfare(txtMobile.Text, txtEmail.Text);
            int newcase = tools.createNewCase(student.Id, int.Parse(cboCaseType.SelectedValue), Guid.Parse(cboAssignedTo.SelectedValue), (Guid)currentuser.ProviderUserKey, txtCaseNotes.Text, int.Parse(cboTime.SelectedValue));

            if (newcase > 0)
            {
                tools.closeCase(newcase, Guid.Parse(cboAssignedTo.SelectedValue), int.Parse(cboCaseType.SelectedValue));
                Response.Redirect("StudentsCases.aspx");
            }
            else
            {
                //There has been an error

            }
        }
    }
}