﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using System.Web.UI.HtmlControls;

namespace SUDatabase.Management.Cases
{
    public partial class ViewCase : System.Web.UI.Page
    {
        public object CloudConfigurationManager { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
           // try
           // {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    if (!IsPostBack)
                    {
                        bindCase();
                        bindCaseNotes();
                        bindTags();
                    }
                    else
                    {
                        saveTime();
                    }
                }
                else
                {
                    Response.Redirect("/Default.aspx");
                }
           /* }
            catch
            {
                Response.Redirect("/Default.aspx");

            }*/
        }


        protected void bindTags()
        {
            Common tools = new Common();

            int caseId = int.Parse(Request.QueryString["caseId"]);


            var tags = tools.returnCaseTags(caseId);

            rptTags.DataSource = tags;
            rptTags.DataBind();

            int i = 0;

            foreach (var tag in tags)
            {
                ((Label)rptTags.Items[i].FindControl("lblTag")).Text = tag.Tag;
                ((Button)rptTags.Items[i].FindControl("btnActive")).CommandArgument = tag.Id.ToString();

                

                i++;
            }

            cboTags.Items.Clear();
           var alltags = tools.returnAllTags().Except(tags);
           cboTags.Items.Add(new ListItem("", ""));
            foreach (var tag in alltags)
            {
                cboTags.Items.Add(new ListItem(tag.Tag, tag.Id.ToString()));
              
            }

        }

        protected void bindCase()
        {
            try
            {
                int caseId = int.Parse(Request.QueryString["caseId"]);

                Common tools = new Common();

                var currentCase = tools.ReturnCase(caseId);

                if (currentCase != null)
                {
                    var student = tools.returnStudent(int.Parse(currentCase.StudentId.ToString()));
                    if (student != null)
                    {
                        txtName.Text = student.FirstName + " " + student.Surname + " (" + student.StudentNumber + ")";
                        try
                        {
                            lblDOB.Text = ((DateTime)student.DateOfBirth).ToString("dd MMM yyyy");
                        }
                        catch
                        {
                            lblDOB.Text = "Unknown Date of Birth";
                        }
                        try
                        {
                            lblLevel.Text = student.Level.ToString();
                        }
                        catch
                        {
                            lblLevel.Text = "Unknown Level";
                        }
                        try
                        {
                            lblFaculty.Text = tools.returnFaculty((int)student.Faculty).FacultyName;
                        }
                        catch { lblFaculty.Text = "Unknown Faculty"; }
                        try
                        {
                            lblcampus.Text = tools.returnCampus((int)student.Campus).Campus;
                        }
                        catch
                        {
                            lblcampus.Text = "Unknown Campus";
                        }
                        try
                        {
                            lblMobile.Text = student.MobileNumber;
                        }
                        catch
                        {
                            lblMobile.Text = "Unknown Mobile";
                        }

                        try
                        {
                            if (student.PersonalEmail != "")
                                lblEmail.Text = student.PersonalEmail;
                            else
                            {
                               lblEmail.Text = student.StudentNumber + "@chester.ac.uk";
                            }
                        }
                        catch
                        {
                            lblEmail.Text = "";
                        }
                    }








                    //Bind Fields

                    var casetypes = tools.ReturnCaseTypes();

                    cboCaseType.Items.Add(new ListItem("No Action Required", "0"));

                    foreach (tblCaseType type in casetypes)
                    {
                        cboCaseType.Items.Add(new ListItem(type.CaseType, type.Id.ToString()));
                    }


                    var remindertypes = tools.ReturnReminderTypes();

                    foreach (tblReminderType type in remindertypes)
                    {
                        cboNextAction.Items.Add(new ListItem(type.Type, type.reminderTypeId.ToString()));
                    }



                    var users = Roles.GetUsersInRole("Staff");

                    foreach (var useris in users)
                    {
                        var memuser = Membership.GetUser(useris);

                        var permissions = tools.getPermissions((Guid)memuser.ProviderUserKey);

                    if (permissions.StudentsUnion==tools.getSU().Id)
                    {

                        ProfileBase profile = ProfileBase.Create(memuser.UserName);

                        if (profile["FirstName"].ToString().Equals(""))
                        {
                            cboAssignedTo.Items.Add(new ListItem(memuser.UserName, memuser.ProviderUserKey.ToString()));
                            cboCreatedBy.Items.Add(new ListItem(memuser.UserName, memuser.ProviderUserKey.ToString()));
                                cboActionBy.Items.Add(new ListItem(memuser.UserName, memuser.ProviderUserKey.ToString()));

                            }
                            else
                        {
                            cboAssignedTo.Items.Add(new ListItem(profile["FirstName"] + " " + profile["LastName"], memuser.ProviderUserKey.ToString()));
                            cboCreatedBy.Items.Add(new ListItem(profile["FirstName"] + " " + profile["LastName"], memuser.ProviderUserKey.ToString()));
                                cboActionBy.Items.Add(new ListItem(profile["FirstName"] + " " + profile["LastName"], memuser.ProviderUserKey.ToString()));

                            }
                        }
                    
                    }
                    cboCreatedBy.Enabled = false;
                    cboCreatedBy.SelectedValue = currentCase.caseOpenedBy.ToString();
                    cboAssignedTo.SelectedValue = currentCase.caseAssignedTo.ToString();
                    cboActionBy.SelectedValue = currentCase.caseAssignedTo.ToString();

                    cboCaseType.SelectedValue = currentCase.caseType.ToString();
                    txtDateCreated.Text = ((DateTime)currentCase.dateOpened).ToString("dd MMM yyyy HH:mm");

                    lblDateTime.Text = DateTime.Now.ToString("dd MMM yyyy HH:mm");

                    var timeunits = tools.ReturnTimeUnits();
                    foreach (tblTimeUnit time in timeunits)
                    {
                        cboTime.Items.Add(new ListItem(time.TimeUnit, time.Id.ToString()));
                    }


                    var currentuser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                    ProfileBase profilecurrent = ProfileBase.Create(currentuser.UserName);
                    if (profilecurrent["FirstName"].ToString().Equals(""))
                    {
                        lblStaffMember.Text = currentuser.UserName;
                     
                    }
                    else
                    {
                    lblStaffMember.Text =  profilecurrent["FirstName"] + " " + profilecurrent["LastName"];
                    }


                    if (currentCase.dateClosed != null)
                    {
                        //case is closed
                        lblClosed.Visible = true;
                        txtClosed.Visible = true;
                        txtClosed.Text = ((DateTime)currentCase.dateClosed).ToString("dd MMM yyyy HH:mm");

                        double days = Math.Floor(((DateTime)currentCase.dateClosed - (DateTime)currentCase.dateOpened).TotalDays);

                        lblAge.Text = days.ToString() + " days";

                        createnotes1.Visible = false;
                        createnotes2.Visible = false;
                        Button2.Visible = false;
                        Button1.Text = "Done";
                        Button1.Visible = false;

                        if(tools.checkAccess("SystemManager")=="Full Access")
                        {
                            btnReopen.Visible = true;
                        }

                    }
                    else
                    {
                        //case is open
                        //Calculate days
                        double days = Math.Floor((DateTime.Now - (DateTime)currentCase.dateOpened).TotalDays);

                       

                        lblAge.Text = days.ToString() + " days";

                    }

                    txtDateCreated.Enabled = false;

                     String access = tools.checkAccess("CaseLog");
                 var currentUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                 if (access == "Full Access" || cboAssignedTo.SelectedValue.Equals(currentUser.ProviderUserKey.ToString()) || tools.checkAccess("SystemManager") == "Full Access")
                 {
                     if (currentCase.dateClosed != null && tools.checkAccess("SystemManager") !="Full Access")
                     {
                         //Case is closed so disable changing fields
                         cboAssignedTo.Enabled = false;
                         cboCaseType.Enabled = false;
                         Button1.Text = "Done";
                     }
                     else
                     {
                         Button1.Text = "Save Changes";
                     }

                 } else if (access == "Read Only")
                 {
                     cboAssignedTo.Enabled = false;
                     txtClosed.Enabled = false;
                     cboCaseType.Enabled = false;
                     Button1.Text = "Done";
                     Button2.Visible = false;
                 }
                 else
                 {
                     cboAssignedTo.Enabled = false;
                     txtClosed.Enabled = false;
                     cboCaseType.Enabled = false;
                     Button1.Text = "Done";
                     Button2.Visible = false;
                 }


                }
                else
                {
                    Response.Redirect("/Default.aspx");
                }
            }
            catch
            {
                Response.Redirect("/Default.aspx");
            }
        }

        protected void bindCaseNotes()
        {
            int caseId = int.Parse(Request.QueryString["caseId"]);

            Common tools = new Common();

            var currentCase = tools.ReturnCase(caseId);


            var casenotes = tools.ReturnCaseNotes(caseId);
            int i = 0;
            int actualtime=0;

            rptCaseNotes.DataSource = casenotes;
            rptCaseNotes.DataBind();

               String access = tools.checkAccess("CaseLog");
                 var currentUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);


            foreach(tblCaseLog note in casenotes)
            {
                var staffmember = Membership.GetUser(note.staffmember);
                ProfileBase profile = ProfileBase.Create(staffmember.UserName);


                ((Label)rptCaseNotes.Items[i].FindControl("lblDateTime")).Text = ((DateTime)note.DateTime).ToString("dd MMM yyyy HH:mm");

                if (profile["FirstName"] != "")
                {
                    ((Label)rptCaseNotes.Items[i].FindControl("lblStaffMember")).Text = profile["FirstName"] + " " + profile["LastName"];
                }
                else
                {
                    ((Label)rptCaseNotes.Items[i].FindControl("lblStaffMember")).Text = staffmember.UserName;
                }

                

               

                if (access == "Read Only" || access == "Full Access" || cboAssignedTo.SelectedValue.Equals(currentUser.ProviderUserKey.ToString()) || note.staffmember.ToString().Equals(currentUser.ProviderUserKey.ToString()))
                 {
                    var attachments = tools.returnNoteAttachments(note.Id);

                     ((Label)rptCaseNotes.Items[i].FindControl("lblNotes")).Text = Server.HtmlDecode(note.notes);

                    LinkButton lblAttachment = (LinkButton)rptCaseNotes.Items[i].FindControl("lblAttachments");
                    LinkButton lblAttachment1 = (LinkButton)rptCaseNotes.Items[i].FindControl("lblAttachment1");
                    LinkButton lblAttachment2 = (LinkButton)rptCaseNotes.Items[i].FindControl("lblAttachment2");

                    int q = 1;
                    foreach (var attachment in attachments)
                    {
                        if (q == 1)
                        {
                            lblAttachment.Text = attachment.docName;
                            lblAttachment.CommandArgument = attachment.attachmentURL;
                        }
                        else if(q==2)
                        {
                            lblAttachment1.Text = attachment.docName;
                            lblAttachment1.CommandArgument = attachment.attachmentURL;

                        }
                        else if (q == 3)
                        {
                            lblAttachment2.Text = attachment.docName;
                            lblAttachment2.CommandArgument = attachment.attachmentURL;

                        }
                        q++;
                    }
                }
                else
                 {
                     ((Label)rptCaseNotes.Items[i].FindControl("lblNotes")).Text = "You do not have the required permission to view case notes";
                 }

                ((Label)rptCaseNotes.Items[i].FindControl("lblNotes")).ID = "Toggle" + note.Id.ToString();
                ((HtmlAnchor)rptCaseNotes.Items[i].FindControl("Expand")).Attributes.Add("href", "javascript:toggle(" + note.Id.ToString() + ");");
                ((HtmlAnchor)rptCaseNotes.Items[i].FindControl("Expand")).ID = "Expand" + note.Id.ToString();



                var time = tools.returnTime((int)note.timeUnits);

                actualtime = actualtime + (int)time.MinutesToAdd;


                DropDownList cboTime = ((DropDownList)rptCaseNotes.Items[i].FindControl("cboTime"));

                var timeunits = tools.ReturnTimeUnits();
                foreach (tblTimeUnit timeunitsis in timeunits)
                {
                    cboTime.Items.Add(new ListItem(timeunitsis.TimeUnit, timeunitsis.Id.ToString()));
                }

                cboTime.SelectedValue = time.Id.ToString();

                cboTime.AutoPostBack = true;

                if (((DateTime.Now - ((DateTime)note.DateTime)).TotalMinutes < 60 && currentCase.dateClosed == null) || ((note.staffmember == (Guid)(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey)) && currentCase.dateClosed == null) || tools.checkAccess("SystemManager") == "Full Access")
                {
                    cboTime.Enabled = true;
                }
                else
                {
                   
                        cboTime.Enabled = false;
                   
                }


             

                i++;

            }

            lblTime.Text = actualtime + " minutes";

         
        }

        protected void saveTime()
        {
             int caseId = int.Parse(Request.QueryString["caseId"]);

            Common tools = new Common();

            var casenotes = tools.ReturnCaseNotes(caseId);
            int i = 0;

               String access = tools.checkAccess("CaseLog");
               var currentUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);


                 foreach (tblCaseLog note in casenotes)
                 {
                     tools.updateNoteTime(note.Id, int.Parse(((DropDownList)rptCaseNotes.Items[i].FindControl("cboTime")).SelectedValue));
                     i++;
                 }

       
                 bindCaseNotes();
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            int caseId = int.Parse(Request.QueryString["caseId"]);

            try
            {
               
                Common tools = new Common();

                int noteId = tools.newCaseLog(caseId, Server.HtmlEncode(txtNotes.Text), int.Parse(cboTime.SelectedValue));

                if(cboNextAction.SelectedValue !="0")
                {
                    DateTime actiondate = DateTime.Now;
                    if (DateTime.TryParse(txtActionDate.Text,out actiondate))
                    {
                        //Create An Action
                        int reminderId = tools.addReminder(caseId, Server.HtmlEncode(txtNotes.Text), int.Parse(cboNextAction.SelectedValue), actiondate, Guid.Parse(cboActionBy.SelectedValue));
                    }
                }

                if (attachment.HasFile)
                {
                    // Retrieve storage account from connection string.

                    var storageAccount = CloudStorageAccount.Parse(
        ConfigurationManager.AppSettings["StorageConnectionString"]);

                    // Create the blob client.
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    // Retrieve reference to a previously created container.
                    CloudBlobContainer container = blobClient.GetContainerReference("caseattachments");

                    String filename = Guid.NewGuid().ToString() + attachment.FileName;

                    // Retrieve reference to a blob named "myblob".
                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

                    // Create or overwrite the "myblob" blob with contents from a local file.
                    using (attachment.PostedFile.InputStream)
                    {
                        blockBlob.UploadFromStream(attachment.PostedFile.InputStream);
                    }


                    //Save Attachment in DB
                    tools.saveAttachment(noteId, attachment.FileName, filename);
                }

                if (attachment1.HasFile)
                {
                    // Retrieve storage account from connection string.

                    var storageAccount = CloudStorageAccount.Parse(
        ConfigurationManager.AppSettings["StorageConnectionString"]);

                    // Create the blob client.
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    // Retrieve reference to a previously created container.
                    CloudBlobContainer container = blobClient.GetContainerReference("caseattachments");

                    String filename = Guid.NewGuid().ToString() + attachment.FileName;

                    // Retrieve reference to a blob named "myblob".
                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

                    // Create or overwrite the "myblob" blob with contents from a local file.
                    using (attachment.PostedFile.InputStream)
                    {
                        blockBlob.UploadFromStream(attachment.PostedFile.InputStream);
                    }


                    //Save Attachment in DB
                    tools.saveAttachment(noteId, attachment.FileName, filename);
                }

                if (attachment2.HasFile)
                {
                    // Retrieve storage account from connection string.

                    var storageAccount = CloudStorageAccount.Parse(
        ConfigurationManager.AppSettings["StorageConnectionString"]);

                    // Create the blob client.
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    // Retrieve reference to a previously created container.
                    CloudBlobContainer container = blobClient.GetContainerReference("caseattachments");

                    String filename = Guid.NewGuid().ToString() + attachment.FileName;

                    // Retrieve reference to a blob named "myblob".
                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

                    // Create or overwrite the "myblob" blob with contents from a local file.
                    using (attachment.PostedFile.InputStream)
                    {
                        blockBlob.UploadFromStream(attachment.PostedFile.InputStream);
                    }


                    //Save Attachment in DB
                    tools.saveAttachment(noteId, attachment.FileName, filename);
                }


                Response.Redirect("ViewCase.aspx?caseId=" + caseId);
            }
            catch
            {
                Response.Redirect("ViewCase.aspx?caseId=" + caseId);

            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            int caseId = int.Parse(Request.QueryString["caseId"]);

            Common tools = new Common();


            tools.closeCase(caseId, Guid.Parse(cboAssignedTo.SelectedValue), int.Parse(cboCaseType.SelectedValue));

            Response.Redirect("../StudentManager/");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int caseId = int.Parse(Request.QueryString["caseId"]);
            Common tools = new Common();

            tools.updateCase(caseId, Guid.Parse(cboAssignedTo.SelectedValue), int.Parse(cboCaseType.SelectedValue));
            Response.Redirect("../StudentManager/");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            int caseId = int.Parse(Request.QueryString["caseId"]);

            Common tools = new Common();
               var currentCase = tools.ReturnCase(caseId);

               tools.setStudentToEdit((int)currentCase.StudentId);
            Response.Redirect("StudentsCases.aspx");

        }

        protected void btnActive_Click(object sender, EventArgs e)
        {
            Common tools = new Common();
            int caseId = int.Parse(Request.QueryString["caseId"]);

            Button senderbut = (Button)sender;
            tools.removeTag(int.Parse(senderbut.CommandArgument), caseId);
            bindTags();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Common tools = new Common();
                int caseId = int.Parse(Request.QueryString["caseId"]);

                tools.insertTag(int.Parse(cboTags.SelectedValue), caseId);
                bindTags();
            }
            catch
            {

            }
        }

        protected async void lblAttachments_Click(object sender, EventArgs e)
        {


            LinkButton senderbut = (LinkButton)sender;

            await Task.Run(() =>
            {
                // Retrieve storage account from connection string.
                var storageAccount = CloudStorageAccount.Parse(
    ConfigurationManager.AppSettings["StorageConnectionString"]);

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("caseattachments");

            // Retrieve reference to a blob named "photo1.jpg".
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(senderbut.CommandArgument);

           using (MemoryStream ms = new MemoryStream())
            {
                blockBlob.DownloadToStream(ms);

                
                    
                    Response.ContentType = blockBlob.Properties.ContentType;
                    Response.AddHeader("Content-Disposition", "Attachment; filename=" + senderbut.Text);
                    Response.AddHeader("Content-Length", blockBlob.Properties.Length.ToString());
                    Response.BinaryWrite(ms.ToArray());

                
                }
            });
        }

        protected void btnReopen_Click(object sender, EventArgs e)
        {
            Common tools = new Common();
            int caseId = int.Parse(Request.QueryString["caseId"]);
            tools.reopenCase(caseId);
            Response.Redirect("ViewCase.aspx?caseId=" + caseId);


        }
    }
}