﻿using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.Cases
{
    public partial class StudentsCases : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();
            var student = tools.returnStudentToEdit();

          if(!Page.IsPostBack)
          {

         
            if (student != null)
            {
                txtName.Text = student.FirstName + " " + student.Surname + " (" + student.StudentNumber + ")";
                try
                {
                    lblDOB.Text = ((DateTime)student.DateOfBirth).ToString("dd MMM yyyy");
                }
                catch
                {
                    lblDOB.Text = "Unknown Date of Birth";
                }
                try
                {
                    lblLevel.Text = student.Level.ToString();
                }
                catch
                {
                    lblLevel.Text = "Unknown Level";
                }
                try
                {
                    lblCourse.Text = student.Course.ToString();
                }
                catch
                {
                    lblCourse.Text = "Unknown Course";
                }
                try
                {
                    lblFaculty.Text = tools.returnFaculty((int)student.Faculty).FacultyName;
                }
                catch { lblFaculty.Text = "Unknown Faculty"; }
                try
                {
                    lblcampus.Text = tools.returnCampus((int)student.Campus).Campus;
                }
                catch
                {
                    lblcampus.Text = "Unknown Campus";
                }

                try
                {
                    txtMobile.Text = student.MobileNumber;
                }
                catch
                {
                    txtMobile.Text = "Unknown Mobile";
                }

                try
                {
                    if (student.PersonalEmail != "")
                    txtEmail.Text = student.PersonalEmail;
                    else
                    {
                        txtEmail.Text = student.StudentNumber + "@chester.ac.uk";
                    }
                }
                catch
                {
                    txtEmail.Text = "";
                }

            }

          }

            var cases = tools.ReturnOpenStudentCases(student.Id);

            rptOpenCases.DataSource = cases;
            rptOpenCases.DataBind();

            int i = 0;

            foreach (tblCase curcase in cases)
            {

               
                ((Label)rptOpenCases.Items[i].FindControl("lblCaseType")).Text = ((tblCaseType)tools.returnCaseType((int)curcase.caseType)).CaseType;
                ((Label)rptOpenCases.Items[i].FindControl("lblDateOpened")).Text = ((DateTime)curcase.dateOpened).ToString("dd MMM yyyy HH:mm");

                var assignedto = Membership.GetUser(curcase.caseAssignedTo);
                ProfileBase profile = ProfileBase.Create(assignedto.UserName);

                ((Label)rptOpenCases.Items[i].FindControl("lblAssignedTo")).Text = profile["FirstName"] + " " + profile["LastName"];

                ((Button)rptOpenCases.Items[i].FindControl("btnView")).CommandArgument = curcase.caseId.ToString();




                i++;
            }


            if (i==0)
            {
                litNoOpen.Visible = true;
            }


            var closedcases = tools.ReturnClosedStudentCases(student.Id);

            rptClosedCases.DataSource = closedcases;
            rptClosedCases.DataBind();

            int x = 0;

            foreach (tblCase curcase in closedcases)
            {


                ((Label)rptClosedCases.Items[x].FindControl("lblCaseType")).Text = ((tblCaseType)tools.returnCaseType((int)curcase.caseType)).CaseType;
                ((Label)rptClosedCases.Items[x].FindControl("lblDateOpened")).Text = ((DateTime)curcase.dateOpened).ToString("dd MMM yyyy HH:mm");
                ((Label)rptClosedCases.Items[x].FindControl("lblDateClosed")).Text = ((DateTime)curcase.dateClosed).ToString("dd MMM yyyy HH:mm");

                var assignedto = Membership.GetUser(curcase.caseAssignedTo);
                ProfileBase profile = ProfileBase.Create(assignedto.UserName);

                ((Label)rptClosedCases.Items[x].FindControl("lblAssignedTo")).Text = profile["FirstName"] + " " + profile["LastName"];

                ((Button)rptClosedCases.Items[x].FindControl("btnView")).CommandArgument = curcase.caseId.ToString();




                x++;
            }

            if (x == 0)
            {
                litnoclosed.Visible = true;
            }
        }

        public void btnView_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            Response.Redirect("../Cases/ViewCase.aspx?caseId="+btnView.CommandArgument);
        }

        protected void btnDone_Click(object sender, EventArgs e)
        {
            Response.Redirect("../StudentManager/Default.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddCase.aspx");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Common tools = new Common();
            tools.updateStudentFromWelfare(txtMobile.Text, txtEmail.Text);
            Response.Redirect("StudentsCases.aspx");
        }
    }
}