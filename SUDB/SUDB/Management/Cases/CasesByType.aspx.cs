﻿using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.Cases
{
    public partial class CasesByType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Common tools = new Common();

                if (tools.checkAccess("CaseLog") == "Full Access" || (tools.checkAccess("CaseLog") == "Read Only"))
                {
                    bindtypes();
                }
                else
                {
                    Response.Redirect("/Default.aspx");
                }

                if (tools.checkAccess("SystemManager") == "Full Access")
                {
                    addnew.Visible = true;
                }
                else
                {
                    addnew.Visible = false;
                }
            }
        }

        protected void bindtypes()
        {
            Common tools = new Common();

            var types = tools.ReturnCaseTypes().Where(p => p.CaseType != "Disciplinary").Where(p => p.CaseType != "C Card");

          

            rptTypes.DataSource = types;
            rptTypes.DataBind();

            int i = 0;

            double totaltime = 0;
            double totopen = 0;
            double totcases = 0;
            double averageage = 0;

            foreach(tblCaseType type in types)
            {
                ((Label)rptTypes.Items[i].FindControl("lblType")).Text = type.CaseType;
                ((Label)rptTypes.Items[i].FindControl("lblTotalOpen")).Text = tools.returnNoCasesType(type.Id).ToString();
                ((Label)rptTypes.Items[i].FindControl("lblTotal")).Text = tools.returnNoCasesTypeClosed(type.Id).ToString();
                ((Label)rptTypes.Items[i].FindControl("lblAge")).Text = tools.returnNoCasesTypeAge(type.Id).ToString() + " days";
                ((Label)rptTypes.Items[i].FindControl("lblTime")).Text = Math.Round((tools.returnNoCasesTypeTime(type.Id)/60),2).ToString() + " hours";
                totopen = totopen + tools.returnNoCasesType(type.Id);
                totcases = totcases + tools.returnNoCasesTypeClosed(type.Id);
                totaltime = totaltime + tools.returnNoCasesTypeTime(type.Id);
                averageage = averageage + tools.returnNoCasesTypeAge(type.Id);
                i++;
            }
            lblTotalTime.Text = Math.Round((totaltime/60),2).ToString() + " hours";
            lblAverageAge.Text = Math.Round((averageage/i),0).ToString() + " days";
            lblTotOpen.Text = totopen.ToString();
            lblTotCases.Text = totcases.ToString();
        }

        protected void btnAddItem_Click(object sender, EventArgs e)
        {
            Common tools = new Common();
            tools.InsertType(txtNewMenuItem.Text);
            Response.Redirect("CasesByType.aspx");
        }
    }
}