﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="CreateStudent.aspx.cs" Inherits="SUDatabase.Management.StudentManager.CreateStudent" %>
<%@ Register src="../../UserControls/StudentManagerMenu.ascx" tagname="StudentManagerMenu" tagprefix="uc1" %>
<%@ Register Src="~/UserControls/CaseMenu.ascx" TagPrefix="uc1" TagName="CaseMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:StudentManagerMenu ID="StudentManagerMenu1" runat="server" />
    <uc1:CaseMenu runat="server" ID="CaseMenu" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <h1>View Student Record</h1>

        <p>
        <asp:Label ID="Label2" CssClass="label" runat="server" Text="Student Number"></asp:Label><br />
        <asp:TextBox ID="txtStudentNo" CssClass="txtbox" runat="server"></asp:TextBox><br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="error" ControlToValidate="txtStudentNo" runat="server" ErrorMessage="You must enter a student number"></asp:RequiredFieldValidator>

    </p>

    <p>
        <asp:Label ID="Label1" CssClass="label" runat="server" Text="Firstname"></asp:Label><br />
        <asp:TextBox ID="txtFirstName" CssClass="txtbox" runat="server"></asp:TextBox><br />
          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="error" ControlToValidate="txtFirstName" runat="server" ErrorMessage="You must enter a Firstname"></asp:RequiredFieldValidator>

    </p>

    
    <p>
        <asp:Label ID="Label12" CssClass="label" runat="server" Text="Surname"></asp:Label><br />
        <asp:TextBox ID="txtSurname" CssClass="txtbox" runat="server"></asp:TextBox><br />
          <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="error" ControlToValidate="txtSurname" runat="server" ErrorMessage="You must enter a Surname"></asp:RequiredFieldValidator>

    </p>



       <asp:Button ID="btnSave" CssClass="submitbutton" runat="server" Text="Save" ValidationGroup="details" OnClick="btnSave_Click" />


    <h2>Archive Students</h2>
    <p>Prior to uploading a new membership database you must first archive all students so that they are no longer members of the Students' Union. Please select a new membership type for your students and click submit.</p>
    <asp:DropDownList ID="cboMembershipType" CssClass="combobox" runat="server"></asp:DropDownList><br /> <asp:Button ID="btnSetType" ValidationGroup="archive" runat="server" CssClass="submitbutton" Text="Update Membership Type" OnClick="btnSetType_Click" />


</asp:Content>
