﻿using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.StudentManager
{
    public partial class ViewStudent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Check Access
            Common tools = new Common();
            if (!tools.checkAccess("StudentManager").Equals("Full Access") && !tools.checkAccess("StudentManager").Equals("Read Only"))
            {
                Response.Redirect("/Default.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    bindStudent();
                    bindSports();
                    bindSanctions();
                    bindTokens();
                }
            }
        }

        protected void bindTokens()
        {

            Common tools = new Common();
            var tokens = tools.getPointAllTransactions(tools.returnStudentToEdit().Id);

            rptTokenCodes.DataSource = tokens;
            rptTokenCodes.DataBind();

            int i = 0;

            try
            {
                lblNoPoints.Text = tokens.Sum(p => p.NumberOfPoints).ToString();
            }
            catch
            {
                lblNoPoints.Text = "0";
            }
            foreach (var trans in tokens)
            {
                ((Label)rptTokenCodes.Items[i].FindControl("lblDate")).Text = ((DateTime)trans.datetime).ToString("dd/MM/yyyy HH:mm");
                ((Label)rptTokenCodes.Items[i].FindControl("lblPoints")).Text = trans.NumberOfPoints.ToString();
                ((Label)rptTokenCodes.Items[i].FindControl("lblReason")).Text = trans.Reason;
                i++;


            }
            
        }
        protected void bindSanctions()
        {
            Common tools = new Common();
            var student = tools.returnStudentToEdit();
            var sanctions = tools.returnSanctions(student.Id);

            rptSanctions.DataSource = sanctions;
            rptSanctions.DataBind();

            int i = 0;

            foreach(var sanction in sanctions)
            {
                var sanctionarea = tools.returnSanctionArea((int)sanction.SanctionAreaId);

                ((Label)rptSanctions.Items[i].FindControl("lblSanctionArea")).Text = sanctionarea.SanctionArea;
                ((Label)rptSanctions.Items[i].FindControl("lblSanctionStart")).Text = ((DateTime)sanction.SanctionStartDate).ToString("dd/MM/yyyy");
                ((Label)rptSanctions.Items[i].FindControl("lblSanctionEnd")).Text = ((DateTime)sanction.SanctionEndDate).ToString("dd/MM/yyyy");
                ((Button)rptSanctions.Items[i].FindControl("btnStrike")).CommandArgument = sanction.Id.ToString();
                ((Literal)rptSanctions.Items[i].FindControl("litSanctionNotes")).Text = sanction.Notes;
                if(sanction.StrikenDate !=null)
                {

                    var strickenby = Membership.GetUser((Guid)sanction.StrikenBy);
                    ProfileBase profile = ProfileBase.Create(strickenby.UserName);

                    ((Button)rptSanctions.Items[i].FindControl("btnStrike")).Visible = false;
                    ((Label)rptSanctions.Items[i].FindControl("lblSanctionEnd")).CssClass = "strike";
                    ((Label)rptSanctions.Items[i].FindControl("lblSanctionStart")).CssClass = "strike";
                    ((Label)rptSanctions.Items[i].FindControl("lblSanctionArea")).CssClass = "strike";
                    ((Literal)rptSanctions.Items[i].FindControl("litSanctionNotes")).Text = "<p class=\"strike\">"+sanction.Notes + "</p>";
                    ((Label)rptSanctions.Items[i].FindControl("lblStrikeInfo")).Text = "Stricken by " + profile["FirstName"] + " " + profile["LastName"] + " on " + ((DateTime)sanction.StrikenDate).ToString("dd/MM/yyyy");
                    ((Label)rptSanctions.Items[i].FindControl("lblStrikeInfo")).Visible = true;
                }

                if (tools.checkAccess("Disciplinary") == "Read Only")
                {
                    ((Button)rptSanctions.Items[i].FindControl("btnStrike")).Visible = false;
                }


               

                i++;
            }

            if(sanctions.Count()==0)
            {
                litNoDisciplinary.Visible = true;
            }


            var sanctionareas = tools.returnSanctionAreas();
            cboSanctionType.Items.Clear();
            foreach(var sanctionarea in sanctionareas)
            {
                cboSanctionType.Items.Add(new ListItem(sanctionarea.SanctionArea, sanctionarea.Id.ToString()));
            }


            if (tools.checkAccess("Disciplinary") == "Full Access" || (tools.checkAccess("Disciplinary") == "Read Only"))
            {
                Disciplinary.Visible = true;
            }
            else
            {
                Disciplinary.Visible = false;
            }

             if (tools.checkAccess("Disciplinary") == "Full Access")
             {
                 createnotes1.Visible = true;
                 createnotes2.Visible = true;
             }
             else
             {
                 createnotes1.Visible = false;
                 createnotes2.Visible = false;
             }

        }

        protected void bindSports()
        {
            Common tools = new Common();
            var student = tools.returnStudentToEdit();

            var sports = tools.returnSportsThatMemberOf(student.Id);
            rptSportsSocs.DataSource = sports;
            rptSportsSocs.DataBind();
            int i = 0;
            foreach(var sport in sports)
            {
                var sportis = tools.returnSportSociety((int)sport.SportSocId);

                ((Label)rptSportsSocs.Items[i].FindControl("lblSportSociety")).Text = sportis.Name;
                ((Label)rptSportsSocs.Items[i].FindControl("lblDateJoined")).Text = ((DateTime)sport.DateJoined).ToString("dd/MM/yyyy");
                ((Label)rptSportsSocs.Items[i].FindControl("lblAmount")).Text = "&pound;"+ (Math.Round((double)sport.Amount,2)).ToString();
            
                ((Label)rptSportsSocs.Items[i].FindControl("lblOrderNumber")).Text = "<a href=\"/Management/DutyOfCare/TransactionDetail.aspx?orderId="+ sport.TransactionId +"\">" + sport.TransactionId.ToString()+ "</a>";
              

                i++;
            }
            if(sports.Count()==0)
            {
               litError.Visible=true;
            }

        }
        protected void bindStudent()
        {


            Common tools = new Common();
            var student = tools.returnStudentToEdit();


            var faculties = tools.ReturnFaculties();

            foreach(tblFaculty faculty in faculties)
            {
                cboFaculty.Items.Add(new ListItem(faculty.FacultyName, faculty.Id.ToString()));
            }

            var campusesare = tools.ReturnCapuses();

            foreach (tblCampuse campus in campusesare)
            {
                cboCampus.Items.Add(new ListItem(campus.Campus, campus.Id.ToString()));
            }

            var memtype = tools.ReturnMembershipType();

            foreach (tblMembershipType type in memtype)
            {
               cboMembershipType.Items.Add(new ListItem(type.MembershipType, type.Id.ToString()));
            }

            try
            {
                lblName.Text = student.FirstName + " " + student.Surname;
            }
            catch { }
            try
            {
                txtDOB.Text = ((DateTime)student.DateOfBirth).ToString("dd MMM yyyy");
            }
            catch { }
            try
            {
                txtFirstAid.Text = ((DateTime)student.FirstAidCertExpiry).ToString("dd MMM yyyy");
            }
            catch { }
            try
            {
                txtMedicalInfo.Text = student.MedicalInformation;
            }
            catch
            {

            }
            try { txtNotes.Text = student.Notes; }
            catch { }
            try { txtLevel.Text = student.Level; }
            catch { }
            try { txtCourse.Text = student.Course; }
            catch { }
            try { txtPeronalEmail.Text = student.PersonalEmail; }
            catch { }
            try { txtTermTime.Text = student.TermTimeAddress; }
            catch { }
            try { txtMobileNumber.Text = student.MobileNumber; }
            catch { }
            try { cboCampus.SelectedValue = student.Campus.ToString(); }
            catch { }
            try { cboMembershipType.SelectedValue = student.MembershipType.ToString(); }
            catch { }
            try { cboGender.SelectedValue = student.Gender.ToString(); }
            catch { }
            try { cboDriver.SelectedValue = student.MiniBusAssessment.ToString(); }
            catch { }
            try { cboFaculty.SelectedValue = student.Faculty.ToString(); }
            catch { }
            try { cboShare.SelectedValue = student.OktoShareMedical.ToString(); }
            catch { }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Common tools = new Common();
            DateTime DOB = DateTime.Now;
            try
            {
                DOB = DateTime.Parse(txtDOB.Text);
            }
            catch
            {

            }

            DateTime FirstAid = DateTime.Now;
            try
            {
                FirstAid = DateTime.Parse(txtFirstAid.Text);
            }
            catch
            {

            }

            var student = tools.updateStudent( int.Parse(cboShare.SelectedValue),FirstAid,txtCourse.Text, int.Parse(cboDriver.SelectedValue),DOB, int.Parse(cboCampus.SelectedValue), int.Parse(cboFaculty.SelectedValue), txtLevel.Text, int.Parse(cboMembershipType.SelectedValue), txtTermTime.Text, txtPeronalEmail.Text, txtMobileNumber.Text, txtMedicalInfo.Text, txtNotes.Text);

            tools.setStudentToEditNull();
            Response.Redirect("Default.aspx");
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            try
            {
                Common tools = new Common();
                if (tools.createSanction(int.Parse(cboSanctionType.SelectedValue), DateTime.Parse(txtStartDate.Text), DateTime.Parse(txtEndDate.Text), txtSanctionNotes.Text))
                {
                    bindSanctions();
                    txtSanctionNotes.Text = "";
                    txtStartDate.Text = "";
                    txtEndDate.Text = "";
                }
                else
                {
                    //Failed
                }
            }
            catch
            {

            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Button senderbut = (Button)sender;
            if (senderbut.Text == "Confirm")
            {
                Common tools = new Common();
                if (tools.strikedisciplinary(int.Parse(senderbut.CommandArgument)))
                {
                    bindSanctions();
                }
            }
            else
            {
                senderbut.Text = "Confirm";
            }


        }

        protected void addPoints_Click(object sender, EventArgs e)
        {
            try
            {
                Common tools = new Common();
                tools.addPoints(txtReason.Text, int.Parse(txtPoints.Text), tools.returnStudentToEdit().Id);
                bindTokens();
            }
            catch
            {

            }
            txtReason.Text = "";
            txtPoints.Text = "";
        }
    }
}