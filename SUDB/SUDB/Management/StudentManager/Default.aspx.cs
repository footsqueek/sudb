﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.StudentManager
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Check Access
           
            //Everyone can search for a student
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ActivityLogClass newActivity = new ActivityLogClass();
            newActivity.logActivity("Searched for Student", "Searched for a student using the search term " + txtSearch.Text + ".");
            Response.Redirect("SearchResults.aspx?query="+txtSearch.Text);
        }
    }
}