﻿using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.StudentManager
{
    public partial class CreateStudent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Check Access
            Common tools = new Common();
            if (!tools.checkAccess("StudentManager").Equals("Full Access") && !tools.checkAccess("StudentManager").Equals("Read Only"))
            {
                Response.Redirect("/Default.aspx");
            }
            else
            {
                bindStudent();
            }
        }


        protected void bindStudent()
        {
            Common tools = new Common();
            var memtype = tools.ReturnMembershipType();

            foreach (tblMembershipType type in memtype)
            {
                cboMembershipType.Items.Add(new ListItem(type.MembershipType, type.Id.ToString()));
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Common tools = new Common();

            var student = tools.createStudent(txtFirstName.Text, txtSurname.Text, txtStudentNo.Text);
            tools.setStudentToEdit(student.Id);
            Response.Redirect("ViewStudent.aspx");
        }

        protected void btnSetType_Click(object sender, EventArgs e)
        {
            Common tools = new Common();
            

            Task.Run(() => tools.updateAllStudentsMemType(int.Parse(cboMembershipType.SelectedValue)));
        }
    }
}