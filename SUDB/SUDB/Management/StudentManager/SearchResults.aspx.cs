﻿using SUDatabase.Classes;
using SUDB.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;

namespace SUDatabase.Management.StudentManager
{
    public partial class SearchResults : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                //Check Access
                bindResults();
            }

        }

        protected void bindResults()
        {

            string query = Request.QueryString["query"];

            if (query != null && query !="")
            {
                
                    Common tools = new Common();

                    var results = tools.studentSearch(query,tools.getSU().Id);

                    rptSearchResults.DataSource = results;
                    rptSearchResults.DataBind();

                    int i = 0;

                    var access = tools.checkAccess("StudentManager");

                    int totalcases = 0;
                    int totalcasesclosed = 0;

                    Points points = new Points();

                    foreach (tblStudent result in results)
                    {
                        var type = tools.ReturnStudentMembershipType((int)result.MembershipType);

                        ((Label)rptSearchResults.Items[i].FindControl("lblStudentNumber")).Text = result.StudentNumber;
                        ((Label)rptSearchResults.Items[i].FindControl("lblStudentName")).Text = result.FirstName + " " + result.Surname;
                        ((Label)rptSearchResults.Items[i].FindControl("lblType")).Text = type.MembershipType;
                        ((Label)rptSearchResults.Items[i].FindControl("lblPoints")).Text = points.returnPointsBalance((int)result.Id).ToString();

                        ((Button)rptSearchResults.Items[i].FindControl("btnEdit")).CommandArgument = result.Id.ToString();
                        ((Button)rptSearchResults.Items[i].FindControl("btnNewCase")).CommandArgument = result.Id.ToString();
                        ((Button)rptSearchResults.Items[i].FindControl("btnViewCases")).CommandArgument = result.Id.ToString();

                        if (access != "Read Only" && access != "Full Access")
                        {
                            ((Button)rptSearchResults.Items[i].FindControl("btnEdit")).Visible = false;

                        }

                        //Get number of cases
                        totalcases = totalcases + tools.returnNoCases(result.Id);
                        totalcasesclosed = totalcasesclosed + tools.returnNoCasesClosed(result.Id);


                        //Populate Flags
                        if (tools.returnNoCases(result.Id) > 0)
                        {
                            ((Image)rptSearchResults.Items[i].FindControl("imgWelfare")).ImageUrl = "~/Images/Icons/Welfare.png";
                        }

                        if (tools.returnSportsThatMemberOf(result.Id).Count() > 0)
                        {
                            ((Image)rptSearchResults.Items[i].FindControl("imgInSport")).ImageUrl = "~/Images/Icons/insport.png";
                        }

                        if (result.MiniBusAssessment == 1)
                        {
                            ((Image)rptSearchResults.Items[i].FindControl("imgDriver")).ImageUrl = "~/Images/Icons/Driver.png";
                        }

                        if (result.FirstAidCertExpiry > DateTime.Now)
                        {
                            ((Image)rptSearchResults.Items[i].FindControl("imgFirstAid")).ImageUrl = "~/Images/Icons/Firstaid.png";
                        }

                        if (result.DateOfBirth > DateTime.Now.AddYears(-18))
                        {
                            ((Image)rptSearchResults.Items[i].FindControl("imgUnder18")).ImageUrl = "~/Images/Icons/18.png";
                        }

                        if (tools.checkbannedfrombar(result.Id))
                        {
                            ((Image)rptSearchResults.Items[i].FindControl("imgBeer")).ImageUrl = "~/Images/Icons/Beer.png";
                        }
                        if (tools.checkbannedfromsports(result.Id))
                        {
                            ((Image)rptSearchResults.Items[i].FindControl("imgSports")).ImageUrl = "~/Images/Icons/Sports.png";
                        }
                        if (tools.checkbanneduni(result.Id))
                        {
                            ((Image)rptSearchResults.Items[i].FindControl("imgUni")).ImageUrl = "~/Images/Icons/Uni.png";
                        }


                        i++;
                    }

                    lblTotalCases.Text = totalcases.ToString();
                    lblTotalCasesClosed.Text = totalcasesclosed.ToString();

                    lblQuery.Text = query;
                    lblresults.Text = i.ToString();

                    if (i == 0)
                    {
                        litNoResults.Visible = true;
                    }
               
            }
            else
            {
                Response.Redirect("Default.aspx");
            }

        }

        public void btnEdit_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            Common tools = new Common();
            tools.setStudentToEdit(int.Parse(btnView.CommandArgument));
            Response.Redirect("ViewStudent.aspx");
        }

        public void btnCreateCase_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            Common tools = new Common();
            tools.setStudentToEdit(int.Parse(btnView.CommandArgument));
            Response.Redirect("../Cases/AddCase.aspx");
        }

        public void btnViewCases_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            Common tools = new Common();
            var test = tools.setStudentToEdit(int.Parse(btnView.CommandArgument));

        
                Response.Redirect("../Cases/StudentsCases.aspx");
           
        }
    }
}