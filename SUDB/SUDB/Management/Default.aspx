﻿<%@ Page Title="Students' Union Database" EnableEventValidation="false" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Management.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="../Java/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="../Java/plugins/jqplot.pieRenderer.min.js"></script>
    <script type="text/javascript" src="../Java/plugins/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="../Java/plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="../Java/plugins/jqplot.pointLabels.min.js"></script>
<script type="text/javascript" src="../Java/plugins/jqplot.highlighter.min.js"></script>
<script type="text/javascript" src="../Java/plugins/jqplot.cursor.min.js"></script>
<script type="text/javascript" src="../Java/plugins/jqplot.pointLabels.min.js"></script>
    <script type="text/javascript" src="../Java/plugins/jqplot.bubbleRenderer.min.js"></script>
<link rel="stylesheet" type="text/css" href="../Java/jquery.jqplot.min.css" />

    <script>
        $(document).ready(function () {
            $("#tablesorted").tablesorter();

        }
);

    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="systemmenuright" runat="server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

            <script class="code" type="text/javascript">

                $(document).ready(function () {

                    var arr = [[576, 660, 1236, "Sports"], [700, 367, 1067, "Societies"]];

                    var plot1 = $.jqplot('chart1', [arr], {
                        
                        seriesDefaults: {
                            renderer: $.jqplot.BubbleRenderer,
                            rendererOptions: {
                                bubbleGradients: true
                            },
                            shadow: true
                           
                        }

                    });
                });
    </script>


        <script class="code" type="text/javascript">
            $(document).ready(function () {
                var line1 = [['Mens Football', 140], ['Tap and Ballet', 82], ['Christian Union', 140]];

                $('#chart2').jqplot([line1], {
                   
                    seriesDefaults: {
                        renderer: $.jqplot.BarRenderer,
                        rendererOptions: {
                            // Set the varyBarColor option to true to use different colors for each bar.
                            // The default series colors are used.
                            varyBarColor: true
                        }
                    },
                    axes: {
                        xaxis: {
                            renderer: $.jqplot.CategoryAxisRenderer
                        }
                    }
                });
            });
    </script>

    <script class="code" type="text/javascript">
    $(document).ready(function(){ 
    var s1 = [['Part Time',7], ['Full Time',13.3]];
         
    var plot8 = $.jqplot('pie8', [s1], {
        grid: {
            drawBorder: false, 
            drawGridlines: false,
            background: '#ffffff',
            shadow:false
        },
        axesDefaults: {
             
        },
        seriesDefaults:{
            renderer:$.jqplot.PieRenderer,
            rendererOptions: {
                showDataLabels: true
            }
        },
        legend: {
            show: true,
            rendererOptions: {
                numberRows: 1
            },
            location: 's'
        }
    }); 
});
</script>


     <script class="code" type="text/javascript">
         $(document).ready(function () {
             var s1 = [['Level 4', 7], ['Level 5', 13.3], ['Level 6', 13.3], ['Level M', 13.3], ['Other', 13.3]];

             var plot8 = $.jqplot('byyear', [s1], {
                 grid: {
                     drawBorder: false,
                     drawGridlines: false,
                     background: '#ffffff',
                     shadow: false
                 },
                 axesDefaults: {

                 },
                 seriesDefaults: {
                     renderer: $.jqplot.PieRenderer,
                     rendererOptions: {
                         showDataLabels: true
                     }
                 },
                 legend: {
                     show: true,
                     rendererOptions: {
                         numberRows: 1
                     },
                     location: 's'
                 }
             });
         });
</script>

    <div class="hometoprow">
 
        <div class="toprowitem">
        <h2>Cases Allocated to You</h2>

        <p>
            <asp:Label ID="lblCasesAllocatedToYou" runat="server" Text="0 <br/> (0)"></asp:Label></p>

        </div>
            <div class="toprowitem">
        <h2>Total Cases</h2>

              <p>
            <asp:Label ID="lblTotalCases" runat="server" Text="0 <br/>(0)"></asp:Label></p>


</div>

            <div class="toprowitem">

        <h2>Sports &amp; Society Members</h2>

       <p>
            <asp:Label ID="lblSportsSocMembers" runat="server" Text="0 <br/> (0)"></asp:Label></p>

                </div>

 
            <div class="toprowitem">


        <h2>Total Students</h2>

             <p>
            <asp:Label ID="lblTotalStudentRecords" runat="server" Text="0 <br/>(0)"></asp:Label></p>
                </div>


 </div>
        <div class="homecontentdiv">

               <h1>Your Open Cases</h1>

 <table id="tablesorted" class="nicetable">
     <thead>
        <tr>
            <th>Case Type</th>
            <th>Student Name</th>
            <th>Date Opened</th>
            <th></th>

        </tr>
         </thead>

     <tbody>
    <asp:Repeater ID="rptOpenCases" runat="server">

        <ItemTemplate>

            <tr>

                <td><asp:Label ID="lblCaseType" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblStudentName" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblDateOpened" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Button id="btnView"
           Text="View Case"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnView_Clickbutton" 
           runat="server"/></td>
            </tr>

        </ItemTemplate>

    </asp:Repeater>

      <asp:Literal ID="litNoOpen" Visible="false" runat="server"><tr><td colspan="4" class="errorhome">You do not have any cases allocated to you.</td></tr></asp:Literal>
         </tbody>

        </table>

            <br /><br />
            <h1>Your Open Reminders</h1>
            <p>All reminders that are past due, or due within the next 7 days are shown below.</p>
 <table id="tablesorted1" class="nicetable">
     <thead>
        <tr>
            <th>Case Type</th>
            <th>Student Name</th>
            <th>Reminder Notes</th>
            <th>Due Date</th>
            <th></th>

        </tr>
         </thead>

     <tbody>
    <asp:Repeater ID="rptReminders" runat="server">

        <ItemTemplate>

            <tr>

                <td><asp:Label ID="lblCaseType" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblStudentName" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblReminderNotes" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblDueDate" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Button id="btnView"
           Text="View Case"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnView_Clickbutton" 
           runat="server"/> <asp:Button id="btnCloseReminder"
           Text="Close Reminder"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnCloseReminder_Clickbutton" 
           runat="server"/></td>
            </tr>

        </ItemTemplate>

    </asp:Repeater>

      <asp:Literal ID="litNoReminders" Visible="false" runat="server"><tr><td colspan="4" class="errorhome">You do not have any reminders.</td></tr></asp:Literal>
         </tbody>

        </table>


   

    </div>

    <!--

      <div class="hometoprow">

     
        

        <h2>Sports &amp Society Size</h2>

               <div id="chart2" style="width:300px; height:275px;"></div>

        </div>

     

        <div class="hometoprow">

        <h2>Sports Vs Societies (Gender)</h2>

             <div id="chart1" style="width:300px; height:275px;"></div>


    </div>
    -->


    <div class="clear"></div>
    <br /><br/>
    <p class="center">*The statistics above are based on live information from the Students' Union Database</p>
</asp:Content>
