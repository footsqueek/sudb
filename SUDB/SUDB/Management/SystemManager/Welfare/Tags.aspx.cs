﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDB.Management.SystemManager.Welfare
{
    public partial class Tags : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                bindTags();
            }
        }

        protected void bindTags()
        {
             Common tools = new Common();

            var tags = tools.returnAllTags();

            rptTags.DataSource = tags;
            rptTags.DataBind();

            int i = 0;

            foreach(var tag in tags)
            {
                ((Label)rptTags.Items[i].FindControl("lblTag")).Text = tag.Tag;
                ((Button)rptTags.Items[i].FindControl("btnActive")).CommandArgument = tag.Id.ToString();

                if(tag.Active==1)
                {
                    ((Button)rptTags.Items[i].FindControl("btnActive")).Text = "Deactivate";

                }
                else
                {
                    ((Button)rptTags.Items[i].FindControl("btnActive")).Text = "Activate Tag";

                }

                i++;
            }
        
        }

        protected void btnActive_Click(object sender, EventArgs e)
        {
            Button senderbut = (Button)sender;

            Common tools = new Common();


            if(senderbut.Text == "Activate Tag")
            {
                tools.activateTag((int.Parse(senderbut.CommandArgument)));
            }
            else
            {
                tools.deactivateTag((int.Parse(senderbut.CommandArgument)));
            }

            bindTags();

        }

        protected void btnAddItem_Click(object sender, EventArgs e)
        {
            Common tools = new Common();


            tools.addTag(txtTag.Text);
            Response.Redirect("Tags.aspx");
        }
    }
}