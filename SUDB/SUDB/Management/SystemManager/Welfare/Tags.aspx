﻿<%@ Page Title="Tag Management" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Tags.aspx.cs" Inherits="SUDB.Management.SystemManager.Welfare.Tags" %>
<%@ Register src="../../../UserControls/SystemManagerMenu.ascx" tagname="SystemManagerMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">


    <uc1:SystemManagerMenu ID="SystemManagerMenu2" runat="server" />


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Welfare Tags</h1>

    <p>The following tags are available to cases in the welfare system.</p>

    <table class="nicetable">

      <tr>

          <th>Tag</th>
          <th>Active</th>

      </tr>
 

    <asp:Repeater ID="rptTags" runat="server">

        <ItemTemplate>

            <tr>

                <td>
                    <asp:Label ID="lblTag"  runat="server" Text="Label"></asp:Label></td>

                <td>
                    <asp:Button ValidationGroup="new" ID="btnActive" CssClass="standardbutton" OnClick="btnActive_Click" runat="server" Text="Deactivate" /></td>
            </tr>

        </ItemTemplate>


    </asp:Repeater>


           </table>


      <div id="addnew" runat="server">
    <asp:TextBox CssClass="txtbox" ID="txtTag" placeholder="Enter new tag name" runat="server" ValidationGroup="add"></asp:TextBox> <asp:Button ValidationGroup="add" CssClass="submitbutton" ID="btnAddItem" runat="server" Text="Add Tag" OnClick="btnAddItem_Click" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="add" ControlToValidate="txtTag" ErrorMessage="<p class='error'>You must enter a tag name</p>"></asp:RequiredFieldValidator>
        </div>
</asp:Content>
