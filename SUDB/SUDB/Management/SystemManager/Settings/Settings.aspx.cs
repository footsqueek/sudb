﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.SystemManager.Settings
{
    public partial class Settings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Check Access
            Common tools = new Common();
            if (!tools.checkAccess("SystemManager").Equals("Full Access") && !tools.checkAccess("SystemManager").Equals("Read Only"))
            {
                Response.Redirect("/Default.aspx");
            }
        }
    }
}