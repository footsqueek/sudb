﻿<%@ Page Title="Users &amp; Permissions" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ViewUser.aspx.cs" Inherits="SUDatabase.Management.SystemManager.Users.ViewUser" %>
<%@ Register src="../../../UserControls/SystemManagerMenu.ascx" tagname="SystemManagerMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:SystemManagerMenu ID="SystemManagerMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Users &amp; Permissions</h1>

    <asp:Literal ID="litSystemIntro" runat="server"><p>The details for the selected user are shown below. Please make any changes required and then click "Save".</p>
</asp:Literal>
    <asp:Literal ID="litError" Visible="false" runat="server"><p class="error">The system failed to create the user. Please try again. If this problem persists please contact Footsqueek support.</p></asp:Literal>

      <p>
        <asp:ValidationSummary CssClass="error" ValidationGroup="details" ID="ValidationSummary1" runat="server" />
    </p>

      <p>
        <asp:Label CssClass="label" ID="lblSu" runat="server" Text="Students' Union"></asp:Label><br />
          <asp:DropDownList ID="cboSU" Enabled="false" runat="server"></asp:DropDownList>
      
      </p>

    <p>
        <asp:Label CssClass="label" ID="Label1" runat="server" Text="Email Address:"></asp:Label><br /> <asp:RequiredFieldValidator ValidationGroup="details" CssClass="hidden" ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="You must enter an e-mail address"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtEmail" CssClass="hidden" ValidationGroup="details" runat="server" ErrorMessage="The email address you have entered is invalid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>

        <asp:TextBox ID="txtEmail" CssClass="txtbox" runat="server"></asp:TextBox>
    </p>
            <p>
        <asp:Label CssClass="label" ID="Label2" runat="server" Text="Firstname:"></asp:Label><br />
        <asp:TextBox ID="txtFirstname" CssClass="txtbox" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="details" CssClass="hidden" runat="server" ControlToValidate="txtFirstname" ErrorMessage="You must enter a Firstname"></asp:RequiredFieldValidator>
                  </p>

            <p>
        <asp:Label CssClass="label" ID="Label3" runat="server" Text="Lastname:"></asp:Label><br />
        <asp:TextBox ID="txtLastname" CssClass="txtbox" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="details" CssClass="hidden" runat="server" ControlToValidate="txtLastname" ErrorMessage="You must enter a Surname"></asp:RequiredFieldValidator>

    </p>

          
            <p class="smallleftpara">
        <asp:Label CssClass="label" ID="Label6" runat="server" Text="Creation Date:"></asp:Label><br />
      <asp:Label ID="lblCreationDate" runat="server" Text=""></asp:Label>
  
    </p>

            <p>
        <asp:Label CssClass="label" ID="Label4" runat="server" Text="Last Login Date:"></asp:Label><br />
      <asp:Label ID="lblLastLogin" runat="server" Text=""></asp:Label>
  
    </p>

           <p class="smallleftpara">
        <asp:Label CssClass="label" ID="Label5" runat="server" Text="Last Activity Date:"></asp:Label><br />
      <asp:Label ID="lblLastActivity" runat="server" Text=""></asp:Label>
  
    </p>

          <p>
        <asp:Label CssClass="label" ID="Label7" runat="server" Text="Date password was last changed:"></asp:Label><br />
      <asp:Label ID="lblLastPassword" runat="server" Text=""></asp:Label>
  
    </p>


              

        <table class="lefttable nicetable">
            <tr>
                <th>Permission</th>
                <th>Access Level</th>
            </tr>

            <tr>
                <td>Duty of Care</td>
                <td>
                    <asp:DropDownList ID="cboDOC" runat="server">
                        <asp:ListItem>No Access</asp:ListItem>
                        <asp:ListItem>Read Only</asp:ListItem>
                        <asp:ListItem>Full Access</asp:ListItem>
                    </asp:DropDownList></td>


            </tr>

            <tr>
                <td>Elections</td>
                <td>
                    <asp:DropDownList ID="cboElections" runat="server">
                        <asp:ListItem>No Access</asp:ListItem>
                        <asp:ListItem Value="Read Only">Clerk</asp:ListItem>
                        <asp:ListItem>Full Access</asp:ListItem>
                    </asp:DropDownList></td>


            </tr>

                 <tr>
                <td>Student Manager</td>
                <td>
                    <asp:DropDownList ID="cboStudentManager" runat="server">
                        <asp:ListItem>No Access</asp:ListItem>
                        <asp:ListItem>Read Only</asp:ListItem>
                        <asp:ListItem>Full Access</asp:ListItem>
                    </asp:DropDownList></td>


            </tr>

                 <tr>
                <td>Reports</td>
                <td>
                    <asp:DropDownList ID="cboReports" runat="server">
                        <asp:ListItem>No Access</asp:ListItem>
                        <asp:ListItem>Read Only</asp:ListItem>
                        <asp:ListItem>Full Access</asp:ListItem>
                    </asp:DropDownList></td>


            </tr>

                 <tr>
                <td>System Manager</td>
                <td>
                    <asp:DropDownList ID="cboSystemManager" runat="server">
                        <asp:ListItem>No Access</asp:ListItem>
                        <asp:ListItem>Read Only</asp:ListItem>
                        <asp:ListItem>Full Access</asp:ListItem>
                    </asp:DropDownList></td>


            </tr>

                 <tr>
                <td>Trip Registration</td>
                <td>
                    <asp:DropDownList ID="cboTripRegistration" runat="server">
                        <asp:ListItem>No Access</asp:ListItem>
                        <asp:ListItem>Read Only</asp:ListItem>
                        <asp:ListItem>Full Access</asp:ListItem>
                    </asp:DropDownList></td>


            </tr>

                

                 <tr>
                <td>Welfare Case Management</td>
                <td>
                    <asp:DropDownList ID="cboCaseLogs" runat="server">
                        <asp:ListItem>No Access</asp:ListItem>
                        <asp:ListItem>Read Only</asp:ListItem>
                        <asp:ListItem>Full Access</asp:ListItem>
                    </asp:DropDownList></td>


            </tr>

                

                 <tr>
                <td>Disciplinary System</td>
                <td>
                    <asp:DropDownList ID="cboDisciplinary" runat="server">
                        <asp:ListItem>No Access</asp:ListItem>
                        <asp:ListItem>Read Only</asp:ListItem>
                        <asp:ListItem>Full Access</asp:ListItem>
                    </asp:DropDownList></td>


            </tr>

             <tr>
                <td>Licenced Outlets</td>
                <td>
                    <asp:DropDownList ID="cboBar" runat="server">
                        <asp:ListItem>No Access</asp:ListItem>
                        <asp:ListItem>Read Only</asp:ListItem>
                        <asp:ListItem>Full Access</asp:ListItem>
                    </asp:DropDownList></td>


            </tr>

                

               



        </table>

        <asp:Button ID="btnSave" CssClass="submitbutton" runat="server" Text="Save" ValidationGroup="details" OnClick="btnSave_Click" />

</asp:Content>
