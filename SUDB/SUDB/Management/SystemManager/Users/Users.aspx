﻿<%@ Page Title="Users &amp; Permissions" Language="C#" EnableEventValidation="false" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="SUDatabase.Management.SystemManager.Users.Users" %>
<%@ Register src="../../../UserControls/SystemManagerMenu.ascx" tagname="SystemManagerMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script>
         $(document).ready(function () {
             $("#tablesorted").tablesorter();
            
         }
 );

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:SystemManagerMenu ID="SystemManagerMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Users &amp; Permissions</h1>

     <asp:Literal ID="litError" Visible="false" runat="server"><p class="error">The system failed to create the user. Please try again. If this problem persists please contact Footsqueek support.</p></asp:Literal>


    <table id="tablesorted" class="nicetable">
        <thead>
        <tr>

            <th>Username</th>
            <th>Full Name</th>
            <th>Last Login</th>
            <th></th>

        </tr>
            </thead>
        <tbody>

    <asp:Repeater ID="rptUsers" runat="server">

        <ItemTemplate>

            <tr>
                <td><asp:Label ID="lblUserName" runat="server" Text="lblUserName"></asp:Label></td>
                <td><asp:Label ID="lblFullName" runat="server" Text="lblFullName"></asp:Label></td>
                <td><asp:Label ID="lblLastLogin" runat="server" Text="lblLastLogin"></asp:Label></td>
                <td><asp:Button id="btnEdit"
           Text="Edit"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnEdit_Clickbutton" 
           runat="server"/><br />

                    <asp:Button id="btnDisable"
                        CssClass="standardbutton"
           Text="Disable"
           CommandName="Disable"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnDisable_Clickbutton" 
           runat="server"/><br />

                     <asp:Button id="btnReset"
                         CssClass="standardbutton"
           Text="Reset Password"
           CommandName="Reset"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnReset_Clickbutton" 
           runat="server"/>


                </td>
            </tr>

        </ItemTemplate>


    </asp:Repeater>
            </tbody>
        </table>

    <asp:Button CssClass="submitbutton" ID="btnNew" runat="server" Text="Create New User" OnClick="btnNew_Click" />
</asp:Content>
