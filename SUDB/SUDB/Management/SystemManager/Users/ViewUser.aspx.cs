﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.SystemManager.Users
{
    public partial class ViewUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Check Access
            Common tools = new Common();
            if (!tools.checkAccess("SystemManager").Equals("Full Access") && !tools.checkAccess("SystemManager").Equals("Read Only"))
            {
                Response.Redirect("/Default.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    bindSU();
                    bindUsers();
                    
                }
            }
        }

       protected void bindSU()
        {
            Common tools = new Common();
            var sus = tools.returnSUs();
            foreach(var su in sus)
            {
                cboSU.Items.Add(new ListItem(su.SUFullName, su.Id.ToString()));
            }

          
         
        }

        protected void bindUsers()
        {
            Common tools = new Common();

            var usertoedit = tools.returnUserToEdit();

          
            if (usertoedit != null)
            {
                cboSU.SelectedValue = usertoedit.StudentsUnion.ToString();

                //There is a user to edit
                MembershipUser userdetails = Membership.GetUser(usertoedit.UserKey);
                ProfileBase profile = ProfileBase.Create(userdetails.UserName);

                IFormatProvider culture = new System.Globalization.CultureInfo("en-US", true);
               

                txtEmail.Text = userdetails.Email.Trim();
                txtEmail.Enabled = false;
                txtFirstname.Text = (string)profile["FirstName"];
                txtLastname.Text = (string)profile["LastName"];
                lblLastActivity.Text = userdetails.LastActivityDate.ToString("dd MMM yyyy HH:mm", culture);
                lblLastPassword.Text = userdetails.LastPasswordChangedDate.ToString("dd MMM yyyy HH:mm", culture);
                lblLastLogin.Text = userdetails.LastLoginDate.ToString("dd MMM yyyy HH:mm", culture);
                lblCreationDate.Text = userdetails.CreationDate.ToString("dd MMM yyyy HH:mm", culture);

               
                cboSU.SelectedValue = usertoedit.StudentsUnion.ToString();

                cboCaseLogs.SelectedValue = tools.checkAccessForUser((Guid)userdetails.ProviderUserKey,"CaseLog");
                cboDisciplinary.SelectedValue = tools.checkAccessForUser((Guid)userdetails.ProviderUserKey, "Disciplinary");
                cboDOC.SelectedValue = tools.checkAccessForUser((Guid)userdetails.ProviderUserKey, "DutyOfCare");
                cboBar.SelectedValue = tools.checkAccessForUser((Guid)userdetails.ProviderUserKey, "Bar");
                cboReports.SelectedValue = tools.checkAccessForUser((Guid)userdetails.ProviderUserKey, "Reports");
                cboStudentManager.SelectedValue = tools.checkAccessForUser((Guid)userdetails.ProviderUserKey, "StudentManager");
                cboSystemManager.SelectedValue = tools.checkAccessForUser((Guid)userdetails.ProviderUserKey, "SystemManager");
                cboTripRegistration.SelectedValue = tools.checkAccessForUser((Guid)userdetails.ProviderUserKey, "TripRegistration");
                cboElections.SelectedValue = tools.checkAccessForUser((Guid)userdetails.ProviderUserKey, "Elections");



                if ((tools.checkAccess("SystemManager").Equals("Read Only") || userdetails.ProviderUserKey.Equals(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey)) && !HttpContext.Current.User.IsInRole("Admin"))
               {
                   //Disable All Control and change save to back
                   cboCaseLogs.Enabled = false;
                   cboDisciplinary.Enabled = false;
                   cboDOC.Enabled = false;
                   cboReports.Enabled = false;
                   cboStudentManager.Enabled = false;
                   cboSystemManager.Enabled = false;
                   cboBar.Enabled = false;
                   cboTripRegistration.Enabled = false;
                   txtFirstname.Enabled = false;
                   txtLastname.Enabled = false;
                   btnSave.Text = "Return to Users";
                   cboElections.Enabled = false;

                   litSystemIntro.Text = "<p>The details for the selected user are shown below. You do not have permission to modify this user.</p>";

               }

                if(User.IsInRole("Admin"))
                {
                    cboSU.Visible = true;
                    cboSU.Enabled = true;
                    lblSu.Visible = true;
                }
                else
                {
                    cboSU.Visible = false;
                    cboSU.Enabled = false;
                    lblSu.Visible = false;
                }


                    
            }
            else
            {

                //We are going to create a new user
                cboSU.SelectedValue= tools.getSU().Id.ToString();
               

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Common tools = new Common();
            var usertoedit = tools.returnUserToEdit();


            if (usertoedit != null)
            {
                tools.updateAccess(usertoedit.UserKey, "DutyOfCare", cboDOC.SelectedValue);
                tools.updateAccess(usertoedit.UserKey, "CaseLog", cboCaseLogs.SelectedValue);
                tools.updateAccess(usertoedit.UserKey, "Disciplinary", cboDisciplinary.SelectedValue);
                tools.updateAccess(usertoedit.UserKey, "Bar", cboBar.SelectedValue);
                tools.updateAccess(usertoedit.UserKey, "Reports", cboReports.SelectedValue);
                tools.updateAccess(usertoedit.UserKey, "StudentManager", cboStudentManager.SelectedValue);
                tools.updateAccess(usertoedit.UserKey, "SystemManager", cboSystemManager.SelectedValue);
                tools.updateAccess(usertoedit.UserKey, "TripRegistration", cboTripRegistration.SelectedValue);
                tools.updateAccess(usertoedit.UserKey, "Elections", cboElections.SelectedValue);

                MembershipUser userdetails = Membership.GetUser(usertoedit.UserKey);
                ProfileBase profile = ProfileBase.Create(userdetails.UserName);

                profile["FirstName"] = txtFirstname.Text;
                profile["LastName"] = txtLastname.Text;
                profile.Save();

                tools.saveName(txtFirstname.Text, txtLastname.Text, usertoedit.UserKey,int.Parse(cboSU.SelectedValue));



                Response.Redirect("Users.aspx");
            }
            else
            {
                //Create new User Account

                Guid newuser = tools.createUser(txtEmail.Text.Trim());
                if(newuser!=Guid.Empty)
                { 
                tools.updateAccess(newuser, "DutyOfCare", cboDOC.SelectedValue);
                tools.updateAccess(newuser, "CaseLog", cboCaseLogs.SelectedValue);
                tools.updateAccess(newuser, "Disciplinary", cboDisciplinary.SelectedValue);
                tools.updateAccess(newuser, "Bar", cboBar.SelectedValue);
                tools.updateAccess(newuser, "Reports", cboReports.SelectedValue);
                tools.updateAccess(newuser, "StudentManager", cboStudentManager.SelectedValue);
                tools.updateAccess(newuser, "SystemManager", cboSystemManager.SelectedValue);
                tools.updateAccess(newuser, "TripRegistration", cboTripRegistration.SelectedValue);

                MembershipUser userdetails = Membership.GetUser(newuser);
                ProfileBase profile = ProfileBase.Create(userdetails.UserName);

                profile["FirstName"] = txtFirstname.Text;
                profile["LastName"] = txtLastname.Text;
                profile.Save();

                 Response.Redirect("Users.aspx");
            }
            else
            {
                //Display Failed Literal
                litError.Visible = true;
            }
            }
          

        }
    }
}