﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Web.Profile;


namespace SUDatabase.Management.SystemManager.Users
{
    public partial class Users : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Check Access
            Common tools = new Common();
            if (!tools.checkAccess("SystemManager").Equals("Full Access") && !tools.checkAccess("SystemManager").Equals("Read Only"))
            {
                Response.Redirect("/Default.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    bindUsers();
                }
            }
        }

        protected void bindUsers()
        {
            Common tools = new Common();
            var StaffUsers = tools.getUsers();

            rptUsers.DataSource = StaffUsers;
            rptUsers.DataBind();

            int i = 0;

            

            foreach(var user in StaffUsers)
            {
                var security = tools.getPermissions(user.Guid);
                if (user.FirstName == null)
                {
                    ProfileBase profile = ProfileBase.Create(user.Email.ToLower().ToString());
                    tools.saveName(profile["FirstName"].ToString(), profile["LastName"].ToString(),user.Guid,(int)security.StudentsUnion);
                    user.FirstName = profile["FirstName"].ToString();
                    user.LastName = profile["LastName"].ToString();
                }
           
                
                
                ((Label)rptUsers.Items[i].FindControl("lblFullName")).Text = user.FirstName + " " + user.LastName;
                ((Label)rptUsers.Items[i].FindControl("lblUserName")).Text = user.Email.ToLower();
                ((Label)rptUsers.Items[i].FindControl("lblLastLogin")).Text = ((DateTime)user.LastLogin).ToString("dd MMM yyyy hh:ss");
                ((Button)rptUsers.Items[i].FindControl("btnEdit")).CommandArgument = user.Guid.ToString();
                ((Button)rptUsers.Items[i].FindControl("btnDisable")).CommandArgument = user.Guid.ToString();
                ((Button)rptUsers.Items[i].FindControl("btnReset")).CommandArgument = user.Guid.ToString();
                ((Button)rptUsers.Items[i].FindControl("btnEdit")).CommandArgument = user.Guid.ToString();
                   

                if(user.approved==true)
                {
                    ((Button)rptUsers.Items[i].FindControl("btnDisable")).Text = "Disable User";
                }
                else
                {
                    ((Button)rptUsers.Items[i].FindControl("btnDisable")).Text = "Enable User";
                    ((Button)rptUsers.Items[i].FindControl("btnReset")).Visible = false;
            
                }

                if((tools.checkAccess("SystemManager").Equals("Read Only") || user.Guid.Equals(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey)) && !HttpContext.Current.User.IsInRole("Admin"))
                {
                    ((Button)rptUsers.Items[i].FindControl("btnEdit")).Text = "View";
                   
                    ((Button)rptUsers.Items[i].FindControl("btnDisable")).Visible=false;
                    ((Button)rptUsers.Items[i].FindControl("btnReset")).Visible=false;

                }
                i++;
            }

            if (tools.checkAccess("SystemManager").Equals("Read Only"))
            {
                btnNew.Visible = false;
            }

        }

        public void btnReset_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
      
            if(btnView.Text =="Click to Confirm")
            {
                Common tools = new Common();

                if (tools.resetPassword(Guid.Parse(btnView.CommandArgument)))
                {
                    Response.Redirect("Users.aspx");
                }
                else
                {
                    litError.Visible = true;
                    litError.Text = "<p class=\"error\">The password reset failed. Please try again. If this problem persists please contact Footsqueek support.</p>";
                }
            }
            else
            {
                btnView.Text = "Click to Confirm";
            }

        }

        public void btnDisable_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);

            MembershipUser userdetails = Membership.GetUser(Guid.Parse(btnView.CommandArgument));
            if(userdetails.IsApproved==true)
            {
                userdetails.IsApproved = false;
                Membership.UpdateUser(userdetails);
                ActivityLogClass newActivity = new ActivityLogClass();
                newActivity.logActivity("Disabled a User Account", "The user account " + userdetails.UserName + " has been disabled");

            }
            else
            {
                userdetails.IsApproved = true;
                Membership.UpdateUser(userdetails);
                ActivityLogClass newActivity = new ActivityLogClass();
                newActivity.logActivity("Enabled a User Account", "The user account " + userdetails.UserName + " has been enabled");

            }
            Response.Redirect("Users.aspx");
        }

        public void btnEdit_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            Common tools = new Common();
            tools.setUserToEdit(Guid.Parse(btnView.CommandArgument));
            Response.Redirect("ViewUser.aspx");
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
  
            Common tools = new Common();
            tools.setUserToEditNull();
            Response.Redirect("ViewUser.aspx");
        }
    }
}