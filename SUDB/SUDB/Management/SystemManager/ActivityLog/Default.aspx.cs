﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SUDatabase.Classes;
using System.Web.Security;
using SUDB;

namespace SUDatabase.Management.SystemManager.ActivityLog
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Populate Filter

            if (!IsPostBack)
            {
                Common tools = new Common();

                var users = tools.getUsers();

                cboUserFilter.Items.Add("All Users");

                foreach (var user in users)
                {
                    IFormatProvider culture = new System.Globalization.CultureInfo("en-US", true);
                    MembershipUser userdetails = Membership.GetUser(user);

                    cboUserFilter.Items.Add(userdetails.UserName);
                }

                bindActivities();
            }
        }

        protected void bindActivities()
        {
            IFormatProvider culture = new System.Globalization.CultureInfo("en-US", true);
            int index = 0;

            try
            {
                index = int.Parse(Request.QueryString["index"]);
            }
            catch
            {
                index = 0;
            }

            string email = "";

            try
            {
                email = Request.QueryString["email"];
            }
            catch
            {
                email = "";
            }

            Guid filteruser = Guid.Empty;

            if(email!=null)
            {
                MembershipUser newuser = Membership.GetUser(email);
                filteruser = (Guid)newuser.ProviderUserKey;
                cboUserFilter.SelectedValue = email;
            }
            else
            {
                filteruser = Guid.Empty;
            }

            ActivityLogClass newActivity = new ActivityLogClass();
            var activities = newActivity.returnActivities(index,filteruser);

            

            rptActivityLog.DataSource = activities;
            rptActivityLog.DataBind();

            int i = 0;

            foreach(tblActivityLog activity in activities)
            {
                MembershipUser userdetails = Membership.GetUser((Guid)activity.UserGuid);

                ((Label)rptActivityLog.Items[i].FindControl("lblUser")).Text = userdetails.UserName;
                ((Label)rptActivityLog.Items[i].FindControl("lblActivityType")).Text = activity.ActivityName;
                ((Label)rptActivityLog.Items[i].FindControl("lblDate")).Text = ((DateTime)activity.DateTime).ToString("dd MMM yyyy HH:mm", culture);
                ((Literal)rptActivityLog.Items[i].FindControl("litActivityDesc")).Text = activity.ActivityDetails;

                i++;

            }

            if(i==0)
            {
                litError.Visible = true;
            }
        }

        protected void cboUserFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboUserFilter.SelectedValue == "All Users")
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                Response.Redirect("Default.aspx?email=" + cboUserFilter.SelectedValue);
            }
        }
    }
}