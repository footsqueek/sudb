﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDB.Management.Bar.Default" %>

<%@ Register Src="~/UserControls/BarMenu.ascx" TagPrefix="uc1" TagName="BarMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">

    <uc1:BarMenu runat="server" id="BarMenu" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Licenced Outlets</h1>

    <p>This section of the system provides information relevant to licenced outlets. Please select an option from the menu.</p>

</asp:Content>
