﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDB.Management.Bar
{
    public partial class Under18 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                bindBarred();
            }
        }

        protected void bindBarred()
        {
            Common tools = new Common();

            var barred = tools.returnUnder18();

            rptMembers.DataSource = barred;
            rptMembers.DataBind();
            int i=0;
            foreach(var member in barred)
            {
                ((Label)rptMembers.Items[i].FindControl("lblName")).Text = member.FirstName + " " + member.Surname;
                ((Label)rptMembers.Items[i].FindControl("lblStudentNumber")).Text = member.StudentNumber;
                ((Label)rptMembers.Items[i].FindControl("lblDOB")).Text = ((DateTime)member.DateOfBirth).ToString("dd/MM/yyyy");
                ((Label)rptMembers.Items[i].FindControl("lblAge")).Text = (DateTime.Now.Year - member.DateOfBirth.Value.Year).ToString();

                if (tools.getSU().Id == 1)
                {
                    ((Literal)rptMembers.Items[i].FindControl("litPhoto")).Text = "<img width=\"96px\" height=\"110px\" src=\"https://ganymede.chester.ac.uk/ple/json-rpc/photo_redirect.php?searchterm=" + member.StudentNumber + "\"/>";
                }
                else
                {
                    ((Literal)rptMembers.Items[i].FindControl("litPhoto")).Text = "No Photo";
                }
                i++;
            }

            if(barred.Count()==0)
            {
                litNoBarred.Visible = true;
            }
        }
    }
}