﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Under18.aspx.cs" Inherits="SUDB.Management.Bar.Under18" %>

<%@ Register Src="~/UserControls/BarMenu.ascx" TagPrefix="uc1" TagName="BarMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script>
         $(document).ready(function () {
             $("#tablesorted").tablesorter();
            
         }
 );

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">

    <uc1:BarMenu runat="server" id="BarMenu" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Under 18</h1>

    <p>The following members are currently under 18 and may not be served alcohol in licenced outlets.</p>

    <table id="tablesorted" class="nicetable">
        <thead>
        <tr>
            <th></th>
            <th>Student Number</th>
            <th>Student Name</th>
             <th>Date of Birth</th>
             <th>Age</th>

        </tr>
            </thead>
        <tbody>
        <asp:Repeater ID="rptMembers" runat="server">

            <ItemTemplate>
                <tr>
                  <td>
                     <asp:Literal ID="litPhoto" runat="server"></asp:Literal> </td>
                     <td><asp:Label ID="lblStudentNumber" runat="server" Text=""></asp:Label></td>
                    <td><asp:Label ID="lblName" runat="server" Text=""></asp:Label></td>
                      <td><asp:Label ID="lblDOB" runat="server" Text=""></asp:Label></td>
                    <td><asp:Label ID="lblAge" runat="server" Text=""></asp:Label></td>

                </tr>
            </ItemTemplate>

        </asp:Repeater>
       </tbody>

    </table>
      <asp:Literal ID="litNoBarred" Visible="false" runat="server"><p class="error">There are no members currently barred from licenced outlets.</p></asp:Literal>

</asp:Content>
