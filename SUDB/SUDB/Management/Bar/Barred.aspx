﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Barred.aspx.cs" Inherits="SUDB.Management.Bar.Barred" %>

<%@ Register Src="~/UserControls/BarMenu.ascx" TagPrefix="uc1" TagName="BarMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script>
         $(document).ready(function () {
             $("#tablesorted").tablesorter();
            
         }
 );

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">

    <uc1:BarMenu runat="server" id="BarMenu" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Barred</h1>

    <p>The following members are currently barred from licenced outlets.</p>

    <table id="tablesorted" class="nicetable">
        <thead>
        <tr>
            <th></th>
            <th>Student Number</th>
            <th>Student Name</th>
           
            <th>Flags</th>
            <th></th>

        </tr>
</thead>
        <tbody>
        <asp:Repeater ID="rptMembers" runat="server">

            <ItemTemplate>
                <tr>
                  <td>
                     <asp:Literal ID="litPhoto" runat="server"></asp:Literal> </td>
                     <td><asp:Label ID="lblStudentNumber" runat="server" Text=""></asp:Label></td>
                    <td><asp:Label ID="lblName" runat="server" Text=""></asp:Label></td>
                     
                 <td colspan="1"><asp:Image ID="imgFirstAid" ImageUrl="~/Images/Icons/Firstaid_g.png" height="25" runat="server" /> <asp:Image ID="imgDriver" ImageUrl="~/Images/Icons/Driver_g.png" height="25" runat="server" /> <asp:Image ID="imgWelfare" ImageUrl="~/Images/Icons/Welfare_g.png" height="25" runat="server" /> <asp:Image ID="imgInSport" ImageUrl="~/Images/Icons/insport_g.png" height="25" runat="server" /> <br /><asp:Image ID="imgUnder18" ImageUrl="~/Images/Icons/18_g.png" height="25" runat="server" /> <asp:Image ID="imgBeer" ImageUrl="~/Images/Icons/Beer_g.png" height="25" runat="server" /> <asp:Image ID="imgSports" ImageUrl="~/Images/Icons/Sports_g.png" height="25" runat="server" /> <asp:Image ID="imgUni" ImageUrl="~/Images/Icons/Uni_g.png" height="25" runat="server" /></td>
                <td rowspan="1">
                    <asp:Button id="btnEdit"
           Text="Edit Student"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnEdit_Clickbutton" 
           runat="server"/>

                </td>

                </tr>
            </ItemTemplate>

        </asp:Repeater>
       </tbody>

    </table>
      <asp:Literal ID="litNoBarred" Visible="false" runat="server"><p class="error">There are no members currently barred from licenced outlets.</p></asp:Literal>

</asp:Content>
