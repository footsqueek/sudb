﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDB.Management.DutyOfCare
{
    public partial class Drivers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

         
                if (!IsPostBack)
                {
                    bindBarred();
                }
            }

        protected void bindBarred()
        {
            Common tools = new Common();

            var barred = tools.returnDrivers();

            rptMembers.DataSource = barred;
            rptMembers.DataBind();
            int i = 0;
            foreach (var member in barred)
            {
                ((Label)rptMembers.Items[i].FindControl("lblName")).Text = member.FirstName + " " + member.Surname;
                ((Label)rptMembers.Items[i].FindControl("lblStudentNumber")).Text = member.StudentNumber;
              
                if (tools.getSU().Id == 1)
                {
                    ((Literal)rptMembers.Items[i].FindControl("litPhoto")).Text = "<img width=\"96px\" height=\"110px\" src=\"https://ganymede.chester.ac.uk/ple/json-rpc/photo_redirect.php?searchterm=" + member.StudentNumber + "\"/>";
                }
                else
                {
                    ((Literal)rptMembers.Items[i].FindControl("litPhoto")).Text = "No Photo";
                }


                ((Button)rptMembers.Items[i].FindControl("btnEdit")).CommandArgument = member.Id.ToString();

                if (tools.checkAccess("StudentManager") == "Full Access" || (tools.checkAccess("StudentManager") == "Read Only"))
                {

                }
                else
                {
                    ((Button)rptMembers.Items[i].FindControl("btnEdit")).Visible = false;
                }

                //Populate Flags
                if (tools.returnNoCases(member.Id) > 0)
                {
                    ((Image)rptMembers.Items[i].FindControl("imgWelfare")).ImageUrl = "~/Images/Icons/Welfare.png";
                }

                if (tools.returnSportsThatMemberOf(member.Id).Count() > 0)
                {
                    ((Image)rptMembers.Items[i].FindControl("imgInSport")).ImageUrl = "~/Images/Icons/insport.png";
                }

                if (member.MiniBusAssessment == 1)
                {
                    ((Image)rptMembers.Items[i].FindControl("imgDriver")).ImageUrl = "~/Images/Icons/Driver.png";
                }

                if (member.FirstAidCertExpiry > DateTime.Now)
                {
                    ((Image)rptMembers.Items[i].FindControl("imgFirstAid")).ImageUrl = "~/Images/Icons/Firstaid.png";
                }

                if (member.DateOfBirth > DateTime.Now.AddYears(-18))
                {
                    ((Image)rptMembers.Items[i].FindControl("imgUnder18")).ImageUrl = "~/Images/Icons/18.png";
                }

                if (tools.checkbannedfrombar(member.Id))
                {
                    ((Image)rptMembers.Items[i].FindControl("imgBeer")).ImageUrl = "~/Images/Icons/Beer.png";
                }
                if (tools.checkbannedfromsports(member.Id))
                {
                    ((Image)rptMembers.Items[i].FindControl("imgSports")).ImageUrl = "~/Images/Icons/Sports.png";
                }
                if (tools.checkbanneduni(member.Id))
                {
                    ((Image)rptMembers.Items[i].FindControl("imgUni")).ImageUrl = "~/Images/Icons/Uni.png";
                }

                i++;
            }

            if (barred.Count() == 0)
            {
                litNoBarred.Visible = true;
            }
        }

        public void btnEdit_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            Common tools = new Common();
            tools.setStudentToEdit(int.Parse(btnView.CommandArgument));
            Response.Redirect("/Management/StudentManager/ViewStudent.aspx");
        }
    }
}