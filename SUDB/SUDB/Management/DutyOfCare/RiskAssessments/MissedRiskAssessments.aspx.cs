﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.DutyOfCare.RiskAssessments
{
    public partial class MissedRiskAssessments : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
                bindMissing();
            
        }

        protected void bindMissing()
        {
          
            
            Common tools = new Common();
            var sessions = tools.returnSessionsWithMissingSubmissions();
            int i = 0;

            if (sessions.Count() == 0)
            {
                litNone.Visible = true;
            }
            else
            {
                litNone.Visible = false;
            }

            rptMissing.DataSource = sessions;
            rptMissing.DataBind();

            foreach(var session in sessions)
            {
                if (!IsPostBack)
                {
                    var missedreasons = tools.returnMissedReasons();
                    ((DropDownList)rptMissing.Items[i].FindControl("cboReasonCategory")).Items.Clear();
                    foreach (var reason in missedreasons)
                    {
                        ((DropDownList)rptMissing.Items[i].FindControl("cboReasonCategory")).Items.Add(new ListItem(reason.MissedReason, reason.Id.ToString()));
                    }
                }

                var sportsociety = tools.returnSportSociety((int)session.SportSocietyId);

                ((Label)rptMissing.Items[i].FindControl("lblSportSociety")).Text = sportsociety.Name;
                ((Label)rptMissing.Items[i].FindControl("lblSessionName")).Text = session.SessionName;
                ((Label)rptMissing.Items[i].FindControl("lblStartTime")).Text = ((DateTime)session.StartDateTime).ToString("dd MMM yyyy HH:mm");
                ((Label)rptMissing.Items[i].FindControl("lblEndTime")).Text = ((DateTime)session.StartDateTime).ToString("dd MMM yyyy HH:mm");
                ((Button)rptMissing.Items[i].FindControl("btnClear")).CommandArgument = session.Id.ToString();

                if (session.RiskAssessmentId == 0)
                {
                    ((Label)rptMissing.Items[i].FindControl("lblMissing")).Text = ((Label)rptMissing.Items[i].FindControl("lblMissing")).Text + "Risk Assessment";
                }
                if (session.RegisterComplete == 0)
                {
                    ((Label)rptMissing.Items[i].FindControl("lblMissing")).Text = ((Label)rptMissing.Items[i].FindControl("lblMissing")).Text + ", Register";
                }
                i++;
            }

            
        }

        protected void btnClear_Clickbutton(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);

            RepeaterItem parentRow = (RepeaterItem)((Button)sender).Parent;

            

            Common tools = new Common();

            int missedreason = int.Parse(((DropDownList)parentRow.FindControl("cboReasonCategory")).SelectedValue);
            string explanation = ((TextBox)parentRow.FindControl("txtReasonCleared")).Text;

            tools.clearMissed(int.Parse(btnView.CommandArgument),missedreason,explanation);

            Response.Redirect("MissedRiskAssessments.aspx");
        }
    }
}