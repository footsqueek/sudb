﻿<%@ Page Title="Missing Submissions" Language="C#" EnableEventValidation="false" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="MissedRiskAssessments.aspx.cs" Inherits="SUDatabase.Management.DutyOfCare.RiskAssessments.MissedRiskAssessments" %>
<%@ Register src="../../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Missing Submissions</h1>

    <p>Below are all of the sessions that have missing submissions. If a sport or society misses a submission their sport or society is "locked" until the miss is cleared on this page.</p>

    <table class="nicetable">
        <thead>
        <tr>

            <th>Sport / Society</th>
            <th>Session Name</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>What's Missing</th>
            <th></th>

        </tr>
            </thead>
        <tbody>
    <asp:Repeater ID="rptMissing" runat="server">

        <ItemTemplate>
            
            <tr>

                <td><asp:Label ID="lblSportSociety" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblSessionName" runat="server" Text="Label"></asp:Label></td>
                   <td><asp:Label ID="lblStartTime" runat="server" Text="Label"></asp:Label></td>
                   <td><asp:Label ID="lblEndTime" runat="server" Text="Label"></asp:Label></td>
                   <td><asp:Label ID="lblMissing" runat="server" Text=""></asp:Label></td>
                <td><asp:Button id="btnClear"
           Text="Clear Missed"
                    CssClass="standardbutton"
           CommandName="Register"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnClear_Clickbutton" 
           runat="server"/></td>
            </tr>

            <tr class="notes">
                  <td><asp:DropDownList ID="cboReasonCategory" runat="server"></asp:DropDownList></td>
                <td colspan="5"><asp:TextBox CssClass="txtbox" ID="txtReasonCleared" TextMode="MultiLine" Rows="2" runat="server"></asp:TextBox></td>

            </tr>


        </ItemTemplate>

    </asp:Repeater>
            </tbody>
        </table>

    
    <asp:Literal ID="litNone" runat="server"><p class="error">There are no uncleared missing submissions.</p></asp:Literal>

</asp:Content>
