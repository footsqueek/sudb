﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.DutyOfCare.RiskAssessments
{
    public partial class checklistreports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bindOpenRisks();
        }

        protected void bindOpenRisks()
        {
            Common tools = new Common();
            var risks = tools.returnOpenRisks();

            if(risks.Count()==0)
            {
                litNone.Visible = true;
            }
            else
            {
                litNone.Visible = false;
            }

            int i = 0;

            rptRiskAssessments.DataSource = risks;
            rptRiskAssessments.DataBind();

            foreach (var risk in risks)
            {
                try
                {
                    var session = tools.returnSession(risk.sessionId);
                    var riskquestion = tools.returnRiskQuestion(risk.RiskQuestionId);
                    ((Label)rptRiskAssessments.Items[i].FindControl("lblRiskId")).Text = risk.RiskQuestionId.ToString();

                    ((Label)rptRiskAssessments.Items[i].FindControl("lblSessionName")).Text = session.SessionName;
                    ((Label)rptRiskAssessments.Items[i].FindControl("lblSessionLocation")).Text = session.Location;
                    ((Label)rptRiskAssessments.Items[i].FindControl("lblRiskQuestion")).Text = riskquestion.RiskInstruction;
                    ((Label)rptRiskAssessments.Items[i].FindControl("lblWhoAtRisk")).Text = risk.WhoIsAtRisk;
                    ((Button)rptRiskAssessments.Items[i].FindControl("btnRiskAssessment")).CommandArgument = session.Id.ToString();
                    ((Button)rptRiskAssessments.Items[i].FindControl("btnClearRisk")).CommandArgument = session.Id.ToString();
                    ((Button)rptRiskAssessments.Items[i].FindControl("btnClearRisk")).CommandName = risk.RiskQuestionId.ToString();
                }
                catch
                {
                    rptRiskAssessments.Items[i].Visible = false;
                }
                i++;
            }
        }


        protected void btnRiskAssessment_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);

            Common tools = new Common();
            var session = tools.returnSession(int.Parse(btnView.CommandArgument));
            tools.setActiveSportSociety((int)session.SportSocietyId);

            Response.Redirect("/Committee/Sessions/RiskAssessment/Default.aspx?sessionId=" + btnView.CommandArgument);
        }

        protected void btnClearRisk_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);

            RepeaterItem parentRow = (RepeaterItem)((Button)sender).Parent;



            string explanation = ((TextBox)parentRow.FindControl("txtAction")).Text;


            Common tools = new Common();
            tools.clearRisk(int.Parse(btnView.CommandArgument), int.Parse(btnView.CommandName),explanation);
            Response.Redirect("checklistreports.aspx");
        }
    }
}