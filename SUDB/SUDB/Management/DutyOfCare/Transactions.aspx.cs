﻿using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.DutyOfCare
{
    public partial class Transactions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if(!IsPostBack)
            {
                txtDate.Text = DateTime.Now.ToShortDateString();
                txtEnd.Text = DateTime.Now.ToShortDateString();
            }
            try
            {
                bindTransactions();
            }
            catch
            {

            }
        }

        protected void bindTransactions()
        {
            Common tools = new Common();

          var transactions = tools.returnTransactionsBetweenDates(DateTime.Parse(txtDate.Text),(DateTime.Parse(txtEnd.Text)));

          decimal approvedamount = 0;
          decimal cashamount = 0;
            decimal aspireamount = 0;
            decimal worldpay = 0;
            rptTransactions.DataSource = transactions;
            rptTransactions.DataBind();

            int i = 0;

            foreach(tblTransaction transaction in transactions)
            {

                ((Label)rptTransactions.Items[i].FindControl("lblOrderNumber")).Text = "<a href=\"TransactionDetail.aspx?orderId="+ transaction.transactionId +"\">" + transaction.transactionId.ToString()+ "</a>";
                ((Label)rptTransactions.Items[i].FindControl("lblTransactionDate")).Text = ((DateTime)transaction.transactionDate).ToString("dd MMM yyyy HH:mm");

                switch (transaction.status)
                {
                    case 0:

                        ((Label)rptTransactions.Items[i].FindControl("lblTransactionStatus")).Text = "Not Processed";


                        break;

                    case 1:

                        ((Label)rptTransactions.Items[i].FindControl("lblTransactionStatus")).Text = "EPDQ";
                        approvedamount = approvedamount + (decimal)transaction.Amount;

                        break;
                    case 2:

                        ((Label)rptTransactions.Items[i].FindControl("lblTransactionStatus")).Text = "Declined";
                        //approvedamount = approvedamount + (decimal)transaction.Amount;

                        break;
                    case 3:

                        ((Label)rptTransactions.Items[i].FindControl("lblTransactionStatus")).Text = "Cash";
                        cashamount = cashamount + (decimal)transaction.Amount;

                        break;

                    case 4:

                        ((Label)rptTransactions.Items[i].FindControl("lblTransactionStatus")).Text = "Aspire";
                        aspireamount = worldpay + (decimal)transaction.Amount;

                        break;
                    case 5:

                        ((Label)rptTransactions.Items[i].FindControl("lblTransactionStatus")).Text = "World Pay";
                        worldpay = worldpay + (decimal)transaction.Amount;

                        break;
                }


                //get Student
                var student = tools.returnStudent((int)transaction.studentId);

                ((Label)rptTransactions.Items[i].FindControl("lblStudentNumber")).Text = student.StudentNumber;
                ((Label)rptTransactions.Items[i].FindControl("lblStudentName")).Text = student.FirstName + " " + student.Surname;

                ((Label)rptTransactions.Items[i].FindControl("lblTransactionAmount")).Text = "&pound;" + Math.Round((double)transaction.Amount,2).ToString();





                i++;
            }

            lblTotalApproved.Text = "&pound;" +  Math.Round(approvedamount,2).ToString();
            lblCash.Text = "&pound;" + Math.Round(cashamount, 2).ToString();
            lblAspire.Text = "&pound;" + Math.Round(aspireamount, 2).ToString();
            lblWorldPay.Text = "&pound;" + Math.Round(worldpay, 2).ToString();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (rptTransactions.Items.Count > 0)
            {
                StringBuilder myCsv = new StringBuilder();

                myCsv.AppendFormat("{0},{1},{2},{3},{4},{5}\r\n", "Order Number", "Student Number", "Name", "Date", "Gross", "Status");


                for (int count = 0; count < rptTransactions.Items.Count; count++)
                {
                    try
                    {
                        Label Product = (Label)rptTransactions.Items[count].FindControl("lblOrderNumber");
                        Label Analysis = (Label)rptTransactions.Items[count].FindControl("lblStudentNumber");
                        Label Unit = (Label)rptTransactions.Items[count].FindControl("lblStudentName");

                        Label Required = (Label)rptTransactions.Items[count].FindControl("lblTransactionDate");
                        Label Cost = (Label)rptTransactions.Items[count].FindControl("lblTransactionAmount");
                        Label Notes = (Label)rptTransactions.Items[count].FindControl("lblTransactionStatus");

                        myCsv.AppendFormat("{0},{1},{2},{3},{4},{5}\r\n", Product.Text.Trim().Replace("<span class=\"smaller\">", " ").Replace("</span>", "").Replace("<br/>", ""), "." + Analysis.Text.Trim().Replace(",", " "), Unit.Text, Required.Text, Cost.Text.Replace("&pound;", ""), Notes.Text, "\"");
                    }
                    catch
                    {
                        //myCsv.AppendFormat("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19}\r\n", count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), "\"" + count.ToString() + "\"");

                    }


                }

               

                Response.Clear();
                Response.ContentType = "application/csv";
                Response.AddHeader("content-disposition", "attachment; filename=DailyBanking" + txtDate.Text + ".csv");
                Response.Write(myCsv.ToString());
                Response.Flush();
                Response.End();
            }
        }
    }
}