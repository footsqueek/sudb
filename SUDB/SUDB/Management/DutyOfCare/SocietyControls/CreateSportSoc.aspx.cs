﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.DutyOfCare.SocietyControls
{
    public partial class CreateSportSoc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Common tools = new Common();
           var sportsoc =  tools.createSportSoc(txtName.Text);
           tools.setActiveSportSociety(sportsoc.Id);
           Response.Redirect("Settings.aspx");
        }
    }
}