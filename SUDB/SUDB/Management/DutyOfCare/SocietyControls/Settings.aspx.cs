﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.DutyOfCare.SocietyControls
{
    public partial class Settings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();
            var types = tools.returnDOCTypes();
            foreach (var type in types)
            {
                cboType.Items.Add(new ListItem(type.DOCType, type.Id.ToString()));
            }

            if (!IsPostBack)
            {
                bindUsers();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Common tools = new Common();
            tools.updateSportSoc(txtName.Text,txtSpecial.Text, txtNominalCode.Text, int.Parse(cboVAT.SelectedValue), int.Parse(cboType.SelectedValue), decimal.Parse(txtPrice.Text), int.Parse(cboAvailable.SelectedValue), int.Parse(cboEnableRegister.SelectedValue), int.Parse(cboEnableHSChecklist.SelectedValue), int.Parse(cboEnforceHSChecklist.SelectedValue), int.Parse(cboEnforceRegister.SelectedValue));

            Response.Redirect("../SportsSocieties.aspx");
        }

        protected void bindUsers()
        {
            Common tools = new Common();
            var StaffUsers = tools.getSportUsers((int)tools.returnActiveSportSoc().Id);

           
            var sportsoc = tools.returnActiveSportSoc();

            lblTotalMembers.Text = tools.getTotalMembers(sportsoc.Id).ToString();


            lblMissedRisk.Text = tools.getTotalMissedRiskAssessments(sportsoc.Id).ToString();
            lblMissedReg.Text = tools.getTotalMissedReg(sportsoc.Id).ToString();
            lblTotSessions.Text = tools.getTotalSessions(sportsoc.Id).ToString();

            txtName.Text = sportsoc.Name;
            txtPrice.Text = (Math.Round((decimal)sportsoc.DefaultPrice,2)).ToString();
            cboVAT.SelectedValue = sportsoc.VAT.ToString();
            cboAvailable.SelectedValue = sportsoc.AvailableToBuy.ToString();
            txtNominalCode.Text = sportsoc.NominalCode;
            txtSpecial.Text = sportsoc.SpecialEmailInstructions;
            cboEnableRegister.SelectedValue = sportsoc.EnableRegister.ToString();
            cboEnableHSChecklist.SelectedValue = sportsoc.EnableHSChecklist.ToString();
            cboEnforceHSChecklist.SelectedValue = sportsoc.EnfoceHSChecklist.ToString();
            cboEnforceRegister.SelectedValue = sportsoc.EnforceRegister.ToString();
            try
            {
                cboType.SelectedValue = sportsoc.Type.ToString();
            }
            catch
            {

            }

            rptUsers.DataSource = StaffUsers;
            rptUsers.DataBind();

            int i = 0;

            foreach (var user in StaffUsers)
            {
                IFormatProvider culture = new System.Globalization.CultureInfo("en-US", true);
                MembershipUser userdetails = Membership.GetUser(user);

                ProfileBase profile = ProfileBase.Create(userdetails.UserName);



                ((Label)rptUsers.Items[i].FindControl("lblFullName")).Text = profile["FirstName"] + " " + profile["LastName"];
                ((Label)rptUsers.Items[i].FindControl("lblUserName")).Text = userdetails.UserName;
                ((Label)rptUsers.Items[i].FindControl("lblLastLogin")).Text = userdetails.LastLoginDate.ToString("dd MMM yyyy HH:mm", culture);
                ((Button)rptUsers.Items[i].FindControl("btnEdit")).CommandArgument = userdetails.ProviderUserKey.ToString();
                ((Button)rptUsers.Items[i].FindControl("btnDisable")).CommandArgument = userdetails.ProviderUserKey.ToString();
                ((Button)rptUsers.Items[i].FindControl("btnReset")).CommandArgument = userdetails.ProviderUserKey.ToString();
                ((Button)rptUsers.Items[i].FindControl("btnEdit")).CommandArgument = userdetails.ProviderUserKey.ToString();


                if (profile["FirstName"]== "")
                {
                    int index = userdetails.UserName.IndexOf("@");
                    if (index >= 0)
                    {
                        String StudentNumber = userdetails.UserName.Substring(0, index);
                       var student = tools.returnStudentByNumber(StudentNumber);
                        if(student!=null)
                        {
                            ((Label)rptUsers.Items[i].FindControl("lblFullName")).Text = student.FirstName + " " + student.Surname;
                            profile["FirstName"] = student.FirstName;
                            profile["LastName"] = student.Surname;

                        }
                        else
                        {
                            ((Label)rptUsers.Items[i].FindControl("lblFullName")).Text = StudentNumber;

                        }
                    }
                }

                if (userdetails.IsApproved == true)
                {
                    ((Button)rptUsers.Items[i].FindControl("btnDisable")).Text = "Disable User";
                }
                else
                {
                    ((Button)rptUsers.Items[i].FindControl("btnDisable")).Text = "Enable User";
                    ((Button)rptUsers.Items[i].FindControl("btnReset")).Visible = false;

                }

                if (userdetails.ProviderUserKey.Equals(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey))
                {
                    ((Button)rptUsers.Items[i].FindControl("btnEdit")).Text = "View";

                    ((Button)rptUsers.Items[i].FindControl("btnDisable")).Visible = false;
                    ((Button)rptUsers.Items[i].FindControl("btnReset")).Visible = false;

                }
                i++;
            }

           

        }

        public void btnReset_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);

            if (btnView.Text == "Click to Confirm")
            {
                Common tools = new Common();

                if (tools.resetPassword(Guid.Parse(btnView.CommandArgument)))
                {
                    Response.Redirect("Settings.aspx");
                }
                else
                {
                    litError.Visible = true;
                    litError.Text = "<p class=\"error\">The password reset failed. Please try again. If this problem persists please contact Footsqueek support.</p>";
                }
            }
            else
            {
                btnView.Text = "Click to Confirm";
            }

        }

        public void btnDisable_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);

            MembershipUser userdetails = Membership.GetUser(Guid.Parse(btnView.CommandArgument));
            if (userdetails.IsApproved == true)
            {
                userdetails.IsApproved = false;
                Membership.UpdateUser(userdetails);
                ActivityLogClass newActivity = new ActivityLogClass();
                newActivity.logActivity("Disabled a User Account", "The user account " + userdetails.UserName + " has been disabled");

            }
            else
            {
                userdetails.IsApproved = true;
                Membership.UpdateUser(userdetails);
                ActivityLogClass newActivity = new ActivityLogClass();
                newActivity.logActivity("Enabled a User Account", "The user account " + userdetails.UserName + " has been enabled");

            }
            Response.Redirect("Settings.aspx");
        }

        public void btnEdit_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            Common tools = new Common();
            tools.removeCommitteeMember(Guid.Parse(btnView.CommandArgument), tools.returnActiveSportSoc().Id);
            Response.Redirect("Settings.aspx");
            
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Common tools = new Common();

            tools.createSportSocUser(txtEmail.Text,tools.returnActiveSportSoc().Id);

            Response.Redirect("Settings.aspx");


        }
    }
}