﻿<%@ Page Title="Sport / Society Settings" EnableEventValidation="false" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="SUDatabase.Management.DutyOfCare.SocietyControls.Settings" %>
<%@ Register src="../../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        
  
    <h1>
        Edit Sport or Society</h1>


      <h2>Settings</h2>
     <p><asp:Label  CssClass="label" ID="Label3" runat="server" Text="Sport / Society Name"></asp:Label><br />  <asp:TextBox CssClass="txtbox" ID="txtName" runat="server"></asp:TextBox>
</p>

     <p><asp:Label  CssClass="label" ID="Label1" runat="server" Text="Default Price"></asp:Label><br />  <asp:TextBox CssClass="txtbox" ID="txtPrice" runat="server"></asp:TextBox>
</p>

     <p><asp:Label  CssClass="label" ID="Label4" runat="server" Text="Nominal Code"></asp:Label><br />  <asp:TextBox CssClass="txtbox" ID="txtNominalCode" runat="server"></asp:TextBox>
</p>

      <p><asp:Label  CssClass="label" ID="Label7" runat="server" Text="Special Instructions (Included on Welcome E-mail)"></asp:Label><br />  <asp:TextBox CssClass="txtbox" ID="txtSpecial" runat="server"></asp:TextBox>
</p>

     <p><asp:Label  CssClass="label" ID="Label5" runat="server" Text="VAT"></asp:Label><br />  <asp:DropDownList CssClass="combobox" ID="cboVAT" runat="server">
         <asp:ListItem Value="1">Yes</asp:ListItem>
         <asp:ListItem Value="0">No</asp:ListItem>
         </asp:DropDownList>
</p>

      <p><asp:Label  CssClass="label" ID="Label6" runat="server" Text="Available to Buy"></asp:Label><br />  <asp:DropDownList CssClass="combobox" ID="cboAvailable" runat="server">
         <asp:ListItem Value="1">Yes</asp:ListItem>
         <asp:ListItem Value="0">No</asp:ListItem>
         </asp:DropDownList>
</p>

     <p><asp:Label  CssClass="label" ID="Label8" runat="server" Text="Enable Register"></asp:Label><br />  <asp:DropDownList CssClass="combobox" ID="cboEnableRegister" runat="server">
         <asp:ListItem Value="1">Yes</asp:ListItem>
         <asp:ListItem Value="0">No</asp:ListItem>
         </asp:DropDownList>
</p>

       <p><asp:Label  CssClass="label" ID="Label9" runat="server" Text="Enable Health and Safety Checklist"></asp:Label><br />  <asp:DropDownList CssClass="combobox" ID="cboEnableHSChecklist" runat="server">
         <asp:ListItem Value="1">Yes</asp:ListItem>
         <asp:ListItem Value="0">No</asp:ListItem>
         </asp:DropDownList>
</p>

       <p><asp:Label  CssClass="label" ID="Label10" runat="server" Text="Enforce Register Submission"></asp:Label><br />  <asp:DropDownList CssClass="combobox" ID="cboEnforceRegister" runat="server">
         <asp:ListItem Value="1">Yes</asp:ListItem>
         <asp:ListItem Value="0">No</asp:ListItem>
         </asp:DropDownList>
</p>

     <p><asp:Label  CssClass="label" ID="Label11" runat="server" Text="Enforce Health and Safety Checklist Submission"></asp:Label><br />  <asp:DropDownList CssClass="combobox" ID="cboEnforceHSChecklist" runat="server">
         <asp:ListItem Value="1">Yes</asp:ListItem>
         <asp:ListItem Value="0">No</asp:ListItem>
         </asp:DropDownList>
</p>


    <p><strong>Total Members:</strong> <asp:Label ID="lblTotalMembers" runat="server" Text="0"></asp:Label></p>
      <p><strong>Total Sessions:</strong> <asp:Label ID="lblTotSessions" runat="server" Text="0"></asp:Label></p>
      <p><strong>Total Missed Risk assessments:</strong> <asp:Label ID="lblMissedRisk" runat="server" Text="0"></asp:Label></p>
      <p><strong>Total Missed Registers:</strong> <asp:Label ID="lblMissedReg" runat="server" Text="0"></asp:Label></p>


     <p><asp:Label  CssClass="label" ID="Label2" runat="server" Text="Type"></asp:Label><br />  <asp:DropDownList CssClass="combobox" ID="cboType" runat="server"></asp:DropDownList></p>

       <asp:Literal ID="litError" Visible="false" runat="server"><p class="error">The system failed to create the user. Please try again. If this problem persists please contact Footsqueek support.</p></asp:Literal>


    <table class="nicetable">

        <tr>
            <th>Student Number</th>
            <th>Student Name</th>
            <th>Last Logged In</th>
            <th></th>
        </tr>

         <asp:Repeater ID="rptUsers" runat="server">

        <ItemTemplate>

            <tr>
                <td><asp:Label ID="lblUserName" runat="server" Text="lblUserName"></asp:Label></td>
                <td><asp:Label ID="lblFullName" runat="server" Text="lblFullName"></asp:Label></td>
                <td><asp:Label ID="lblLastLogin" runat="server" Text="lblLastLogin"></asp:Label></td>
                <td><asp:Button id="btnEdit"
           Text="Remove Access"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnEdit_Clickbutton" 
           runat="server"/><br />

                    <asp:Button id="btnDisable"
                        CssClass="standardbutton"
           Text="Disable"
           CommandName="Disable"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnDisable_Clickbutton" 
           runat="server"/><br />

                     <asp:Button id="btnReset"
                         CssClass="standardbutton"
           Text="Reset Password"
           CommandName="Reset"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnReset_Clickbutton" 
           runat="server"/>


                </td>
            </tr>

        </ItemTemplate>


    </asp:Repeater>

    </table>

    <asp:TextBox ID="txtEmail" placeholder="Enter E-mail Address" CssClass="txtbox" runat="server"></asp:TextBox> <asp:Button CssClass="submitbutton" ID="btnNew" runat="server" Text="Create New User" OnClick="btnNew_Click" />

     <p><asp:Button  CssClass="submitbutton" ID="Button1" runat="server" Text="Save Changes" OnClick="Button1_Click" /> 
                         



</asp:Content>
