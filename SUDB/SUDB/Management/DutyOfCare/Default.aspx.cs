﻿using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.DutyOfCare
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
         if(!IsPostBack)
         {
             txtDate.Text = DateTime.Now.ToShortDateString();

             Common tools = new Common();

             lblCommitteeLoggedIn.Text = tools.totalUsersLoggedInToday().ToString();
         }

         try
         {
             bindActivities();
         }
         catch
         { }
        }

        protected void bindActivities()
        {
            IFormatProvider culture = new System.Globalization.CultureInfo("en-US", true);
           

            
            

            ActivityLogClass newActivity = new ActivityLogClass();
            var activities = newActivity.returnDOCActivities(DateTime.Parse(txtDate.Text));

            if(activities.Count()==0)
            {
                litNone.Visible = true;
            }
            else
            {
                litNone.Visible = false;
            }

            rptActivityLog.DataSource = activities;
            rptActivityLog.DataBind();

            int i = 0;

            foreach (tblActivityLog activity in activities)
            {
                MembershipUser userdetails = Membership.GetUser((Guid)activity.UserGuid);

                ((Label)rptActivityLog.Items[i].FindControl("lblUser")).Text = userdetails.UserName;
                ((Label)rptActivityLog.Items[i].FindControl("lblActivityType")).Text = activity.ActivityName;
                ((Label)rptActivityLog.Items[i].FindControl("lblDate")).Text = ((DateTime)activity.DateTime).ToString("dd MMM yyyy HH:mm", culture);
                ((Label)rptActivityLog.Items[i].FindControl("lblActivity")).Text = activity.ActivityDetails;

                i++;

            }
        }
    }
}