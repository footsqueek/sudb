﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="FullTransactionDetail.aspx.cs" Inherits="SUDB.Management.DutyOfCare.FullTransactionDetail" %>
<%@ Register src="../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script>
         $(document).ready(function () {
             $("#tablesorted").tablesorter();

             $('#txtDate').datetimepicker({
                 timepicker: false,
                 format: 'd M Y',
                 lang: 'en',
                 maxDate: '0'

             });

             $('#txtEnd').datetimepicker({
                 timepicker: false,
                 format: 'd M Y',
                 lang: 'en',
                 maxDate: '0'

             });

         }
 );

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <h1>Transactions</h1>
    <p>The transactions processed through the sports and societies shop are shown below.</p>

       <p><strong>Please enter Start date: </strong> <asp:TextBox ID="txtDate" runat="server"></asp:TextBox> </p>
      <p><strong>Please enter End date: </strong> <asp:TextBox ID="txtEnd" runat="server"></asp:TextBox> <asp:Button ID="Button1" runat="server" Text="Update" /> </p>

    <table id="tablesorted" class="nicetable">
        <thead>
        <tr>

            <th>Order No.</th>
            <th>Student Number</th>
            <th>Name</th>
            <th>Date</th>
            <th>Purchase Of</th>
            <th>Nominal Code</th>
            <th>Gross</th>
            <th>Status</th>

        </tr>
</thead>
        <tbody>
    <asp:Repeater ID="rptTransactions" runat="server">

        <ItemTemplate>
            <tr>
           <td> <asp:Label ID="lblOrderNumber" runat="server" Text="0"></asp:Label></td>
           <td> <asp:Label ID="lblStudentNumber" runat="server" Text="0"></asp:Label></td>
           <td> <asp:Label ID="lblStudentName" runat="server" Text="0"></asp:Label></td>
            <td> <asp:Label ID="lblTransactionDate" runat="server" Text="0"></asp:Label></td>
            <td> <asp:Label ID="lblPurchaseOf" runat="server" Text="0"></asp:Label></td>
            <td> <asp:Label ID="lblNominalCode" runat="server" Text="0"></asp:Label></td>
            <td> <asp:Label ID="lblTransactionAmount" runat="server" Text="0"></asp:Label></td>
            <td> <asp:Label ID="lblTransactionStatus" runat="server" Text="0"></asp:Label></td>
                </tr>
        </ItemTemplate>


    </asp:Repeater>
            </tbody>
    </table>
    <br /><br />
    <p><strong>Total Approved: </strong><asp:Label ID="lblTotalApproved" runat="server" Text="Label"></asp:Label></p>
     <p><strong>Total Paid By Cash: </strong><asp:Label ID="lblCash" runat="server" Text="Label"></asp:Label></p>
    <p><strong>Total Paid By Aspire: </strong><asp:Label ID="lblAspire" runat="server" Text="Label"></asp:Label></p>
     <p><strong>Total Paid By WorldPay: </strong><asp:Label ID="lblWorldPay" runat="server" Text="Label"></asp:Label></p>
    <asp:Button ID="btnExport" CssClass="submitbutton" runat="server" Text="Export" OnClick="btnExport_Click" />

    
</asp:Content>