﻿using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.DutyOfCare
{
    public partial class SportsSocieties : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindSportsSocs();
            }
        }

        protected void bindSportsSocs()
        {
            Common tools = new Common();

            var sportssocs = tools.ReturnSportsSocs();

            if(Request.QueryString["archive"]=="true")
            {
                sportssocs = tools.ReturnArchivedSportsSocs();
                btnArchive.Text = "View Active";
            }

            rptSportsSocs.DataSource = sportssocs;
            rptSportsSocs.DataBind();
            int i = 0;

            foreach(tblSportsSociety sport in sportssocs)
            {
                ((Label)rptSportsSocs.Items[i].FindControl("lblName")).Text = sport.Name;
                ((Label)rptSportsSocs.Items[i].FindControl("lblType")).Text = tools.returnDOCType((int)sport.Type).DOCType;
                ((Label)rptSportsSocs.Items[i].FindControl("lblPrice")).Text = "&pound;" + Math.Round(tools.getPrice(sport.Id),2).ToString();
                ((Label)rptSportsSocs.Items[i].FindControl("lblTotMembers")).Text = tools.getTotalMembers(sport.Id).ToString();
                ((Label)rptSportsSocs.Items[i].FindControl("lblTotSessions")).Text = tools.getTotalSessions(sport.Id).ToString();
                ((Button)rptSportsSocs.Items[i].FindControl("btnView")).CommandArgument = sport.Id.ToString();
                ((Button)rptSportsSocs.Items[i].FindControl("btnEdit")).CommandArgument = sport.Id.ToString();
                ((Button)rptSportsSocs.Items[i].FindControl("btnLock")).CommandArgument = sport.Id.ToString();
                ((Button)rptSportsSocs.Items[i].FindControl("btnArchive")).CommandArgument = sport.Id.ToString();

                //((Label)rptSportsSocs.Items[i].FindControl("lblMissedChecklist")).Text = tools.getTotalMissedRiskAssessments(sport.Id).ToString();
                //((Label)rptSportsSocs.Items[i].FindControl("lblMissedReg")).Text = tools.getTotalMissedReg(sport.Id).ToString();
               

                if(sport.Lock==1)
                {
                    ((Button)rptSportsSocs.Items[i].FindControl("btnLock")).Text = "Unlock";
                    ((Button)rptSportsSocs.Items[i].FindControl("btnView")).Visible = false;
                }

                if (sport.Archived == 1)
                {
                    ((Button)rptSportsSocs.Items[i].FindControl("btnArchive")).Text = "Restore";
                    ((Button)rptSportsSocs.Items[i].FindControl("btnView")).Visible = false;
              
                }
             
                i++;
            }

        }

        public void btnView_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);

            Common tools = new Common();
            tools.setActiveSportSociety(int.Parse(btnView.CommandArgument));

            Response.Redirect("/Committee/Membership.aspx");
        }

        public void btnEdit_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnEdit = ((Button)sender);

            Common tools = new Common();
            tools.setActiveSportSociety(int.Parse(btnEdit.CommandArgument));

            Response.Redirect("SocietyControls/Settings.aspx");
        }

        public void btnLock_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnLock = ((Button)sender);

            Common tools = new Common();
            tools.lockSportSoc(int.Parse(btnLock.CommandArgument));

            Response.Redirect("SportsSocieties.aspx");
        }

        public void btnArchive_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnLock = ((Button)sender);

            if (btnLock.Text == "Click to Confirm")
            {

                Common tools = new Common();
                tools.archiveSportSoc(int.Parse(btnLock.CommandArgument));

                Response.Redirect("SportsSocieties.aspx");
            }
            else
            {
                btnLock.Text = "Click to Confirm";
            }
        }

        protected void btnArchive_Click(object sender, EventArgs e)
        {
            if (btnArchive.Text == "View Archived")
            {
                Response.Redirect("SportsSocieties.aspx?archive=true");
            }
            else
            {
                Response.Redirect("SportsSocieties.aspx");
            }
        }

    }
}