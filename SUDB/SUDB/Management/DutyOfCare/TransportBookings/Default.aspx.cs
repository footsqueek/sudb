﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.DutyOfCare.TransportBookings
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();

            var requests = tools.returnActiveTransportRequests();

            if (requests.Count() == 0)
            {
                litNone.Visible = true;
            }
            else
            {
                litNone.Visible = false;
            }

            rptRoomBookings.DataSource = requests;
            rptRoomBookings.DataBind();

            int i = 0;

            foreach (var request in requests)
            {

                try
                {
                    //Lookup sport soc
                    var sport = tools.returnSportSociety((int)request.SportSociety);

                    var sessions = tools.returnSessionsByTransport(request.Id).FirstOrDefault();

                    ((Label)rptRoomBookings.Items[i].FindControl("lblSportSociety")).Text = sport.Name;
                    ((Label)rptRoomBookings.Items[i].FindControl("lblRequestHeadline")).Text = request.Headline;
                    ((Label)rptRoomBookings.Items[i].FindControl("lblRequestOpened")).Text = ((DateTime)sessions.StartDateTime).ToString();
                    ((Button)rptRoomBookings.Items[i].FindControl("btnView")).CommandArgument = request.Id.ToString();
                   
                }
                catch
                {
                    rptRoomBookings.Items[i].Visible = false;
                }
                i++;
            }
        }

        protected void btnRoom_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);

            Common tools = new Common();
            var transportrequest = tools.returnTransportRequestAdmin(int.Parse(btnView.CommandArgument));
            tools.setActiveSportSociety((int)transportrequest.SportSociety);

            Response.Redirect("/Committee/Sessions/TransportBooking/Default.aspx?transportId=" + btnView.CommandArgument);
        }
    }
}