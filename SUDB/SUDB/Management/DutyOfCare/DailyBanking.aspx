﻿<%@ Page Title="Daily Banking" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="DailyBanking.aspx.cs" Inherits="SUDatabase.Management.DutyOfCare.DailyBanking" %>
<%@ Register src="../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script>
    $(document).ready(

  /* This is the function that will get executed after the DOM is fully loaded */
  function () {

     $('#txtDate').datetimepicker({
     timepicker:false,
          format: 'd M Y',
          lang: 'en',
          maxDate: '0'
        
     });

     $('#txtEnd').datetimepicker({
         timepicker: false,
         format: 'd M Y',
         lang: 'en',
         maxDate: '0'

     });

    
  }

);

        </script>

     <script>
         $(document).ready(function () {
             $("#tablesorted").tablesorter();
         }
 );

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Daily Banking</h1>

       <p>Each product line is displayed below itemising what money should be attributed to each account each day.</p>

       <p><strong>Please enter Start date: </strong> <asp:TextBox ID="txtDate" runat="server"></asp:TextBox></p>
      <p><strong>Please enter End date: </strong> <asp:TextBox ID="txtEnd" runat="server"></asp:TextBox> <asp:Button CssClass="blackbutton" ID="Button1" runat="server" Text="Update" /> </p>

       <table id="tablesorted" class="nicetable">
           <thead>
        <tr>

            <th>Sport / Society</th>
            <th>Type</th>
            <th>Nominal Code</th>
            <th>Net</th>
            <th>VAT</th>
            <th>Gross</th>

        </tr>
           </thead>
           <tbody>
    <asp:Repeater ID="rptTransactions" runat="server">

        <ItemTemplate>
            <tr>
           <td> <asp:Label ID="lblSportSociety" runat="server" Text="0"></asp:Label></td>
           <td> <asp:Label ID="lblType" runat="server" Text="0"></asp:Label></td>
           <td> <asp:Label ID="lblNominalCode" runat="server" Text="0"></asp:Label></td>
            <td> <asp:Label ID="lblNet" runat="server" Text="0"></asp:Label></td>
                    <td> <asp:Label ID="lblVAT" runat="server" Text="0"></asp:Label></td>
            <td> <asp:Label ID="lblGross" runat="server" Text="0"></asp:Label></td>
                </tr>
        </ItemTemplate>


    </asp:Repeater>

           <tr>

               <th colspan="3"></th>
                 <th><asp:Label ID="lblTotalNet" runat="server" Text="Label"></asp:Label></th>
 
               <th><asp:Label ID="lblTotalVAT" runat="server" Text="Label"></asp:Label></th>

               <th><asp:Label ID="lblTotalGross" runat="server" Text="Label"></asp:Label></th>

           </tr>

 </tbody>
    </table>

    <asp:Button ID="btnExport" CssClass="submitbutton" runat="server" Text="Export To CSV" OnClick="btnExport_Click" />
   

</asp:Content>
