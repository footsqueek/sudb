﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.DutyOfCare.Sessions
{
    public partial class ManageFixtures : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Populate Sports
            Common tools = new Common();

            
           


            if(IsPostBack)
            {
                if (cboTeam.Enabled == false)
                {
                    Button1.Visible = true;
                    btnCreate.Visible = true;

                    txtPlaying.Enabled = true;
                    cboHomeAway.Enabled = true;
                    txtDate.Enabled = true;
                    txtDepart.Enabled = true;
                    txtVenue.Enabled = true;
                    txtTransport.Enabled = true;
                    txtDepart.Enabled = true;
                    txtNoTeas.Enabled = true;
                    txtTeamTeasTime.Enabled = true;

                    cboTeam.Enabled = true;
                    cboClub.Enabled = false;
                    var teams = tools.returnTeamsFor(int.Parse(cboClub.SelectedValue));
                    cboTeam.Items.Add(new ListItem("", "0"));
                    foreach (var team in teams)
                    {
                        cboTeam.Items.Add(new ListItem(team.TeamName, team.Id.ToString()));
                    }
                }
              

            }
            else
            {

                Button1.Visible = false;
                btnCreate.Visible = false;

                txtPlaying.Enabled = false;
                cboHomeAway.Enabled = false;
                txtDate.Enabled = false;
                txtDepart.Enabled = false;
                txtVenue.Enabled = false;
                txtTransport.Enabled = false;
                txtDepart.Enabled = false;
                txtNoTeas.Enabled = false;
                txtTeamTeasTime.Enabled = false;


                var thingstobuy = tools.returnThingsToBuy(1, tools.getSU().Id);

                if (tools.getSU().Id==3)
                {
                    thingstobuy = tools.returnThingsToBuy(6, tools.getSU().Id);
                }

                cboClub.Items.Add(new ListItem("", "0"));

                foreach (var thing in thingstobuy)
                {
                    cboClub.Items.Add(new ListItem(thing.Name, thing.Id.ToString()));
                }

                if (tools.getSU().Id == 1)
                {
                    //Also Add Wazza
                    var thingstobuywazza = tools.returnThingsToBuy(3, 1);
                    foreach (var thing in thingstobuywazza)
                    {
                        cboClub.Items.Add(new ListItem(thing.Name, thing.Id.ToString()));
                    }
                }


                DateTime today = DateTime.Today;
                // The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
                int daysUntilWednesday = ((int)DayOfWeek.Wednesday - (int)today.DayOfWeek + 7) % 7;
                DateTime nextWednesday = today.AddDays(daysUntilWednesday);

                txtDate.Text = nextWednesday.ToString("dd/MM/yyyy hh:mm");
                txtTeamTeasTime.Text = nextWednesday.ToString("dd/MM/yyyy hh:mm");
                txtDepart.Text = nextWednesday.ToString("dd/MM/yyyy hh:mm");
            }

        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Common tools = new Common();

            tools.setActiveSportSociety(int.Parse(cboClub.SelectedValue));


            int tripid = 0;
            if (cboHomeAway.SelectedValue == "Away")
            {
             
                tripid = -1;
            

          

         
            DateTime depart = DateTime.Now;
            DateTime success;
            
            if(DateTime.TryParse(txtDepart.Text,out success))
            {
                depart = success;
            }

            DateTime teastime = DateTime.Now;
            DateTime teassuccess;

            if (DateTime.TryParse(txtDepart.Text, out teassuccess))
            {
                teastime = teassuccess;
            }


            tools.createSession(cboCup.SelectedValue,tripid, int.Parse(cboTeam.SelectedValue), int.Parse(cboClub.SelectedValue), txtPlaying.Text.Trim(), txtVenue.Text.Trim(), 0, 0, DateTime.Parse(txtDate.Text), DateTime.Parse(txtDate.Text).AddHours(1), 1, txtPostCode.Text.Trim(), txtTransport.Text.Trim(), depart, int.Parse(txtNoTeas.Text), teastime, 1);
            Response.Redirect("ManageFixtures.aspx");

            }
            else
            {
                DateTime depart = DateTime.Now;
                DateTime success;

                if (DateTime.TryParse(txtDepart.Text, out success))
                {
                    depart = success;
                }

                DateTime teastime = DateTime.Now;
                DateTime teassuccess;

                if (DateTime.TryParse(txtDepart.Text, out teassuccess))
                {
                    teastime = teassuccess;
                }


                tools.createSession(cboCup.SelectedValue,tripid, int.Parse(cboTeam.SelectedValue), int.Parse(cboClub.SelectedValue), txtPlaying.Text.Trim(), txtVenue.Text.Trim(), 0, 0, DateTime.Parse(txtDate.Text), DateTime.Parse(txtDate.Text).AddHours(1), 1, txtPostCode.Text.Trim(), txtTransport.Text.Trim(), depart, int.Parse(txtNoTeas.Text), teastime, 1);
                Response.Redirect("ManageFixtures.aspx");

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Common tools = new Common();

            tools.setActiveSportSociety(int.Parse(cboClub.SelectedValue));

            int tripid = 0;
            if (cboHomeAway.SelectedValue == "Away")
            {
               
                tripid = -1;
           

            DateTime depart = DateTime.Now;
            DateTime success;

            if (DateTime.TryParse(txtDepart.Text, out success))
            {
                depart = success;
            }

            DateTime teastime = DateTime.Now;
            DateTime teassuccess;

            if (DateTime.TryParse(txtDepart.Text, out teassuccess))
            {
                teastime = teassuccess;
            }
                Console.Write(tripid + "The trip ID");

            tools.createSession(cboCup.SelectedValue, tripid, int.Parse(cboTeam.SelectedValue), int.Parse(cboClub.SelectedValue), txtPlaying.Text.Trim(), txtVenue.Text.Trim(), 0, 0, DateTime.Parse(txtDate.Text), DateTime.Parse(txtDate.Text).AddHours(1), 1, txtPostCode.Text.Trim(), txtTransport.Text.Trim(), depart, int.Parse(txtNoTeas.Text), teastime, 1);
           
            Response.Redirect("Fixtures.aspx");
            }
            else
            {
                DateTime depart = DateTime.Now;
                DateTime success;

                if (DateTime.TryParse(txtDepart.Text, out success))
                {
                    depart = success;
                }

                DateTime teastime = DateTime.Now;
                DateTime teassuccess;

                if (DateTime.TryParse(txtDepart.Text, out teassuccess))
                {
                    teastime = teassuccess;
                }


                tools.createSession(cboCup.SelectedValue,tripid, int.Parse(cboTeam.SelectedValue), int.Parse(cboClub.SelectedValue), txtPlaying.Text.Trim(), txtVenue.Text.Trim(), 0, 0, DateTime.Parse(txtDate.Text), DateTime.Parse(txtDate.Text).AddHours(1), 1, txtPostCode.Text.Trim(), txtTransport.Text.Trim(), depart, int.Parse(txtNoTeas.Text), teastime, 1);

                Response.Redirect("Fixtures.aspx");
            }
        }
    }
}