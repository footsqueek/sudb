﻿<%@ Page Title="Manage Fixtures" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ManageFixtures.aspx.cs" Inherits="SUDatabase.Management.DutyOfCare.Sessions.ManageFixtures" %>
<%@ Register src="../../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
<script>

    $(document).ready(

  /* This is the function that will get executed after the DOM is fully loaded */
  function () {

     $('#txtDate').datetimepicker({
          format: 'd M Y H:i',
          lang: 'en',
          minDate: '0'
        
     });
  }

);


    
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

   
    <h1>Add Fixtures</h1>


    <p>Please enter the details of the fixture below:</p>

          <p><asp:Label CssClass="label" ID="Label5" runat="server" Text="Select Club"></asp:Label><br />
           <asp:DropDownList ID="cboClub" AutoPostBack="true" CssClass="combobox" runat="server">    
           </asp:DropDownList></p>


                  <p><asp:Label CssClass="label" ID="Label1" runat="server" Text="Select Team"></asp:Label><br />
           <asp:DropDownList ID="cboTeam" Enabled="false" CssClass="combobox" runat="server">
              
           </asp:DropDownList></p>


        <p><asp:Label CssClass="label" ID="Label6" runat="server" Text="Playing"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtPlaying" Text="" Enabled="true" runat="server"></asp:TextBox><br /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtPlaying" CssClass="error" runat="server" ErrorMessage="You must enter a session name"></asp:RequiredFieldValidator></p>
 
        <p><asp:Label CssClass="label" ID="Label2" runat="server" Text="Home or Away"></asp:Label><br />
           <asp:DropDownList ID="cboHomeAway" CssClass="combobox" runat="server">
              <asp:ListItem Value="Home" Text="Home"></asp:ListItem>
              <asp:ListItem Value="Away" Text="Away"></asp:ListItem>
           </asp:DropDownList></p>

     <p><asp:Label CssClass="label" ID="Label12" runat="server" Text="Cup or League"></asp:Label><br />
           <asp:DropDownList ID="cboCup" CssClass="combobox" runat="server">
              <asp:ListItem Value="" Text=""></asp:ListItem>
                 <asp:ListItem Value="C" Text="Cup"></asp:ListItem>
              <asp:ListItem Value="L" Text="League"></asp:ListItem>
           </asp:DropDownList></p>

         <p><asp:Label CssClass="label" ID="Label3" runat="server" Text="Date / Time"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtDate" Text="" Enabled="true" runat="server"></asp:TextBox><br /><asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtDate" CssClass="error" runat="server" ErrorMessage="You must enter the team we are playing"></asp:RequiredFieldValidator></p>
 
         <p><asp:Label CssClass="label" ID="Label4" runat="server" Text="Venue"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtVenue" Text="" Enabled="true" runat="server"></asp:TextBox><br /><asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtVenue" CssClass="error" runat="server" ErrorMessage="You must enter a venue"></asp:RequiredFieldValidator></p>
 
         <p><asp:Label CssClass="label" ID="Label7" runat="server" Text="PostCode"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtPostCode" Text="" Enabled="true" runat="server"></asp:TextBox><br /><asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtPostCode" CssClass="error" runat="server" ErrorMessage="You must enter a postcode"></asp:RequiredFieldValidator></p>
 
         <p><asp:Label CssClass="label" ID="Label8" runat="server" Text="Transport"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtTransport" Text="" Enabled="true" runat="server"></asp:TextBox></p>
 
         <p><asp:Label CssClass="label" ID="Label9" runat="server" Text="Departure Date Time"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtDepart" Text="" Enabled="true" runat="server"></asp:TextBox></p>
 
         <p><asp:Label CssClass="label" ID="Label10" runat="server" Text="Number of Team Teas"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtNoTeas" Text="" Enabled="true" runat="server">0</asp:TextBox><br /><asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="txtNoTeas" CssClass="error" runat="server" ErrorMessage="You must enter a number of team teas"></asp:RequiredFieldValidator></p>
 
 
         <p><asp:Label CssClass="label" ID="Label11" runat="server" Text="Team Teas Date / Time"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtTeamTeasTime" Text="" Enabled="true" runat="server"></asp:TextBox></p>
 
          <asp:Button ID="btnCreate" CssClass="submitbutton" runat="server" Text="Create and Add More" OnClick="btnCreate_Click" />
      <asp:Button ID="Button1" CssClass="submitbutton" runat="server" Text="Create and Finish" OnClick="Button1_Click" />

</asp:Content>
