﻿using Microsoft.VisualBasic.FileIO;
using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.DutyOfCare.Sessions
{
    public partial class Fixtures : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                txtFixturesDate.Text = DateTime.Now.ToString("dd MMM yyyy");
                bindFixtures();
            
            }
            

        }

        protected void bindFixtures()
        {
            try
            {
                DateTime fixdate = DateTime.Parse(Request.QueryString["date"]);

                if (fixdate != null)
                {
                    txtFixturesDate.Text = fixdate.ToString("dd MMM yyyy");
                    lblFixturesDate.Text = fixdate.ToString("dd MMM yyyy");
                }
            }
            catch
            {

            }


            Common tools = new Common();

            var fixtures = tools.returnFixtures(DateTime.Parse(txtFixturesDate.Text));

            rptFixtures.DataSource = fixtures;
            rptFixtures.DataBind();

            int i = 0;

            foreach(var fixture in fixtures)
            {

                var sportsoc = tools.returnSportSociety((int)fixture.SportSocietyId);
              
                if (fixture.teamId > 0)
                {
                    try
                    {
                        var team = tools.returnTeam((int)fixture.teamId);
                        ((Label)rptFixtures.Items[i].FindControl("lblActualTeam")).Text = team.TeamName;
                    }
                    catch
                    {

                    }

                }
                else
                {
                  


                }

                ((Label)rptFixtures.Items[i].FindControl("lblTeam")).Text = sportsoc.Name;
               
                if(fixture.TripRegId>0)
                {
                    ((Label)rptFixtures.Items[i].FindControl("lblHomeAway")).Text = "A";
                    ((Label)rptFixtures.Items[i].FindControl("lblDepart")).Text = ((DateTime)fixture.Depart).ToString("HH:mm");
                    ((Label)rptFixtures.Items[i].FindControl("lblMeet")).Text = ((DateTime)fixture.Depart.Value.AddMinutes(-15)).ToString("hh:mm");
             
                }
                else
                {
                    ((Label)rptFixtures.Items[i].FindControl("lblHomeAway")).Text = "H";
                    ((Label)rptFixtures.Items[i].FindControl("lblDepart")).Text ="";
                    ((Label)rptFixtures.Items[i].FindControl("lblMeet")).Text = "";
             
                }

                ((Label)rptFixtures.Items[i].FindControl("lblv")).Text = fixture.SessionName;
                ((Label)rptFixtures.Items[i].FindControl("lblVenue")).Text = fixture.Location;
                ((Label)rptFixtures.Items[i].FindControl("lblPostCode")).Text = fixture.PostCode;
                ((Label)rptFixtures.Items[i].FindControl("lblTime")).Text = ((DateTime)fixture.StartDateTime).ToString("HH:mm");
                ((Label)rptFixtures.Items[i].FindControl("lblTeasTime")).Text = ((DateTime)fixture.TeasTime).ToString("HH:mm");
                ((Label)rptFixtures.Items[i].FindControl("lblTeas")).Text = fixture.NoTeas.ToString();
                ((Label)rptFixtures.Items[i].FindControl("lblTransport")).Text = fixture.Transport;
                ((Label)rptFixtures.Items[i].FindControl("lblOurScore")).Text = fixture.ourScore.ToString();
                ((Label)rptFixtures.Items[i].FindControl("lblTheirScore")).Text = fixture.theirScore.ToString();              
                ((Button)rptFixtures.Items[i].FindControl("btnEdit")).CommandArgument = fixture.Id.ToString();
                ((Button)rptFixtures.Items[i].FindControl("btnCancel")).CommandArgument = fixture.Id.ToString();

                if (fixture.CupLeague != null)
                {
                    ((Label)rptFixtures.Items[i].FindControl("lblCup")).Text = fixture.CupLeague;

                }

                if (fixture.NoTeas==0)
                {
                    ((Label)rptFixtures.Items[i].FindControl("lblTeasTime")).Text = "";
                    ((Label)rptFixtures.Items[i].FindControl("lblTeas")).Text = "";
               
                }

                
                i++;
            }

        }

        protected void btnCreateFixture_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Committee/Sessions/CreateSession.aspx");
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);
            //Cancel Code

            if (btnView.Text == "Click to Confirm")
            {
                Common tools = new Common();
                tools.deleteSession(int.Parse(btnView.CommandArgument));
                bindFixtures();
            }
            else
            {
                btnView.Text = "Click to Confirm";
            }


        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);
            Common tools = new Common();
            var session = tools.returnSession(int.Parse(btnView.CommandArgument));
            tools.setActiveSportSociety((int)session.SportSocietyId);
            Response.Redirect("/Committee/Sessions/CreateSession.aspx?sessionId=" + btnView.CommandArgument);
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
           /* Common tools = new Common();
            StreamReader csvreader = new StreamReader(FileUpload1.FileContent);

            TextFieldParser parser = new TextFieldParser(csvreader);
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");
            while (!parser.EndOfData)
            {
                //Process row

                int i = 0;

                int tripId=0; 
                int TeamId=0;
                int SportSocietyId=0; 
                String name = "";
                String Location=""; 
                int LinkedRoom=0; 
                DateTime StartDateTime = DateTime.Now; 
                DateTime EndDateTime =DateTime.Now;
                int noWeeks=1; 
                string postcode = ""; 
                string transport=""; 
                DateTime depart = DateTime.Now; 
                int numberteas=0;
                DateTime teasTime= DateTime.Now;
                int BUCS=1;

                string[] fields = parser.ReadFields();
                foreach (string field in fields)
                {
                    i++;
                    
                    switch(i)
                    {
                        case 1:
                            name = field;
                            break;
                        case 2:
                            Location=field;
                            break;
                        case 3:
                            LinkedRoom=int.Parse(field);
                            break;
                        case 4:
                            StartDateTime = DateTime.Parse(field);
                            break;
                        case 5:
                            EndDateTime = DateTime.Parse(field).AddMinutes(90);
                            break;
                        case 6:
                            SportSocietyId = int.Parse(field);
                            break;
                   
                        case 7:
                            break;
                        case 8:
                            break;
                        case 9:
                            
                            break;
                        case 10:
                            break;
                        case 11:
                            tripId = int.Parse(field);
                            break;
                        case 12:
                            break;
                        case 13:
                           TeamId = int.Parse(field);
                            break;
                        case 14:
                            postcode = field;
                            break;
                        case 15:
                            transport = field;
                            break;
                        case 16:
                            depart =DateTime.Parse(field);
                            break;
                        case 17:
                            numberteas = int.Parse(field);
                            break;
                        case 18:
                            teasTime =DateTime.Parse(field);
                            break;
                        case 19:
                            
                            break;
                        case 20:
                            BUCS = int.Parse(field);
                            break;
                        case 21:
                            break;
                        case 22:
                            break;
                    }
                    
                    if(i<22)
                    {
                        i++;
                    }
                    else
                    {

                        tools.createSession(tripId,TeamId,SportSocietyId,name,Location,LinkedRoom,0,StartDateTime,EndDateTime,noWeeks,postcode,transport,depart,numberteas,teasTime,BUCS);

                tripId=0; 
                TeamId=0;
                SportSocietyId = 0;
                name = "";
                Location=""; 
                LinkedRoom=0; 
                StartDateTime = DateTime.Now; 
                EndDateTime =DateTime.Now;
                noWeeks=1; 
                postcode = ""; 
                transport=""; 
                depart = DateTime.Now; 
                numberteas=0;
                teasTime= DateTime.Now;
                BUCS=1;

                        i=0;
                    }
                    //TODO: Process field
                }
            }
            parser.Close();
            * */


            if (FileUpload1.HasFile)
            {
                DataClasses1DataContext db = new DataClasses1DataContext();

             
                StreamReader reader = new StreamReader(FileUpload1.FileContent);
                while (!reader.EndOfStream)
                {
                    try
                    {
                        string textLine = reader.ReadLine();

                        string[] splitstring = textLine.Split(new string[] { "," }, StringSplitOptions.None);

                        Common tools = new Common();
                        int tripId = 0;
                        int SportSocietyId = int.Parse(splitstring[5]);
                        if(splitstring[18]=="A")
                        {
                            var trip = tools.createTrip(SportSocietyId);
                            tripId = trip.tripId;
                        }
                       

                      
                        int LinkedRoom=0;
                        DateTime EndDateTime = DateTime.Now;
                        DateTime depart = DateTime.Now;
                         DateTime teasTime = DateTime.Now;
                        int numberteas =0;
                        try
                        {
                            tripId = int.Parse(splitstring[10]);
                        }
                        catch
                        {

                        }
                        int TeamId = int.Parse(splitstring[12]);
                        
                        String name = splitstring[0];
                        String Location = splitstring[1];
                        try
                        {
                            LinkedRoom = int.Parse(splitstring[2]);
                        }
                        catch
                        {

                        }
                        DateTime StartDateTime = DateTime.Parse(splitstring[3]);
                        try
                        {
                            EndDateTime = DateTime.Parse(splitstring[4]);
                        }
                        catch
                        {
                            EndDateTime = DateTime.Parse(splitstring[3]);
                        }
                        int noWeeks = 1;
                        string postcode = splitstring[13];
                        string transport = splitstring[14];
                        try
                        {
                        depart = DateTime.Parse(splitstring[15]);
                        }
                        catch
                        {

                        }
                        try
                        {
                        numberteas = int.Parse(splitstring[16]);
                        }
                        catch
                        {


                        }
                        try
                        {

                        
                        teasTime = DateTime.Parse(splitstring[17]);
                        }
                        catch
                        {

                        }
                        int BUCS = 1;

                        tools.createSession("",tripId, TeamId, SportSocietyId, name, Location, LinkedRoom, 0, StartDateTime, EndDateTime, noWeeks, postcode, transport, depart, numberteas, teasTime, BUCS);



                    }
                    catch
                    {

                    }
                }

            }

            Response.Redirect("Fixtures.aspx");
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            Response.Redirect("Fixtures.aspx?date=" + (DateTime.Parse(txtFixturesDate.Text)).ToString());
        }
    }
}