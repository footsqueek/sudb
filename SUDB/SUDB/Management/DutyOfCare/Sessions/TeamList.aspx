﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="TeamList.aspx.cs" Inherits="SUDatabase.Management.DutyOfCare.Sessions.TeamList" %>
<%@ Register src="../../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script>
         $(document).ready(function () {
             $("#tablesorted").tablesorter();
             $("#tablesorted1").tablesorter();
         }
 );

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Team List</h1>

    <h2>Sports and Societies</h2>
    <p>Listed below are all of the Sports and Societies and each of their system ID's</p>

    <table id="tablesorted" class="nicetable">
        <thead>
        <tr>
            <th>Sport Soc Id</th>
            <th>Sport Soc Name</th>
        </tr>
            </thead>
        <tbody>
    <asp:Repeater ID="rptsocs" runat="server">

        <ItemTemplate>

           <tr>

               <td>  <asp:Label ID="lblID" runat="server" Text="Label"></asp:Label></td>
                <td>  <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label></td>

           </tr>

        </ItemTemplate>

    </asp:Repeater>
            </tbody>
    </table>


    <h2>Team List</h2>
    <p>Listed below are all of the individual teams and groups that have been created by Sports and Societies.</p>

        <table class="nicetable">
        <tr>
            <th>Sport Soc Id</th>
            <th>Sport Soc Name</th>
             <th>Team Id</th>
             <th>Team Name</th>
        </tr>
    <asp:Repeater ID="rptTeams" runat="server">

        <ItemTemplate>

           <tr>

               <td>  <asp:Label ID="lblID" runat="server" Text="Label"></asp:Label></td>
                <td>  <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label></td>
                 <td>  <asp:Label ID="lblTeamId" runat="server" Text="Label"></asp:Label></td>
               <td>  <asp:Label ID="lblTeamName" runat="server" Text="Label"></asp:Label></td>

           </tr>

        </ItemTemplate>

    </asp:Repeater>
    </table>

</asp:Content>
