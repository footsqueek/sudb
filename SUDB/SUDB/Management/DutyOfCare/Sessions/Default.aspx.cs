﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.DutyOfCare.RiskAssessment
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();
            var sessions = tools.returnSessionsNext14();

            int i = 0;

            rptSessionsNext14.DataSource = sessions;
            rptSessionsNext14.DataBind();

            foreach(var session in sessions)
            {
                var sport = tools.returnSportSociety((int)session.SportSocietyId);

                ((Label)rptSessionsNext14.Items[i].FindControl("lblSportSociety")).Text = sport.Name;
                ((Label)rptSessionsNext14.Items[i].FindControl("SessionName")).Text = session.SessionName;
                ((Label)rptSessionsNext14.Items[i].FindControl("Location")).Text = session.Location;
                ((Label)rptSessionsNext14.Items[i].FindControl("DateTime")).Text = session.StartDateTime.ToString();
                ((Button)rptSessionsNext14.Items[i].FindControl("btnRegister")).CommandArgument = session.Id.ToString();
                ((Button)rptSessionsNext14.Items[i].FindControl("btnRiskAssessment")).CommandArgument = session.Id.ToString();
                ((Button)rptSessionsNext14.Items[i].FindControl("btnRoom")).CommandArgument = session.linkedRoomBookingRequest.ToString();
                ((Button)rptSessionsNext14.Items[i].FindControl("btnTripReg")).CommandArgument = session.Id.ToString();

             

                if (session.linkedRoomBookingRequest == 0 || session.linkedRoomBookingRequest == null)
                {
                    ((Button)rptSessionsNext14.Items[i].FindControl("btnRoom")).Visible = false;


                }
                else
                {
                    try
                    {
                  
                        var room = tools.returnAllRoomRequest((int)session.linkedRoomBookingRequest);
                        if (room.RequestStatus == 1)
                        {
                            ((Button)rptSessionsNext14.Items[i].FindControl("btnRoom")).CssClass = "standardgreen";
                        }
                        else if (room.RequestStatus == 2)
                        {
                            ((Button)rptSessionsNext14.Items[i].FindControl("btnRoom")).CssClass = "standardorange";
                        
                        }
                 
                    }
                    catch
                    {
                        ((Button)rptSessionsNext14.Items[i].FindControl("btnRoom")).Visible = false;
                          }
                }

                if (session.TripRegId == 0 || session.TripRegId == null)
                {
                    ((Button)rptSessionsNext14.Items[i].FindControl("btnTripReg")).Visible = false;
                }
                else
                {
                    try
                    {
                        var trip = tools.returnTrip((int)session.TripRegId);
                        if (trip.ApprovalStatus == 2)
                        {
                            ((Button)rptSessionsNext14.Items[i].FindControl("btnTripReg")).CssClass = "standardgreen";
                        }
                        else if (trip.ApprovalStatus == 1)
                        {
                            ((Button)rptSessionsNext14.Items[i].FindControl("btnTripReg")).CssClass = "standardorange";
                        }
                    }
                    catch
                    {
                        ((Button)rptSessionsNext14.Items[i].FindControl("btnTripReg")).Visible = false;
                    }
                }

                if (session.RegisterComplete == 1)
                {
                    ((Button)rptSessionsNext14.Items[i].FindControl("btnRegister")).CssClass = "standardgreen";
                }
                else
                {
                    ((Button)rptSessionsNext14.Items[i].FindControl("btnRegister")).CssClass = "standardorange";
                }

                i++;
            }



            sessions = tools.returnSessionsLast14();

            i = 0;

            rptSessionsLast14.DataSource = sessions;
            rptSessionsLast14.DataBind();

            foreach (var session in sessions)
            {
                var sport = tools.returnSportSociety((int)session.SportSocietyId);

                try
                {
                    ((Label)rptSessionsLast14.Items[i].FindControl("lblSportSociety")).Text = sport.Name;
                }
                catch
                {

                }
                ((Label)rptSessionsLast14.Items[i].FindControl("SessionName")).Text = session.SessionName;
                ((Label)rptSessionsLast14.Items[i].FindControl("Location")).Text = session.Location;
                ((Label)rptSessionsLast14.Items[i].FindControl("DateTime")).Text = session.StartDateTime.ToString();
                ((Button)rptSessionsLast14.Items[i].FindControl("btnRegister")).CommandArgument = session.Id.ToString();
                ((Button)rptSessionsLast14.Items[i].FindControl("btnRiskAssessment")).CommandArgument = session.Id.ToString();
                ((Button)rptSessionsLast14.Items[i].FindControl("btnRoom")).CommandArgument = session.linkedRoomBookingRequest.ToString();
                ((Button)rptSessionsLast14.Items[i].FindControl("btnTripReg")).CommandArgument = session.Id.ToString();



                if (session.linkedRoomBookingRequest == 0 || session.linkedRoomBookingRequest == null)
                {
                    ((Button)rptSessionsLast14.Items[i].FindControl("btnRoom")).Visible = false;


                }
                else
                {
                    try
                    {
                        var room = tools.returnAllRoomRequest((int)session.linkedRoomBookingRequest);
                        if (room.RequestStatus == 1)
                        {
                            ((Button)rptSessionsLast14.Items[i].FindControl("btnRoom")).CssClass = "standardgreen";
                        }
                        else if (room.RequestStatus == 2)
                        {
                            ((Button)rptSessionsLast14.Items[i].FindControl("btnRoom")).CssClass = "standardorange";
                        }
                    }
                    catch
                    {
                        ((Button)rptSessionsLast14.Items[i].FindControl("btnRoom")).Visible = false;
                    }
                }

                if (session.TripRegId == 0 || session.TripRegId == null)
                {
                    ((Button)rptSessionsLast14.Items[i].FindControl("btnTripReg")).Visible = false;
                }
                else
                {
                    try
                    {
                        var trip = tools.returnTrip((int)session.TripRegId);
                        if (trip.ApprovalStatus == 2)
                        {
                            ((Button)rptSessionsLast14.Items[i].FindControl("btnTripReg")).CssClass = "standardgreen";
                        }
                        else if (trip.ApprovalStatus == 1)
                        {
                            ((Button)rptSessionsLast14.Items[i].FindControl("btnTripReg")).CssClass = "standardorange";
                        }
                    }
                    catch
                    {
                        ((Button)rptSessionsLast14.Items[i].FindControl("btnTripReg")).Visible = false;
                    }
                }

                if (session.RegisterComplete == 1)
                {
                    ((Button)rptSessionsLast14.Items[i].FindControl("btnRegister")).CssClass = "standardgreen";
                }
                else
                {
                    ((Button)rptSessionsLast14.Items[i].FindControl("btnRegister")).CssClass = "standardorange";
                }

               //get risk assessment
                if (session.RiskAssessmentId == 1)
                {
                    ((Button)rptSessionsLast14.Items[i].FindControl("btnRiskAssessment")).CssClass = "standardgreen";
                }
                else
                {
                    ((Button)rptSessionsLast14.Items[i].FindControl("btnRiskAssessment")).CssClass = "standardorange";
                }

                i++;
            }
           }
        

        protected void btnRiskAssessment_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);
            Common tools = new Common();
            var session = tools.returnSession(int.Parse(btnView.CommandArgument));
            tools.setActiveSportSociety((int)session.SportSocietyId);
            Response.Redirect("/Committee/Sessions/RiskAssessment/Default.aspx?sessionId=" + btnView.CommandArgument);
        }

        protected void btnTripReg_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);
            Common tools = new Common();
            var session = tools.returnSession(int.Parse(btnView.CommandArgument));
            tools.setActiveSportSociety((int)session.SportSocietyId);
            Response.Redirect("/Committee/Sessions/TripRegistration/Default.aspx?sessionId=" + btnView.CommandArgument);
        }

        protected void btnRoom_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);
            Common tools = new Common();
            var session = tools.returnSession(int.Parse(btnView.CommandArgument));
            tools.setActiveSportSociety((int)session.SportSocietyId);
            Response.Redirect("/Committee/Sessions/RoomBooking/Default.aspx?roomId=" + btnView.CommandArgument);
        }

        public void btnRegister_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            Common tools = new Common();
            var session = tools.returnSession(int.Parse(btnView.CommandArgument));
            tools.setActiveSportSociety((int)session.SportSocietyId);
            Response.Redirect("/Committee/Sessions/Register/Default.aspx?sessionId=" + btnView.CommandArgument);
        }
    }
}