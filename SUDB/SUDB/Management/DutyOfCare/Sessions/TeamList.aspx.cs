﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.DutyOfCare.Sessions
{
    public partial class TeamList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();

            var sports = tools.ReturnSportsSocs();

            rptsocs.DataSource = sports;
            rptsocs.DataBind();
            int i=0;

            foreach(var sport in sports)
            {
                ((Label)rptsocs.Items[i].FindControl("lblID")).Text = sport.Id.ToString();
                ((Label)rptsocs.Items[i].FindControl("lblName")).Text = sport.Name.ToString();
                i++;
            }


           

            var teamsare = tools.ReturnTeams();

            rptTeams.DataSource = teamsare;
            rptTeams.DataBind();
            int q = 0;

            foreach (var team in teamsare)
            {
                var sport = tools.returnSportSociety((int)team.SportSocietyId);

                ((Label)rptTeams.Items[q].FindControl("lblID")).Text = sport.Id.ToString();
                ((Label)rptTeams.Items[q].FindControl("lblName")).Text = sport.Name.ToString();
                ((Label)rptTeams.Items[q].FindControl("lblTeamId")).Text = team.Id.ToString();
                ((Label)rptTeams.Items[q].FindControl("lblTeamName")).Text = team.TeamName;
                q++;
            }

        }
    }
}