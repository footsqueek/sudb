﻿using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.DutyOfCare
{
    public partial class TransactionDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                bindCart();
            }
        }

        protected void bindCart()
        {
            try
            {

                Common tools = new Common();

                int transId = int.Parse(Request.QueryString["orderId"]);

                var transaction = tools.returnTransaction(transId);
                lblDate.Text = ((DateTime)transaction.transactionDate).ToString("dd MMM yyyy HH:mm");
                lblOrderNo.Text = transaction.transactionId.ToString();

                var student = tools.returnStudent((int)transaction.studentId);
                lblStudent.Text = student.FirstName + " " + student.Surname + " (<a href=\"/Management/StudentManager/SearchResults.aspx?query="+ student.StudentNumber + "\">" + student.StudentNumber + "</a>)";

                cboStatus.SelectedValue = transaction.status.ToString();

                var cart = tools.returnTransactionDetail(transId);

                rptCart.DataSource = cart;
                rptCart.DataBind();

                int i = 0;

                double totalprice = 0;
                double totalvat = 0;
                double totalgross = 0;

                foreach (tblMembership item in cart)
                {
                    var sport = tools.returnSportSociety(int.Parse(item.SportSocId.ToString()));

                    ((Label)rptCart.Items[i].FindControl("lblItem")).Text = "<a href=\"ViewSportSociety.aspx?Id="+sport.Id.ToString()+"\">" + sport.Name + "</a>";
                    ((Label)rptCart.Items[i].FindControl("lblNominal")).Text = sport.NominalCode;

                    ((Label)rptCart.Items[i].FindControl("lblGross")).Text = "&pound;" + Math.Round((double)item.Amount, 2).ToString();

                    ((Button)rptCart.Items[i].FindControl("btnRefund")).CommandArgument = item.Id.ToString();

                    if(item.Refunded!=null)
                    {
                        ((Button)rptCart.Items[i].FindControl("btnRefund")).Enabled = false;
                        ((Button)rptCart.Items[i].FindControl("btnRefund")).Text = ((DateTime)item.Refunded).ToString("dd mmm yyyy");

                    }



                    if (sport.VAT == 1)
                    {
                        ((Label)rptCart.Items[i].FindControl("lblVAT")).Text = "&pound;" + Math.Round((double)item.Amount/120*20,2).ToString();
                        ((Label)rptCart.Items[i].FindControl("lblprice")).Text = "&pound;" + Math.Round((double)item.Amount - ((double)item.Amount) / 120 * 20,2).ToString();

                        totalprice = totalprice + Math.Round((double)item.Amount - ((double)item.Amount) / 120 * 20, 2);
                        totalvat = totalvat + Math.Round(((double)item.Amount / 120 * 20), 2);
                        totalgross = totalgross + Math.Round((double)item.Amount, 2);
                    }
                    else
                    {
                        ((Label)rptCart.Items[i].FindControl("lblprice")).Text = "&pound;" + Math.Round((double)item.Amount, 2).ToString();
                        ((Label)rptCart.Items[i].FindControl("lblVAT")).Text = "&pound;" + "0.00";

                        totalprice = totalprice + Math.Round((double)item.Amount, 2);
                        totalvat = totalvat + 0;
                        totalgross = totalgross + Math.Round((double)item.Amount, 2);
                    }
                   


                    i++;
                }

                lblTotal.Text = "&pound;" + Math.Round(totalprice, 2);
                lblGross.Text = "&pound;" + Math.Round(totalgross, 2);
                lblVAT.Text = "&pound;" + Math.Round(totalvat, 2);
            }
            catch
            {
                Response.Redirect("Transactions.aspx");
            }

        }

        protected void btnRefund_Clickbutton(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);
           
            if(btnView.Text == "Please Confirm")
            {
                Common tools = new Common();
                tools.refundMembership(int.Parse(btnView.CommandArgument));
                bindCart();
            }
            else
            {
                btnView.Text = "Please Confirm";
            }


        }

        protected void btnCash_Click(object sender, EventArgs e)
        {
            Common tools = new Common();

            int transId = int.Parse(Request.QueryString["orderId"]);

            var transaction = tools.returnTransaction(transId);

         
            tools.updateTransactionStatus(transId, int.Parse(cboStatus.SelectedValue));
            Response.Redirect("Transactions.aspx");
        }
    }
}