﻿using SUDatabase.Classes;
using SUDB.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDB.Management.DutyOfCare
{
    public partial class FullTransactionDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtDate.Text = DateTime.Now.ToShortDateString();
                txtEnd.Text = DateTime.Now.ToShortDateString();
            }
            try
            {
                bindTransactions();
            }
            catch
            {

            }
        }

        protected void bindTransactions()
        {
            Common tools = new Common();

          var transactions = tools.returnTransactionsDetailsBetweenDates(DateTime.Parse(txtDate.Text),(DateTime.Parse(txtEnd.Text)));

          decimal approvedamount = 0;
          decimal cashamount = 0;
            decimal worldpayamount = 0;
            decimal aspireamount = 0;
            rptTransactions.DataSource = transactions;
            rptTransactions.DataBind();

            int i = 0;
            
            foreach(fulltransactiondetailmember transaction in transactions)
            {
                
                ((Label)rptTransactions.Items[i].FindControl("lblOrderNumber")).Text = "<a href=\"TransactionDetail.aspx?orderId="+ transaction.transactionId +"\">" + transaction.transactionId.ToString()+ "</a>";
                ((Label)rptTransactions.Items[i].FindControl("lblTransactionDate")).Text = ((DateTime)transaction.Date).ToString("dd MMM yyyy HH:mm");

                switch (transaction.Status)
                {
                    case 0:

                        ((Label)rptTransactions.Items[i].FindControl("lblTransactionStatus")).Text = "Not Processed";


                        break;

                    case 1:

                        ((Label)rptTransactions.Items[i].FindControl("lblTransactionStatus")).Text = "EPDQ";
                        approvedamount = approvedamount + (decimal)transaction.Gross;

                        break;
                    case 2:

                        ((Label)rptTransactions.Items[i].FindControl("lblTransactionStatus")).Text = "Declined";
                        //approvedamount = approvedamount + (decimal)transaction.Cast<tblMembership>().FirstOrDefault().Amount;

                        break;
                    case 3:

                        ((Label)rptTransactions.Items[i].FindControl("lblTransactionStatus")).Text = "Cash";
                        cashamount = cashamount + (decimal)transaction.Gross;
                        break;
                    case 4:

                        ((Label)rptTransactions.Items[i].FindControl("lblTransactionStatus")).Text = "Aspire";
                        aspireamount = aspireamount + (decimal)transaction.Gross;

                        break;
                    case 5:

                        ((Label)rptTransactions.Items[i].FindControl("lblTransactionStatus")).Text = "World Pay";
                        worldpayamount =worldpayamount + (decimal)transaction.Gross;

                        break;

                }


            
                ((Label)rptTransactions.Items[i].FindControl("lblStudentNumber")).Text = transaction.StudentNumber; 
                ((Label)rptTransactions.Items[i].FindControl("lblStudentName")).Text = transaction.FirstName + " " + transaction.Lastname;

                ((Label)rptTransactions.Items[i].FindControl("lblTransactionAmount")).Text = "&pound;" + Math.Round((double)transaction.Gross,2).ToString();

                ((Label)rptTransactions.Items[i].FindControl("lblPurchaseOf")).Text = transaction.SportSoc;
                ((Label)rptTransactions.Items[i].FindControl("lblNominalCode")).Text = transaction.NominalCode; 
                

                i++;
            }

            lblTotalApproved.Text = "&pound;" +  Math.Round(approvedamount,2).ToString();
            lblCash.Text = "&pound;" + Math.Round(cashamount, 2).ToString();
            lblAspire.Text = "&pound;" + Math.Round(aspireamount, 2).ToString();
            lblWorldPay.Text = "&pound;" + Math.Round(worldpayamount, 2).ToString();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (rptTransactions.Items.Count > 0)
            {
                StringBuilder myCsv = new StringBuilder();

                myCsv.AppendFormat("{0},{1},{2},{3},{4},{5},{6},{7}\r\n", "Order Number", "Student Number", "Name", "Date", "Gross", "Status", "Purchase Of","Nominal Code");


                for (int count = 0; count < rptTransactions.Items.Count; count++)
                {
                    //try
                    //{
                        Label Product = (Label)rptTransactions.Items[count].FindControl("lblOrderNumber");
                        Label Analysis = (Label)rptTransactions.Items[count].FindControl("lblStudentNumber");
                        Label Unit = (Label)rptTransactions.Items[count].FindControl("lblStudentName");

                        Label Required = (Label)rptTransactions.Items[count].FindControl("lblTransactionDate");
                        Label Cost = (Label)rptTransactions.Items[count].FindControl("lblTransactionAmount");
                        Label Notes = (Label)rptTransactions.Items[count].FindControl("lblTransactionStatus");

                        Label Sport = (Label)rptTransactions.Items[count].FindControl("lblPurchaseOf");
                        Label Nominal = (Label)rptTransactions.Items[count].FindControl("lblNominalCode");

                        myCsv.AppendFormat("{0},{1},{2},{3},{4},{5},{6},{7}\r\n", Product.Text.Trim().Replace("<span class=\"smaller\">", " ").Replace("</span>", "").Replace("<br/>", ""), "." + Analysis.Text.Trim().Replace(",", " "), Unit.Text, Required.Text.Replace("&pound;", ""), Cost.Text.Replace("&pound;", ""), Notes.Text.Replace("&pound;", ""), Sport.Text,Nominal.Text + "\"");
                    /*}
                    catch
                    {
                        //myCsv.AppendFormat("{0},{1},{2},{3},{4},{5},{6},{7}\r\n", count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString() + "\"");

                    }*/


                }

               

                Response.Clear();
                Response.ContentType = "application/csv";
                Response.AddHeader("content-disposition", "attachment; filename=DailyBanking" + txtDate.Text + ".csv");
                Response.Write(myCsv.ToString());
                Response.Flush();
                Response.End();
            }
        }
    }
}