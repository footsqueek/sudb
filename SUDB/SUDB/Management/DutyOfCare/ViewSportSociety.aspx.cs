﻿using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.DutyOfCare
{
    public partial class ViewSportSociety : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            int currentyear = DateTime.Now.Year;
            if(DateTime.Now<new DateTime(DateTime.Now.Year,08,01))
            {
                currentyear = currentyear-1;
            }

            for(int i = 2014;i<currentyear+1;i++)
            {
                cboYears.Items.Add(new ListItem(i.ToString() + " / " + (i + 1).ToString().Substring(2, 2), i.ToString()));
        

            }

            int id = int.Parse(Request.QueryString["Id"]);

            bindmembers(id);
        }

      public void  bindmembers(int id)
        {

            Common tools = new Common();

            var sportsociety = tools.returnSportSociety(id);
            lblSportSocietyName.Text = sportsociety.Name;

            var members = tools.returnSportSocietyMembers(id, int.Parse(cboYears.SelectedValue));

            rptMembers.DataSource = members;
            rptMembers.DataBind();

            int i = 0;

            double total = 0;

          foreach(tblMembership member in members)
          {

              ((Label)rptMembers.Items[i].FindControl("lblDateJoined")).Text = ((DateTime)member.DateJoined).ToString("dd MMM yyyy HH:mm");

              var student = tools.returnStudent((int)member.StudentId);


              ((Label)rptMembers.Items[i].FindControl("lblStudentNumber")).Text = "<a href=\"/Management/StudentManager/SearchResults.aspx?query="+ student.StudentNumber +"\">" + student.StudentNumber + "</a>";
              ((Label)rptMembers.Items[i].FindControl("lblStudentName")).Text = student.FirstName + " " + student.Surname;

              ((Label)rptMembers.Items[i].FindControl("lblNetAmount")).Text = "&pound;" + Math.Round((double)member.Amount,2);
              ((Label)rptMembers.Items[i].FindControl("lblGross")).Text = "&pound;" + Math.Round((double)member.Amount, 2);
              ((Label)rptMembers.Items[i].FindControl("lblVAT")).Text = "&pound;" + "0";

              total = total + (double)member.Amount;
              i++;
          }

          lblNet.Text = "&pound;" + Math.Round(total,2).ToString();
          lblGross.Text = "&pound;" + Math.Round(total, 2).ToString();
          lblVAT.Text = "&pound;" + "0";

          lblTotalMembers.Text = (i).ToString() + " Members";
        }
    }
}