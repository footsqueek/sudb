﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.DutyOfCare.RoomBookings
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();

            var requests = tools.returnActiveRoomRequests();

            if(requests.Count()==0)
            {
                litNone.Visible = true;
            }
            else
            {
                litNone.Visible = false;
            }

            rptRoomBookings.DataSource = requests;
            rptRoomBookings.DataBind();

            int i = 0;

            foreach(var request in requests)
            {
                //Lookup sport soc
                var sport = tools.returnSportSociety((int)request.SportSociety);
                var sessions = tools.returnSessionsByRoom(request.Id).FirstOrDefault();

                ((Label)rptRoomBookings.Items[i].FindControl("lblSportSociety")).Text = sport.Name;
                ((Label)rptRoomBookings.Items[i].FindControl("lblRequestHeadline")).Text = request.Headline;
                try
                {
                    ((Label)rptRoomBookings.Items[i].FindControl("lblRequestOpened")).Text = ((DateTime)sessions.StartDateTime).ToString();
                }
                catch
                {
                    ((Label)rptRoomBookings.Items[i].FindControl("lblRequestOpened")).Text = "";
                }
                ((Button)rptRoomBookings.Items[i].FindControl("btnView")).CommandArgument = request.Id.ToString();
                i++;
            }
        }

        protected void btnRoom_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);

            Common tools = new Common();
            var session = tools.returnRoomRequestAdmin(int.Parse(btnView.CommandArgument));
            tools.setActiveSportSociety((int)session.SportSociety);

            Response.Redirect("/Committee/Sessions/RoomBooking/Default.aspx?roomId=" + btnView.CommandArgument);
        }
    }
}