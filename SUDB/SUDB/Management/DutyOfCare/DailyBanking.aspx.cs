﻿using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.DutyOfCare
{
    public partial class DailyBanking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                txtDate.Text = DateTime.Now.ToShortDateString();
                txtEnd.Text = DateTime.Now.ToShortDateString();
            }
            try
            {
                bindSportsSocs();
            }
            catch
            {

            }
        }

        protected void bindSportsSocs()
        {
            Common tools = new Common();
            var sportssocs = tools.ReturnAllSportsSocs();

            decimal totalnet = 0;
            decimal totalvat = 0;
            decimal totalgross = 0;

            int i = 0;
            rptTransactions.DataSource = sportssocs;
            rptTransactions.DataBind();

            foreach(tblSportsSociety sportis in sportssocs)
            {
                
                if (tools.returnNETForSportBetweenDates(DateTime.Parse(txtDate.Text),DateTime.Parse(txtEnd.Text), sportis.Id)=="0.00")
                {
                     rptTransactions.Items[i].Visible=false;
                }
                else
                {
                    ((Label)rptTransactions.Items[i].FindControl("lblSportSociety")).Text = sportis.Name;
                    ((Label)rptTransactions.Items[i].FindControl("lblType")).Text = tools.returnDOCType((int)sportis.Type).DOCType;
                    ((Label)rptTransactions.Items[i].FindControl("lblNominalCode")).Text = sportis.NominalCode;
                    ((Label)rptTransactions.Items[i].FindControl("lblNet")).Text = "&pound;" + tools.returnNETForSportBetweenDates(DateTime.Parse(txtDate.Text), DateTime.Parse(txtEnd.Text), sportis.Id);
                    ((Label)rptTransactions.Items[i].FindControl("lblVat")).Text = "&pound;" + tools.returnVATForSportBetweenDates(DateTime.Parse(txtDate.Text), DateTime.Parse(txtEnd.Text), sportis.Id);
                    ((Label)rptTransactions.Items[i].FindControl("lblGross")).Text = "&pound;" + tools.returnGrossForSportBetweenDates(DateTime.Parse(txtDate.Text), DateTime.Parse(txtEnd.Text), sportis.Id);

                    totalnet = totalnet + decimal.Parse(tools.returnNETForSportBetweenDates(DateTime.Parse(txtDate.Text), DateTime.Parse(txtEnd.Text), sportis.Id));
                    totalvat = totalvat + decimal.Parse(tools.returnVATForSportBetweenDates(DateTime.Parse(txtDate.Text), DateTime.Parse(txtEnd.Text), sportis.Id));
                    totalgross = totalgross + decimal.Parse(tools.returnGrossForSportBetweenDates(DateTime.Parse(txtDate.Text), DateTime.Parse(txtEnd.Text), sportis.Id));


                }

                i++;
            }

            lblTotalGross.Text = "&pound;" + Math.Round(totalgross, 2).ToString();

            lblTotalNet.Text = "&pound;" + Math.Round(totalnet, 2).ToString();

            lblTotalVAT.Text = "&pound;" + Math.Round(totalvat, 2).ToString();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (rptTransactions.Items.Count > 0)
            {
                StringBuilder myCsv = new StringBuilder();

                myCsv.AppendFormat("{0},{1},{2},{3},{4},{5}\r\n", "Sport/Society", "Type", "Nominal Code", "Net", "VAT", "Gross");


                for (int count = 0; count < rptTransactions.Items.Count; count++)
                {
                    try
                    {

                        if (((Label)rptTransactions.Items[count].FindControl("lblNet")).Text != "0")
                        {
                            Label Product = (Label)rptTransactions.Items[count].FindControl("lblSportSociety");
                            Label Analysis = (Label)rptTransactions.Items[count].FindControl("lblType");
                            Label Unit = (Label)rptTransactions.Items[count].FindControl("lblNominalCode");

                            Label Required = (Label)rptTransactions.Items[count].FindControl("lblNet");
                            Label Cost = (Label)rptTransactions.Items[count].FindControl("lblVAT");
                            Label Notes = (Label)rptTransactions.Items[count].FindControl("lblGross");

                            myCsv.AppendFormat("{0},{1},{2},{3},{4},{5}\r\n", Product.Text.Trim().Replace("<span class=\"smaller\">", " ").Replace("</span>", "").Replace("<br/>", ""), "." + Analysis.Text.Trim().Replace(",", " "), Unit.Text, Required.Text.Replace("&pound;", ""), Cost.Text.Replace("&pound;", ""), Notes.Text.Replace("&pound;", ""), "\"");
                        }
                        }
                    catch
                    {
                       // myCsv.AppendFormat("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19}\r\n", count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), count.ToString(), "\"" + count.ToString() + "\"");

                    }


                }

               

                Response.Clear();
                Response.ContentType = "application/csv";
                Response.AddHeader("content-disposition", "attachment; filename=DailyBanking" + txtDate.Text + ".csv");
                Response.Write(myCsv.ToString());
                Response.Flush();
                Response.End();
            }
        }
    }
}