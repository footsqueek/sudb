﻿<%@ Page Title="Transaction Details" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="TransactionDetail.aspx.cs" Inherits="SUDatabase.Management.DutyOfCare.TransactionDetail" %>
<%@ Register src="../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script>
         $(document).ready(function () {
             $("#tablesorted").tablesorter();
         }
 );

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Transaction Detail</h1>

    <p>The detail below refers to order number <asp:Label ID="lblOrderNo" runat="server" Text="Label"></asp:Label> placed on <asp:Label ID="lblDate" runat="server" Text="Label"></asp:Label> by <asp:Label ID="lblStudent" runat="server" Text="Label"></asp:Label></p>

    <p>The current transaction status is <asp:DropDownList Enabled="true" CssClass="comboxbox" ID="cboStatus" runat="server">
        <asp:ListItem Value="0">Not Processed</asp:ListItem>
        <asp:ListItem Value="1">EPDQ</asp:ListItem>
         <asp:ListItem Value="5">WorldPay</asp:ListItem>
         <asp:ListItem Value="4">Aspire</asp:ListItem>
        <asp:ListItem Value="2">Declined</asp:ListItem>
         <asp:ListItem Value="3">Paid By Cash</asp:ListItem>
        </asp:DropDownList></p>

  <table id="tablesorted" class="lefttable nicetable">
      <thead>
      <tr>

          <th>Sport / Society</th>
           <th>Nominal Code</th>
          <th>Net</th>
          <th>VAT</th>
          <th>Gross</th>
          <th></th>
      </tr>
          </thead>
      <tbody>
        <asp:Repeater ID="rptCart" runat="server">

            <ItemTemplate>
<tr>
    
                  <td> <asp:Label CssClass="cartlabel" ID="lblItem" runat="server" Text="Label"></asp:Label></td>
     <td> <asp:Label CssClass="cartlabel" ID="lblNominal" runat="server" Text="Label"></asp:Label> </td>
                <td> <asp:Label CssClass="cartlabel" ID="lblPrice" runat="server" Text="Label"></asp:Label> </td>
      <td> <asp:Label CssClass="cartlabel" ID="lblVAT" runat="server" Text="Label"></asp:Label> </td>
      <td> <asp:Label CssClass="cartlabel" ID="lblGross" runat="server" Text="Label"></asp:Label> </td>
         <td> <asp:Button id="btnRefund"
           Text="Refund"
                    CssClass="standardbutton"
           CommandName="Refund"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnRefund_Clickbutton" 
           runat="server"/> </td>
                </tr>
            </ItemTemplate>

        </asp:Repeater>
      <tr> 
      
          <th colspan="2">  <asp:Label  CssClass="cartlabel" ID="lblItem" runat="server" Text="Total"></asp:Label> </th>
              <th>   <asp:Label CssClass="cartlabel" ID="lblTotal" runat="server" Text="Label"></asp:Label> </th>
                    <th>   <asp:Label CssClass="cartlabel" ID="lblVAT" runat="server" Text="Label"></asp:Label> </th>
                    <th>   <asp:Label CssClass="cartlabel" ID="lblGross" runat="server" Text="Label"></asp:Label> </th>
      <th></th>    
      </tr>
          </tbody>
    </table>

    <asp:Button ID="btnCash" CssClass="submitbutton" runat="server" Text="Save Changes" OnClick="btnCash_Click" />

</asp:Content>
