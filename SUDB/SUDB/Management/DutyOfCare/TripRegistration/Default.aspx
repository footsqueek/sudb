﻿<%@ Page Title="Trip Registrations" EnableEventValidation="false" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Management.DutyOfCare.TripRegistration.Default" %>
<%@ Register src="../../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script>
         $(document).ready(function () {
             $("#tablesorted").tablesorter();
             $("#tablesorted1").tablesorter();
             $("#tablesorted2").tablesorter();
         }
 );

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Trip Registrations</h1>

    <p>The trip registration system allows you to track Sports and Societies who have submitted trip registrations.</p>

    <h2>Awaiting Approval</h2>

    <table id="tablesorted" class="nicetable">
        <thead>
        <tr>

            <th>Sport or Society</th>
            <th>Destination</th>
            <th>Departing</th>
            <th>Returning</th>
      
            <th>Actions</th>

        </tr>
</thead>
        <tbody>
    <asp:Repeater ID="rptAwaitingApproval" runat="server">

        <ItemTemplate>

            <tr>

                   <td> <asp:Label ID="lblSportSociety" runat="server" Text=""></asp:Label>  </td>
                   <td> <asp:Label ID="lblDestination" runat="server" Text=""></asp:Label> </td>
                   <td> <asp:Label ID="lblDeparting" runat="server" Text=""></asp:Label> </td>
                   <td> <asp:Label ID="lblReturning" runat="server" Text=""></asp:Label> </td>

                   <td> <asp:Button id="btnTrip"
           Text="View Trip"
                    CssClass="standardbutton"
           CommandName="ViewTrip"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnTripReg_Click" 
           runat="server"/> </td>

            </tr>


        </ItemTemplate>


    </asp:Repeater>
            </tbody>
    </table>

       <h2>Upcoming and Active Approved Trips</h2>

    <table id="tablesorted1" class="nicetable">
        <thead>
        <tr>

               <th>Sport or Society</th>
            <th>Destination</th>
            <th>Departing</th>
            <th>Returning</th>

            <th>Actions</th>

        </tr>
</thead>
        <tbody>
    <asp:Repeater ID="rptApproved" runat="server">

          <ItemTemplate>

            <tr>

                   <td> <asp:Label ID="lblSportSociety" runat="server" Text=""></asp:Label>  </td>
                   <td> <asp:Label ID="lblDestination" runat="server" Text=""></asp:Label> </td>
                   <td> <asp:Label ID="lblDeparting" runat="server" Text=""></asp:Label> </td>
                   <td> <asp:Label ID="lblReturning" runat="server" Text=""></asp:Label> </td>

                   <td> <asp:Button id="btnTrip"
           Text="View Trip"
                    CssClass="standardbutton"
           CommandName="ViewTrip"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnTripReg_Click" 
           runat="server"/> </td>

            </tr>


        </ItemTemplate>



    </asp:Repeater>
            </tbody>
    </table>


       <h2>Upcoming and Active Rejected Trips</h2>

    <table id="tablesorted2" class="nicetable">
        <thead>
        <tr>

            <th>Sport or Society</th>
            <th>Destination</th>
            <th>Departing</th>
            <th>Returning</th>
  
            <th>Actions</th>

        </tr>
            </thead>
        <tbody>
    <asp:Repeater ID="rptRejected" runat="server">

        <ItemTemplate>
            <tr>

                   <td> <asp:Label ID="lblSportSociety" runat="server" Text=""></asp:Label>  </td>
                   <td> <asp:Label ID="lblDestination" runat="server" Text=""></asp:Label> </td>
                   <td> <asp:Label ID="lblDeparting" runat="server" Text=""></asp:Label> </td>
                   <td> <asp:Label ID="lblReturning" runat="server" Text=""></asp:Label> </td>
                   <td> <asp:Button id="btnTrip"
           Text="View Trip"
                    CssClass="standardbutton"
           CommandName="ViewTrip"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnTripReg_Click" 
           runat="server"/> </td>

            </tr>


        </ItemTemplate>


    </asp:Repeater>
            </tbody>
    </table>

    </asp:Content>
