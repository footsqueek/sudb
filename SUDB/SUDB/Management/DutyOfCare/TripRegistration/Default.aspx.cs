﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.DutyOfCare.TripRegistration
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          bindAwaitingApproval();
          bindApproved();
          bindRejected();
        }

        protected void bindAwaitingApproval()
        {
            Common tools = new Common();
            var trips = tools.returnTripsAwaitingApproval();

            int i = 0;
            rptAwaitingApproval.DataSource = trips;
            rptAwaitingApproval.DataBind();

            foreach(var trip in trips)
            {
                var society = tools.returnSportSociety((int)trip.SportSocietyId);
                ((Label)rptAwaitingApproval.Items[i].FindControl("lblSportSociety")).Text = society.Name;
             
                ((Label)rptAwaitingApproval.Items[i].FindControl("lblDestination")).Text = trip.TripDestinationAddress;
                try
                {
                    ((Label)rptAwaitingApproval.Items[i].FindControl("lblDeparting")).Text = ((DateTime)trip.DepartureTime).ToString("dd MMM yyyy HH:mm");
                }
                catch { }
                try
                {
                    ((Label)rptAwaitingApproval.Items[i].FindControl("lblReturning")).Text = ((DateTime)trip.ReturnTime).ToString("dd MMM yyyy HH:mm");
                }
                catch
                { }

                try
                {
                    var session = tools.returnSessionFromTrip(trip.tripId);

                    ((Button)rptAwaitingApproval.Items[i].FindControl("btnTrip")).CommandArgument = session.Id.ToString();
                }
                catch
                {
                      rptAwaitingApproval.Items[i].Visible=false;
                }
              
                i++;
            }
        }


        protected void bindApproved()
        {
            Common tools = new Common();
            var trips = tools.returnTripsApproved();

            int i = 0;
            rptApproved.DataSource = trips;
            rptApproved.DataBind();

            foreach (var trip in trips)
            {
                var society = tools.returnSportSociety((int)trip.SportSocietyId);
                ((Label)rptApproved.Items[i].FindControl("lblSportSociety")).Text = society.Name;

                ((Label)rptApproved.Items[i].FindControl("lblDestination")).Text = trip.TripDestinationAddress;
                ((Label)rptApproved.Items[i].FindControl("lblDeparting")).Text = ((DateTime)trip.DepartureTime).ToString("dd MMM yyyy HH:mm");
                ((Label)rptApproved.Items[i].FindControl("lblReturning")).Text = ((DateTime)trip.ReturnTime).ToString("dd MMM yyyy HH:mm");

                try
                {
                    var session = tools.returnSessionFromTrip(trip.tripId);

                    ((Button)rptApproved.Items[i].FindControl("btnTrip")).CommandArgument = session.Id.ToString();
                }
                catch
                {
                    rptApproved.Items[i].Visible = false;
                }

                i++;
            }
        }


        protected void bindRejected()
        {
            Common tools = new Common();
            var trips = tools.returnTripsRejected();

            int i = 0;
            rptRejected.DataSource = trips;
            rptRejected.DataBind();

            foreach (var trip in trips)
            {
                var society = tools.returnSportSociety((int)trip.SportSocietyId);
                ((Label)rptRejected.Items[i].FindControl("lblSportSociety")).Text = society.Name;

                ((Label)rptRejected.Items[i].FindControl("lblDestination")).Text = trip.TripDestinationAddress;
                ((Label)rptRejected.Items[i].FindControl("lblDeparting")).Text = ((DateTime)trip.DepartureTime).ToString("dd MMM yyyy HH:mm");
                ((Label)rptRejected.Items[i].FindControl("lblReturning")).Text = ((DateTime)trip.ReturnTime).ToString("dd MMM yyyy HH:mm");
                //((Label)rptRejected.Items[i].FindControl("lblNumberMembers")).Text = "0";

                try
                {
                    var session = tools.returnSessionFromTrip(trip.tripId);

                    ((Button)rptRejected.Items[i].FindControl("btnTrip")).CommandArgument = session.Id.ToString();
                }
                catch
                {
                    rptRejected.Items[i].Visible = false;
                }

                i++;
            }
        }


        protected void btnTripReg_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);

            //Set Active Sport Soc
            Common tools = new Common();

            var session = tools.returnSession(int.Parse(btnView.CommandArgument));

            tools.setActiveSportSociety((int)session.SportSocietyId);

            Response.Redirect("/Committee/Sessions/TripRegistration/Default.aspx?sessionId=" + btnView.CommandArgument);
        }

   
    }
}