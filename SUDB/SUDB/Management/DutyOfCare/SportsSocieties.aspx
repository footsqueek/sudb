﻿<%@ Page Title="Sports and Societies" Language="C#" MaintainScrollPositionOnPostback="true" EnableEventValidation="false" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="SportsSocieties.aspx.cs" Inherits="SUDatabase.Management.DutyOfCare.SportsSocieties" %>
<%@ Register src="../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
   
    <script>
        $(document).ready(function () {
            $("#tablesorted").tablesorter();
        }
);

    </script>
       
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Sports and Societies</h1>

      <p class="custsearch"><input type="text" placeholder="Search" class="searchboxcust" id="search" autocomplete="off"/></p>


    <table id="tablesorted" class="nicetable">
        <thead>
        <tr>

            <th>Name</th>
            <th>Type</th>
            <th>Current Price</th>
            <th>No. Members</th>
            <th>No. Sessions</th>
            <!--<th>Missed Registers</th>
            <th>Missed H&S Checklists</th>-->
            <th></th>

        </tr>
        </thead>
        <tbody>
    <asp:Repeater ID="rptSportsSocs" runat="server">

        <ItemTemplate>
            <tr>

               <td><asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>  </td>
               <td><asp:Label ID="lblType" runat="server" Text="Label"></asp:Label>  </td>
               <td><asp:Label ID="lblPrice" runat="server" Text="Label"></asp:Label>  </td>
               <td><asp:Label ID="lblTotMembers" runat="server" Text="Label"></asp:Label>  </td>
               <td><asp:Label ID="lblTotSessions" runat="server" Text="Label"></asp:Label>  </td>
                 <!-- <td><asp:Label ID="lblMissedReg" runat="server" Text="Label"></asp:Label>  </td>
                 <td><asp:Label ID="lblMissedChecklist" runat="server" Text="Label"></asp:Label>  </td>-->
               <td><asp:Button id="btnView"
           Text="View"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnView_Clickbutton" 
           runat="server"/><br />
                   <asp:Button id="btnEdit"
           Text="Edit"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnEdit_Clickbutton" 
           runat="server"/><br />
                   <asp:Button id="btnLock"
           Text="Lock"
                    CssClass="standardbutton"
           CommandName="Lock"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnLock_Clickbutton" 
           runat="server"/><br />
                   <asp:Button id="btnArchive"
           Text="Archive"
                    CssClass="standardbutton"
           CommandName="Archive"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnArchive_Clickbutton" 
           runat="server"/>
               </td>

            </tr>


        </ItemTemplate>


    </asp:Repeater>
            </tbody>
    </table>

    <asp:Button ID="btnArchive" CssClass="submitbutton" runat="server" Text="View Archived" OnClick="btnArchive_Click" />
    <script>

         $("#search").keyup(function () {
             var value = this.value.toLowerCase();

             $("#tablesorted").find("tr").each(function (index) {
                 if (!index) return;
                 $(this).find("td").each(function () {
                     var id = $(this).text().toLowerCase().trim();
                     var not_found = (id.indexOf(value) == -1);
                     $(this).closest('tr').toggle(!not_found);
                     return not_found;
                 });
             });
         });

        </script>
</asp:Content>
