﻿<%@ Page Title="Create New Election" EnableEventValidation="false" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Management.Elections.CreateNew.Default" %>
<%@ Register src="../../../UserControls/ElectionsMenu.ascx" tagname="ElectionsMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:ElectionsMenu ID="ElectionsMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Create New Election</h1>

    <p>To create a new election please complete the details below</p>

    <asp:Literal Visible="false" ID="litError" runat="server"><p class="error">There was an error creating your election, please check the fields below and try again.</p></asp:Literal>

     <p>
        <asp:Label ID="Label2" CssClass="label" runat="server" Text="Election Name"></asp:Label><br />
        <asp:TextBox ID="txtElectionName" CssClass="txtbox" runat="server"></asp:TextBox><br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="error" ControlToValidate="txtElectionName" runat="server" ErrorMessage="You must enter an election name"></asp:RequiredFieldValidator>

    </p>

    <p>
        <asp:Label ID="Label1" CssClass="label" runat="server" Text="Start Date / Time"></asp:Label><br />
        <asp:TextBox ID="txtStartDate" CssClass="txtbox" runat="server"></asp:TextBox><br />
          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="error" ControlToValidate="txtStartDate" runat="server" ErrorMessage="You must enter an election start date"></asp:RequiredFieldValidator>

    </p>

      <p>
        <asp:Label ID="Label3" CssClass="label" runat="server" Text="End Date / Time"></asp:Label><br />
        <asp:TextBox ID="txtEndDate" CssClass="txtbox" runat="server"></asp:TextBox><br />
          <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="error" ControlToValidate="txtEndDate" runat="server" ErrorMessage="You must enter an election end date"></asp:RequiredFieldValidator>

    </p>


    <asp:Button ID="btnCreate" runat="server" CssClass="submitbutton" Text="Create Election" OnClick="btnCreate_Click" />

</asp:Content>
