﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.Elections.CreateNew
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();
            if (tools.checkAccess("Elections") == "Full Access")
            {
                if (!IsPostBack)
                {
                    //Full access or nothing!
                    txtStartDate.Text = DateTime.Now.ToString("dd/MM/yyyy hh:ss");
                    txtEndDate.Text = DateTime.Now.ToString("dd/MM/yyyy hh:ss");
                }
            }
            else
            {
                Response.Redirect("/Default.aspx");
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {

                //Check election dates
                if (DateTime.Parse(txtStartDate.Text) > DateTime.Now && DateTime.Parse(txtEndDate.Text) > DateTime.Parse(txtStartDate.Text))
                {

                    Common tools = new Common();
                    tools.createElection(txtElectionName.Text, DateTime.Parse(txtStartDate.Text), DateTime.Parse(txtEndDate.Text));
                    Response.Redirect("../ManageElections/");
                }
                else
                {
                    litError.Visible = true;
                    litError.Text = "<p class\"error\">There has been an error creating your election. The start date must be in the future and the end date must be after the start date.</p>";
                }
            }
            catch
            {
                litError.Visible = true;
            }
        }
    }
}