﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.Elections.ManageElections
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();
            if (tools.checkAccess("Elections") == "Full Access")
            {
                //Full access or nothing!

                bindCompleted();
                bindInProgressMode();
                bindDesignMode();
            }
            else
            {
                Response.Redirect("/Default.aspx");
            }
        }

        public void btnCompletedReports_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            //Response.Redirect("../Cases/ViewCase.aspx?caseId=" + btnView.CommandArgument);
        }

        public void btnInProgressReports_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            //Response.Redirect("../Cases/ViewCase.aspx?caseId=" + btnView.CommandArgument);
        }

        public void btnModify_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            //Response.Redirect("../Cases/ViewCase.aspx?caseId=" + btnView.CommandArgument);
        }

        public void btnCandidates_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            Common tools = new Common();
            tools.setCurrentElection(int.Parse(btnView.CommandArgument));
            Response.Redirect("Positions/Default.aspx");
        }

        public void btnDelete_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            //Response.Redirect("../Cases/ViewCase.aspx?caseId=" + btnView.CommandArgument);
        }

        protected void bindDesignMode()
        {
            Common tools = new Common();
            var elections = tools.returnElectionsInDesign();

            rptDesignMode.DataSource = elections;
            rptDesignMode.DataBind();
            int i = 0;

            foreach(var election in elections)
            {
                ((Label)rptDesignMode.Items[i].FindControl("lblName")).Text = election.ElectionName;
                ((Label)rptDesignMode.Items[i].FindControl("lblStartDate")).Text = ((DateTime)election.StartDateTime).ToString("dd MMM yyyy HH:ss");
                ((Label)rptDesignMode.Items[i].FindControl("lblEndDate")).Text = ((DateTime)election.EndDateTime).ToString("dd MMM yyyy HH:ss");
                ((Button)rptDesignMode.Items[i].FindControl("btnDelete")).CommandArgument = election.electionId.ToString();
                ((Button)rptDesignMode.Items[i].FindControl("btnModify")).CommandArgument = election.electionId.ToString();
                ((Button)rptDesignMode.Items[i].FindControl("btnCandidates")).CommandArgument = election.electionId.ToString();
               
                i++;
            }

            if (elections.Count() == 0)
            {
                litDesignModeNoItems.Visible = true;
            }
        }


        protected void bindInProgressMode()
        {
            Common tools = new Common();
            var elections = tools.returnElectionsInProgress();

            rptInProgress.DataSource = elections;
            rptInProgress.DataBind();
            int i = 0;

            foreach (var election in elections)
            {
                ((Label)rptInProgress.Items[i].FindControl("lblName")).Text = election.ElectionName;
                ((Label)rptInProgress.Items[i].FindControl("lblStartDate")).Text = ((DateTime)election.StartDateTime).ToString("dd MMM yyyy HH:ss");
                ((Label)rptInProgress.Items[i].FindControl("lblEndDate")).Text = ((DateTime)election.EndDateTime).ToString("dd MMM yyyy HH:ss");
                ((Button)rptInProgress.Items[i].FindControl("btnReports")).CommandArgument = election.electionId.ToString();
               
                i++;
            }
            if (elections.Count() == 0)
            {
                litInProgressNoItems.Visible = true;
            }
        }

        protected void bindCompleted()
        {
            Common tools = new Common();
            var elections = tools.returnElectionsCompleted();

            rptCompletedElections.DataSource = elections;
            rptCompletedElections.DataBind();
            int i = 0;

            foreach (var election in elections)
            {
                ((Label)rptCompletedElections.Items[i].FindControl("lblName")).Text = election.ElectionName;
                ((Label)rptCompletedElections.Items[i].FindControl("lblStartDate")).Text = ((DateTime)election.StartDateTime).ToString("dd MMM yyyy HH:ss");
                ((Label)rptCompletedElections.Items[i].FindControl("lblEndDate")).Text = ((DateTime)election.EndDateTime).ToString("dd MMM yyyy HH:ss");
                ((Button)rptCompletedElections.Items[i].FindControl("btnReports")).CommandArgument = election.electionId.ToString();

                i++;
            }

            if(elections.Count()==0)
            {
                litCompletedNoItems.Visible = true;
            }
        }
    }
}