﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.Elections.ManageElections.Positions
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();
            if (tools.checkAccess("Elections") == "Full Access")
            {
                //Full access or nothing!

               

                var election = tools.returnCurrentElection();
                litElectionName.Text = election.ElectionName;

                if (election.StartDateTime > DateTime.Now)
                {

                    bindPositions();
                }
                else
                {
                    //election not in design mode
                    Response.Redirect("../");
                }
                
            }
            else
            {
                Response.Redirect("/Default.aspx");
            }
        }


        public void btnModify_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            //Response.Redirect("../Cases/ViewCase.aspx?caseId=" + btnView.CommandArgument);
        }

        public void btnCandidates_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            Common tools = new Common();
            tools.setCurrentPosition(int.Parse(btnView.CommandArgument));
            Response.Redirect("Candidates/Default.aspx");
        }

        public void btnDelete_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            //Response.Redirect("../Cases/ViewCase.aspx?caseId=" + btnView.CommandArgument);
        }


        protected void bindPositions()
        {
            Common tools = new Common();
            var positions = tools.returnElectionPositions();

            rptPositions.DataSource = positions;
            rptPositions.DataBind();
            int i = 0;

            foreach(var position in positions)
            {
                ((Label)rptPositions.Items[i].FindControl("lblPositionName")).Text = position.PositionName;
                ((Label)rptPositions.Items[i].FindControl("lblSystem")).Text = position.votingType;
                ((Label)rptPositions.Items[i].FindControl("lblSeats")).Text = position.numberSeats.ToString();
               
               
                ((Button)rptPositions.Items[i].FindControl("btnDelete")).CommandArgument = position.electionId.ToString();
                ((Button)rptPositions.Items[i].FindControl("btnModify")).CommandArgument = position.electionId.ToString();
                ((Button)rptPositions.Items[i].FindControl("btnCandidates")).CommandArgument = position.electionId.ToString();
                i++;
            
            }

            if(positions.Count()==0)
            {
                litNoItems.Visible = true;
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Response.Redirect("CreatePosition.aspx");
        }
    }
}