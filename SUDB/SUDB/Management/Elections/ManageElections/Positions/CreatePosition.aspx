﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="CreatePosition.aspx.cs" Inherits="SUDatabase.Management.Elections.ManageElections.Positions.CreatePosition" %>
<%@ Register src="../../../../UserControls/ElectionsMenu.ascx" tagname="ElectionsMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:ElectionsMenu ID="ElectionsMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Create Position</h1>
     <h2><asp:Label ID="litElectionName" runat="server" Text="Label"></asp:Label></h2>


    <p>To create a position in this election please complete the form below.</p>

    
    <asp:Literal Visible="false" ID="litError" runat="server"><p class="error">There was an error creating your position, please check the fields below and try again.</p></asp:Literal>

     <p>
        <asp:Label ID="Label2" CssClass="label" runat="server" Text="Position Name"></asp:Label><br />
        <asp:TextBox ID="txtPositionName" CssClass="txtbox" runat="server"></asp:TextBox><br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="error" ControlToValidate="txtPositionName" runat="server" ErrorMessage="You must enter a position name"></asp:RequiredFieldValidator>

    </p>

    <p>
        <asp:Label ID="Label1" CssClass="label" runat="server" Text="Position Description"></asp:Label><br />
        <asp:TextBox ID="txtPositionDescription" CssClass="txtbox" TextMode="MultiLine" Rows="5" runat="server"></asp:TextBox><br />
          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="error" ControlToValidate="txtPositionDescription" runat="server" ErrorMessage="You must enter a position description"></asp:RequiredFieldValidator>

    </p>

      <p>
        <asp:Label ID="Label3" CssClass="label" runat="server" Text="Number of Seats"></asp:Label><br />
       
          <asp:DropDownList CssClass="combobox" ID="cboSeats" runat="server">
              <asp:ListItem>1</asp:ListItem>
               <asp:ListItem>2</asp:ListItem>
               <asp:ListItem>3</asp:ListItem>
               <asp:ListItem>4</asp:ListItem>
               <asp:ListItem>5</asp:ListItem>
               <asp:ListItem>6</asp:ListItem>
               <asp:ListItem>7</asp:ListItem>
               <asp:ListItem>8</asp:ListItem>
               <asp:ListItem>9</asp:ListItem>
               <asp:ListItem>10</asp:ListItem>

          </asp:DropDownList>
    </p>

          <p>
        <asp:Label ID="Label4" CssClass="label" runat="server" Text="Voting System"></asp:Label><br />
       
          <asp:DropDownList CssClass="combobox" ID="cboSystem" runat="server">
              <asp:ListItem Value="STV">Single Transferable Vote</asp:ListItem>
               <asp:ListItem Value="FPP">First Past the Post</asp:ListItem>
              

          </asp:DropDownList>
    </p>
    <asp:Button ID="btnCreate" CssClass="submitbutton" runat="server" Text="Create Position" OnClick="btnCreate_Click" />
</asp:Content>
