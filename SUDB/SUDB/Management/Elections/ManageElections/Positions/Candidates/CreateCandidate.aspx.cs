﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.Elections.ManageElections.Positions.Candidates
{
    public partial class CreateCandidate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();
            if (tools.checkAccess("Elections") == "Full Access")
            {
                //Full access or nothing!



                var election = tools.returnCurrentElection();
                litElectionName.Text = election.ElectionName;
                var position = tools.returnCurrentPosition();
                lblPosition.Text = position.PositionName;

                if (election.StartDateTime > DateTime.Now)
                {

                }
                else
                {
                    //election not in design mode
                    Response.Redirect("../");
                }

            }
            else
            {
                Response.Redirect("/Default.aspx");
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Common tools = new Common();

            if (fileManifesto.HasFile && filePhoto.HasFile)
            {

              var candidate =  tools.createCandidate(txtCandidateName.Text, txtYouTube.Text,fileManifesto.FileName,filePhoto.FileName);
              fileManifesto.SaveAs(Server.MapPath("~/") + "Public/ElectionDocuments/CandidateManifestos/" + candidate.candidateId.ToString() + fileManifesto.FileName);
              filePhoto.SaveAs(Server.MapPath("~/") + "Public/ElectionDocuments/CandidatePhotos/" + candidate.candidateId.ToString() + filePhoto.FileName);
              Response.Redirect("Default.aspx");
            }
            else
            {
                litError.Visible = true;
            }
        }
    }
}