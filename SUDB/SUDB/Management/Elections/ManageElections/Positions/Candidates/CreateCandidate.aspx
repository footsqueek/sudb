﻿<%@ Page Title="Create Candidate" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="CreateCandidate.aspx.cs" Inherits="SUDatabase.Management.Elections.ManageElections.Positions.Candidates.CreateCandidate" %>
<%@ Register src="../../../../../UserControls/ElectionsMenu.ascx" tagname="ElectionsMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:ElectionsMenu ID="ElectionsMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Create Candidate</h1>

     <h2><asp:Label ID="litElectionName" runat="server" Text="Label"></asp:Label></h2>


    <p>To create a candidate for the position of <asp:Label ID="lblPosition" runat="server" Text="Label"></asp:Label> in this election please complete the form below.</p>

    
    <asp:Literal Visible="false" ID="litError" runat="server"><p class="error">There was an error creating this candidate, please check the fields below and try again.</p></asp:Literal>

     <p>
        <asp:Label ID="Label2" CssClass="label" runat="server" Text="Candidate Name"></asp:Label><br />
        <asp:TextBox ID="txtCandidateName" CssClass="txtbox" runat="server"></asp:TextBox><br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="error" ControlToValidate="txtCandidateName" runat="server" ErrorMessage="You must enter a position name"></asp:RequiredFieldValidator>

    </p>

    <p>
        <asp:Label ID="Label1" CssClass="label" runat="server" Text="Manifesto"></asp:Label><br />
        <asp:FileUpload CssClass="combobox" ID="fileManifesto" runat="server" />
    </p>

       <p>
        <asp:Label ID="Label3" CssClass="label" runat="server" Text="Photo"></asp:Label><br />
        <asp:FileUpload CssClass="combobox" ID="filePhoto" runat="server" />
    </p>

    <p>
        <asp:Label ID="Label4" CssClass="label" runat="server" Text="YouTube Link"></asp:Label><br />
        <asp:TextBox ID="txtYouTube" CssClass="txtbox" runat="server"></asp:TextBox><br />
         
    </p>

    <asp:Button ID="btnCreate" CssClass="submitbutton" runat="server" Text="Create Candidate" OnClick="btnCreate_Click" />
</asp:Content>
