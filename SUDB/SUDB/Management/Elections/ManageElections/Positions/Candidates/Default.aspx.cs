﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.Elections.ManageElections.Positions.Candidates
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();
            if (tools.checkAccess("Elections") == "Full Access")
            {
                //Full access or nothing!



                var election = tools.returnCurrentElection();
                litElectionName.Text = election.ElectionName;
                var position = tools.returnCurrentPosition();
                lblPosition.Text = position.PositionName;

                if (election.StartDateTime > DateTime.Now)
                {

                    bindCandidates();
                }
                else
                {
                    //election not in design mode
                    Response.Redirect("../");
                }

            }
            else
            {
                Response.Redirect("/Default.aspx");
            }
        }

        public void btnModify_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            //Response.Redirect("../Cases/ViewCase.aspx?caseId=" + btnView.CommandArgument);
        }
        public void btnDelete_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            //Response.Redirect("../Cases/ViewCase.aspx?caseId=" + btnView.CommandArgument);
        }


        private void bindCandidates()
        {

            Common tools = new Common();
            var positions = tools.returnPositionCandidates();

            rptPositions.DataSource = positions;
            rptPositions.DataBind();
            int i = 0;

            foreach (var position in positions)
            {
                ((Label)rptPositions.Items[i].FindControl("lblCandidateName")).Text = position.CandidateName;
                ((Label)rptPositions.Items[i].FindControl("lblPhoto")).Text = "<img src=\"/Public/ElectionDocuments/CandidatePhotos/"+ position.candidateId + position.PhotoFileName + "\" alt=\"Photo of "+position.CandidateName+"\" width=\"100px\"/>";
              
                ((Button)rptPositions.Items[i].FindControl("btnDelete")).CommandArgument = position.candidateId.ToString();
                ((Button)rptPositions.Items[i].FindControl("btnModify")).CommandArgument = position.candidateId.ToString();
                i++;
            }

            if (positions.Count() == 0)
            {
                litNoItems.Visible = true;
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Response.Redirect("CreateCandidate.aspx");
        }
    }
}