﻿<%@ Page Title="Election Candidates" EnableEventValidation="false" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Management.Elections.ManageElections.Positions.Candidates.Default" %>
<%@ Register src="../../../../../UserControls/ElectionsMenu.ascx" tagname="ElectionsMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:ElectionsMenu ID="ElectionsMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <h1>Election Candidates</h1>

    <h2><asp:Label ID="litElectionName" runat="server" Text="Label"></asp:Label></h2>

    <p>The candidates listed below have been created for the position of <asp:Label ID="lblPosition" runat="server" Text="Label"></asp:Label></p>

    <table class="nicetable">

        <tr>

            <th>Candidate Name</th>
            <th>Photo</th>
            <th>Manifesto</th>
            <th>Video Link</th>
            <th></th>

        </tr>

        <asp:Repeater ID="rptPositions" runat="server">


            <ItemTemplate>


                <tr>

                    <td>
                        <asp:Label ID="lblCandidateName" runat="server" Text="Label"></asp:Label>  </td>
                      <td>
                        <asp:Label ID="lblPhoto" runat="server" Text="Label"></asp:Label>  </td> 
                    <td>
                        <asp:Label ID="lblManifesto" runat="server" Text="Label"></asp:Label>  </td>
                     <td>
                        <asp:Label ID="lblVideo" runat="server" Text="Label"></asp:Label>  </td>

                    <td><asp:Button id="btnModify"
           Text="Modify Candidate"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnModify_Clickbutton" 
           runat="server"/> <asp:Button id="btnDelete"
           Text="Delete"
                    CssClass="standardbutton"
           CommandName="Delete"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnDelete_Clickbutton" 
           runat="server"/></td>

                </tr>

            </ItemTemplate>


        </asp:Repeater>

    </table>

      <asp:Literal ID="litNoItems" Visible="false" runat="server"><p class="error">There are currently no candidates for this position.</p></asp:Literal>

    <asp:Button ID="btnCreate" CssClass="submitbutton" runat="server" Text="Create Candidate" OnClick="btnCreate_Click" />

</asp:Content>
