﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.Elections.ManageElections.Positions
{
    public partial class CreatePosition : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             Common tools = new Common();
            if (tools.checkAccess("Elections") == "Full Access")
            {
                //Full access or nothing!

               

                var election = tools.returnCurrentElection();
                litElectionName.Text = election.ElectionName;

                if (election.StartDateTime > DateTime.Now)
                {

                   
                }
                else
                {
                    //election not in design mode
                    Response.Redirect("../");
                }
                
            }
            else
            {
                Response.Redirect("/Default.aspx");
            }
        }
       

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {

                //Check election dates
               
                    Common tools = new Common();
                    tools.createPosition(txtPositionName.Text, txtPositionDescription.Text, int.Parse(cboSeats.SelectedValue), cboSystem.SelectedValue);
                    Response.Redirect("Default.aspx");
                
            }
            catch
            {
                litError.Visible = true;
            }

        }
    }
}