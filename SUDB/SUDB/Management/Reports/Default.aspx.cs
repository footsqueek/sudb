﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Management.Reports
{
    public partial class Default : System.Web.UI.Page
    {

        protected void bindCampus()
        {

            Common tools = new Common();
            var campuses = tools.ReturnCapuses();
            rptCampusDemographics.DataSource = campuses;
            rptCampusDemographics.DataBind();

            int i = 0;

            foreach(var campus in campuses)
            {
                var students = tools.ReturnCampusStudents(campus.Id);

                ((Label)rptCampusDemographics.Items[i].FindControl("lblCampus")).Text = campus.Campus;
                ((Label)rptCampusDemographics.Items[i].FindControl("lblMale")).Text = students.Where(p=>p.Gender=="M").Count().ToString();
                ((Label)rptCampusDemographics.Items[i].FindControl("lblFemale")).Text = students.Where(p => p.Gender == "F").Count().ToString();
                ((Label)rptCampusDemographics.Items[i].FindControl("lblOther")).Text = (students.Count()- students.Where(p => p.Gender == "F").Count()- students.Where(p => p.Gender == "M").Count()).ToString();
                ((Label)rptCampusDemographics.Items[i].FindControl("lblTotal")).Text = students.Count().ToString();
                i++;
            }

        }
        protected void bindLevels()
        {

            Common tools = new Common();
            var levels = tools.ReturnLevels();
            rptLevelDemographics.DataSource = levels;
            rptLevelDemographics.DataBind();

            int i = 0;

            foreach (var level in levels)
            {
                var students = tools.ReturnLevelStudents(level.levelis.ToString());

                ((Label)rptLevelDemographics.Items[i].FindControl("lblLevel")).Text = level.levelis;
                ((Label)rptLevelDemographics.Items[i].FindControl("lblMale")).Text = students.Where(p => p.Gender == "M").Count().ToString();
                ((Label)rptLevelDemographics.Items[i].FindControl("lblFemale")).Text = students.Where(p => p.Gender == "F").Count().ToString();
                ((Label)rptLevelDemographics.Items[i].FindControl("lblOther")).Text = (students.Count() - students.Where(p => p.Gender == "F").Count() - students.Where(p => p.Gender == "M").Count()).ToString();
                ((Label)rptLevelDemographics.Items[i].FindControl("lblTotal")).Text = students.Count().ToString();
                i++;
            }

        }

        protected void bindFaculty()
        {

            Common tools = new Common();
            var levels = tools.ReturnFaculties();
            rptFacultyDemographics.DataSource = levels;
            rptFacultyDemographics.DataBind();

            int i = 0;

            foreach (var level in levels)
            {
                var students = tools.ReturnFacultyStudents(level.Id);

                ((Label)rptFacultyDemographics.Items[i].FindControl("lblLevel")).Text = level.FacultyName;
                ((Label)rptFacultyDemographics.Items[i].FindControl("lblMale")).Text = students.Where(p => p.Gender == "M").Count().ToString();
                ((Label)rptFacultyDemographics.Items[i].FindControl("lblFemale")).Text = students.Where(p => p.Gender == "F").Count().ToString();
                ((Label)rptFacultyDemographics.Items[i].FindControl("lblOther")).Text = (students.Count() - students.Where(p => p.Gender == "F").Count() - students.Where(p => p.Gender == "M").Count()).ToString();
                ((Label)rptFacultyDemographics.Items[i].FindControl("lblTotal")).Text = students.Count().ToString();
                i++;
            }

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();

            try
            {
                var revenue = tools.returnTransactionsThisYear();
                lblTotalRevenue.Text = "&pound;" + Math.Round((double)revenue.Sum(p => p.Amount), 2).ToString();
            }
            catch
            {
                lblTotalRevenue.Text = "&pound;0"; 
            }
            var sportsocmembers = tools.returnTotalSportSocietyMembers();
            lblTotMembers.Text = sportsocmembers.ToString();

            var individualsportsocmembers = tools.returnTotalIndividualSportSocietyMembers();
            lblIndividualMembers.Text = individualsportsocmembers.ToString();

            var totalstudents = tools.returnNoStudents();
            try
            {
                decimal percent = (100 / (decimal)totalstudents) * (decimal)sportsocmembers;
                lblTotalMembersPercent.Text = Math.Round(percent, 0).ToString() + "%";
            }
            catch
            {
                lblTotalMembersPercent.Text = "0%";
            }
            try
            {
                decimal uniquepercent = (100 / (decimal)totalstudents) * (decimal)individualsportsocmembers;
                lblUniqueMembersPercent.Text = Math.Round(uniquepercent, 0).ToString() + "%";
            }
            catch
            {
                lblUniqueMembersPercent.Text = "0%";
            }

            lblTotalStudents.Text = totalstudents.ToString();

            try
            {
                lblPointsExposure.Text = tools.getTotalPointTransactions().ToString() + " Points";
            }
            catch
            {
                lblPointsExposure.Text = "0 Points";
            }

            bindCampus();
            bindLevels();
            bindFaculty();
        }
    }
}