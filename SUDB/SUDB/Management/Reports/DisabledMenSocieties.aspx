﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="DisabledMenSocieties.aspx.cs" Inherits="SUDB.Management.Reports.DisabledMenSocieties" %>
<%@ Register src="../../UserControls/ReportsMenu.ascx" tagname="ReportsMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:ReportsMenu ID="ReportsMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

         <h1>Participation in Societies</h1>

    <p>This report details the participation rates in Societies of disabled Men.</p>

    <p>Total disabled Men actively participating <asp:Label ID="lblTotalMen" runat="server" Text="Label"></asp:Label></p>

    <table class="nicetable">

        <tr>
            <th>Name</th>
            <th>Sport / Society</th>
            <th>Sessions (Sept - Dec)</th>
            <th>Sessions (Jan - Mar)</th>
            <th>Sessions (Apr - Jul)</th>
            <th>Total</th>
        </tr>
    <asp:Repeater ID="rptReport" runat="server">
        <ItemTemplate>
        <tr>

            <td><asp:Label ID="lblName" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblSportSoc" runat="server" Text="Label"></asp:Label></td>
            <td><asp:Label ID="lblT1" runat="server" Text="Label"></asp:Label></td>
            <td><asp:Label ID="lblT2" runat="server" Text="Label"></asp:Label></td>
            <td><asp:Label ID="lblT3" runat="server" Text="Label"></asp:Label></td>
            <td><asp:Label ID="lblTotal" runat="server" Text="Label"></asp:Label></td>

        </tr>
        </ItemTemplate>
    </asp:Repeater>

         <tr>

            <th>Totals</th>
             <th></th>
            <th><asp:Label ID="lblTotalT1" runat="server" Text="Label"></asp:Label></th>
            <th><asp:Label ID="lblTotalT2" runat="server" Text="Label"></asp:Label></th>
            <th><asp:Label ID="lblTotalT3" runat="server" Text="Label"></asp:Label></th>
            <th><asp:Label ID="lblTotalSes" runat="server" Text="Label"></asp:Label></th>

        </tr>

         <tr>

            <th>Average Sessions Attended</th>
             <th></th>
            <th><asp:Label ID="lblT1Average" runat="server" Text="Label"></asp:Label></th>
            <th><asp:Label ID="lblT2Average" runat="server" Text="Label"></asp:Label></th>
            <th><asp:Label ID="lblT3Average" runat="server" Text="Label"></asp:Label></th>
            <th><asp:Label ID="lblAverage" runat="server" Text="Label"></asp:Label></th>

        </tr>

        </table>

</asp:Content>
