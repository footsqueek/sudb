﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="MembersByTypeDrilldown.aspx.cs" Inherits="SUDB.Management.Reports.MembersByTypeDrilldown" %>
<%@ Register src="../../UserControls/ReportsMenu.ascx" tagname="ReportsMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:ReportsMenu ID="ReportsMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Members By Type</h1>

    <h2>Member Type: <asp:Label ID="lblType" runat="server" Text="Label"></asp:Label></h2>
    
    <p>The following students are a member of sports and societies during the current academic year.</p>

    <table class="nicetable">
        <tr>
            <th>Student Number</th>
            <th>Student Name</th>
        </tr>
    <asp:Repeater ID="rptStudents" runat="server">

        <ItemTemplate>
            <tr>

                <td>
                    <asp:Label ID="lblStudentNo" runat="server" Text="Label"></asp:Label></td>
                 <td>
                    <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label></td>

            </tr>

        </ItemTemplate>

    </asp:Repeater>
        
    </table>

</asp:Content>
