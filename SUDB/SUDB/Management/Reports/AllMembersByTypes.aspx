﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="AllMembersByTypes.aspx.cs" Inherits="SUDB.Management.Reports.AllMembersByType" %>
<%@ Register src="../../UserControls/ReportsMenu.ascx" tagname="ReportsMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:ReportsMenu ID="ReportsMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Sport and Society Members By Type</h1>

    <p>The following table shows students categorised by type.</p>

    <table class="nicetable">
        <tr>
            <th>Membership Type</th>
            <th>Number of Members</th>
        </tr>

        <asp:Repeater ID="rptType" runat="server">
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:Label ID="lblType" runat="server" Text="Label"></asp:Label></td>
              <td>
                        <asp:Label ID="lblNo" runat="server" Text="Label"></asp:Label></td>
              
                      </tr>
            </ItemTemplate>

        </asp:Repeater>
    </table>


</asp:Content>
