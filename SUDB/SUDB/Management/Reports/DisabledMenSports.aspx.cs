﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDB.Management.Reports
{
    public partial class DisabledMenSports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();

            Common tools = new Common();
            var su = tools.getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }


            DateTime lowestDateJoined = new DateTime(currentyear, (int)su.YearStartMonth, 01);
            DateTime highestDateJoined = new DateTime(currentyear + 1, (int)su.YearStartMonth - 1, 31);

            var memberships = from p in db.tblMemberships join x in db.tblSportsSocieties on p.SportSocId equals x.Id join r in db.tblStudents on p.StudentId equals r.Id join h in db.tblTransactions on p.TransactionId equals h.transactionId where x.Type == 1 where (h.status == 1 || h.status == 3 || h.status == 4 || h.status == 5) where p.Refunded == null where x.Archived == 0 where r.Gender == "M" where r.Disability == 1 where p.DateJoined >= lowestDateJoined where p.DateJoined <= highestDateJoined where x.SU == su.Id orderby r.Surname ascending select new { r.Id, r.FirstName, r.Surname, x.Name, SportId = x.Id };

            rptReport.DataSource = memberships;
            rptReport.DataBind();

            int i = 0;

            int totalsessions = 0;
            int totalt1 = 0;
            int totalt2 = 0;
            int totalt3 = 0;

            var participation = from p in db.tblRegisters join d in db.tblSessions on p.SessionId equals d.Id where p.AttendanceStatus == 1 select new { p.StudentId, d.StartDateTime, d.SportSocietyId };


            foreach (var membership in memberships)
            {
                ((Label)rptReport.Items[i].FindControl("lblName")).Text = membership.FirstName + " " + membership.Surname;
                ((Label)rptReport.Items[i].FindControl("lblSportSoc")).Text = membership.Name;

                var filterpart = from d in participation where d.SportSocietyId == membership.SportId where (d.StudentId == membership.Id) select d;
                var totalparticipation = filterpart.Count();

                ((Label)rptReport.Items[i].FindControl("lblTotal")).Text = totalparticipation.ToString();

                var filterpartt1 = from d in participation where d.SportSocietyId == membership.SportId where (d.StudentId == membership.Id) where d.StartDateTime.Value.Month >= 9 where d.StartDateTime.Value.Month <= 12 select d;
                var totalparticipationt1 = filterpartt1.Count();

                ((Label)rptReport.Items[i].FindControl("lblT1")).Text = totalparticipationt1.ToString();


                var filterpartt2 = from d in participation where d.SportSocietyId == membership.SportId where (d.StudentId == membership.Id) where d.StartDateTime.Value.Month >= 1 where d.StartDateTime.Value.Month <= 3 select d;
                var totalparticipationt2 = filterpartt2.Count();

                ((Label)rptReport.Items[i].FindControl("lblT2")).Text = totalparticipationt2.ToString();

                var filterpartt3 = from d in participation where d.SportSocietyId == membership.SportId where (d.StudentId == membership.Id) where d.StartDateTime.Value.Month >= 4 where d.StartDateTime.Value.Month <= 7 select d;
                var totalparticipationt3 = filterpartt3.Count();

                ((Label)rptReport.Items[i].FindControl("lblT3")).Text = totalparticipationt3.ToString();

                if (totalparticipation == 0)
                {
                    rptReport.Items[i].Visible = false;
                }

                totalsessions = totalsessions + totalparticipation;
                totalt1 = totalt1 + totalparticipationt1;
                totalt2 = totalt2 + totalparticipationt2;
                totalt3 = totalt3 + totalparticipationt3;
                i++;
            }

            lblTotalSes.Text = totalsessions.ToString();
            lblTotalT1.Text = totalt1.ToString();
            lblTotalT2.Text = totalt2.ToString();
            lblTotalT3.Text = totalt3.ToString();

            try
            {
                var totaldisabled = memberships.Count();
                lblAverage.Text = (totalsessions / totaldisabled).ToString();
                lblT1Average.Text = (totalt1 / totaldisabled).ToString();
                lblT2Average.Text = (totalt2 / totaldisabled).ToString();
                lblT3Average.Text = (totalt3 / totaldisabled).ToString();
             lblTotalMen.Text = totaldisabled.ToString();
      }
            catch
            {

            }
             }
    }
}