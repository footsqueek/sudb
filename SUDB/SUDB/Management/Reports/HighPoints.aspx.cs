﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDB.Management.Reports
{
    public partial class HighPoints : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            returnPoints();
        }

        protected void returnPoints()
        {
            Common tools = new Common();
            var students = tools.returnPointsGreaterThan(10);
            rptstudents.DataSource = students;
            rptstudents.DataBind();
            int i = 0;

            foreach(var student in students)
            {
                ((Label)rptstudents.Items[i].FindControl("lblStudentNo")).Text = student.studentNo;
                ((Label)rptstudents.Items[i].FindControl("lblName")).Text = student.studentFirstname + " " + student.studentSurname;
                ((Label)rptstudents.Items[i].FindControl("lblPoints")).Text = student.totalPoints.ToString() + " Points";
                i++;

            }
        }
    }
}