﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SUDB.Classes;
using SUDatabase.Classes;

namespace SUDB.Management.Reports.QRCodes
{
    public partial class ScanDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Guid token = Guid.Parse(Request.QueryString["token"]);
                Common tools = new Common();
                var tokenis = tools.getToken(token);
                lblTokenDesc.Text = tokenis.Description;

                bindTokens(token);
            }
            catch
            {

            }
        }

        protected void bindTokens(Guid token)
        {
           
            Common tools = new Common();
            var tokens = tools.getPointTransactions(token);

            rptTokenCodes.DataSource = tokens;
            rptTokenCodes.DataBind();

            int i = 0;

            foreach (var trans in tokens)
            {
                var student = tools.returnStudent((int)trans.StudentId);
                ((Label)rptTokenCodes.Items[i].FindControl("lblScanDate")).Text = ((DateTime)trans.datetime).ToString("dd/MM/yyyy HH:mm");
                ((Label)rptTokenCodes.Items[i].FindControl("lblPoints")).Text = trans.NumberOfPoints.ToString();
                ((Label)rptTokenCodes.Items[i].FindControl("lblStudentName")).Text = student.FirstName + " " + student.Surname;
                ((Label)rptTokenCodes.Items[i].FindControl("lblEmail")).Text = student.StudentNumber + tools.getSU().emailextension;
                i++;


            }
            if(tokens.Count()==0)
            {
                litError.Visible = true;
            }
            else
            {
                litError.Visible = false;
            }
        }
    }
}