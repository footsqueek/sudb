﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SUDB.Classes;
using SUDatabase.Classes;

namespace SUDB.Management.Reports.QRCodes
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bindTokens();
        }

        protected void bindTokens()
        {
            Common tools = new Common();
            var tokens = tools.getTokens();

            rptTokenCodes.DataSource = tokens;
            rptTokenCodes.DataBind();

            int i = 0;

            foreach(var token in tokens)
            {
                ((Label)rptTokenCodes.Items[i].FindControl("lblDescription")).Text = "<a href=\"ScanDetails.aspx?token="+token.tokenCode.ToString()+"\">" +token.Description+"</a>";
                if (token.RedemptionsLeft == -1)
                {
                    ((Label)rptTokenCodes.Items[i].FindControl("lblRedemptions")).Text = "Unlimited";

                }
                else
                {
                    ((Label)rptTokenCodes.Items[i].FindControl("lblRedemptions")).Text = token.RedemptionsLeft.ToString();
                }
                    ((Label)rptTokenCodes.Items[i].FindControl("lblPoints")).Text = token.NumberOfPoints.ToString();
                ((Label)rptTokenCodes.Items[i].FindControl("lblTotalScans")).Text = tools.getPointTransactions(token.tokenCode).Count().ToString();
                i++;


            }
            if (tokens.Count() == 0)
            { 

                litError.Visible = true;

            }   
            else
            {
                litError.Visible = false;
            }
        }
    }
}