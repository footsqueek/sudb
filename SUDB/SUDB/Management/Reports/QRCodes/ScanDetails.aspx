﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ScanDetails.aspx.cs" Inherits="SUDB.Management.Reports.QRCodes.ScanDetails" %>
<%@ Register src="../../../UserControls/ReportsMenu.ascx" tagname="ReportsMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:ReportsMenu ID="ReportsMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Token Transactions</h1>

    <p>The following table contains detailed scan information for the token <asp:Label ID="lblTokenDesc" runat="server" Text=""></asp:Label></p>

    <table class="nicetable">
        <tr>
            <th>Scan Date</th>
            <th>Number of Points</th>
            <th>Student Name</th>
            <th>E-Mail Address</th>

        </tr>

        <asp:Repeater ID="rptTokenCodes" runat="server">
            <ItemTemplate>
                <tr>
                    <td><asp:Label ID="lblScanDate" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblPoints" runat="server" Text="Label"></asp:Label></td>
              <td><asp:Label ID="lblStudentName" runat="server" Text="Label"></asp:Label></td>
              <td><asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label></td>
              
                </tr>

            </ItemTemplate>

        </asp:Repeater>



    </table>



    <asp:Literal ID="litError" Visible="false" runat="server"><p class="error">There are no transactions associated with this token.</p></asp:Literal>


</asp:Content>
