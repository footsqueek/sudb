﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDB.Management.Reports.QRCodes.Default" %>
<%@ Register src="../../../UserControls/ReportsMenu.ascx" tagname="ReportsMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:ReportsMenu ID="ReportsMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>QR Code Scan Reports</h1>

    <p>Below are a list of QR Codes in the SUDB System. Click on a QR Code to see more detailed information.</p>

    <table class="nicetable">
        <tr>
            <th>Token Descripton</th>
            <th>Redemptions Left</th>
            <th>Number of Points</th>
            <th>Number of Scans</th>

        </tr>

        <asp:Repeater ID="rptTokenCodes" runat="server">
            <ItemTemplate>
                <tr>
                    <td><asp:Label ID="lblDescription" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblRedemptions" runat="server" Text="Label"></asp:Label></td>
              <td><asp:Label ID="lblPoints" runat="server" Text="Label"></asp:Label></td>
              <td><asp:Label ID="lblTotalScans" runat="server" Text="Label"></asp:Label></td>
              
                </tr>

            </ItemTemplate>

        </asp:Repeater>



    </table>
     <asp:Literal ID="litError" Visible="false" runat="server"><p class="error">There are no tokens in the system</p></asp:Literal>


</asp:Content>
