﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDB.Management.Reports
{
    public partial class MembersByType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bindType();
        }

        protected void bindType()
        {
            Common tools = new Common();
            var memtype = tools.returnTotalIndividualSportSocietyMembersType();

            rptType.DataSource = memtype;
            rptType.DataBind();

            int i = 0;

            foreach(var type in memtype)
            {
                ((Label)rptType.Items[i].FindControl("lblType")).Text = "<a href=\"MembersByTypeDrilldown.aspx?type="+type.memTypeId.ToString()+"\">" + type.memType +"</a>";
                ((Label)rptType.Items[i].FindControl("lblNo")).Text = type.count.ToString();
                i++;
            }
        }
    }
}