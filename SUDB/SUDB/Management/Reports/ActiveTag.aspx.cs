﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDB.Management.Reports
{
    public partial class ActiveTag : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindTags();
            }
        }

        protected void bindTags()
        {
            Common tools = new Common();

            var tags = tools.returnActiveTags();

            rptTags.DataSource = tags;
            rptTags.DataBind();

            int i = 0;

            foreach (var tag in tags)
            {
                ((Label)rptTags.Items[i].FindControl("lblTag")).Text = tag.Tag;
                ((Label)rptTags.Items[i].FindControl("lblNoCases")).Text = tools.returnnotagcases(tag.Id).ToString();

                ((Label)rptTags.Items[i].FindControl("lblTime")).Text = tools.returnTimetagcases(tag.Id).ToString() + " Minutes";



                i++;
            }

        }
    }
}