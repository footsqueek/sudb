﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="HighPoints.aspx.cs" Inherits="SUDB.Management.Reports.HighPoints" %>
<%@ Register src="../../UserControls/ReportsMenu.ascx" tagname="ReportsMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:ReportsMenu ID="ReportsMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h1>High Points</h1>

    <p>This report shows students who have more than 10 points</p>

    <table class="nicetable">
        <tr>
            <th>Student Number</th>
            <th>Name</th>
            <th>Points</th>
        </tr>
    <asp:Repeater ID="rptstudents" runat="server">
        <ItemTemplate>
            <tr>
                <td>
                    <asp:Label ID="lblStudentNo" runat="server" Text="Label"></asp:Label></td>
          <td>
                    <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label></td>
          <td>
                    <asp:Label ID="lblPoints" runat="server" Text="Label"></asp:Label></td>
          
                  </tr>

        </ItemTemplate>

    </asp:Repeater>
        </table>
</asp:Content>
