﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDB.Management.Reports
{
    public partial class AllMembersByTypeDrilldown : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bindStudents();

            }
            catch
            {
                Response.Redirect("MembersByType.aspx");
            }
        }

        protected void bindStudents()
        {
            Common tools = new Common();
            var students = tools.returnAllMembersType(int.Parse(Request.QueryString["type"]));

            var type = tools.ReturnStudentMembershipType(int.Parse(Request.QueryString["type"]));
            lblType.Text = type.MembershipType;

            int i = 0;
            rptStudents.DataSource = students;
            rptStudents.DataBind();

            foreach(var student in students)
            {
                ((Label)rptStudents.Items[i].FindControl("lblStudentNo")).Text = student.StudentNumber;
                ((Label)rptStudents.Items[i].FindControl("lblName")).Text = student.FirstName + " " + student.Surname;
                i++;

            }
        }
    }
}