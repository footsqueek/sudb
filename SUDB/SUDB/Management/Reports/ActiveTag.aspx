﻿<%@ Page Title="Active Welfare Tag Report" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ActiveTag.aspx.cs" Inherits="SUDB.Management.Reports.ActiveTag" %>
<%@ Register src="../../UserControls/ReportsMenu.ascx" tagname="ReportsMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:ReportsMenu ID="ReportsMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Active Welfare Tag Report</h1>

       <p>The following tags are available to cases in the welfare system.</p>

    <table class="nicetable">

      <tr>

          <th>Tag</th>
          <th>Number of Cases</th>
          <th>Total Time</th>

      </tr>
 

    <asp:Repeater ID="rptTags" runat="server">

        <ItemTemplate>

            <tr>

                <td>
                    <asp:Label ID="lblTag"  runat="server" Text="Label"></asp:Label></td>


                 <td>
                    <asp:Label ID="lblNoCases"  runat="server" Text="0"></asp:Label></td>

                 <td>
                    <asp:Label ID="lblTime"  runat="server" Text="0 Minutes"></asp:Label></td>




                     </tr>

        </ItemTemplate>


    </asp:Repeater>


           </table>

</asp:Content>
