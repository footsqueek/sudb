﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ViewTrip.aspx.cs" Inherits="SUDB.Management.DutyOfCare.TripRegistration.ViewTrip" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <h1>Trip Registration</h1>

    <p>Every activity session that takes places away from University campus must have an approved trip registration prior to the activity taking place.</p>

   


    <h2>Trip Details</h2>

    <p>The current status of this trip is <strong><asp:Label ID="lblStatus" runat="server" Text="Not Yet Reviewed"></asp:Label></strong>. It was reviewed by <strong><asp:Label ID="lblStaff" runat="server" Text="a staff member"></asp:Label></strong> on <strong><asp:Label ID="lblReviewDate" runat="server" Text="Label"></asp:Label></strong></p>

     <p>
        <asp:Label ID="Label5" CssClass="label" runat="server" Text="Sport / Society"></asp:Label>
         <asp:TextBox ID="txtSportSocoety" Enabled="false" Text="Sport or Society" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>
     <p>
        <asp:Label ID="lblDepartureTime" CssClass="label" runat="server" Text="Departure Date / Time"></asp:Label>
         <asp:TextBox ID="txtDepartureTime" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>

    
     <p>
        <asp:Label ID="Label1" CssClass="label" runat="server" Text="Return Date / Time"></asp:Label>
         <asp:TextBox ID="txtReturnDateTime"  CssClass="txtbox" runat="server"></asp:TextBox>

    </p>

    
     <p>
        <asp:Label ID="Label2" CssClass="label" runat="server" Text="Address of Destination"></asp:Label>
         <asp:TextBox ID="txtDestinationAddress"  CssClass="txtbox" runat="server"></asp:TextBox>

    </p>

   
 
    <table class="lefttable nicetable">
        <tr>
            <th>Member Name</th>
            <th>Trip Attendee</th>
            <th>Driver</th>
            <th>1st Aider</th>
            <th>Mode of Transport</th>

        </tr>
    <asp:Repeater ID="rptRegister" runat="server">

        <ItemTemplate>

            <tr>
                <asp:Label ID="lblId" Visible="false" runat="server" Text=""></asp:Label> 
                <td><asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>  </td>
                <td><div class="onoffswitch">
          
    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" runat="server" id="chkAttendee">
    <label class="onoffswitch-label" runat="server" id="lblattendee">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</div>                    
</td>

                   <td><div class="onoffswitch">
                 
    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" runat="server" id="chkDriver">
    <label class="onoffswitch-label" runat="server" id="lblDriver">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</div>                    
</td>

                   <td>
                       
                       <asp:Image Height="25px" ID="imgFirstAid" ImageUrl="~/Images/Icons/first-aid.jpg" runat="server" />
                                        
</td>

                <td><asp:DropDownList ID="cboModeofTransport" runat="server"></asp:DropDownList>  </td>

            </tr>

        </ItemTemplate>

    </asp:Repeater>
    </table>

     
    <h2>Trip Approval</h2>
    <p>Before you can go on a trip your trip registration must be approved. Details of your trip approval are shown below.</p>
        <p>
        <asp:Label ID="Label4" CssClass="label" runat="server" Text="Staff Member"></asp:Label>
            <asp:Label ID="lblStaffMember" runat="server" Text="No staff member assigned"></asp:Label>

    </p>

     <p>
        <asp:Label ID="Label3" CssClass="label" runat="server" Text="Staff Comments"></asp:Label>
         <asp:TextBox ID="txtComments" Enabled="false" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>

    
    <asp:Button ID="btnReturn" CssClass="submitbutton" runat="server" Text="Back to Trips" OnClick="btnReturn_Click" />


  
</asp:Content>
