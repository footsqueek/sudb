﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDB.Management.TripRegistration
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();
            if (!tools.checkAccess("TripRegistration").Equals("Full Access"))
                Response.Redirect("/Default.aspx");
            else
                bindApproved();
          
        }


        protected void btnTripReg_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);

            //Set Active Sport Soc
            Common tools = new Common();

            var session = tools.returnSession(int.Parse(btnView.CommandArgument));

            tools.setActiveSportSociety((int)session.SportSocietyId);

            Response.Redirect("ViewTrip.aspx?sessionId=" + btnView.CommandArgument);
        }

        protected void bindApproved()
        {
            Common tools = new Common();
            var trips = tools.returnTripsApproved();

            int i = 0;
            rptApproved.DataSource = trips;
            rptApproved.DataBind();

            foreach (var trip in trips)
            {
                var society = tools.returnSportSociety((int)trip.SportSocietyId);
                ((Label)rptApproved.Items[i].FindControl("lblSportSociety")).Text = society.Name;

                ((Label)rptApproved.Items[i].FindControl("lblDestination")).Text = trip.TripDestinationAddress;
                ((Label)rptApproved.Items[i].FindControl("lblDeparting")).Text = ((DateTime)trip.DepartureTime).ToString("dd MMM yyyy HH:mm");
                ((Label)rptApproved.Items[i].FindControl("lblReturning")).Text = ((DateTime)trip.ReturnTime).ToString("dd MMM yyyy HH:mm");

                try
                {
                    var session = tools.returnSessionFromTrip(trip.tripId);

                    ((Button)rptApproved.Items[i].FindControl("btnTrip")).CommandArgument = session.Id.ToString();
                }
                catch
                {
                    rptApproved.Items[i].Visible = false;
                }

                i++;
            }
        }
    }
}