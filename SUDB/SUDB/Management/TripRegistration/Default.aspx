﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDB.Management.TripRegistration.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script>
         $(document).ready(function () {
             $("#tablesorted").tablesorter();
            
         }
 );

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Trip Registration</h1>

    <p>All approved and active trip registrations are shown below. Active trip registrations include all trips that have departed in the last 10 days, and all trips scheduled in the future.</p>

       <h2>Upcoming and Active Approved Trips</h2>

    <table id="tablesorted" class="nicetable">
        <thead>
        <tr>

               <th>Sport or Society</th>
            <th>Destination</th>
            <th>Departing</th>
            <th>Returning</th>

            <th>Actions</th>

        </tr>
            </thead>
        <tbody>
    <asp:Repeater ID="rptApproved" runat="server">

          <ItemTemplate>

            <tr>

                   <td> <asp:Label ID="lblSportSociety" runat="server" Text=""></asp:Label>  </td>
                   <td> <asp:Label ID="lblDestination" runat="server" Text=""></asp:Label> </td>
                   <td> <asp:Label ID="lblDeparting" runat="server" Text=""></asp:Label> </td>
                   <td> <asp:Label ID="lblReturning" runat="server" Text=""></asp:Label> </td>

                   <td> <asp:Button id="btnTrip"
           Text="View Trip"
                    CssClass="standardbutton"
           CommandName="ViewTrip"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnTripReg_Click" 
           runat="server"/> </td>

            </tr>


        </ItemTemplate>



    </asp:Repeater>
            </tbody>
    </table>

</asp:Content>
