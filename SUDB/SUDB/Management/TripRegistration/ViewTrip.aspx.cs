﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace SUDB.Management.DutyOfCare.TripRegistration
{
    public partial class ViewTrip : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Common tools = new Common();
            if (!tools.checkAccess("TripRegistration").Equals("Full Access"))
                Response.Redirect("/Default.aspx");
            else
            bindSessionRegister();
        }

        public void bindSessionRegister()
        {
            Common tools = new Common();



            var register = tools.returnMembersforSession(int.Parse(Request.QueryString["sessionId"]));

            var session = tools.returnSession(int.Parse(Request.QueryString["sessionId"]));

            var sport = tools.returnSportSociety((int)session.SportSocietyId);
            txtSportSocoety.Text = sport.Name;

            var trip = tools.returnTrip((int)session.TripRegId);

            //Check if trip is in active sport or society
            if (trip.SportSocietyId != tools.returnActiveSportSoc().Id)
            {
                Response.Redirect("../Default.aspx");
            }

            try
            {
                txtDepartureTime.Text = ((DateTime)trip.DepartureTime).ToString("dd MMM yyyy HH:mm");
                txtReturnDateTime.Text = ((DateTime)trip.ReturnTime).ToString("dd MMM yyyy HH:mm");
                txtDestinationAddress.Text = trip.TripDestinationAddress;

                if (!IsPostBack)
                {
                    txtComments.Text = trip.ApprovalComments;
                }
                try
                {
                    lblStaffMember.Text = System.Web.Security.Membership.GetUser((Guid)trip.tripReviewedBy).UserName;
                    lblStaff.Text = System.Web.Security.Membership.GetUser((Guid)trip.tripReviewedBy).UserName;
                    try
                    {
                        ProfileBase profile = ProfileBase.Create(System.Web.Security.Membership.GetUser((Guid)trip.tripReviewedBy).UserName);
                        lblStaffMember.Text = profile["FirstName"] + " " + profile["LastName"];
                        lblStaff.Text = profile["FirstName"] + " " + profile["LastName"];
                    }
                    catch
                    {

                    }
                    if (trip.reviewdate != null)
                    {
                        lblReviewDate.Text = ((DateTime)trip.reviewdate).ToString("dd/MM/yyyy at hh:mm");
                    }
                    else
                    {
                        lblReviewDate.Text = "an unknown date";
                    }
                }
                catch
                {

                }

                if (trip.ApprovalStatus == 2)
                {
                    lblStatus.Text = "Approved";
                }

                if (trip.ApprovalStatus == 3)
                {
                    lblStatus.Text = "Rejected";
                }

                if (trip.ApprovalStatus == 0)
                {
                    lblStatus.Text = "Not Yet Submitted";
                }
            }
            catch
            {

            }

            if (txtDepartureTime.Text == "")
            {
                if (session.Depart != null)
                {
                    txtDepartureTime.Text = ((DateTime)session.Depart).ToString("dd MMM yyyy HH:mm");
                    txtDestinationAddress.Text = session.Location + " " + session.PostCode;
                }
                else
                {
                    txtDepartureTime.Text = ((DateTime)session.StartDateTime).ToString("dd MMM yyyy HH:mm");
                    txtDestinationAddress.Text = session.Location + " " + session.PostCode;
                }
                txtReturnDateTime.Text = ((DateTime)session.EndDateTime).ToString("dd MMM yyyy HH:mm");
            }


            if (trip.ApprovalStatus == 2)
            {
                //Trip Approved no Changes
                txtDepartureTime.Enabled = false;
                txtDestinationAddress.Enabled = false;
                txtReturnDateTime.Enabled = false;
          
            }



            rptRegister.DataSource = register;
            rptRegister.DataBind();



            int i = 0;

            foreach (var reg in register)
            {
                var student = tools.returnStudent((int)reg.StudentId);
                ((Label)rptRegister.Items[i].FindControl("lblId")).Text = reg.Id.ToString();

                ((Label)rptRegister.Items[i].FindControl("lblName")).Text = student.FirstName + " " + student.Surname;

                ((HtmlInputCheckBox)rptRegister.Items[i].FindControl("chkAttendee")).ID = reg.Id.ToString();
                ((HtmlGenericControl)rptRegister.Items[i].FindControl("lblattendee")).Attributes.Add("for", reg.Id.ToString());
                ((HtmlInputCheckBox)rptRegister.Items[i].FindControl("chkDriver")).ID = reg.Id.ToString() + "Driver";
                ((HtmlGenericControl)rptRegister.Items[i].FindControl("lblDriver")).Attributes.Add("for", reg.Id.ToString() + "Driver");



                if (student.MiniBusAssessment != 1)
                {
                    ((HtmlInputCheckBox)rptRegister.Items[i].FindControl(reg.Id.ToString() + "Driver")).Visible = false;
                    ((HtmlGenericControl)rptRegister.Items[i].FindControl("lblDriver")).Visible = false;

                }

                if (student.FirstAidCertExpiry > session.EndDateTime)
                {
                    ((Image)rptRegister.Items[i].FindControl("imgFirstAid")).Visible = true;

                }
                else
                {
                    ((Image)rptRegister.Items[i].FindControl("imgFirstAid")).Visible = false;
                }


                var mode = tools.returnModeofTransport();

                foreach (var transport in mode)
                {
                    ((DropDownList)rptRegister.Items[i].FindControl("cboModeofTransport")).Items.Add(new ListItem(transport.TransportMode.ToString(), transport.Id.ToString())); ;

                }




                if (!IsPostBack)
                {
                    try
                    {
                        var attendeestatus = tools.returnTripAttendeeStatus((int)trip.tripId, (int)reg.StudentId);

                        if (attendeestatus.attendanceStatus == 1)
                        {
                            ((HtmlInputCheckBox)rptRegister.Items[i].FindControl(reg.Id.ToString())).Checked = true;

                        }
                        else
                        {
                            //Hide this attendee
                            rptRegister.Items[i].Visible = false;
                        }

                        if (attendeestatus.Driver == 1)
                        {
                            ((HtmlInputCheckBox)rptRegister.Items[i].FindControl(reg.Id.ToString() + "Driver")).Checked = true;

                        }

                        ((DropDownList)rptRegister.Items[i].FindControl("cboModeofTransport")).SelectedValue = attendeestatus.ModeofTransport.ToString();


                    }
                    catch
                    {
                        //Leave Empty
                    }

                }

                if (trip.ApprovalStatus == 2)
                {
                    //Trip is Approved
                    ((HtmlInputCheckBox)rptRegister.Items[i].FindControl(reg.Id.ToString() + "Driver")).Attributes.Add("Disabled", "true");
                    ((HtmlInputCheckBox)rptRegister.Items[i].FindControl(reg.Id.ToString())).Attributes.Add("Disabled", "true");
                    ((DropDownList)rptRegister.Items[i].FindControl("cboModeofTransport")).Enabled = false;
                }

                i++;
            }

          
                txtComments.Enabled = false;
              
           
              
            
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}