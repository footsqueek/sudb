﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using SUDB;

namespace SUDatabase.Management
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Roles.IsUserInRole("Staff"))
            {
                Response.Redirect("/");
            }

            bindStaffOpenCases();
            bindMyReminders();
            bindStats();
        }

        protected void bindStats()
        {

            Common tools = new Common();

            var user = Membership.GetUser();

            var totalstaffcases = tools.returnNoStaffCases((Guid)user.ProviderUserKey);
            var totalstaffcasesclose = tools.returnNoStaffCasesClosed((Guid)user.ProviderUserKey);

            var totalcases = tools.returnNoCases();
            var totalcasesclose = tools.returnNoCasesClosed();

            var totalsportsocmembers = tools.returnTotalSportSocietyMembers();
            var totstudents = tools.returnNoStudents();

            lblCasesAllocatedToYou.Text = totalstaffcases.ToString() + " (" + totalstaffcasesclose.ToString() + ")";

            lblTotalCases.Text = totalcases.ToString() + " (" + totalcasesclose.ToString() + ")";

            lblTotalStudentRecords.Text = totstudents.ToString();
            lblSportsSocMembers.Text = totalsportsocmembers.ToString();

        }

      protected void bindStaffOpenCases()
            {

                Common tools = new Common();


                var member = Membership.GetUser(User.Identity.Name);
                var cases = tools.ReturnOpenStaffCases((Guid)member.ProviderUserKey);

           
            

            rptOpenCases.DataSource = cases;
            rptOpenCases.DataBind();

            int i = 0;

            foreach (tblCase curcase in cases)
            {
                //Lookup Student
                var student = tools.returnStudent((int)curcase.StudentId);

                ((Label)rptOpenCases.Items[i].FindControl("lblCaseType")).Text = ((tblCaseType)tools.returnCaseType((int)curcase.caseType)).CaseType;
                ((Label)rptOpenCases.Items[i].FindControl("lblDateOpened")).Text = ((DateTime)curcase.dateOpened).ToString("dd MMM yyyy HH:mm");
                ((Label)rptOpenCases.Items[i].FindControl("lblStudentName")).Text = student.FirstName + " " + student.Surname + " (" + student.StudentNumber + ")";

              
                ((Button)rptOpenCases.Items[i].FindControl("btnView")).CommandArgument = curcase.caseId.ToString();




                i++;
            }


            if (i == 0)
            {
                litNoOpen.Visible = true;
            }
            }


        protected void bindMyReminders()
        {

            Common tools = new Common();


            var member = Membership.GetUser(User.Identity.Name);
            var cases = tools.ReturnOpenStaffReminders((Guid)member.ProviderUserKey);




            rptReminders.DataSource = cases;
            rptReminders.DataBind();

            int i = 0;

            foreach (tblReminder curcase in cases)
            {

                var getcase = tools.ReturnCase((int)curcase.linkedCase);

                //Lookup Student
                var student = tools.returnStudent((int)getcase.StudentId);

                ((Label)rptReminders.Items[i].FindControl("lblCaseType")).Text = ((tblCaseType)tools.returnCaseType((int)getcase.caseType)).CaseType;
                ((Label)rptReminders.Items[i].FindControl("lblReminderNotes")).Text = Server.HtmlDecode(curcase.reminderNotes);

                ((Label)rptReminders.Items[i].FindControl("lblDueDate")).Text = ((DateTime)curcase.actionDate).ToString("dd MMM yyyy");
                ((Label)rptReminders.Items[i].FindControl("lblStudentName")).Text = student.FirstName + " " + student.Surname + " (" + student.StudentNumber + ")";


                ((Button)rptReminders.Items[i].FindControl("btnView")).CommandArgument = curcase.linkedCase.ToString();

                ((Button)rptReminders.Items[i].FindControl("btnCloseReminder")).CommandArgument = curcase.reminderId.ToString();



                i++;
            }


            if (i == 0)
            {
                litNoReminders.Visible = true;
            }
        }

        public void btnView_Clickbutton(object sender, CommandEventArgs e)
      {
          Button btnView = ((Button)sender);
          Response.Redirect("Cases/ViewCase.aspx?caseId=" + btnView.CommandArgument);
      }

        public void btnCloseReminder_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);

            Common tools = new Common();
            tools.closeReminder(int.Parse(btnView.CommandArgument));

            bindMyReminders();
        }
    }
}