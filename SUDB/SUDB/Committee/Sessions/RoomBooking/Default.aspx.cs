﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Committee.Sessions.RoomBooking
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int roomId = int.Parse(Request.QueryString["roomId"]);

                Common tools = new Common();

                var room = tools.returnRoomRequest(roomId);

                if (room != null)
                {
                    txtDetails.Text = room.RequestDetails;
                    txtResponse.Text = room.UnionResponse;
                    txtHeadline.Text = room.Headline;
                    try
                    {
                        if (room.RequestStatus == 1)
                        {
                            cboStatus.SelectedValue = room.RequestStatus.ToString();
                        }
                        else
                        {
                            cboStatus.SelectedValue = "0";
                        }
                    }
                    catch
                    {
                        //leave it set as zero!
                    }

                    var sessions = tools.returnSessionsByRoom(roomId);

                    rptsessions.DataSource = sessions;
                    rptsessions.DataBind();

                    int i = 0;

                    foreach (var session in sessions)
                    {
                        ((Label)rptsessions.Items[i].FindControl("lblSessionName")).Text = session.SessionName;
                        ((Label)rptsessions.Items[i].FindControl("lblStart")).Text = ((DateTime)session.StartDateTime).ToString();
                        ((Label)rptsessions.Items[i].FindControl("lblEnd")).Text = ((DateTime)session.EndDateTime).ToString();
                        i++;
                    }

                     if (tools.checkAccess("DutyOfCare") == "Full Access" || (tools.checkAccess("DutyOfCare") == "Read Only"))
                {
                    cboStatus.Enabled = true;
                    txtResponse.Enabled = true;
                }
                else
                {
                    if(room.RequestStatus==1)
                    {
                        txtHeadline.Enabled = false;
                        txtDetails.Enabled = false;

                    }

                }

                }
                else
                {
                    Response.Redirect("../");
                }

              
               
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Common tools = new Common();

            tools.updateRoomRequest(int.Parse(Request.QueryString["roomId"]), txtHeadline.Text, txtDetails.Text, txtResponse.Text, int.Parse(cboStatus.SelectedValue));

            Response.Redirect("../");
           
        }
    }
}