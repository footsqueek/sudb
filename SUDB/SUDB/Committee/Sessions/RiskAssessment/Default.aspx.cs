﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace SUDatabase.Committee.Sessions.RiskAssessment
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
                bindRiskQuestions();
            
        }

        protected void bindRiskQuestions()
        {
            Common tools = new Common();

            var riskquestions = tools.returnRiskQuestions();

            if (!IsPostBack)
            {
                rptRiskAssessment.DataSource = riskquestions;
                rptRiskAssessment.DataBind();
            }

            var session = tools.returnSession(int.Parse(Request.QueryString["sessionId"]));

            lblActivityName.Text = session.SessionName;
            lblStart.Text = ((DateTime)session.StartDateTime).ToString("dd/MM/yyyy HH:mm");
            lblEnd.Text = ((DateTime)session.EndDateTime).ToString("dd/MM/yyyy HH:mm");
            int i = 0;

            foreach(var question in riskquestions)
            {
               
                ((Label)rptRiskAssessment.Items[i].FindControl("lblId")).Text = question.Id.ToString();
                ((Label)rptRiskAssessment.Items[i].FindControl("lblRiskHeading")).Text = question.RiskHeading;
                ((Label)rptRiskAssessment.Items[i].FindControl("lblRiskInstruction")).Text = question.RiskInstruction;
                ((HtmlInputCheckBox)rptRiskAssessment.Items[i].FindControl("myonoffswitch")).ID = question.Id.ToString();
                ((HtmlGenericControl)rptRiskAssessment.Items[i].FindControl("lblBut")).Attributes.Add("for", question.Id.ToString());
              
                    try
                    {
                        var riskstatus = tools.returnRiskStatus((int)session.Id, (int)question.Id);

                        if (riskstatus.Status == 1)
                        {
                            ((HtmlInputCheckBox)rptRiskAssessment.Items[i].FindControl(question.Id.ToString())).Checked = true;

                        }
                        if (!IsPostBack)
                        {
                            ((TextBox)rptRiskAssessment.Items[i].FindControl("txtWhoAtRisk")).Text = riskstatus.WhoIsAtRisk;
                            ((TextBox)rptRiskAssessment.Items[i].FindControl("txtActionTaken")).Text = riskstatus.ActionTaken;
                        }
                    }
                    catch
                    {
                        //Leave switch as off
                    }
                

                if (((DateTime)session.EndDateTime).AddDays(1) < DateTime.Now)
                {
                    ((HtmlInputCheckBox)rptRiskAssessment.Items[i].FindControl(question.Id.ToString())).Attributes.Add("Disabled", "true"); ;
                    ((TextBox)rptRiskAssessment.Items[i].FindControl("txtWhoAtRisk")).Enabled = false;
                    ((TextBox)rptRiskAssessment.Items[i].FindControl("txtActionTaken")).Enabled = false;
                    btnSubmitRiskAssessment.Enabled = false;
                }

                i++;
            }
        }

        protected void btnSubmitRiskAssessment_Click(object sender, EventArgs e)
        {

            int SessionId = int.Parse(Request.QueryString["sessionId"]);

            Common tools = new Common();

            foreach (RepeaterItem item in rptRiskAssessment.Items)
            {

                int riskId = int.Parse(((Label)item.FindControl("lblId")).Text);
                int riskstatus = 1;

                if (!((HtmlInputCheckBox)item.FindControl(riskId.ToString())).Checked)
                {
                    riskstatus = 0;
                }

                 var currentuser = System.Web.Security.Membership.GetUser(HttpContext.Current.User.Identity.Name);

                tools.submitRisk(SessionId,riskId,riskstatus,(Guid)currentuser.ProviderUserKey,((TextBox)item.FindControl("txtWhoAtRisk")).Text,((TextBox)item.FindControl("txtActionTaken")).Text,"",0);

                


            }

            tools.setRiskAssessmentComplete(SessionId);

            Response.Redirect("../Default.aspx");
        }
    }
}