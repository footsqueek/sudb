﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Committee.Sessions.RiskAssessment.Default" %>
<%@ Register src="../../../UserControls/CommitteeMenu.ascx" tagname="CommitteeMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CommitteeMenu ID="CommitteeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Health and Safety Checklist</h1>

    <p>A risk assessment must be completed prior to every activity. Failure to complete a risk assessment could lead to your Sport or Society being disciplined.</p>

    <h2><asp:Label ID="lblActivityName" runat="server" Text="Training"></asp:Label></h2>

    <p><span class="labelwidth">Start Date: <asp:Label ID="lblStart" runat="server" Text=""></asp:Label></span><br />
      <span class="labelwidth">End Date: <asp:Label ID="lblEnd" runat="server" Text=""></asp:Label></span></p>

    <table class="nicetable">

        <asp:Repeater ID="rptRiskAssessment" runat="server">

            <ItemTemplate>
                   <asp:Label ID="lblId" Visible="false" runat="server" Text="Label"></asp:Label> 
                <tr>

                    <td><h3><asp:Label ID="lblRiskHeading" runat="server" Text="Label"></asp:Label></h3>
                        <p>
                            <asp:Label ID="lblRiskInstruction" runat="server" Text="Label"></asp:Label></p>

                    </td>

                      <td><br /><div class="onoffswitchrisk">

    <input type="checkbox" name="onoffswitch" class="onoffswitchrisk-checkbox" runat="server" id="myonoffswitch">
    <label class="onoffswitchrisk-label" runat="server" id="lblBut">
        <span class="onoffswitchrisk-inner"></span>
        <span class="onoffswitchrisk-switch"></span>
    </label>
</div>                    
</td>

                </tr>

                <tr>

                    <td><strong>Who is at Risk</strong><asp:TextBox CssClass="txtbox" ID="txtWhoAtRisk" runat="server"></asp:TextBox></td>

                </tr>

                  <tr>

                    <td><strong>Action Taken</strong><asp:TextBox CssClass="txtbox" ID="txtActionTaken" runat="server"></asp:TextBox></td>

                </tr>

            </ItemTemplate>

        </asp:Repeater>


    </table>

    <p>If the answer is NO to any of the following - Please complete the box below the risk (who / what are at risk and the action taken).</p>

    <p>I click submit with the knowledge that I have carried out a risk assessment for this activity to the best of my ability, immediately prior to its commencement.</p>

    <asp:Button ID="btnSubmitRiskAssessment" runat="server" CssClass="submitbutton" Text="Submit H&amp;S Checklist" OnClick="btnSubmitRiskAssessment_Click" />

</asp:Content>
