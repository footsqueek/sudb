﻿<%@ Page Title="Sessions" Language="C#" MaintainScrollPositionOnPostback="true" EnableEventValidation="false" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Committee.Sessions.Default" %>
<%@ Register src="../../UserControls/CommitteeMenu.ascx" tagname="CommitteeMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script>
         $(document).ready(function () {
             $("#tablesorted").tablesorter();
             $("#tablesorted1").tablesorter();
            
         }
 );

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CommitteeMenu ID="CommitteeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <h1><asp:Label ID="lblSportSocietyName" runat="server" Text="Label"></asp:Label></h1>

      <p>Please select the year that you would like to view: <asp:DropDownList ID="cboYears" AutoPostBack="true" runat="server"></asp:DropDownList></p>

    <h2>Upcoming and Active Sessions</h2>

     <table id="tablesorted" class="nicetable">
         <thead>
        <tr>

     
            <th>Session Name</th>
            <th>Team</th>
            <th>Location</th>
            <th>Start Date / Time</th>

            <th>End Date / Time</th>
            <th>Required Submissions</th>


        </tr>
</thead>
         <tbody>
    <asp:Repeater ID="rptActiveSessions" runat="server">

        <ItemTemplate>

            <tr>
                   <td> <asp:Label ID="lblSessionName" runat="server" Text="Label"></asp:Label> </td>
                <td> <asp:Label ID="lblTeam" runat="server" Text=""></asp:Label> </td>
                   <td> <asp:Label ID="lblLocation" runat="server" Text="Label"></asp:Label> </td>
                   <td> <asp:Label ID="lblStartDateTime" runat="server" Text="Label"></asp:Label> </td>
                   <td> <asp:Label ID="lblEndDateTime" runat="server" Text="Label"></asp:Label> </td>
                   <td>  <asp:Button id="btnRegister"
           Text="Register"
                    CssClass="standardbutton"
           CommandName="Register"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnRegister_Clickbutton" 
           runat="server"/>  <asp:Button id="btnRiskAssessment"
           Text="H&S Checklist"
                    CssClass="standardorange"
           CommandName="Register"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnRiskAssessment_Click" 
           runat="server"/> <asp:Button id="btnTripReg"
           Text="Trip Registration"
                    CssClass="standardbutton"
           CommandName="Trip Registration"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnTripReg_Click" 
           runat="server"/> <asp:Button id="btnRoom"
           Text="Room Request"
                    CssClass="standardbutton"
           CommandName="Room Request"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnRoom_Click" 
           runat="server"/> <asp:Button id="btnTransport"
           Text="Transport"
                    CssClass="standardbutton"
           CommandName="Transport"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnTransport_Click" 
           runat="server"/> <asp:Button id="btnScore"
           Text="Submit Score"
                    CssClass="standardbutton"
           CommandName="SubmitScore"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnScore_Click" 
           runat="server"/><asp:Button id="btnEdit"
           Text="Edit Session"
                    CssClass="blackbutton"
           CommandName="Edit Session"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnEdit_Click" 
           runat="server"/> <asp:Button id="btnCancel"
           Text="Cancel Session"
                    CssClass="blackbutton"
           CommandName="Cancel Session"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnCancel_Click" 
           runat="server"/> </td>

            </tr>


        </ItemTemplate>


    </asp:Repeater>
             </tbody>
    </table>

    <h2>Past Sessions</h2>

     <table id="tablesorted1" class="nicetable">
         <thead>
        <tr>

     
            <th>Session Name</th>
            <th>Location</th>
            <th>Start Date / Time</th>

            <th>End Date / Time</th>
            <th>Required Submissions</th>


        </tr>
             </thead>
         <tbody>
    <asp:Repeater ID="rptPastSessions" runat="server">

        <ItemTemplate>

            <tr>
                   <td> <asp:Label ID="lblSessionName" runat="server" Text="Label"></asp:Label> </td>
                   <td> <asp:Label ID="lblLocation" runat="server" Text="Label"></asp:Label> </td>
                   <td> <asp:Label ID="lblStartDateTime" runat="server" Text="Label"></asp:Label> </td>
                   <td> <asp:Label ID="lblEndDateTime" runat="server" Text="Label"></asp:Label> </td>
                           <td>  <asp:Button id="btnRegister"
           Text="Register"
                    CssClass="standardbutton"
           CommandName="Register"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnRegister_Clickbutton" 
           runat="server"/>  <asp:Button id="btnRiskAssessment"
           Text="H&S Checklist"
                    CssClass="standardorange"
           CommandName="Register"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnRiskAssessment_Click" 
           runat="server"/> <asp:Button id="btnTripReg"
           Text="Trip Registration"
                    CssClass="standardbutton"
           CommandName="Trip Registration"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnTripReg_Click" 
           runat="server"/> <asp:Button id="btnRoom"
           Text="Room Request"
                    CssClass="standardbutton"
           CommandName="Room Request"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnRoom_Click" 
           runat="server"/> <asp:Button id="btnTransport"
           Text="Transport"
                    CssClass="standardbutton"
           CommandName="Transport"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnTransport_Click" 
           runat="server"/> <asp:Button id="btnScore"
           Text="Submit Score"
                    CssClass="standardbutton"
           CommandName="SubmitScore"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnScore_Click" 
           runat="server"/><asp:Button id="btnEdit"
           Text="Edit Session"
                    CssClass="blackbutton"
           CommandName="Edit Session"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnEdit_Click" 
           runat="server"/> <asp:Button id="btnCancel"
           Text="Cancel Session"
                    CssClass="blackbutton"
           CommandName="Cancel Session"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnCancel_Click" 
           runat="server"/> </td>

            </tr>

            </tr>


        </ItemTemplate>


    </asp:Repeater>
             </tbody>
    </table>

    <asp:Button ID="btnCreate" CssClass="submitbutton" runat="server" Text="Create Session" OnClick="btnCreate_Click" />
</asp:Content>
