﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Committee.Sessions
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int currentyear = DateTime.Now.Year;
                if (DateTime.Now < new DateTime(DateTime.Now.Year, 08, 01))
                {
                    currentyear = currentyear - 1;
                }
                cboYears.Items.Clear();
                for (int i = 2014; i < currentyear + 1; i++)
                {
                    cboYears.Items.Add(new ListItem(i.ToString() + " / " + (i + 1).ToString().Substring(2, 2), i.ToString()));
                }
                cboYears.SelectedValue = currentyear.ToString();
            }


            Common tools = new Common();

            int id = tools.returnActiveSportSoc().Id;
            var sportsociety = tools.returnSportSociety(id);
            lblSportSocietyName.Text = sportsociety.Name;

            if (!IsPostBack)
            {
                bindActiveSessions();
                bindPastSessions();
            }
            
       }

        public void btnRegister_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            Response.Redirect("Register/Default.aspx?sessionId=" + btnView.CommandArgument);
        }

        protected void bindActiveSessions()
        {
            Common tools = new Common();
            var sessions = tools.returnFutureSessions(tools.returnActiveSportSoc().Id);

            rptActiveSessions.DataSource = sessions;
            rptActiveSessions.DataBind();

            int i = 0;

            foreach(var session in sessions)
            {

                if (session.teamId > 0)
                {
                    try
                    {
                        var team = tools.returnTeam((int)session.teamId);
                        ((Label)rptActiveSessions.Items[i].FindControl("lblTeam")).Text = team.TeamName;
                    }
                    catch
                    {
                        //No Team Returned - team deleted?
                        ((Label)rptActiveSessions.Items[i].FindControl("lblTeam")).Text = "Team Deleted";
                    }

                }
                else
                {



                }

                ((Label)rptActiveSessions.Items[i].FindControl("lblSessionName")).Text = session.SessionName;
                ((Label)rptActiveSessions.Items[i].FindControl("lblLocation")).Text = session.Location;
                ((Label)rptActiveSessions.Items[i].FindControl("lblStartDateTime")).Text = ((DateTime)session.StartDateTime).ToString("dd MMM yyyy HH:mm");
                ((Label)rptActiveSessions.Items[i].FindControl("lblEndDateTime")).Text = ((DateTime)session.EndDateTime).ToString("dd MMM yyyy HH:mm");
                ((Button)rptActiveSessions.Items[i].FindControl("btnRegister")).CommandArgument=session.Id.ToString();
                ((Button)rptActiveSessions.Items[i].FindControl("btnRiskAssessment")).CommandArgument = session.Id.ToString();
                ((Button)rptActiveSessions.Items[i].FindControl("btnCancel")).CommandArgument = session.Id.ToString();
                ((Button)rptActiveSessions.Items[i].FindControl("btnEdit")).CommandArgument = session.Id.ToString();
                ((Button)rptActiveSessions.Items[i].FindControl("btnRoom")).CommandArgument = session.linkedRoomBookingRequest.ToString();
                ((Button)rptActiveSessions.Items[i].FindControl("btnTransport")).CommandArgument = session.linkedTransportBookingRequest.ToString();

                ((Button)rptActiveSessions.Items[i].FindControl("btnTripReg")).CommandArgument = session.Id.ToString();
                ((Button)rptActiveSessions.Items[i].FindControl("btnScore")).CommandArgument = session.Id.ToString();


                if (((DateTime)session.StartDateTime) < DateTime.Now)
                {
                    if(session.BUCS==1)
                    {
                        ((Button)rptActiveSessions.Items[i].FindControl("btnScore")).Visible = true;
                    }
                    else
                    {
                        ((Button)rptActiveSessions.Items[i].FindControl("btnScore")).Visible = false;
                    }
                }
                else
                {
                    ((Button)rptActiveSessions.Items[i].FindControl("btnScore")).Visible = false;
                }

                if(session.ourScore !=null)
                {
                    ((Button)rptActiveSessions.Items[i].FindControl("btnScore")).CssClass = "standardgreen";
                }

                if (((DateTime)session.StartDateTime)<DateTime.Now.AddDays(1))
                {

                }
                else
                {
                    //Hide Register and Risk Assessment
                    ((Button)rptActiveSessions.Items[i].FindControl("btnRegister")).Visible = false;
                    ((Button)rptActiveSessions.Items[i].FindControl("btnRiskAssessment")).Visible = false;
                }


               if(session.RiskAssessmentId==1)
               {
                   ((Button)rptActiveSessions.Items[i].FindControl("btnRiskAssessment")).CssClass = "standardgreen";
               }
              

                if (session.linkedRoomBookingRequest == 0 || session.linkedRoomBookingRequest == null)
                {
                    ((Button)rptActiveSessions.Items[i].FindControl("btnRoom")).Visible = false;

                    
                }
                else
                {
                    var room = tools.returnRoomRequest((int)session.linkedRoomBookingRequest);
                    if (room.RequestStatus == 1)
                    {
                        ((Button)rptActiveSessions.Items[i].FindControl("btnRoom")).CssClass = "standardgreen";
                    }
                    else if (room.RequestStatus == 2)
                    {
                        ((Button)rptActiveSessions.Items[i].FindControl("btnRoom")).CssClass = "standardorange";
                    }
                    else if (room.RequestStatus == 0)
                    {
                        ((Button)rptActiveSessions.Items[i].FindControl("btnRoom")).CssClass = "standardblue";
                    }
                }


                if (session.linkedTransportBookingRequest == 0 || session.linkedTransportBookingRequest == null)
                {
                    ((Button)rptActiveSessions.Items[i].FindControl("btnTransport")).Visible = false;


                }
                else
                {
                    var room = tools.returnTransportRequest((int)session.linkedTransportBookingRequest);
                    if (room.RequestStatus == 1)
                    {
                        ((Button)rptActiveSessions.Items[i].FindControl("btnTransport")).CssClass = "standardgreen";
                    }
                    else if (room.RequestStatus == 2)
                    {
                        ((Button)rptActiveSessions.Items[i].FindControl("btnTransport")).CssClass = "standardorange";
                    }
                    else if (room.RequestStatus == 0)
                    {
                        ((Button)rptActiveSessions.Items[i].FindControl("btnTransport")).CssClass = "standardblue";
                    }
                }


                if(session.TripRegId ==0 || session.TripRegId ==null)
                {
                    ((Button)rptActiveSessions.Items[i].FindControl("btnTripReg")).Visible = false;
                }
                else
                {
                   
                        var trip = tools.returnTrip((int)session.TripRegId);
                        if (trip.ApprovalStatus == 2)
                        {
                            ((Button)rptActiveSessions.Items[i].FindControl("btnTripReg")).CssClass = "standardgreen";
                        }
                        else if (trip.ApprovalStatus == 1)
                        {
                            ((Button)rptActiveSessions.Items[i].FindControl("btnTripReg")).CssClass = "standardorange";
                        }
                   
                }

                if(session.RegisterComplete==1)
                {
                    ((Button)rptActiveSessions.Items[i].FindControl("btnRegister")).CssClass = "standardgreen";
                }
                else
                {
                    ((Button)rptActiveSessions.Items[i].FindControl("btnRegister")).CssClass = "standardorange";
                }

                if (((DateTime)session.StartDateTime) < DateTime.Now)
                {
                    ((Button)rptActiveSessions.Items[i].FindControl("btnCancel")).Visible = false;
                    ((Button)rptActiveSessions.Items[i].FindControl("btnEdit")).Visible = false;
                }
               
                i++;
            }
        }


        protected void bindPastSessions()
        {
            Common tools = new Common();
            var sessions = tools.returnPastSessions(tools.returnActiveSportSoc().Id);

            rptPastSessions.DataSource = sessions;
            rptPastSessions.DataBind();

            int i = 0;

            foreach (var session in sessions)
            {

                ((Label)rptPastSessions.Items[i].FindControl("lblSessionName")).Text = session.SessionName;
                ((Label)rptPastSessions.Items[i].FindControl("lblLocation")).Text = session.Location;
                ((Label)rptPastSessions.Items[i].FindControl("lblStartDateTime")).Text = ((DateTime)session.StartDateTime).ToString("dd MMM yyyy HH:mm");
                ((Label)rptPastSessions.Items[i].FindControl("lblEndDateTime")).Text = ((DateTime)session.EndDateTime).ToString("dd MMM yyyy HH:mm");
                ((Button)rptPastSessions.Items[i].FindControl("btnRegister")).CommandArgument = session.Id.ToString();
                ((Button)rptPastSessions.Items[i].FindControl("btnRiskAssessment")).CommandArgument = session.Id.ToString();
                ((Button)rptPastSessions.Items[i].FindControl("btnCancel")).CommandArgument = session.Id.ToString();
                ((Button)rptPastSessions.Items[i].FindControl("btnEdit")).CommandArgument = session.Id.ToString();
                ((Button)rptPastSessions.Items[i].FindControl("btnRoom")).CommandArgument = session.linkedRoomBookingRequest.ToString();
                ((Button)rptPastSessions.Items[i].FindControl("btnTransport")).CommandArgument = session.linkedTransportBookingRequest.ToString();

                ((Button)rptPastSessions.Items[i].FindControl("btnTripReg")).CommandArgument = session.Id.ToString();
                ((Button)rptPastSessions.Items[i].FindControl("btnScore")).CommandArgument = session.Id.ToString();


                if (((DateTime)session.StartDateTime) < DateTime.Now)
                {
                    if (session.BUCS == 1)
                    {
                        ((Button)rptPastSessions.Items[i].FindControl("btnScore")).Visible = true;
                    }
                    else
                    {
                        ((Button)rptPastSessions.Items[i].FindControl("btnScore")).Visible = false;
                    }
                }
                else
                {
                    ((Button)rptPastSessions.Items[i].FindControl("btnScore")).Visible = false;
                }

                if (session.ourScore != null)
                {
                    ((Button)rptPastSessions.Items[i].FindControl("btnScore")).CssClass = "standardgreen";
                }

                if (((DateTime)session.StartDateTime) < DateTime.Now.AddDays(1))
                {

                }
                else
                {
                    //Hide Register and Risk Assessment
                    ((Button)rptPastSessions.Items[i].FindControl("btnRegister")).Visible = false;
                    ((Button)rptPastSessions.Items[i].FindControl("btnRiskAssessment")).Visible = false;
                }


                if (session.RiskAssessmentId == 1)
                {
                    ((Button)rptPastSessions.Items[i].FindControl("btnRiskAssessment")).CssClass = "standardgreen";
                }


                if (session.linkedRoomBookingRequest == 0 || session.linkedRoomBookingRequest == null)
                {
                    ((Button)rptPastSessions.Items[i].FindControl("btnRoom")).Visible = false;


                }
                else
                {
                    var room = tools.returnRoomRequest((int)session.linkedRoomBookingRequest);
                    if (room.RequestStatus == 1)
                    {
                        ((Button)rptPastSessions.Items[i].FindControl("btnRoom")).CssClass = "standardgreen";
                    }
                    else if (room.RequestStatus == 2)
                    {
                        ((Button)rptPastSessions.Items[i].FindControl("btnRoom")).CssClass = "standardorange";
                    }
                }


                if (session.linkedTransportBookingRequest == 0 || session.linkedTransportBookingRequest == null)
                {
                    ((Button)rptPastSessions.Items[i].FindControl("btnTransport")).Visible = false;


                }
                else
                {
                    var room = tools.returnTransportRequest((int)session.linkedTransportBookingRequest);
                    if (room.RequestStatus == 1)
                    {
                        ((Button)rptPastSessions.Items[i].FindControl("btnTransport")).CssClass = "standardgreen";
                    }
                    else if (room.RequestStatus == 2)
                    {
                        ((Button)rptPastSessions.Items[i].FindControl("btnTransport")).CssClass = "standardorange";
                    }
                }


                if (session.TripRegId == 0 || session.TripRegId == null)
                {
                    ((Button)rptPastSessions.Items[i].FindControl("btnTripReg")).Visible = false;
                }
                else
                {
                    try
                    {
                        var trip = tools.returnTrip((int)session.TripRegId);
                        if (trip.ApprovalStatus == 2)
                        {
                            ((Button)rptPastSessions.Items[i].FindControl("btnTripReg")).CssClass = "standardgreen";
                        }
                        else if (trip.ApprovalStatus == 1)
                        {
                            ((Button)rptPastSessions.Items[i].FindControl("btnTripReg")).CssClass = "standardorange";
                        }
                    }
                    catch
                    {

                    }

                }

                if (session.RegisterComplete == 1)
                {
                    ((Button)rptPastSessions.Items[i].FindControl("btnRegister")).CssClass = "standardgreen";
                }
                else
                {
                    ((Button)rptPastSessions.Items[i].FindControl("btnRegister")).CssClass = "standardorange";
                }

                if (((DateTime)session.StartDateTime) < DateTime.Now)
                {
                    ((Button)rptPastSessions.Items[i].FindControl("btnCancel")).Visible = false;
                    ((Button)rptPastSessions.Items[i].FindControl("btnEdit")).Visible = false;
                }
             
                
                i++;
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
          
            Response.Redirect("CreateSession.aspx");
        }

        protected void btnRiskAssessment_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);
            Response.Redirect("RiskAssessment/Default.aspx?sessionId=" + btnView.CommandArgument);
        }

        protected void btnScore_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);
            Response.Redirect("SubmitResults/Default.aspx?sessionId=" + btnView.CommandArgument);
        }


        protected void btnTripReg_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);
            Response.Redirect("TripRegistration/Default.aspx?sessionId=" + btnView.CommandArgument);
        }

        protected void btnRoom_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);
            Response.Redirect("RoomBooking/Default.aspx?roomId=" + btnView.CommandArgument);
        }

        protected void btnTransport_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);
            Response.Redirect("TransportBooking/Default.aspx?transportId=" + btnView.CommandArgument);
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);
            //Cancel Code

            if(btnView.Text == "Click to Confirm")
            {
                Common tools = new Common();
                tools.deleteSession(int.Parse(btnView.CommandArgument));
                bindActiveSessions();
                bindPastSessions();
            }
            else
            {
                btnView.Text = "Click to Confirm";
            }

         
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            Button btnView = ((Button)sender);
            Response.Redirect("CreateSession.aspx?sessionId=" + btnView.CommandArgument);
        }
    }
}