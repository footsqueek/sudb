﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Committee.Sessions
{
    public partial class CreateSession : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();

            int id = tools.returnActiveSportSoc().Id;
            var sportsociety = tools.returnSportSociety(id);
            lblSportSocietyName.Text = sportsociety.Name;

            if (tools.checkAccess("DutyOfCare") == "Full Access" || (tools.checkAccess("DutyOfCare") == "Read Only"))
            {

            }
            else
            {
                txtDepart.Enabled = false;
                txtTeamTeasTime.Enabled = false;
                txtTeas.Enabled = false;
                txtTransport.Enabled = false;
            }

            var teams = tools.returnTeams();
            foreach(var team in teams)
            {
                cboTeam.Items.Add(new ListItem(team.TeamName, team.Id.ToString()));
            }

            var rooms = tools.returnRooms();
            foreach (var room in rooms)
            {
                cboRoomRequest.Items.Add(new ListItem(room.Headline, room.Id.ToString()));
            }

            var transport = tools.returnTransport();
            foreach (var room in transport)
            {
                cboTransportRequest.Items.Add(new ListItem(room.Headline, room.Id.ToString()));
            }

            int sessionId = 0;
            try
            {
                sessionId = int.Parse(Request.QueryString["sessionId"]);
            }
            catch
            {
                sessionId = 0;
            }
            try
            {
                

                if (sessionId != 0)
                {
                    //lookup session
                    var session = tools.returnSession(sessionId);


                    //Need to check that user has access to this session
                    //Do code later
                    if (!IsPostBack)
                    {
                        
                        txtName.Text = session.SessionName;
                        txtLocation.Text = session.Location;
                        txtRepeat.Visible = false;
                        lblweeks.Visible = false;
                        RangeValidator1.Visible = false;
                        RangeValidator1.Enabled = false;
                        txtStartDate.Text = ((DateTime)session.StartDateTime).ToString("dd MMM yyyy HH:mm");
                        txtEndDate.Text = ((DateTime)session.EndDateTime).ToString("dd MMM yyyy HH:mm");
                        txtDepart.Text = ((DateTime)session.Depart).ToString("dd MMM yyyy HH:mm");
                        txtTransport.Text = session.Transport;
                        txtTeas.Text = session.NoTeas.ToString();
                        txtTeamTeasTime.Text = ((DateTime)session.TeasTime).ToString("dd MMM yyyy HH:mm");
                        txtPostCode.Text = session.PostCode;
                        cboBucs.SelectedValue = session.BUCS.ToString();
                        try
                        {
                            cboRoomRequest.SelectedValue = session.linkedRoomBookingRequest.ToString();
                        }
                        catch
                        {

                        }

                        try
                        {
                            cboTransportRequest.SelectedValue = session.linkedTransportBookingRequest.ToString();
                        }
                        catch
                        {

                        }

                        try
                        {
                            cboTeam.SelectedValue = session.teamId.ToString();
                        }
                        catch
                        {

                        }
                        if (session.TripRegId != null)
                        {
                            cboTrip.Items.Clear();
                            cboTrip.Items.Add(new ListItem("No", "0"));
                            if(session.TripRegId==0)
                            {
                                cboTrip.Items.Add(new ListItem("Create New Trip Registration", "-1"));
                            }
                            else
                            {
                                var trip = tools.returnTrip((int)session.TripRegId);
                                cboTrip.Items.Add(new ListItem("Yes, Trip Registration #" +trip.tripId.ToString() , trip.tripId.ToString()));
                            }
                            cboTrip.SelectedValue = session.TripRegId.ToString();
                        }

                        btnCreate.Text = "Update Session";
                    }
                }
                else
                {


                    if (!IsPostBack)
                    {
                        txtStartDate.Text = DateTime.Now.ToString("dd MMM yyyy HH:mm");
                        txtEndDate.Text = DateTime.Now.ToString("dd MMM yyyy HH:mm");
                        txtDepart.Text = DateTime.Now.ToString("dd MMM yyyy HH:mm");
                        txtTeamTeasTime.Text = DateTime.Now.ToString("dd MMM yyyy HH:mm");
                    }

                }
            }
            catch
            {

            }

            

       }

        protected void btnCreate_Click(object sender, EventArgs e)
        {

            Common tools = new Common();

            int sessionId = 0;
            try
            {
                sessionId = int.Parse(Request.QueryString["sessionId"]);
            }
            catch
            {
                sessionId = 0;
            }

            try
            {
                
      
                if (sessionId != 0)
                {

                    var session = tools.returnSession(sessionId);
                    int sportsoc = (int)session.SportSocietyId;
                    int tripid =int.Parse(cboTrip.SelectedValue);
                    int roomRequest = int.Parse(cboRoomRequest.SelectedValue);
                  
                    if(cboTrip.SelectedValue=="-1")
                    {
                        var trip = tools.createTrip(sportsoc);
                        tripid = trip.tripId;
                    }

                    if (cboRoomRequest.SelectedValue == "-1")
                    {
                        var room = tools.createRoomRequest(sportsoc,txtName.Text + " Room");
                        roomRequest = room.Id;
                    }

                    int transportRequest = int.Parse(cboTransportRequest.SelectedValue);

                    if (cboTransportRequest.SelectedValue == "-1")
                    {
                        var transport = tools.createTransportRequest(sportsoc, txtName.Text + " Transport");
                        transportRequest = transport.Id;
                    }


                    DateTime departtime = DateTime.Now;
                    try
                    {
                        departtime = DateTime.Parse(txtDepart.Text);
                    }
                    catch
                    {

                    }

                    DateTime teastime = DateTime.Now;
                    try
                    {
                        teastime = DateTime.Parse(txtTeamTeasTime.Text);
                    }
                    catch
                    {

                    }

                    tools.updateSession(tripid,sessionId, int.Parse(cboTeam.SelectedValue), sportsoc, txtName.Text.Trim(), txtLocation.Text.Trim(), roomRequest,transportRequest, DateTime.Parse(txtStartDate.Text.Trim()), DateTime.Parse(txtEndDate.Text.Trim()),txtPostCode.Text.Trim(),txtTransport.Text.Trim(),departtime,int.Parse(txtTeas.Text),teastime,int.Parse(cboBucs.SelectedValue));
                    Response.Redirect("Default.aspx");
                }
                else
                {
                    if (DateTime.Parse(txtEndDate.Text) < DateTime.Parse(txtStartDate.Text))
                    {
                        //Throw error
                        litError.Text = "<p class=\"error\">The session end date must be after the start date.</p>";
                        litError.Visible = true;
                    }
                    else if (DateTime.Parse(txtEndDate.Text) == DateTime.Parse(txtStartDate.Text))
                    {
                        //Throw error
                        litError.Text = "<p class=\"error\">The session end date must be after the start date.</p>";
                        litError.Visible = true;
                    }
                    else if (DateTime.Parse(txtStartDate.Text) < DateTime.Now)
                    {
                        //Throw error
                        litError.Text = "<p class=\"error\">The session start date must be in the future.</p>";
                        litError.Visible = true;
                    }
                    else
                    {


                        int tripid = int.Parse(cboTrip.SelectedValue);

                        int roomrequest = int.Parse(cboRoomRequest.SelectedValue);

                        if (cboTrip.SelectedValue == "-1")
                        {
                           
                        }

                        if (cboRoomRequest.SelectedValue == "-1")
                        {
                            var room = tools.createRoomRequest(tools.returnActiveSportSoc().Id,txtName.Text + " Room");
                            roomrequest = room.Id;
                        }

                        int transportrequest = int.Parse(cboTransportRequest.SelectedValue);
                        if (cboTransportRequest.SelectedValue == "-1")
                        {
                            var room = tools.createTransportRequest(tools.returnActiveSportSoc().Id, txtName.Text + " Transport");
                            transportrequest = room.Id;
                        }


                        tools.createSession("",tripid,int.Parse(cboTeam.SelectedValue), tools.returnActiveSportSoc().Id, txtName.Text.Trim(), txtLocation.Text.Trim(), roomrequest,transportrequest, DateTime.Parse(txtStartDate.Text), DateTime.Parse(txtEndDate.Text), int.Parse(txtRepeat.Text.Trim()),txtPostCode.Text.Trim(),txtTransport.Text.Trim(),DateTime.Parse(txtDepart.Text),int.Parse(txtTeas.Text),DateTime.Parse(txtTeamTeasTime.Text),int.Parse(cboBucs.SelectedValue));
                        
                        //Get session trip is linked to

                        if(tripid>0)
                        {
                            //Redirect to Trip
                            //All done so do to default
                            Response.Redirect("Default.aspx");
                        }
                        else
                        {
                           if(roomrequest>0)
                           {
                               //Redirect to Room Request
                               //All done so do to default
                               Response.Redirect("RoomBooking/Default.aspx?roomId="+roomrequest.ToString());
                           }
                           else
                           {
                               if(transportrequest>0)
                               {
                                   // Redirect to Transport
                                   Response.Redirect("TransportBooking/Default.aspx?transportId=" + transportrequest.ToString());
                               }
                               else
                               {
                                   //All done so do to default
                                   Response.Redirect("Default.aspx");
                               }
                           }
                        }

                      
                    }
                }
            }
            catch
            {
            }
            
        }
    }
}