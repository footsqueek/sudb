﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Committee.Sessions.SubmitResults
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int sessionId = int.Parse(Request.QueryString["sessionId"]);
                Common tools = new Common();

                var session = tools.returnSession(sessionId);

                txtSessionName.Text = session.SessionName;
                txtOurs.Text = session.ourScore.ToString();
                txtTheirs.Text = session.theirScore.ToString();
                txtSessionName.Enabled = false;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Common tools = new Common();
            int sessionId = int.Parse(Request.QueryString["sessionId"]);

            tools.submitScore(sessionId, int.Parse(txtOurs.Text), int.Parse(txtTheirs.Text));
            Response.Redirect("../");
        }
    }
}