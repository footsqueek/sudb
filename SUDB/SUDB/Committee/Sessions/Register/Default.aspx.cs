﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace SUDatabase.Committee.Sessions.Register
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ///Check that user is allowed to take this register
            ///Code to be added
          
                bindSessionRegister();
            
        }

        public void bindSessionRegister()
        {
            //Need to check the user has permission to do this register!!!


            Common tools = new Common();
            var register = tools.returnMembersforSession(int.Parse(Request.QueryString["sessionId"]));

            var session = tools.returnSession(int.Parse(Request.QueryString["sessionId"]));
            lblSessionName.Text = session.SessionName + " (" + ((DateTime)session.StartDateTime).ToString("dd MMM yyyy HH:mm") + " - " + ((DateTime)session.EndDateTime).ToString("dd MMM yyyy HH:mm") + ")";

            if (!IsPostBack)
            {
                txtOther.Text = session.SessionNotes;
            }
            rptRegister.DataSource = register;
            rptRegister.DataBind();

            int i = 0;

            foreach(var reg in register)
            {
                var student = tools.returnStudent((int)reg.StudentId);

                ((Label)rptRegister.Items[i].FindControl("lblName")).Text = student.FirstName + " " + student.Surname;

                ((Label)rptRegister.Items[i].FindControl("lblId")).Text = reg.Id.ToString();

                ((HtmlInputCheckBox)rptRegister.Items[i].FindControl("myonoffswitch")).ID = reg.Id.ToString();
                ((HtmlGenericControl)rptRegister.Items[i].FindControl("lblBut")).Attributes.Add("for", reg.Id.ToString());

                if (!IsPostBack)
                {
                    try
                    {
                        var attendeestatus = tools.returnAttendeeStatus((int)session.Id, (int)reg.StudentId);

                        if (attendeestatus.AttendanceStatus == 1)
                        {
                            ((HtmlInputCheckBox)rptRegister.Items[i].FindControl(reg.Id.ToString())).Checked = true;

                        }
                    }
                    catch
                    {
                        //Leave switch as off
                    }
                }

                if (((DateTime)session.EndDateTime).AddDays(1) < DateTime.Now)
                {
                    ((HtmlInputCheckBox)rptRegister.Items[i].FindControl(reg.Id.ToString())).Attributes.Add("Disabled","true"); ;
                    txtOther.Enabled = false;
                    btnSubmit.Enabled = false;
                }

                i++;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            int SessionId = int.Parse(Request.QueryString["sessionId"]);

            Common tools = new Common();

            foreach(RepeaterItem item in rptRegister.Items)
            {

               int memberId = int.Parse(((Label)item.FindControl("lblId")).Text);
               int attendancestatus = 0;

               var membership = tools.returnMembership(memberId);
               
                   if (((HtmlInputCheckBox)item.FindControl(memberId.ToString())).Checked)
                   {
                       attendancestatus = 1;
                   }
               
                 

                

                   tools.submitRegisterAttendee(SessionId, (int)membership.StudentId, attendancestatus);


            }
            tools.addSessionNotes(SessionId, txtOther.Text);
            tools.setRegisterComplete(SessionId);
           

            Response.Redirect("../Default.aspx");
        }

        protected void btnAll_Click(object sender, EventArgs e)
        {
            int SessionId = int.Parse(Request.QueryString["sessionId"]);

            Common tools = new Common();

            foreach (RepeaterItem item in rptRegister.Items)
            {

                int memberId = int.Parse(((Label)item.FindControl("lblId")).Text);
                int attendancestatus = 0;

                var membership = tools.returnMembership(memberId);

                
                    attendancestatus = 1;
               





                tools.submitRegisterAttendee(SessionId, (int)membership.StudentId, attendancestatus);


            }
            tools.addSessionNotes(SessionId, txtOther.Text);
            tools.setRegisterComplete(SessionId);


            Response.Redirect("Default.aspx?sessionId="+SessionId.ToString());
        
        }
    }
}