﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace SUDatabase.Committee.Sessions.TripRegistration
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bindSessionRegister();
        }
        public void bindSessionRegister()
        {
            Common tools = new Common();



            var register = tools.returnMembersforSession(int.Parse(Request.QueryString["sessionId"]));

            var session = tools.returnSession(int.Parse(Request.QueryString["sessionId"]));

            var sport = tools.returnSportSociety((int)session.SportSocietyId);
            txtSportSocoety.Text = sport.Name;

            var trip = tools.returnTrip((int)session.TripRegId);

            //Check if trip is in active sport or society
            if(trip.SportSocietyId != tools.returnActiveSportSoc().Id)
            {
                Response.Redirect("../Default.aspx");
            }

            try
            {
                txtDepartureTime.Text = ((DateTime)trip.DepartureTime).ToString("dd MMM yyyy HH:mm");
                txtReturnDateTime.Text = ((DateTime)trip.ReturnTime).ToString("dd MMM yyyy HH:mm");
                txtDestinationAddress.Text = trip.TripDestinationAddress;
                txtAccomodation.Text = trip.Accomodation;

                if (!IsPostBack)
                {
                    txtComments.Text = trip.ApprovalComments;
                    txtAmendment.Text = trip.TripAmendment;
                }
                try
                {
                    lblStaffMember.Text = System.Web.Security.Membership.GetUser((Guid)trip.tripReviewedBy).UserName;
                }
                catch
                {

                }

                if(trip.ApprovalStatus==2)
                {
                    lblStatus.Text = "Approved";
                    amendment.Visible = true;
                }

                if (trip.ApprovalStatus == 3)
                {
                    lblStatus.Text = "Rejected";
                }

                if (trip.ApprovalStatus == 0)
                {
                    lblStatus.Text = "Not Yet Submitted";
                }
            }
            catch
            {

            }

            if(txtDepartureTime.Text == "")
            {
                if(session.Depart !=null)
                {
                    txtDepartureTime.Text = ((DateTime)session.StartDateTime).ToString("dd MMM yyyy HH:mm");
                    txtDestinationAddress.Text = session.Location + " " + session.PostCode;
                }
                else
                {
                    txtDepartureTime.Text = ((DateTime)session.StartDateTime).ToString("dd MMM yyyy HH:mm");
                    txtDestinationAddress.Text = session.Location + " " + session.PostCode;
                }
                txtReturnDateTime.Text = ((DateTime)session.EndDateTime).ToString("dd MMM yyyy HH:mm");
            }

            
                if (trip.ApprovalStatus == 2)
                {
                    //Trip Approved no Changes
                    txtDepartureTime.Enabled = false;
                    txtDestinationAddress.Enabled = false;
                    txtReturnDateTime.Enabled = false;
                    txtAccomodation.Enabled = false;
                    btnSubmitTripReg.Visible = false;
                }


               
                    rptRegister.DataSource = register;
                    rptRegister.DataBind();
               
     

            int i = 0;

            foreach (var reg in register)
            {
                var student = tools.returnStudent((int)reg.StudentId);
                ((Label)rptRegister.Items[i].FindControl("lblId")).Text = reg.Id.ToString();

                ((Label)rptRegister.Items[i].FindControl("lblName")).Text = student.FirstName + " " + student.Surname;

                ((HtmlInputCheckBox)rptRegister.Items[i].FindControl("chkAttendee")).ID = reg.Id.ToString();
                ((HtmlGenericControl)rptRegister.Items[i].FindControl("lblattendee")).Attributes.Add("for", reg.Id.ToString());
                ((HtmlInputCheckBox)rptRegister.Items[i].FindControl("chkDriver")).ID = reg.Id.ToString() + "Driver";
                ((HtmlGenericControl)rptRegister.Items[i].FindControl("lblDriver")).Attributes.Add("for", reg.Id.ToString()+"Driver");


                ((DropDownList)rptRegister.Items[i].FindControl("cboModeofTransport")).ID = reg.Id.ToString() + "Mode";

                if (student.MiniBusAssessment != 1)
                {
                    ((HtmlInputCheckBox)rptRegister.Items[i].FindControl(reg.Id.ToString() + "Driver")).Visible = false;
                    ((HtmlGenericControl)rptRegister.Items[i].FindControl("lblDriver")).Visible = false;
            
                }

                if(student.FirstAidCertExpiry > session.EndDateTime)
                {
                    ((Image)rptRegister.Items[i].FindControl("imgFirstAid")).Visible=true;
            
                }
                else
                {
                    ((Image)rptRegister.Items[i].FindControl("imgFirstAid")).Visible = false;
                }
                
                if (!IsPostBack)
                {
                    var mode = tools.returnModeofTransport();
                    ((DropDownList)rptRegister.Items[i].FindControl(reg.Id.ToString() + "Mode")).Items.Clear();
                    foreach (var transport in mode)
                    {
                        ((DropDownList)rptRegister.Items[i].FindControl(reg.Id.ToString() + "Mode")).Items.Add(new ListItem(transport.TransportMode.ToString(), transport.Id.ToString())); ;

                    }
                }
               

               

                if (!IsPostBack)
                {
                    try
                    {

                        var attendeestatus = tools.returnTripAttendeeStatus((int)trip.tripId, (int)reg.StudentId);

                        if (attendeestatus.attendanceStatus == 1)
                        {
                            ((HtmlInputCheckBox)rptRegister.Items[i].FindControl(reg.Id.ToString())).Checked = true;

                        }

                        if (attendeestatus.Driver == 1)
                        {
                            ((HtmlInputCheckBox)rptRegister.Items[i].FindControl(reg.Id.ToString() + "Driver")).Checked = true;

                        }

                        ((DropDownList)rptRegister.Items[i].FindControl(reg.Id.ToString() + "Mode")).SelectedValue = attendeestatus.ModeofTransport.ToString();

                    }
                    catch
                    {

                    }
                   
                }

                if (trip.ApprovalStatus == 2)
                {
                    //Trip is Approved
                    ((HtmlInputCheckBox)rptRegister.Items[i].FindControl(reg.Id.ToString() + "Driver")).Attributes.Add("Disabled", "true");
                    ((HtmlInputCheckBox)rptRegister.Items[i].FindControl(reg.Id.ToString())).Attributes.Add("Disabled", "true");
                    ((DropDownList)rptRegister.Items[i].FindControl(reg.Id.ToString() + "Mode")).Enabled = false;
                }

                i++;
            }

            if (tools.checkAccess("DutyOfCare") == "Full Access" || (tools.checkAccess("DutyOfCare") == "Read Only"))
            {
                txtComments.Enabled = true;
                btnApprove.Visible = true;
                btnReject.Visible = true;
            }

            if (((DateTime)session.StartDateTime) < DateTime.Now)
            {
                btnSubmitTripReg.Enabled = false;
                btnSubmitTripReg.ToolTip = "The session has started so no trip registration may be submitted";
            }
        }
        protected void btnSubmitTripReg_Click(object sender, EventArgs e)
        {
    
                int SessionId = int.Parse(Request.QueryString["sessionId"]);

                Common tools = new Common();
                var session = tools.returnSession(int.Parse(Request.QueryString["sessionId"]));
                var trip = tools.returnTrip((int)session.TripRegId);

                tools.updateTrip(trip.tripId, DateTime.Parse(txtDepartureTime.Text), DateTime.Parse(txtReturnDateTime.Text), txtDestinationAddress.Text,txtAccomodation.Text);

        

              
                foreach (RepeaterItem item in rptRegister.Items)
                {
        
                    int memberId = int.Parse(((Label)item.FindControl("lblId")).Text);
                    int attendancestatus = 0;
                    int driver = 0;

                    var membership = tools.returnMembership(memberId);

                    if (((HtmlInputCheckBox)item.FindControl(membership.Id.ToString())).Checked)
                    {
                        attendancestatus = 1;
                    }

                    if (((HtmlInputCheckBox)item.FindControl(membership.Id.ToString() + "Driver")).Checked)
                    {
                        driver = 1;
                    }


                    DropDownList cboModeofTransport = (DropDownList)item.FindControl(membership.Id.ToString() + "Mode");


                    tools.submitTripAttendee(trip.tripId, (int)membership.StudentId, attendancestatus, int.Parse(cboModeofTransport.SelectedValue), driver);


                }

              
                Response.Redirect("../Default.aspx");

        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {

            int SessionId = int.Parse(Request.QueryString["sessionId"]);

                Common tools = new Common();
                var session = tools.returnSession(int.Parse(Request.QueryString["sessionId"]));

        

                var trip = tools.returnTrip((int)session.TripRegId);

                tools.approveTrip(trip.tripId, txtComments.Text);
            Response.Redirect("/Management/DutyOfCare/TripRegistration/");
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            int SessionId = int.Parse(Request.QueryString["sessionId"]);

            Common tools = new Common();
            var session = tools.returnSession(int.Parse(Request.QueryString["sessionId"]));



            var trip = tools.returnTrip((int)session.TripRegId);

            tools.rejectTrip(trip.tripId, txtComments.Text);
            Response.Redirect("/Management/DutyOfCare/TripRegistration/");
          
        }

        protected void btnSubmitAmendment_Click(object sender, EventArgs e)
        {
            int SessionId = int.Parse(Request.QueryString["sessionId"]);

            Common tools = new Common();
            var session = tools.returnSession(int.Parse(Request.QueryString["sessionId"]));



            var trip = tools.returnTrip((int)session.TripRegId);

            tools.tripamendment(trip.tripId, txtAmendment.Text);
            Response.Redirect("../Default.aspx");

        }
    }
}