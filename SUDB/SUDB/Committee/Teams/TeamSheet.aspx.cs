﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Committee.Teams
{
    public partial class TeamSheet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            //try
            //{
                Common tools = new Common();




                var teammembers = tools.returnTeamMembers(int.Parse(Request.QueryString["teamId"]));

                var team = tools.returnTeam(int.Parse(Request.QueryString["teamId"]));

                MembershipUser useris = System.Web.Security.Membership.GetUser(HttpContext.Current.User.Identity.Name);
                //Check if User can access sport soc
               /* if (!tools.canUserAccessSportSoc((Guid)useris.ProviderUserKey, (int)team.SportSocietyId))
                {
                    Response.Redirect("Default.aspx");
                }*/

                lblTeamName.Text = team.TeamName;

                rptRegister.DataSource = teammembers;
                rptRegister.DataBind();
                int i = 0;

                foreach (var teammember in teammembers)
                {
                    var student = tools.returnStudent(teammember.StudentId);

                    ((Label)rptRegister.Items[i].FindControl("lblName")).Text = student.FirstName + " " + student.Surname;

                    if (tools.getSU().Id == 1)
                    {
                        ((Literal)rptRegister.Items[i].FindControl("litPhoto")).Text = "<img width=\"96px\" height=\"110px\" src=\"https://ganymede.chester.ac.uk/ple/json-rpc/photo_redirect.php?searchterm=" + student.StudentNumber + "\"/>";
                    }
                    else
                    {
                        ((Literal)rptRegister.Items[i].FindControl("litPhoto")).Text = "No Photo";
                    }
                    i++;
                }
           /* }
            catch
            {
                Response.Redirect("Default.aspx");
            }*/

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}