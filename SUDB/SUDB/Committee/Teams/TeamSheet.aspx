﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="TeamSheet.aspx.cs" Inherits="SUDatabase.Committee.Teams.TeamSheet" %>
<%@ Register src="../../UserControls/CommitteeMenu.ascx" tagname="CommitteeMenu" tagprefix="uc1" %>
<%@ Register src="../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CommitteeMenu ID="CommitteeMenu1" runat="server" />
    <uc2:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Team / Group Sheet</h1>

    <h2><asp:Label ID="lblTeamName" runat="server" Text="Label"></asp:Label></h2>

     <table class="nicetable">
        <tr>
            <th>Member Name</th>
            <th></th>

        </tr>
    <asp:Repeater ID="rptRegister" runat="server">

        <ItemTemplate>
            <asp:Label ID="lblId" Visible="false" runat="server" Text="Label"></asp:Label> 
            <tr>
                 <td>
                     <asp:Literal ID="litPhoto" runat="server"></asp:Literal> </td>
                <td><asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>  </td>
             

            </tr>

        </ItemTemplate>

    </asp:Repeater>
    </table>
    <br />
    <asp:Button ID="btnSubmit" runat="server" CssClass="submitbutton" Text="Back to Teams" OnClick="btnSubmit_Click" />


</asp:Content>
