﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Committee.Teams
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int currentyear = DateTime.Now.Year;
                if (DateTime.Now < new DateTime(DateTime.Now.Year, 08, 01))
                {
                    currentyear = currentyear - 1;
                }

                cboYears.Items.Clear();
                for (int i = 2014; i < currentyear + 1; i++)
                {
                    cboYears.Items.Add(new ListItem(i.ToString() + " / " + (i + 1).ToString().Substring(2, 2), i.ToString()));
                }
                cboYears.SelectedValue = currentyear.ToString();
            }

            Common tools = new Common();

            int id = tools.returnActiveSportSoc().Id;
            var sportsociety = tools.returnSportSociety(id);
            lblSportSocietyName.Text = sportsociety.Name;
            if (!IsPostBack)
            {
                bindTeams();
            }
        }

        public void bindTeams()
        {
            Common tools = new Common();
            var teams = tools.returnTeams();

            rptTeams.DataSource = teams;
            rptTeams.DataBind();

            int i = 0;

            foreach(var team in teams)
            {
                ((Label)rptTeams.Items[i].FindControl("lblTeamName")).Text = team.TeamName;
                ((Button)rptTeams.Items[i].FindControl("btnDelete")).CommandArgument = team.Id.ToString();
                ((Button)rptTeams.Items[i].FindControl("btnEdit")).CommandArgument = team.Id.ToString();
                ((Button)rptTeams.Items[i].FindControl("btnView")).CommandArgument = team.Id.ToString();
                i++;
            }
        }

        public void btnView_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            Response.Redirect("TeamSheet.aspx?teamId=" + btnView.CommandArgument);
        }

        public void btnEdit_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            Response.Redirect("EditMembership.aspx?teamId=" + btnView.CommandArgument);
        }

        public void btnDelete_Clickbutton(object sender, CommandEventArgs e)
        {
            Button btnView = ((Button)sender);
            if(btnView.Text == "Click to Confirm")
            {
                //Delete
                Common tools = new Common();
                tools.deleteTeam(int.Parse(btnView.CommandArgument));
                rptTeams.DataBind();
            }
            else
            {
                btnView.Text = "Click to Confirm";
            }
        }

        protected void btnCreateTeam_Click(object sender, EventArgs e)
        {
            Response.Redirect("CreateTeam.aspx");
        }
    }
}