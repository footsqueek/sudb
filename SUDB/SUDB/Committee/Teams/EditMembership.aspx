﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="EditMembership.aspx.cs" Inherits="SUDatabase.Committee.Teams.EditMembership" %>
<%@ Register src="../../UserControls/CommitteeMenu.ascx" tagname="CommitteeMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CommitteeMenu ID="CommitteeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Edit Team / Group Membership</h1>

      <p>Please select the year that you would like to view: <asp:DropDownList ID="cboYears" AutoPostBack="true" runat="server"></asp:DropDownList></p>


    <p>Select which members are to be a part of <asp:Label ID="lblTeamName" runat="server" Text="this team"></asp:Label></p>

     <table class="nicetable">
        <tr>
            <th>Member Name</th>
            <th></th>

        </tr>
    <asp:Repeater ID="rptRegister" runat="server">

        <ItemTemplate>
            <asp:Label ID="lblId" Visible="false" runat="server" Text="Label"></asp:Label> 
            <tr>

                <td><asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>  </td>
                <td><div class="onoffswitch">

    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" runat="server" id="myonoffswitch">
    <label class="onoffswitch-label" runat="server" id="lblBut">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</div>                    
</td>

            </tr>

        </ItemTemplate>

    </asp:Repeater>
    </table>

      <br />
    <asp:Button ID="btnSubmit" runat="server" CssClass="submitbutton" Text="Save Changes" OnClick="btnSubmit_Click" />
</asp:Content>
