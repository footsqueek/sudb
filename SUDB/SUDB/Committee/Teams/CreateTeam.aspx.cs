﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Committee.Teams
{
    public partial class CreateTeam : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();

            int id = tools.returnActiveSportSoc().Id;
            var sportsociety = tools.returnSportSociety(id);
            lblSportSocietyName.Text = sportsociety.Name;
        }

        protected void btnCreateTeam_Click(object sender, EventArgs e)
        {
            Common tools = new Common();

            tools.createTeam(txtName.Text.Trim());
            Response.Redirect("Default.aspx");
        }
    }
}