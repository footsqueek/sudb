﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace SUDatabase.Committee.Teams
{
    public partial class EditMembership : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                int currentyear = DateTime.Now.Year;
                if (DateTime.Now < new DateTime(DateTime.Now.Year, 08, 01))
                {
                    currentyear = currentyear - 1;
                }
                cboYears.Items.Clear();
                for (int i = 2014; i < currentyear + 1; i++)
                {
                    cboYears.Items.Add(new ListItem(i.ToString() + " / " + (i + 1).ToString().Substring(2, 2), i.ToString()));

                }
                cboYears.SelectedValue = currentyear.ToString();
            }
            try
            {
               
                    bindSessionRegister();
            
            }
            catch
            {
                Response.Redirect("Default.aspx");
            }
        }


        public void bindSessionRegister()
        {
            //Need to check the user has permission to do this register!!!
             Common tools = new Common();
              var team = tools.returnTeam(int.Parse(Request.QueryString["teamId"]));

              var register = tools.returnSportSocietyMembers((int)team.SportSocietyId, int.Parse(cboYears.SelectedValue));
           
              MembershipUser useris = System.Web.Security.Membership.GetUser(HttpContext.Current.User.Identity.Name);
              //Check if User can access sport soc
              if (!tools.canUserAccessSportSoc((Guid)useris.ProviderUserKey, (int)team.SportSocietyId))
              {
                  Response.Redirect("Default.aspx");
              }

          
            lblTeamName.Text = team.TeamName;

            if (!IsPostBack)
            {
                rptRegister.DataSource = register;
                rptRegister.DataBind();
            }

            int i = 0;

            foreach (var reg in register)
            {
                var student = tools.returnStudent((int)reg.StudentId);

                ((Label)rptRegister.Items[i].FindControl("lblName")).Text = student.FirstName + " " + student.Surname;

                ((Label)rptRegister.Items[i].FindControl("lblId")).Text = reg.Id.ToString();

                ((HtmlInputCheckBox)rptRegister.Items[i].FindControl("myonoffswitch")).ID = reg.Id.ToString();
                ((HtmlGenericControl)rptRegister.Items[i].FindControl("lblBut")).Attributes.Add("for", reg.Id.ToString());

                if (!IsPostBack)
                {
                    try
                    {
                        var attendeestatus = tools.returnTeamStatus((int)team.Id, (int)reg.StudentId);

                        if (attendeestatus != null)
                        {
                            ((HtmlInputCheckBox)rptRegister.Items[i].FindControl(reg.Id.ToString())).Checked = true;
                        }


                    }
                    catch
                    {
                        //Leave switch as off
                    }
                }



                i++;
            }
        }
        

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int teamId = int.Parse(Request.QueryString["teamId"]);

            Common tools = new Common();

            foreach (RepeaterItem item in rptRegister.Items)
            {

                int memberId = int.Parse(((Label)item.FindControl("lblId")).Text);
                int attendancestatus = 0;

                var membership = tools.returnMembership(memberId);

                if (((HtmlInputCheckBox)item.FindControl(memberId.ToString())).Checked)
                {
                    attendancestatus = 1;
                }





                tools.submitTeamMember(teamId, (int)membership.StudentId, attendancestatus);


            }



            Response.Redirect("Default.aspx");
        }
    }
}