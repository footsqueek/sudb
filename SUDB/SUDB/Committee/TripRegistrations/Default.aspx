﻿<%@ Page Title="Trip Registrations" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Committee.TripRegistrations.Default" %>
<%@ Register src="../../UserControls/CommitteeMenu.ascx" tagname="CommitteeMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CommitteeMenu ID="CommitteeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <h1><asp:Label ID="lblSportSocietyName" runat="server" Text="Label"></asp:Label></h1>

      <p>Please select the year that you would like to view: <asp:DropDownList ID="cboYears" runat="server"></asp:DropDownList></p>

    <h2>Trip Registrations</h2>
</asp:Content>
