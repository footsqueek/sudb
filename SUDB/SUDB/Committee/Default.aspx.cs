﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Committee
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Check if Sport or Society is Locked Out
            Common tools = new Common();
            if(tools.isSportSockLockedOut())
            {
                LockedOut.Visible = true;
            }
            else
            {
                LockedOut.Visible = false;
            }
        }
    }
}