﻿using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Committee
{
    public partial class Membership : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int currentyear = DateTime.Now.Year;
                if (DateTime.Now < new DateTime(DateTime.Now.Year, 08, 01))
                {
                    currentyear = currentyear - 1;
                }

                cboYears.Items.Clear();
                for (int i = 2014; i < currentyear + 1; i++)
                {
                    cboYears.Items.Add(new ListItem(i.ToString() + " / " + (i + 1).ToString().Substring(2, 2), i.ToString()));
                }
                cboYears.SelectedValue = currentyear.ToString();
            }


            Common tools = new Common();

            int id = tools.returnActiveSportSoc().Id;
            var sportsociety = tools.returnSportSociety(id);
            lblSportSocietyName.Text = sportsociety.Name;

            bindmembers(id);
        }


         public void  bindmembers(int id)
        {

            Common tools = new Common();

            var sportsociety = tools.returnSportSociety(id);

            var members = tools.returnSportSocietyMembers(id, int.Parse(cboYears.SelectedValue));

            rptMembers.DataSource = members;
            rptMembers.DataBind();

            int i = 0;

            double total = 0;
            double vat = 0;
            double net = 0;

            int totalsessions = tools.returnSessionsThisYear().Count();
            int avgattendance = 0;

            foreach (tblMembership member in members)
          {

              ((Label)rptMembers.Items[i].FindControl("lblDateJoined")).Text = ((DateTime)member.DateJoined).ToString("dd MMM yyyy HH:mm");
               var student = tools.returnStudent((int)member.StudentId);
               try
               {
                   ((Label)rptMembers.Items[i].FindControl("lblDOB")).Text = ((DateTime)student.DateOfBirth).ToString("dd MMM yyyy");
               }
              catch
               {
                   ((Label)rptMembers.Items[i].FindControl("lblDOB")).Text = "";
               }
              ((Label)rptMembers.Items[i].FindControl("lblStudentNumber")).Text = student.StudentNumber;
              ((Label)rptMembers.Items[i].FindControl("lblStudentName")).Text = student.FirstName + " " + student.Surname;
              ((Literal)rptMembers.Items[i].FindControl("litPhoto")).Text = "<img width=\"96px\" height=\"110px\" src=\"https://ganymede.chester.ac.uk/ple/json-rpc/photo_redirect.php?searchterm=" + student.StudentNumber + "\"/>";
                  
            
              if(sportsociety.VAT==1)
              {
                  ((Label)rptMembers.Items[i].FindControl("lblVat")).Text = "&pound;" + Math.Round((double)member.Amount / 120 * 20, 2);
                  ((Label)rptMembers.Items[i].FindControl("lblNetAmount")).Text = "&pound;" + Math.Round(((double)member.Amount - (double)member.Amount / 120 * 20), 2);
                  vat = vat + Math.Round((double)member.Amount / 120 * 20,2);
                  net = net +  Math.Round(((double)member.Amount - (double)member.Amount / 120 * 20),2);

              }
              else
              {
                  ((Label)rptMembers.Items[i].FindControl("lblVat")).Text = "&pound;" + "0";
                  ((Label)rptMembers.Items[i].FindControl("lblNetAmount")).Text = "&pound;" + (Math.Round((double)member.Amount, 2));
                
                  net = net + Math.Round((double)member.Amount, 2);

              }

                try
                {
                    int sessionsattended = tools.returnSessionsAttended((int)member.StudentId).Count();

                    int attendance = (100 / totalsessions * sessionsattended);

                    if (attendance > 100)
                    {
                        attendance = 100;
                    }

                  ((Label)rptMembers.Items[i].FindControl("lblAttendance")).Text = attendance + "%";

                    avgattendance = avgattendance + attendance;
                }
                catch
                {
                    ((Label)rptMembers.Items[i].FindControl("lblAttendance")).Text = "0%";

                }

((Label)rptMembers.Items[i].FindControl("lblGross")).Text = "&pound;" + Math.Round((double)member.Amount, 2);

              if(student.MedicalInformation != "" && student.MedicalInformation !=null)
              {

                  ((ImageButton)rptMembers.Items[i].FindControl("imgMedical")).Visible=true;
                  ((ImageButton)rptMembers.Items[i].FindControl("imgMedical")).PostBackUrl = "MedReport.aspx?studentid="+student.Id.ToString();
              }
              else
              {
                  ((ImageButton)rptMembers.Items[i].FindControl("imgMedical")).Visible = false;
              }

              if (!User.IsInRole("Staff"))
              {
                  if(student.OktoShareMedical==0)
                  {
                      ((ImageButton)rptMembers.Items[i].FindControl("imgMedical")).Visible = false;
                  }
              }

              total = total + (double)member.Amount;
              i++;
          }

          if (tools.checkAccess("DutyOfCare") == "Full Access" || (tools.checkAccess("DutyOfCare") == "Read Only"))
          {
              lblNet.Text = "&pound;" + Math.Round(net, 2).ToString();
              lblGross.Text = "&pound;" + Math.Round(total, 2).ToString();
              lblVAT.Text = "&pound;" + Math.Round(vat,2);
              menu1.Visible = true;
              menu2.Visible = false;
          }
          else
          {
              lblNet.Text = "";
              lblGross.Text = "";
              lblVAT.Text = "";
              menu1.Visible = false;
              menu2.Visible = true;
          }


        

          lblTotalMembers.Text = (i).ToString() + " Members";
            lblAvgAttendance.Text = (avgattendance / i).ToString() + "%";
        }
    }
}
