﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Committee.Transport
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, 08, 01))
            {
                currentyear = currentyear - 1;
            }

            for (int i = 2014; i < currentyear + 1; i++)
            {
                cboYears.Items.Add(new ListItem(i.ToString() + " / " + (i + 1).ToString().Substring(2, 2), i.ToString()));
        
            }


            Common tools = new Common();

            int id = tools.returnActiveSportSoc().Id;
            var sportsociety = tools.returnSportSociety(id);
            lblSportSocietyName.Text = sportsociety.Name;
        }
    }
}