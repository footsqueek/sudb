﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDB.Committee
{
    public partial class MedReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bindStudent();
        }

        protected void bindStudent()
        {
            try
            {
                Common tools = new Common();
                var student = tools.returnStudent(int.Parse(Request.QueryString["studentid"]));
                txtName.Text = student.FirstName + " " + student.Surname + " (" + student.StudentNumber + ")";
                txtMedicalInfo.Text = student.MedicalInformation;

                if (student.MedicalInformation == "")
                {
                    Response.Redirect("Membership.aspx");

                }

                if (!User.IsInRole("Staff"))
                {
                    if (student.OktoShareMedical == 0)
                    {
                        Response.Redirect("Membership.aspx");
                    }
                }
            }
            catch
            {
                Response.Redirect("Membership.aspx");
            }
        }
    }
}