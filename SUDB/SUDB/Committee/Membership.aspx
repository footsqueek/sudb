﻿<%@ Page Title="Membership" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Membership.aspx.cs" Inherits="SUDatabase.Committee.Membership" %>
<%@ Register src="../UserControls/CommitteeMenu.ascx" tagname="CommitteeMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CommitteeMenu ID="CommitteeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <h1><asp:Label ID="lblSportSocietyName" runat="server" Text="Label"></asp:Label></h1>

      <p>Please select the year that you would like to view: <asp:DropDownList ID="cboYears" AutoPostBack="true" runat="server"></asp:DropDownList></p>

     <h2>Membership</h2>

    <p>Average Attendance this Year has Been <asp:Label ID="lblAvgAttendance" runat="server" Text="Label"></asp:Label></p>

    <table class="nicetable">

        <tr runat="server" id="menu1">
              
            <th>Student Number</th>
             <th>Photo</th>
              <th>Name</th>
             <th>Date of Birth</th>
              <th>Date Joined</th>
              
              <th>Gross</th>
              <th>Medical</th>
            <th>Attendance</th>
        </tr>

        
        <tr runat="server" id="menu2" visible="false">

            <th>Student Number</th>
             <th>Photo</th>
              <th>Name</th>
             <th>Date of Birth</th>
              <th>Date Joined</th>
            
              <th>Gross</th>
            <th>Medical</th>
                   <th>Attendance</th>
        </tr>

    <asp:Repeater ID="rptMembers" runat="server">

        <ItemTemplate>

            <tr>
                <td> <asp:Label ID="lblStudentNumber" runat="server" Text="Label"></asp:Label></td>
                 <td>
                     <asp:Literal ID="litPhoto" runat="server"></asp:Literal> </td>
                <td> <asp:Label ID="lblStudentName" runat="server" Text="Label"></asp:Label></td>
                <td> <asp:Label ID="lblDOB" runat="server" Text="Label"></asp:Label></td>
                <td> <asp:Label ID="lblDateJoined" runat="server" Text="Label"></asp:Label></td>
                <!--<td> <asp:Label ID="lblNetAmount" runat="server" Text="Label"></asp:Label></td>
                <td> <asp:Label ID="lblVat" runat="server" Text="Label"></asp:Label></td>-->
                <td> <asp:Label ID="lblGross" runat="server" Text="Label"></asp:Label></td>
                <td class="center"><asp:ImageButton ImageUrl="~/Images/Icons/Firstaid.png" Enabled="true" Visible="false" PostBackUrl="MedReport.aspx" Width="30px" ID="imgMedical" runat="server" /> </td>
                    <td> <asp:Label ID="lblAttendance" runat="server" Text="0%"></asp:Label></td>
            </tr>

        </ItemTemplate>

    </asp:Repeater>
        
        <tr>

            <th colspan="1">Total</th>
              <th colspan="4">  <asp:Label ID="lblTotalMembers" runat="server" Text="Label"></asp:Label>
           
                <asp:Label ID="lblNet" Visible="false" runat="server" Text="Label"></asp:Label>
           
                <asp:Label ID="lblVAT" Visible="false" runat="server" Text="Label"></asp:Label></th>
            <th>
                <asp:Label ID="lblGross" runat="server" Text="Label"></asp:Label></th>
            <th></th>
           
                <th></th>
          
           

        </tr>
        </table>



</asp:Content>
