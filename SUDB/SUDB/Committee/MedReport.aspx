﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="MedReport.aspx.cs" Inherits="SUDB.Committee.MedReport" %>

<%@ Register Src="~/UserControls/CommitteeMenu.ascx" TagPrefix="uc1" TagName="CommitteeMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CommitteeMenu runat="server" ID="CommitteeMenu" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Medical Report</h1>

    <p>This student has shared medical information with the students' union and has given us permission to allow you, as a sport or society committee member permission to see it. Please keep this information confidential.</p>

     <p><asp:Label CssClass="label" ID="Label6" runat="server" Text="Student Name"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtName" Text="" Enabled="false" runat="server"></asp:TextBox></p>
   
     <p>
        <asp:Label ID="Label10" CssClass="label" runat="server" Text="Medical Information"></asp:Label><br />
        <asp:TextBox ID="txtMedicalInfo" Enabled="false" TextMode="MultiLine" Rows="5" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>


    <asp:Button ID="btnBack" PostBackUrl="~/Committee/Membership.aspx" CssClass="submitbutton" runat="server" Text="Back to Membership" />

</asp:Content>
