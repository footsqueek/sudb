﻿using SUDatabase.Classes;
using SUDB.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

           
          

            if (User.Identity.IsAuthenticated)
            {
                if (Request.QueryString["action"] == "logout")
                {
                    HttpContext.Current.Session.Clear();
                    HttpContext.Current.Session.Abandon();
                    HttpContext.Current.User = null;
                    System.Web.Security.FormsAuthentication.SignOut();
                    Response.Redirect("/");
                }
                else
                {
                    if (User.IsInRole("Staff"))
                    {

                        Common tools = new Common();
                        tools.userLoggedIn(HttpContext.Current.User.Identity.Name);

                        Response.Redirect("/Management/");

                    }
                    else if (User.IsInRole("ClubCaptain"))
                    {
                        Common tools = new Common();
                        tools.userLoggedIn(HttpContext.Current.User.Identity.Name);

                        Response.Redirect("/Committee/");
                    }
                    else
                    {
                        //Logout the user
                        Response.Redirect("/Broke/");
                    }
                }
            }
        }

        protected void Login1_LoggedIn(object sender, EventArgs e)
        {
           


        }
    }
}