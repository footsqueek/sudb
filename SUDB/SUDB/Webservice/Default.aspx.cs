﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using SUDatabase.Classes;
using SUDB;

namespace SUDatabase.Webservice
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
           {
                Response.ContentType = "application/json";

                string action=Request.QueryString["action"];
                string guid=Request.QueryString["guid"];
                string sessionId = Request.QueryString["sessionId"];
                string ourScore = Request.QueryString["ourScore"];
                string theirScore = Request.QueryString["theirScore"];
                string riskId = Request.QueryString["riskId"];
                string riskVal = Request.QueryString["riskVal"];
                string who = Request.QueryString["who"];
                string riskaction = Request.QueryString["riskaction"];
                string memberId = Request.QueryString["attendeeId"];
                string attendancestatus = Request.QueryString["status"];
                string outputtext="";
            
            switch(action)
            {
                case "returnMembers":
                   outputtext = returnMembers(guid);
                    break;
                case "submitScore":
                    outputtext = submitScore(int.Parse(sessionId),int.Parse(ourScore),int.Parse(theirScore));
                    break;
                case "returnActivitySessions":
                    outputtext = returnActivitySessions(guid);
                    break;
                case "returnCurrentSoc":
                    outputtext = returnCurrentSoc(guid);
                    break;
                case "checkAuth":
                    outputtext = checkAuth(Request.QueryString["username"], Request.QueryString["password"]);
                    break;
                case "returnRegister":
                    outputtext = returnRegister(guid,sessionId);
                    break;
                case "registerComplete":
                    outputtext = registerComplete(int.Parse(sessionId));
                    break;
                case "submitRisk":
                    outputtext = submitRisk(int.Parse(sessionId),int.Parse(riskId),Guid.Parse(guid),int.Parse(riskVal),who,riskaction);
                    break;
                case "submitAttendee":
                    outputtext = submitAttendee(int.Parse(sessionId), int.Parse(memberId), int.Parse(attendancestatus));
                    break;
                default:
                    outputtext = "Fatal action";
                    break;

            }

            Response.Write(outputtext);
            }
            catch(Exception eq)
            {
                //Fatal error
                Response.Write("Fatal" + eq.ToString());
            }

        }

         DataClasses1DataContext db = new DataClasses1DataContext();
        public class Member
        {
            public string StudentId { get; set; }
            public string StudentNumber { get; set; }
            public string StudentName { get; set; }
            public string StudentDOB { get; set; }
            public string Medical { get; set; }

        }

        public class Session
        {
            public string SessionId { get; set; }
            public string SessionName { get; set; }
            public string Location { get; set; }
            public string StartTime { get; set; }
            public string EndTime { get; set; }
            public string trip { get; set; }
            public string registercomplete { get; set; }
            public string tripRegId { get; set; }
            public string riskAssessmentId { get; set; }
            public string ourScore { get; set; }
            public string theirScore { get; set; }
            public string postCode { get; set; }
            public string transport { get; set; }
            public string depart{ get; set; }

            public string NoTeas { get; set; }
            public string BUCS { get; set; }

        }

        public class currentSoc
        {
            public string currentSocName { get; set; }

        }

        public class UserDetails
        {
            public string Guid { get; set; }
            public string SU { get; set; }

        }

        public string checkAuth(String username, String password)
        {
            bool user = Membership.ValidateUser(username, password);
           Common tools = new Common();
            if (user)
            {
                var member = Membership.GetUser(username);

                var su = tools.getPermissions((Guid)member.ProviderUserKey);

                UserDetails[] e = new UserDetails[1];
                e[0] = new UserDetails();
                e[0].Guid = member.ProviderUserKey.ToString();
                e[0].SU = su.StudentsUnion.ToString();
                return new JavaScriptSerializer().Serialize(e);
            }
            else
            {
                UserDetails[] e = new UserDetails[1];
                e[0] = new UserDetails();
                e[0].Guid = "false";
                e[0].SU = "0";
                return new JavaScriptSerializer().Serialize(e);
            }

        }

        public string returnRegister(String memberGuid,String sessionId)
        {
            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, 08, 01))
            {
                currentyear = currentyear - 1;
            }

            tblStaffSecurity staff = db.tblStaffSecurities.Where(p => p.UserKey == Guid.Parse(memberGuid)).Single();

            DateTime lowestDateJoined = new DateTime(currentyear, 08, 01);
            DateTime highestDateJoined = new DateTime(currentyear + 1, 07, 31);

            var members = from p in db.tblMemberships join u in db.tblStudents on p.StudentId equals u.Id join r in db.tblTransactions on p.TransactionId equals r.transactionId where (p.DateJoined.Value < highestDateJoined) where (p.DateJoined.Value > lowestDateJoined) where (p.SportSocId == staff.ActiveSportSociety) where (r.status == 1) orderby u.Surname ascending select u;

            

            Member[] e = new Member[members.Count()];
            int i = 0;
            foreach (var member in members)
            {
                e[i] = new Member();
                e[i].StudentId = member.Id.ToString();
                e[i].StudentNumber = member.StudentNumber;
                e[i].StudentName = member.FirstName + " " + member.Surname;
                e[i].StudentDOB = member.DateOfBirth.ToString();
                e[i].Medical = member.MedicalInformation;
                i++;
            }


            return new JavaScriptSerializer().Serialize(e);
        }

        public string returnCurrentSoc(String memberGuid)
        {
            tblStaffSecurity staff = db.tblStaffSecurities.Where(p => p.UserKey == Guid.Parse(memberGuid)).Single();

          

            var activesportsoc = db.tblSportsSocieties.Where(p => p.Id.Equals(staff.ActiveSportSociety)).Single();

            currentSoc[] e = new currentSoc[1];
            e[0] = new currentSoc();
            e[0].currentSocName = activesportsoc.Name;
            return new JavaScriptSerializer().Serialize(e);
        }


        public string submitScore(int SessionId, int ourScore, int theirScore)
        {

            var session = db.tblSessions.Where(p => p.Id == SessionId).Single();

            session.ourScore = ourScore;
            session.theirScore = theirScore;
            db.SubmitChanges();
            UserDetails[] e = new UserDetails[1];
            e[0] = new UserDetails();
            e[0].Guid = "true";
            return new JavaScriptSerializer().Serialize(e);

        }

        public string registerComplete(int SessionId)
        {

            Common tools = new Common();

            tools.setRegisterComplete(SessionId);

            UserDetails[] e = new UserDetails[1];
            e[0] = new UserDetails();
            e[0].Guid = "true";
            return new JavaScriptSerializer().Serialize(e);

        }
        public string submitAttendee(int SessionId, int memberId, int status)
        {

            Common tools = new Common();

            tools.submitRegisterAttendee(SessionId, memberId, status);

            UserDetails[] e = new UserDetails[1];
            e[0] = new UserDetails();
            e[0].Guid = "true";
            return new JavaScriptSerializer().Serialize(e);

        }

        public string submitRisk(int SessionId,int riskID, Guid guid, int riskValue, string who, string action)
        {

            Common tools = new Common();
            tools.submitRisk(SessionId, riskID, riskValue, guid, who, action, "", 0);
            tools.setRiskAssessmentComplete(SessionId);
            UserDetails[] e = new UserDetails[1];
            e[0] = new UserDetails();
            e[0].Guid = "true";
            return new JavaScriptSerializer().Serialize(e);

        }


            public string returnMembers(String memberGuid)
            {
                int currentyear = DateTime.Now.Year;
                if (DateTime.Now < new DateTime(DateTime.Now.Year, 08, 01))
                {
                    currentyear = currentyear - 1;
                }

                tblStaffSecurity staff = db.tblStaffSecurities.Where(p => p.UserKey == Guid.Parse(memberGuid)).Single();

                DateTime lowestDateJoined = new DateTime(currentyear, 08, 01);
                DateTime highestDateJoined = new DateTime(currentyear + 1, 07, 31);

                var members = from p in db.tblMemberships join u in db.tblStudents on p.StudentId equals u.Id join r in db.tblTransactions on p.TransactionId equals r.transactionId where (p.DateJoined.Value < highestDateJoined) where (p.DateJoined.Value > lowestDateJoined) where (p.SportSocId == staff.ActiveSportSociety) where (r.status == 1) select u;

                Member[] e = new Member[members.Count()];
                int i = 0;
                foreach (var member in members)
                {
                    e[i] = new Member();
                    e[i].StudentNumber = member.StudentNumber;
                    e[i].StudentName = member.FirstName + " " + member.Surname;
                    e[i].StudentDOB = member.DateOfBirth.ToString();
                    e[i].Medical = member.MedicalInformation;
                    i++;
                }


                return new JavaScriptSerializer().Serialize(e);
            }


            public string returnActivitySessions(String memberGuid)
            {
                int currentyear = DateTime.Now.Year;
                if (DateTime.Now < new DateTime(DateTime.Now.Year, 08, 01))
                {
                    currentyear = currentyear - 1;
                }

                tblStaffSecurity staff = db.tblStaffSecurities.Where(p => p.UserKey == Guid.Parse(memberGuid)).Single();

                DateTime lowestDateJoined = new DateTime(currentyear, 08, 01);
                DateTime highestDateJoined = new DateTime(currentyear + 1, 07, 31);

                var members = from p in db.tblSessions where (p.SportSocietyId == staff.ActiveSportSociety) where (p.StartDateTime < DateTime.Now.AddDays(1)) where (p.EndDateTime > DateTime.Now.AddDays(-1)) select p;

                Session[] e = new Session[members.Count()];
                int i = 0;
                foreach (var member in members)
                {
                    e[i] = new Session();
                    e[i].SessionId = member.Id.ToString();
                    e[i].SessionName = member.SessionName.ToString();
                    e[i].StartTime = member.StartDateTime.ToString();
                    e[i].EndTime = member.EndDateTime.ToString();
                    e[i].ourScore = member.ourScore.ToString();
                    e[i].theirScore = member.theirScore.ToString();
                    e[i].riskAssessmentId = member.RiskAssessmentId.ToString();
                    e[i].registercomplete = member.RegisterComplete.ToString();    
                    i++;
                }


                return new JavaScriptSerializer().Serialize(e);
            }
        }

    }