﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.Security;
using SUDB;

namespace SUDatabase.Webservice
{
    /// <summary>
    /// Summary description for SUDBWebService
    /// </summary>
    [WebService(Namespace = "http://www.sudb.co.uk/Webservice")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SUDBWebService : System.Web.Services.WebService
    {

        DataClasses1DataContext db = new DataClasses1DataContext();

        public class Member
        {
            public string StudentNumber { get; set; }
            public string StudentName { get; set; }
            public string StudentDOB { get; set; }
            public string Medical { get; set; }
            
        }


        public class User
        {
            public string Guid { get; set; }

        }


        [WebMethod]
        public string checkAuth(String username, String password)
        {
           bool user = Membership.ValidateUser(username, password);
            if(user)
            {
                var member = Membership.GetUser(username);
                User[] e = new User[1];
             e[0] = new User();
             e[0].Guid = member.ProviderUserKey.ToString();
             return new JavaScriptSerializer().Serialize(e);
            }
            else
            {
                User[] e = new User[1];
                e[0] = new User();
                e[0].Guid = "false";
                return new JavaScriptSerializer().Serialize(e);
            }
           
        }

        [WebMethod]
        public string returnMembers(String memberGuid)
        {
            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, 08, 01))
            {
                currentyear = currentyear - 1;
            }

            tblStaffSecurity staff = db.tblStaffSecurities.Where(p => p.UserKey == Guid.Parse(memberGuid)).Single();

            DateTime lowestDateJoined = new DateTime(currentyear, 08, 01);
            DateTime highestDateJoined = new DateTime(currentyear + 1, 07, 31);

            var members = from p in db.tblMemberships join u in db.tblStudents on p.StudentId equals u.Id join r in db.tblTransactions on p.TransactionId equals r.transactionId where (p.DateJoined.Value < highestDateJoined) where (p.DateJoined.Value > lowestDateJoined) where (p.SportSocId == staff.ActiveSportSociety) where (r.status == 1) select u;

            Member[] e = new Member[members.Count()];
            int i = 0;
         foreach(var member in members)
         {
             e[i] = new Member();
             e[i].StudentNumber = member.StudentNumber;
             e[i].StudentName = member.FirstName + " " + member.Surname;
             e[i].StudentDOB = member.DateOfBirth.ToString();
             e[i].Medical = member.MedicalInformation;
             i++;
         }

  
            return new JavaScriptSerializer().Serialize(e);
        }
    }
}
