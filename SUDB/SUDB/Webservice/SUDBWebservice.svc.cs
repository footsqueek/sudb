﻿using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Script.Serialization;

namespace SUDatabase.Webservice
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SUDBWebservice" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SUDBWebservice.svc or SUDBWebservice.svc.cs at the Solution Explorer and start debugging.
    public class SUDBWebservice : ISUDBWebservice
    {

        DataClasses1DataContext db = new DataClasses1DataContext();
        public class Member
        {
            public string StudentNumber { get; set; }
            public string StudentName { get; set; }
            public string StudentDOB { get; set; }
            public string Medical { get; set; }

        }


        public class User
        {
            public string Guid { get; set; }

        }
            public string returnMembers(String memberGuid)
            {
                int currentyear = DateTime.Now.Year;
                if (DateTime.Now < new DateTime(DateTime.Now.Year, 08, 01))
                {
                    currentyear = currentyear - 1;
                }

                tblStaffSecurity staff = db.tblStaffSecurities.Where(p => p.UserKey == Guid.Parse(memberGuid)).Single();

                DateTime lowestDateJoined = new DateTime(currentyear, 08, 01);
                DateTime highestDateJoined = new DateTime(currentyear + 1, 07, 31);

                var members = from p in db.tblMemberships join u in db.tblStudents on p.StudentId equals u.Id join r in db.tblTransactions on p.TransactionId equals r.transactionId where (p.DateJoined.Value < highestDateJoined) where (p.DateJoined.Value > lowestDateJoined) where (p.SportSocId == staff.ActiveSportSociety) where (r.status == 1) select u;

                Member[] e = new Member[members.Count()];
                int i = 0;
                foreach (var member in members)
                {
                    e[i] = new Member();
                    e[i].StudentNumber = member.StudentNumber;
                    e[i].StudentName = member.FirstName + " " + member.Surname;
                    e[i].StudentDOB = member.DateOfBirth.ToString();
                    e[i].Medical = member.MedicalInformation;
                    i++;
                }


                return new JavaScriptSerializer().Serialize(e);
            }
        }

}
