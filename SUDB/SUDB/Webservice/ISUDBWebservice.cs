﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Script.Serialization;

namespace SUDatabase.Webservice
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISUDBWebservice" in both code and config file together.
    [ServiceContract]
    public interface ISUDBWebservice
    {
        [OperationContract]
        string returnMembers(String memberGuid);
    }

    
}
