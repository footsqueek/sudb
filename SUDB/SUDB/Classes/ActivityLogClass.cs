﻿using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace SUDatabase.Classes
{
    public class ActivityLogClass
    {

        DataClasses1DataContext db = new DataClasses1DataContext();
        
        

        public void logActivity(String strActivityName, String strActivityDescription)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                //Lookup SU
                var userdetail = db.tblStaffSecurities.Where(p => p.UserKey.Equals(user.ProviderUserKey)).FirstOrDefault();

                tblActivityLog newActivity = new tblActivityLog
                {
                    UserGuid = (Guid)user.ProviderUserKey,
                    DateTime = DateTime.Now,
                    ActivityName = strActivityName,
                    ActivityDetails = strActivityDescription,
                    StudentUnion=userdetail.StudentsUnion,
                    Type=1

                };
                db.tblActivityLogs.InsertOnSubmit(newActivity);
                db.SubmitChanges();
            }
            else
            {
                tblActivityLog newActivity = new tblActivityLog
              {
                  DateTime = DateTime.Now,
                  ActivityName = strActivityName,
                  ActivityDetails = strActivityDescription,
                  Type=1

              };
                db.tblActivityLogs.InsertOnSubmit(newActivity);
                db.SubmitChanges();
            }
        }



        public void logDOCActivity(String strActivityName, String strActivityDescription)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                //Lookup SU
                var userdetail = db.tblStaffSecurities.Where(p => p.UserKey.Equals(user.ProviderUserKey)).FirstOrDefault();

                tblActivityLog newActivity = new tblActivityLog
                {
                    UserGuid = (Guid)user.ProviderUserKey,
                    DateTime = DateTime.Now,
                    ActivityName = strActivityName,
                    ActivityDetails = strActivityDescription,
                    StudentUnion = userdetail.StudentsUnion,
                    Type = 2

                };
                db.tblActivityLogs.InsertOnSubmit(newActivity);
                db.SubmitChanges();
            }
            else
            {
                tblActivityLog newActivity = new tblActivityLog
                {
                    DateTime = DateTime.Now,
                    ActivityName = strActivityName,
                    ActivityDetails = strActivityDescription,
                    Type=1
                };
                db.tblActivityLogs.InsertOnSubmit(newActivity);
                db.SubmitChanges();
            }
        }



        public IQueryable returnActivities(int startIndex, Guid filteruser)
        {
               MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                //Lookup SU
                var userdetail = db.tblStaffSecurities.Where(p => p.UserKey.Equals(user.ProviderUserKey)).FirstOrDefault();

                var activities = from p in db.tblActivityLogs where p.StudentUnion.Equals(userdetail.StudentsUnion) select p;
               activities = activities.OrderByDescending(p => p.DateTime);

               if (filteruser != Guid.Empty)
               {
                   activities = activities.Where(p => p.UserGuid.Equals(filteruser));
               }
               activities = activities.Take(200);
                return activities;


        }

        public IQueryable<tblActivityLog> returnDOCActivities(DateTime date)
        {
            MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            //Lookup SU
            var userdetail = db.tblStaffSecurities.Where(p => p.UserKey.Equals(user.ProviderUserKey)).FirstOrDefault();

            var activities = from p in db.tblActivityLogs where p.StudentUnion.Equals(userdetail.StudentsUnion) where p.Type==2 where p.DateTime.Value.Date == date.Date select p;
            activities = activities.OrderByDescending(p => p.DateTime);

            
            //activities = activities.Take(500);
            return activities;


        }
    }
}