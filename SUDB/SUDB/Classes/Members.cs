﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SUDB.Classes
{
   
        public class fulltransactiondetailmember
        {
            public int transactionId{get;set;}
            public string StudentNumber { get; set; }
            public string FirstName { get; set; }
            public string Lastname { get; set; }
            public string SportSoc { get; set; }
            public DateTime Date { get; set; }
            public string NominalCode { get; set; }

        
            public decimal Gross { get; set; }
            public int Status { get; set; }
        }

   
}