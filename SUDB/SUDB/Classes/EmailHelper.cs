﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace SUDatabase.Classes
{
    public class EmailHelper
    {
        public bool Send(string to, string subject, string body)
        {

            try
            {
                var mailMessage = new MailMessage("admin@sudb.co.uk", to, subject, body);
                Send(mailMessage);

                ActivityLogClass log = new ActivityLogClass();
                log.logActivity("Email Sent", "An email has been sent to " + to + " " + subject + " " + body);

                return true;
            }
            catch (Exception e)
            {
                ActivityLogClass log = new ActivityLogClass();
                log.logActivity("Email Failed", "An email failed to " + to + " " + subject + " " + body + e.ToString());
                return false;
            }

        }

        public static void Send(MailMessage message)
        {
            var smtpClient = new SmtpClient();
            smtpClient.Send(message);
        }
    }
}