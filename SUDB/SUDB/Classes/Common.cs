﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using SUDatabase.Classes;
using System.Web.Profile;
using SUDB;
using SUDB.Classes;
using System.Threading.Tasks;

namespace SUDatabase.Classes
{
    public class Common
    {
  
        DataClasses1DataContext db = new DataClasses1DataContext();
        ActivityLogClass activitylogger = new ActivityLogClass();

        public bool saveAttachment(int noteId, string docname, string url)
        {
            try
            {
                tblCaseNoteAttachment attachment = new tblCaseNoteAttachment
                {
                    docName = docname,
                    attachmentURL = url,
                    noteId=noteId,
                };
                db.tblCaseNoteAttachments.InsertOnSubmit(attachment);
                db.SubmitChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public string returnDBName()
        {
          return db.Connection.Database;
        }
        public IQueryable<tblCaseNoteAttachment> returnNoteAttachments(int noteId)
        {
            var attachments = from p in db.tblCaseNoteAttachments where p.noteId == noteId select p;
            return attachments;
        }

        public void refundMembership(int membershipId)
        {

            var membership = db.tblMemberships.Where(p => p.Id == membershipId).Single();
            membership.Refunded = DateTime.Now;
            db.SubmitChanges();

        }

        public bool strikedisciplinary(int sanctionId)
        {
            try
            {
                MembershipUser user = Membership.GetUser();

                var sanction = db.tblSanctions.Where(p => p.Id == sanctionId).Single();
                sanction.StrikenDate = DateTime.Now;
                sanction.StrikenBy = (Guid)user.ProviderUserKey;
                db.SubmitChanges(); 
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool createSanction(int SanctionArea, DateTime startdate, DateTime enddate, String notes)
        {
            try
            {
                MembershipUser user = Membership.GetUser();

                tblSanction newsanction = new tblSanction
                {
                    SanctionAreaId = SanctionArea,
                    SanctionStartDate = startdate,
                    SanctionEndDate = enddate,
                    Notes = notes,
                    CreatedBy = (Guid)user.ProviderUserKey,
                    StudentId=returnStudentToEdit().Id,
                };

                db.tblSanctions.InsertOnSubmit(newsanction);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }

        }

        public IQueryable<tblStudentUnion> returnSUs()
        {
            var sus = from p in db.tblStudentUnions select p;
            return sus;
        }

        public IQueryable<tblSanction> returnSanctions(int StudentId)
        {
            var sanctions = from p in db.tblSanctions where p.StudentId == StudentId select p;
            return sanctions;
        }

        public IQueryable<tblSanctionArea1> returnSanctionAreas()
        {
            var sanctions = from p in db.tblSanctionArea1s where p.SU==getSU().Id select p;
            return sanctions;
        }

        public tblSanctionArea1 returnSanctionArea(int SanctionArea)
        {
            var sanctionarea = db.tblSanctionArea1s.Where(p => p.Id == SanctionArea).Single();
            return sanctionarea;
        }



        public void userLoggedIn(string username)
        {
            
              activitylogger.logActivity("User Logged In", "User " + username + " has logged in");

        }

        public IQueryable<tblWelfareTag> returnAllTags()
        {
            var su = getSU();

            var tags = from p in db.tblWelfareTags where p.SU == su.Id select p;
            return tags;
        }

        public void addTag(string tagname)
        {
            var su = getSU();

            tblWelfareTag newtag = new tblWelfareTag
            {
                Tag = tagname,
                SU = su.Id,
                Active = 1
            };
            db.tblWelfareTags.InsertOnSubmit(newtag);
            db.SubmitChanges();

        }

        public IQueryable<tblWelfareTag> returnActiveTags()
        {
            var su = getSU();

            var tags = from p in db.tblWelfareTags where p.Active==1 where p.SU == su.Id select p;
            return tags;
        }

        public IQueryable<tblWelfareTag> returnCaseTags(int caseId)
        {
            var su = getSU();

            var tags = from p in db.tblCaseTags join x in db.tblWelfareTags on p.tagId equals x.Id where p.caseId==caseId select x;
            return tags;
        }

        public int returnnotagcases(int tagId)
        {
            var cases = from p in db.tblCaseTags where p.tagId == tagId select p;
            return cases.Count();
        }

        public int returnTimetagcases(int tagId)
        {
            try
            {
                var cases = from p in db.tblCaseTags join x in db.tblCases on p.caseId equals x.caseId join q in db.tblCaseLogs on x.caseId equals q.caseId join t in db.tblTimeUnits on q.timeUnits equals t.Id select t;
                return (int)cases.Sum(p => p.MinutesToAdd);
            }
            catch
            {
                return 0;
            }
        }


        public void removeTag(int tagId, int caseId)
        {
            var tag = db.tblCaseTags.Where(p => p.tagId == tagId).Where(p => p.caseId == caseId).Single();

            db.tblCaseTags.DeleteOnSubmit(tag);
            db.SubmitChanges();

        }

        public void insertTag(int tagId, int caseId)
        {
            try
            {
                tblCaseTag newtag = new tblCaseTag
                {
                    caseId = caseId,
                    tagId = tagId
                };
                db.tblCaseTags.InsertOnSubmit(newtag);
                db.SubmitChanges();
            }
            catch
            {

            }

        }


        public void deactivateTag(int tagId)
        {
            var tag = db.tblWelfareTags.Where(p => p.Id == tagId).Single();
            tag.Active = 0;
            db.SubmitChanges();

        }

        public void activateTag(int tagId)
        {
            var tag = db.tblWelfareTags.Where(p => p.Id == tagId).Single();
            tag.Active = 1;
            db.SubmitChanges();

        }

        public int totalUsersLoggedInToday()
        {
            var loggedinevents = (from p in db.tblActivityLogs where p.ActivityName == "User Logged In" where p.DateTime.Value.Date == DateTime.Now.Date where p.StudentUnion == getSU().Id select new { p.UserGuid}).Distinct();

            return loggedinevents.Count();
        }
        public tblMembership returnMembership(int membershipId)
        {
            var membership = db.tblMemberships.Where(p => p.Id == membershipId).Single();
            return membership;
        }
        public IQueryable<tblMembership> returnMembersforSession(int SessionId)
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }


            DateTime lowestDateJoined = new DateTime(currentyear, (int)su.YearStartMonth, 01);
            DateTime highestDateJoined = new DateTime(currentyear + 1, (int)su.YearStartMonth-1, 31);

            var session = returnSession(SessionId);

           

            if (session.teamId == 0)
            {
                var members = from p in db.tblMemberships join r in db.tblTransactions on p.TransactionId equals r.transactionId join d in db.tblStudents on p.StudentId equals d.Id where (p.DateJoined.Value < highestDateJoined) where (p.DateJoined.Value > lowestDateJoined) where (p.SportSocId == session.SportSocietyId) where (r.status == 1 || r.status == 3 || r.status == 4 || r.status == 5) where p.Refunded == null orderby d.Surname select p;
                return members;
            }
            else
            {
                var members = from p in db.tblMemberships join r in db.tblTransactions on p.TransactionId equals r.transactionId join d in db.tblStudents on p.StudentId equals d.Id join e in db.tblTeamMembers on p.StudentId equals e.StudentId where e.teamId == session.teamId where (p.DateJoined.Value < highestDateJoined) where (p.DateJoined.Value > lowestDateJoined) where p.Refunded == null where (p.SportSocId == session.SportSocietyId) where (r.status == 1 || r.status == 4 || r.status == 3 || r.status == 5) orderby d.Surname select p;
                var teammembers = returnTeamMembers((int)session.teamId);

                var newmembers = from q in teammembers join x in members on q.StudentId equals x.StudentId select x;
          

                return newmembers;
            }
        }



        public tblStudent createStudent(string firstname, string surname, string studentNo)
        {
            tblStudent newstudent = new tblStudent
            {
                FirstName = firstname,
                Surname = surname,
                StudentNumber = studentNo,
                StudentsUnion=getSU().Id,
                Faculty=ReturnFaculties().First().Id,
                Campus=ReturnCapuses().First().Id,
            };
            db.tblStudents.InsertOnSubmit(newstudent);
            db.SubmitChanges();

            activitylogger.logActivity("Create Student", "Student " + studentNo + " has been created.");

            return newstudent;
        }

        public void updateMedical(int StudentId, String Medical)
        {
            var student = db.tblStudents.Where(p => p.Id == StudentId).FirstOrDefault();
            try
            {
                student.MedicalInformation=Medical;
                db.SubmitChanges();
            }
            catch
            {
            }

        }

        public void updateMobile(int StudentId, String Mobile)
        {
            var student = db.tblStudents.Where(p => p.Id == StudentId).FirstOrDefault();
            try
            {
                student.MobileNumber = Mobile;
                db.SubmitChanges();
            }
            catch
            {
            }

        }

        public void setoktoshare(int StudentId)
        {


            var student = db.tblStudents.Where(p => p.Id == StudentId).Single();
            try
            {
                student.OktoShareMedical = 1;
                db.SubmitChanges();
            }
            catch
            {
            }

        }

        public void updateBilling(int StudentId, String Billing1, String Billing2, String BillingCity, String BillingPostcode, String BillingPhone)
        {
            var student = db.tblStudents.Where(p => p.Id == StudentId).Single();

            student.Billing1 = Billing1;
            student.Billing2 = Billing2;
            student.BillingCity = BillingCity;
            student.BillingPostCode = BillingPostcode;
            student.BillingPhoneNumber = BillingPhone;
            db.SubmitChanges();

        }

        public void setHasDisability(int StudentId)
        {


            var student = db.tblStudents.Where(p => p.Id == StudentId).Single();
            try
            {
                student.Disability = 1;
                db.SubmitChanges();
            }
            catch
            {
            }

        }

        public tblStudent updateStudent(int oktoshare, DateTime FirstAid,String Course, int Driver, DateTime DOB, int Campus, int Faculty, string level, int memtype, string address, string email, string mobile, string medical, string notes)
        {
            var student = db.tblStudents.Where(p => p.Id == returnStudentToEdit().Id).Single();
            try
            {
                if (DOB.Date == DateTime.Now.Date)
                {
                }
                else
                {
                    student.DateOfBirth = DOB;
                }
            }
            catch { }
            try
            {
                student.FirstAidCertExpiry = FirstAid;
            }
            catch { }
            try
            { student.Campus = Campus; }
            catch { }
            try
            { student.Course = Course; }
            catch { }
            try
            { student.Faculty = Faculty; }
            catch { }
            try
            { student.MiniBusAssessment = Driver; }
            catch { }
            try
            { student.OktoShareMedical = oktoshare; }
            catch { }
            try
            { student.Level = level; }
            catch { }
            try
            {student.MembershipType = memtype;}
            catch { }
            try
            { student.TermTimeAddress = address; }
            catch { }
            try { student.PersonalEmail = email; }
            catch { }
            try { student.MobileNumber = mobile; }
            catch { }
            try { student.MedicalInformation = medical; }
            catch { }
            try { student.Notes = notes; }
            catch { }
            db.SubmitChanges();

            activitylogger.logActivity("Student Updated", "Student " + returnStudentToEdit().Id.ToString() + " has been updated.");


            return student;
        }

        public tblStudent updateStudentFromWelfare(string mobile, string personalEmail)
        {
            var student = db.tblStudents.Where(p => p.Id == returnStudentToEdit().Id).Single();
            student.PersonalEmail = personalEmail;
            student.MobileNumber = mobile;
            db.SubmitChanges();
            activitylogger.logActivity("Student Updated", "Student " + returnStudentToEdit().Id.ToString() + " has been updated.");

            return student;
        }


        public void updateEmailPrefs(int cases, int transport, int rooms, int tripreg)
        {
            var user = Membership.GetUser();

            var permissions = getPermissions((Guid)user.ProviderUserKey);

            permissions.transportBookingEmails = transport;
            permissions.roomBookingEmails = rooms;
            permissions.tripRegEmails = tripreg;
            permissions.caseEmails = cases;
            db.SubmitChanges();
        }

        public tblSession returnSession(int SessionId)
        {
            var session = db.tblSessions.Where(p => p.Id == SessionId).Single();
            return session;
        }

        public tblSession returnSessionFromTrip(int tripId)
        {
            var session = db.tblSessions.Where(p => p.TripRegId == tripId).First();
            return session;
        }

        public tblRiskQuestion returnRiskQuestion(int questionId)
        {
            var riskquestion = db.tblRiskQuestions.Where(p => p.Id == questionId).Single();
            return riskquestion;
        }

        public void clearRisk(int SessionId, int RiskQuestionId, string txtAction)
        {
            var risk = db.tblRiskChecklists.Where(p => p.RiskQuestionId == RiskQuestionId).Where(p => p.sessionId == SessionId).Single();
            risk.UnionActionStatus = 1;
            risk.ActionTaken = txtAction;
            db.SubmitChanges();

            activitylogger.logDOCActivity("Clear Risk", risk.WhoIsAtRisk + " | " + risk.ActionTaken + " has been cleared");


        }

        public tblRoomRequest returnRoomRequest(int requestId)
        {
            try
            {
                var room = db.tblRoomRequests.Where(p => p.Id == requestId).Where(p => p.SportSociety == returnActiveSportSoc().Id).Single();
                return room;
            }
            catch
            {
                return null;
            }
        }

        public tblRoomRequest returnAllRoomRequest(int requestId)
        {
            try
            {
                var room = db.tblRoomRequests.Where(p => p.Id == requestId).Single();
                return room;
            }
            catch
            {
                return null;
            }
        }

        public tblRoomRequest returnRoomRequestAdmin(int requestId)
        {
            try
            {
                var room = db.tblRoomRequests.Where(p => p.Id == requestId).Single();
                return room;
            }
            catch
            {
                return null;
            }
        }

        public tblTransportRequest returnTransportRequest(int requestId)
        {
            try
            {
                var room = db.tblTransportRequests.Where(p => p.Id == requestId).Where(p => p.SportSociety == returnActiveSportSoc().Id).Single();
                return room;
            }
            catch
            {
                return null;
            }
        }

        public tblTransportRequest returnTransportRequestAdmin(int requestId)
        {
            try
            {
                var room = db.tblTransportRequests.Where(p => p.Id == requestId).Single();
                return room;
            }
            catch
            {
                return null;
            }
        }

        public IQueryable<tblRoomRequest> returnRooms()
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }

            var room = from p in db.tblRoomRequests where p.RequestDate.Value.Year>=currentyear where p.SportSociety == returnActiveSportSoc().Id select p;
            return room;
        }

        public IQueryable<tblTransportRequest> returnTransport()
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }

            var room = from p in db.tblTransportRequests where p.RequestDate.Value.Year>=currentyear where p.SportSociety == returnActiveSportSoc().Id select p;
            return room;
        }


        public IQueryable<tblRiskChecklist> returnOpenRisks()
        {
            var risks = from p in db.tblRiskChecklists join x in db.tblSessions on p.sessionId equals x.Id join q in db.tblSportsSocieties on x.SportSocietyId equals q.Id where q.SU==getSU().Id where p.UnionActionStatus==0 where p.Status==0 select p;
            activitylogger.logDOCActivity("Viewed Open Risks", "All open Risks have been reviewed");
            return risks;
        }

        public IQueryable<tblSession> returnSessionsByRoom(int RoomRequestId)
        {
            var session = from p in db.tblSessions where p.linkedRoomBookingRequest==RoomRequestId select p;
            return session;
        }

        public IQueryable<tblSession> returnSessionsByTransport(int TransportRequestId)
        {
            var session = from p in db.tblSessions where p.linkedTransportBookingRequest == TransportRequestId select p;
            return session;
        }

        public IQueryable<tblSession> returnSessionsWithMissingSubmissions()
        {
            var sessions = from p in db.tblSessions join x in db.tblSportsSocieties on p.SportSocietyId equals x.Id where x.SU==getSU().Id where (p.RiskAssessmentId==0 || p.RegisterComplete==0) where p.EndDateTime<DateTime.Now.AddDays(-1) where p.clearMiss==0  select p;
            activitylogger.logDOCActivity("Viewed Missing Submissions", "Sessions with missing submissions have been reviewed");
            return sessions;
        }

        public IQueryable<tblSession> returnSessionsThisYear()
        {
            var active = returnActiveSportSoc();

            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }

            DateTime lowestDateJoined = new DateTime(currentyear, (int)su.YearStartMonth, 01);
            DateTime highestDateJoined = new DateTime(currentyear + 1, (int)su.YearStartMonth - 1, 31);


            var sessions = from p in db.tblSessions join x in db.tblSportsSocieties on p.SportSocietyId equals x.Id where p.SportSocietyId==active.Id where x.SU == getSU().Id where p.StartDateTime>=lowestDateJoined where p.EndDateTime<DateTime.Now select p;
            return sessions;
        }


        public IQueryable<tblRegister> returnSessionsAttended(int studentId)
        {
            var active = returnActiveSportSoc();

            var su = getSU();

          
            var register = from p in db.tblRegisters where p.StudentId == studentId join x in db.tblSessions on p.SessionId equals x.Id where x.SportSocietyId == active.Id where p.AttendanceStatus == 1 select p;

             return register;
        }

        public bool isSportSockLockedOut()
        {
            try
            {
                var sessions = from p in db.tblSessions where (p.RiskAssessmentId == 0 || p.RegisterComplete == 0) where p.EndDateTime < DateTime.Now.AddDays(-1) where p.SportSocietyId == returnActiveSportSoc().Id where p.clearMiss == 0 select p;
                if (sessions.Count() > 0)
                {
                    return true;
                }
                else
                {
                    var sportsoc = returnActiveSportSoc();
                    if (sportsoc.Lock == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch
            {
                return false;
            }
            
        }

        public void clearMissed(int SessionId, int missedreason, string missedexplanation)
        {
            var session = db.tblSessions.Where(p=>p.Id==SessionId).Single();
            session.clearMiss = 1;
            session.missedReason = missedreason;
            session.missedExplanation = missedexplanation;
            db.SubmitChanges();

            var sportsoc = returnSportSociety((int)session.SportSocietyId);

            activitylogger.logDOCActivity("Society Unlocked", "Missed submissions cleared: "+ sportsoc.Name);
            

        }

        public IQueryable<tblSession> returnSessionsNext14()
        {
            var session = from p in db.tblSessions join d in db.tblSportsSocieties on p.SportSocietyId equals d.Id where d.SU==getSU().Id where p.StartDateTime< DateTime.Now.AddDays(14) where p.StartDateTime >DateTime.Now select p;
            return session;
        }

        public IQueryable<tblSession> returnSessionsLast14()
        {
            var session = from p in db.tblSessions join d in db.tblSportsSocieties on p.SportSocietyId equals d.Id where d.SU == getSU().Id where p.StartDateTime > DateTime.Now.AddDays(-14) where p.StartDateTime < DateTime.Now orderby (p.StartDateTime) descending select p;
            return session;
        }

        public IQueryable<tblTeam> returnTeams()
        {
            var teams = from p in db.tblTeams where (p.SportSocietyId==returnActiveSportSoc().Id) select p;
            return teams;
        }

        public IQueryable<tblTeam> returnTeamsFor(int sportId)
        {
            var teams = from p in db.tblTeams where (p.SportSocietyId == sportId) select p;
            return teams;
        }

        public IQueryable<tblTeamMember> returnTeamMembers(int TeamId)
        {
            var teammembers = from p in db.tblTeamMembers where (p.teamId == TeamId) select p;
            return teammembers;
        }


        public IQueryable<tblRiskQuestion> returnRiskQuestions()
        {
          
            var riskquestions = from p in db.tblRiskQuestions where (p.SU == getSU().Id) select p;
            return riskquestions;
        }

        public IQueryable<tblModeofTransport> returnModeofTransport()
        {

            var mode = from p in db.tblModeofTransports where (p.SU == getSU().Id) select p;
            return mode;
        }

        public IQueryable<tblTripRegistration> returnTripsAwaitingApproval()
        {

            var trips = from p in db.tblTripRegistrations join q in db.tblSportsSocieties on p.SportSocietyId equals q.Id where (p.ApprovalStatus==1) where (q.SU == getSU().Id) select p;
            activitylogger.logDOCActivity("Trips", "Trips awaiting approval have been reviewed");
            return trips;
        }

        public IQueryable<tblTripRegistration> returnTripsApproved()
        {

            var trips = from p in db.tblTripRegistrations join q in db.tblSportsSocieties on p.SportSocietyId equals q.Id where (p.ApprovalStatus == 2) where (q.SU == getSU().Id) where p.DepartureTime > DateTime.Now.AddDays(-10) orderby p.DepartureTime ascending select p;
             return trips;
        }

        public IQueryable<tblTripRegistration> returnTripsRejected()
        {

            var trips = from p in db.tblTripRegistrations join q in db.tblSportsSocieties on p.SportSocietyId equals q.Id where (p.ApprovalStatus == 3) where (q.SU == getSU().Id) where p.DepartureTime > DateTime.Now.AddDays(-10) select p;
            return trips;
        }

        public IQueryable<tblDOCType> returnDOCTypes()
        {

            var types = from p in db.tblDOCTypes where p.StudentsUnion==getSU().Id select p;
            return types;
        }

        public tblTripRegistration returnTrip(int tripId)
        {
            
                var mode = db.tblTripRegistrations.Where(p => p.tripId == tripId).FirstOrDefault();
                return mode;
          
        }

        public tblTripRegistration createTrip(int SportSocietyId)
        {
            tblTripRegistration newtrip = new tblTripRegistration
            {
                SportSocietyId = SportSocietyId,
                ApprovalStatus = 0,
            };
            var sportsoc = returnSportSociety(SportSocietyId);
            activitylogger.logDOCActivity(sportsoc.Name, "Created a trip");

            db.tblTripRegistrations.InsertOnSubmit(newtrip);
            db.SubmitChanges();

            return newtrip;
        }

        public tblRoomRequest createRoomRequest(int SportSocietyId, string defaultheadline)
        {
            tblRoomRequest newtrip = new tblRoomRequest
            {
                SportSociety=SportSocietyId,
             RequestDate = DateTime.Now,
             Headline = defaultheadline
            };
            var sportsoc = returnSportSociety(SportSocietyId);
            activitylogger.logDOCActivity(sportsoc.Name, "Created a room booking request");

            db.tblRoomRequests.InsertOnSubmit(newtrip);
            db.SubmitChanges();

            return newtrip;
        }

        public tblTransportRequest createTransportRequest(int SportSocietyId, string defaultheadline)
        {
            tblTransportRequest newtrip = new tblTransportRequest
            {
                SportSociety = SportSocietyId,
                RequestDate = DateTime.Now,
                Headline = defaultheadline
            };
            var sportsoc = returnSportSociety(SportSocietyId);
            activitylogger.logDOCActivity(sportsoc.Name, "Created a transport booking request");

            db.tblTransportRequests.InsertOnSubmit(newtrip);
            db.SubmitChanges();

            return newtrip;
        }

        public tblTeam returnTeam(int teamId)
        {
            try
            {
                var teams = db.tblTeams.Where(p => p.Id == teamId).Single();
                return teams;
            }
            catch
            {
                return null;
            }
        }


        public void createTeam(string teamName)
        {
            tblTeam newTeam = new tblTeam
            {
                TeamName = teamName,
                SportSocietyId = returnActiveSportSoc().Id
            };

            var sportsoc = returnSportSociety(returnActiveSportSoc().Id);
            activitylogger.logDOCActivity(sportsoc.Name, "Created a new team called " + teamName);


            db.tblTeams.InsertOnSubmit(newTeam);
            db.SubmitChanges();
        }

        public void deleteTeam(int teamId)
        {
            var team = db.tblTeams.Where(p => p.Id == teamId).Single();
            var sportsoc = returnSportSociety(returnActiveSportSoc().Id);
            activitylogger.logDOCActivity(sportsoc.Name, "Deleted a new team called " + team.TeamName);




            db.tblTeams.DeleteOnSubmit(team);
            db.SubmitChanges();

            //Loop through team members and delete them to
            var members = from p in db.tblTeamMembers where (p.teamId == teamId) select p;
            db.tblTeamMembers.DeleteAllOnSubmit(members);
            db.SubmitChanges();
        }


        public void deleteSession(int sessionId)
        {
            var team = db.tblSessions.Where(p => p.Id == sessionId).Single();

            var sportsoc = returnSportSociety(returnActiveSportSoc().Id);
            activitylogger.logDOCActivity(sportsoc.Name, "Deleted a session called " + team.SessionName);



            db.tblSessions.DeleteOnSubmit(team);
            db.SubmitChanges();
        }


        public tblMembership createMembership(int StudentId, int SportSocId, int TransactionId)
        {
            tblMembership newMembership = new tblMembership
            {
                SportSocId = SportSocId,
                StudentId = StudentId,
                TransactionId = TransactionId,
                DateJoined = DateTime.Now,
                Amount = getPrice(SportSocId)
            };
            db.tblMemberships.InsertOnSubmit(newMembership);
            db.SubmitChanges();

            return newMembership;
        }

        public IQueryable<tblMembership> returnSportSocietyMembers(int SportSociety, int Year)
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }

            DateTime lowestDateJoined = new DateTime(Year, (int)su.YearStartMonth, 01);
            DateTime highestDateJoined = new DateTime(Year+1, (int)su.YearStartMonth-1, 31);

            var members = from p in db.tblMemberships join s in db.tblStudents on p.StudentId equals s.Id join t in db.tblMembershipTypes on s.MembershipType equals t.Id where t.CanJoinSports == 1 join r in db.tblTransactions on p.TransactionId equals r.transactionId where (p.DateJoined.Value < highestDateJoined) where (p.DateJoined.Value > lowestDateJoined) where (p.SportSocId == SportSociety) where (r.status == 1 || r.status == 3 || r.status == 4 || r.status == 5) where p.Refunded ==null orderby s.Surname ascending select p;
            return members;

        }

        public bool checkIfMember(int SportSociety, int StudentId)
        {
            var su = db.tblStudentUnions.Where(p=>p.Id==1).Single();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }

            DateTime lowestDateJoined = new DateTime(currentyear, (int)su.YearStartMonth, 01);
            DateTime highestDateJoined = new DateTime(currentyear + 1, (int)su.YearStartMonth - 1, 31);

            var members = from p in db.tblMemberships where p.StudentId==StudentId join s in db.tblStudents on p.StudentId equals s.Id join t in db.tblMembershipTypes on s.MembershipType equals t.Id where t.CanJoinSports == 1 join r in db.tblTransactions on p.TransactionId equals r.transactionId where (p.DateJoined.Value < highestDateJoined) where (p.DateJoined.Value > lowestDateJoined) where (p.SportSocId == SportSociety) where (r.status == 1 || r.status == 3 || r.status == 4 || r.status == 5) where p.Refunded == null orderby s.Surname ascending select p;
            if (members.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public int returnTotalSportSocietyMembers()
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }

            DateTime lowestDateJoined = new DateTime(currentyear, (int)su.YearStartMonth, 01);
            DateTime highestDateJoined = new DateTime(currentyear + 1, (int)su.YearStartMonth - 1, 31);

            var members = from p in db.tblMemberships join sp in db.tblSportsSocieties on p.SportSocId equals sp.Id join ty in db.tblDOCTypes on sp.Type equals ty.Id join r in db.tblTransactions on p.TransactionId equals r.transactionId join s in db.tblStudents on p.StudentId equals s.Id join t in db.tblMembershipTypes on s.MembershipType equals t.Id where t.CanJoinSports == 1 where (s.StudentsUnion == su.Id) where (p.DateJoined.Value < highestDateJoined) join y in db.tblSportsSocieties on p.SportSocId equals y.Id where y.Archived != 1 where ty.IncludeInStats ==1 where (t.CanJoinSports==1) where (p.DateJoined.Value > lowestDateJoined) where (r.status == 1 || r.status == 3 || r.status == 4 || r.status == 5) where p.Refunded == null select p;
            return members.Count();

        }



        public int returnTotalIndividualSportSocietyMembers()
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }

            DateTime lowestDateJoined = new DateTime(currentyear, (int)su.YearStartMonth, 01);
            DateTime highestDateJoined = new DateTime(currentyear + 1, (int)su.YearStartMonth - 1, 31);

            var members = (from p in db.tblMemberships join sp in db.tblSportsSocieties on p.SportSocId equals sp.Id join ty in db.tblDOCTypes on sp.Type equals ty.Id join r in db.tblTransactions on p.TransactionId equals r.transactionId join s in db.tblStudents on p.StudentId equals s.Id join t in db.tblMembershipTypes on s.MembershipType equals t.Id where t.CanJoinSports == 1 where ty.IncludeInStats==1 where (s.StudentsUnion == su.Id) where (p.DateJoined.Value < highestDateJoined) join y in db.tblSportsSocieties on p.SportSocId equals y.Id where (t.CanJoinSports==1) where y.Archived != 1 where (p.DateJoined.Value > lowestDateJoined) where (r.status == 1 || r.status == 3 || r.status == 4 || r.status == 5) where p.Refunded == null select new { p.StudentId }).Distinct();
            return members.Count();

        }


        public IQueryable<tblStudent> returnIndividualSportSocietyMembersType(int type)
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }

            DateTime lowestDateJoined = new DateTime(currentyear, (int)su.YearStartMonth, 01);
            DateTime highestDateJoined = new DateTime(currentyear + 1, (int)su.YearStartMonth - 1, 31);

            var members = (from p in db.tblMemberships join r in db.tblTransactions on p.TransactionId equals r.transactionId join s in db.tblStudents on p.StudentId equals s.Id join t in db.tblMembershipTypes on s.MembershipType equals t.Id where t.CanJoinSports == 1 where (s.StudentsUnion == su.Id) where (p.DateJoined.Value < highestDateJoined) join y in db.tblSportsSocieties on p.SportSocId equals y.Id where (t.CanJoinSports == 1) where y.Archived != 1 where (p.DateJoined.Value > lowestDateJoined) where (r.status == 1 || r.status == 3 || r.status == 4 || r.status == 5) where p.Refunded == null where t.Id== type select s ).Distinct();
            return members;

        }


        public IQueryable<tblStudent> returnAllMembersType(int type)
        {
            var su = getSU();

                  
            var members = (from s in db.tblStudents join t in db.tblMembershipTypes on s.MembershipType equals t.Id where (s.StudentsUnion == su.Id) where t.Id == type select s).Distinct();
            return members;

        }



        public class membershipType
        {
            public int memTypeId { get; set; }
            public string memType { get; set; }
            public int count { get; set; }
        }

        public IQueryable<membershipType> returnMembersByType()
        {
            var su = getSU();

                 
            var members = (from s in db.tblStudents
                           join t in db.tblMembershipTypes on s.MembershipType equals t.Id
                          
                           where (s.StudentsUnion == su.Id)
                           group t by t.MembershipType into grp
                           select new membershipType { memTypeId = grp.First().Id, memType = grp.Key, count = grp.Count() });
            return members;

        }

        public IQueryable<membershipType> returnTotalIndividualSportSocietyMembersType()
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }

            DateTime lowestDateJoined = new DateTime(currentyear, (int)su.YearStartMonth, 01);
            DateTime highestDateJoined = new DateTime(currentyear + 1, (int)su.YearStartMonth - 1, 31);

            var members = (from p in db.tblMemberships join r in db.tblTransactions on p.TransactionId equals r.transactionId join s in db.tblStudents on p.StudentId equals s.Id join t in db.tblMembershipTypes on s.MembershipType equals t.Id where t.CanJoinSports == 1 where (s.StudentsUnion == su.Id) where (p.DateJoined.Value < highestDateJoined) join y in db.tblSportsSocieties on p.SportSocId equals y.Id where (t.CanJoinSports == 1) where y.Archived != 1 where (p.DateJoined.Value > lowestDateJoined) where (r.status == 1 || r.status == 3 || r.status == 4 || r.status == 5) where p.Refunded == null
                           group t by t.MembershipType into grp
                           select new membershipType {memTypeId=grp.First().Id, memType = grp.Key, count = grp.Count() });
            return members;

        }


        public tblTransaction createTransaction(double amount, int studentId)
        {
            tblTransaction newTransaction = new tblTransaction
            {
                transactionDate=DateTime.Now,
                Amount = (decimal)amount,
                status = 0,
                studentId = studentId,
                StudentUnion = 1
                
                
            };
            db.tblTransactions.InsertOnSubmit(newTransaction);
            db.SubmitChanges();
            return newTransaction;
        }


        public tblTransaction createTransactionUWSU(double amount, int studentId)
        {
            tblTransaction newTransaction = new tblTransaction
            {
                transactionDate = DateTime.Now,
                Amount = (decimal)amount,
                status = 0,
                studentId = studentId,
                StudentUnion = 3


            };
            db.tblTransactions.InsertOnSubmit(newTransaction);
            db.SubmitChanges();
            return newTransaction;
        }

        public tblTransaction updateTransactionStatus(int transactionId, int status)
        {

            var transaction = db.tblTransactions.Where(p => p.transactionId == transactionId).Single();
            transaction.status = status;
                db.SubmitChanges();

         

                return transaction;
        }

        public tblTransaction updateTransactionPoints(int transactionId, int status)
        {

            var transaction = db.tblTransactions.Where(p => p.transactionId == transactionId).Single();
            transaction.status = status;
            db.SubmitChanges();

            if (status == 1 || status == 5 || status==4 || status==3)
            {
                //Loop through order and award points for purchase
                var memberships = from p in db.tblMemberships join s in db.tblStudents on p.StudentId equals s.Id where p.TransactionId == transaction.transactionId select new { p, s };
                foreach (var membership in memberships)
                {
                    //Lookup sport soc
                    var sportsoc = db.tblSportsSocieties.Where(p => p.Id == membership.p.SportSocId).Single();
                    var doctype = returnDOCType((int)sportsoc.Type);
                    tblPointsTransaction newpoints = new tblPointsTransaction
                    {
                        datetime = DateTime.Now,
                        Reason = "Joining " + sportsoc.Name,
                        NumberOfPoints = doctype.PointsAwardForJoining,
                        StudentId = transaction.studentId
                    };
                    db.tblPointsTransactions.InsertOnSubmit(newpoints);
                    db.SubmitChanges();

                    SendPushNotification sendpush = new SendPushNotification();
                    sendpush.SendPushNotificationWithDevices(membership.s.UUID.ToString(), "Joining " + sportsoc.Name);
                }
            }

            return transaction;
        }

        public IQueryable<tblPointToken> getTokens()
        {
            var tokens = from p in db.tblPointTokens where p.StudentsUnion == getSU().Id orderby p.NumberOfPoints ascending select p;
            return tokens;
        }

        public tblPointToken getToken(Guid token)
        {
            var tokenis = db.tblPointTokens.Where(p => p.tokenCode == token).Single();
            return tokenis;
        }

        public IQueryable<tblPointsTransaction> getPointTransactions(Guid tokencode)
        {
            var tokens = from p in db.tblPointsTransactions where p.tokenCode == tokencode select p;
            return tokens;
        }

        public int getTotalPointTransactions()
        {
            var tokens = from p in db.tblPointsTransactions join s in db.tblStudents on p.StudentId equals s.Id where s.StudentsUnion==getSU().Id select p;
            return (int)tokens.Sum(p=>p.NumberOfPoints);
        }

        public bool addPoints(string reason, int number, int StudentId)
        {
            tblPointsTransaction newtrans = new tblPointsTransaction
            {
                StudentId = StudentId,
                Reason = reason,
                NumberOfPoints = number,
                datetime = DateTime.Now,
                PersonExecuting = (Guid)Membership.GetUser().ProviderUserKey

            };
            db.tblPointsTransactions.InsertOnSubmit(newtrans);
            db.SubmitChanges();

            try
            {
                var student = returnStudent(StudentId);

                SendPushNotification newpush = new SendPushNotification();
                newpush.SendPushNotificationWithDevices(student.UUID, reason + " : " + number + " Points");
            }
            catch
            {

            }

            return true;
            }

        public IQueryable<tblPointsTransaction> getPointAllTransactions(int studentId)
        {
            var tokens = from p in db.tblPointsTransactions where p.StudentId == studentId where p.NumberOfPoints !=0 select p;
            return tokens;
        }

        public tblStudentUnion getSU()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                var values = getPermissions((Guid)user.ProviderUserKey);

                tblStudentUnion su = db.tblStudentUnions.Where(p => p.Id == values.StudentsUnion).FirstOrDefault();

                return su;
             }
             else
             {
                 return null;
             }


          
        }



        public IQueryable<tblSportsSociety> returnThingsToBuy(int Type, int SU)
        {

            var thingstobuy = from p in db.tblSportsSocieties where (p.Type == Type) where (p.SU == SU) where(p.AvailableToBuy==1) where (p.Archived!=1) select p;
            return thingstobuy;


        }

        public IQueryable<tblMembership> returnTransactionDetail(int transId)
        {

            var transdetail = from p in db.tblMemberships where (p.TransactionId==transId) select p;
            return transdetail;


        }

        public tblTransaction returnTransaction(int transId)
        {

            var trans = db.tblTransactions.Where(p => p.transactionId == transId).Single();
            return trans;


        }


        public IQueryable returnTransactions(DateTime transdate)
        {

            var SU = getSU();

            var transactions = from p in db.tblTransactions where (p.StudentUnion == SU.Id) where (p.transactionDate.Value.Date == transdate.Date) orderby (p.transactionDate) descending select p;
            return transactions;


        }

        public IQueryable returnTransactionsBetweenDates(DateTime transdate, DateTime enddate)
        {

            var SU = getSU();

            var transactions = from p in db.tblTransactions where (p.StudentUnion == SU.Id) where (p.transactionDate.Value.Date >= transdate.Date) where (p.transactionDate.Value.Date <= enddate.Date) orderby (p.transactionDate) descending select p;
            return transactions;


        }

        public IQueryable<fulltransactiondetailmember> returnTransactionsDetailsBetweenDates(DateTime transdate, DateTime enddate)
        {

            var SU = getSU();

           var trans = from x in db.tblMemberships join q in db.tblStudents on x.StudentId equals q.Id join s in db.tblSportsSocieties on x.SportSocId equals s.Id join p in db.tblTransactions on x.TransactionId equals p.transactionId where (p.StudentUnion == SU.Id) where (p.transactionDate.Value.Date >= transdate.Date) where (p.transactionDate.Value.Date <= enddate.Date) orderby (p.transactionDate) descending select new fulltransactiondetailmember{Date=(DateTime)x.DateJoined, transactionId= p.transactionId,StudentNumber=q.StudentNumber,FirstName=q.FirstName,Lastname=q.Surname,SportSoc=s.Name,NominalCode=s.NominalCode,Gross=(decimal)p.Amount,Status=(int)p.status};
            return trans;


        }

        public IQueryable<tblTransaction> returnTransactionsThisYear()
        {

            var SU = getSU();
            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)SU.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }


            DateTime startreport = new DateTime(currentyear, (int)SU.YearStartMonth, 1);
            DateTime endreport = new DateTime(currentyear + 1, (int)SU.YearStartMonth, 31);
            endreport = endreport.AddMonths(-1);

            var transactions = from p in db.tblTransactions where (p.StudentUnion == SU.Id) where (p.transactionDate.Value.Date > startreport) where (p.transactionDate.Value.Date < endreport) where (p.status==1 || p.status==3 || p.status == 4 || p.status==5) orderby (p.transactionDate) descending select p;
            return transactions;


        }

        public bool isStudent(string studentNo, DateTime dob)
        {
            return true;
        }

        public bool saveName(string firstname, string surname, Guid userguid, int su)
        {
            var user = db.tblStaffSecurities.Where(p => p.UserKey == userguid).Single();
            user.Firstname = firstname;
            user.Surname = surname;
            user.StudentsUnion = su;
            db.SubmitChanges();
            return true;
        }

        public IQueryable<UserObject> getUsers()
        {
            List<Tuple<Guid,string,string,bool>> exportlist = new List<Tuple<Guid,string,string,bool>>();
            int currentsu = getSU().Id;

            Guid staffroleid = Guid.Parse("0fc1db4e-0a4b-4299-b433-2aa94df6c55f");

            var infobase = from p in db.tblStaffSecurities where (p.StudentsUnion==currentsu) join x in db.aspnet_UsersInRoles on p.UserKey equals x.UserId join e in db.aspnet_Memberships on p.UserKey equals e.UserId where (x.RoleId==staffroleid) select new UserObject {Email=e.Email.ToLower(),FirstName=p.Firstname,LastName=p.Surname,LastLogin=e.LastLoginDate,Guid=e.UserId,approved=e.IsApproved};

            /*
            foreach (var user in infobase)
            {
                //var userdets = Membership.GetUser(user);
                //var info = infobase.Where(p=>p.p.UserKey==(Guid)userdets.ProviderUserKey).FirstOrDefault();

                //if (info != null)
                //{
                    exportlist.Add(new Tuple<Guid, string, string, bool>(user.p.UserKey, user.e.Email.ToLower(), ((DateTime)user.e.LastLoginDate).ToString("dd MMM yyyy HH:mm"), user.e.IsApproved));
               // }
                
                

            }*/
           

            return infobase;
        }

        public Guid[] getSportUsers(int sportSocID)
        {
            //Get Current Users Students' Union
            var users = from p in db.tblSportSocietyAccesses where p.SportSocietyId == sportSocID select new { p.UserId };

            return users.AsEnumerable().Select(r => r.UserId).ToArray();
        }

        public bool resetPassword(Guid ProviderUserKey)
        {
            //This class changes the password for the specified user
            try
            {
                MembershipUser user = Membership.GetUser(ProviderUserKey);
                user.UnlockUser();
                user.IsApproved = true;
                String tempassword = user.ResetPassword();
                Membership.UpdateUser(user);
                //E-mail new password to user
                //Get E-mail helper class

                try
                {
                    ProfileBase profile = ProfileBase.Create(user.UserName);


                    EmailHelper newEmail = new EmailHelper();
                    newEmail.Send(user.Email, "SU Database Password Reset", "Dear " + profile["FirstName"] + ",\n\n Your password for the Students' Union Database has been reset. Your new password is:\n" + tempassword + "\n\n Kind Regards\nThe SU Database Team");
                }
                catch
                {
                    EmailHelper newEmail = new EmailHelper();
                    newEmail.Send(user.Email, "SU Database Password Reset", "Dear " + user.Email + ",\n\n Your password for the Students' Union Database has been reset. Your new password is:\n" + tempassword + "\n\n Kind Regards\nThe SU Database Team");
            
                }

                //Log this event
                ActivityLogClass newActivity = new ActivityLogClass();
                newActivity.logActivity("Reset Password", "Password has been reset for " + user.UserName);

                return true;
            }
            catch
            {
                return false;
            }

        }

        public bool ChangePassword (String oldPassword, String newPassword)
        {
            //This class changes the password for the specified user
            try
            {
                MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
     
                if(user.ChangePassword(oldPassword, newPassword))
                {
                    Membership.UpdateUser(user);
                    //Log this event
                    ActivityLogClass newActivity = new ActivityLogClass();
                    newActivity.logActivity("Password Change", user.UserName + "changed their password");

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
            
        }

        public class studentPoints
        {
            public string studentNo { get; set; }
            public string studentFirstname { get; set; }
            public string studentSurname { get; set; }
            public int totalPoints { get; set; }

        }

        public IQueryable<studentPoints> returnPointsGreaterThan(int noPoints)
        {
            var students = from p in db.tblPointsTransactions join s in db.tblStudents on p.StudentId equals s.Id group p by p.StudentId into k select new { studentId = k.Key, points = k.Sum(p => p.NumberOfPoints) };
            var join = from p in students join r in db.tblStudents on p.studentId equals r.Id where p.points>noPoints select new studentPoints { studentNo = r.StudentNumber, studentFirstname = r.FirstName, studentSurname = r.Surname, totalPoints = (int)p.points };
            return join;
        }
        public bool updateNoteTime(int noteId, int timeUnit)
        {
            var note = db.tblCaseLogs.Where(p => p.Id == noteId).FirstOrDefault();
            note.timeUnits = timeUnit;
            db.SubmitChanges();
            return true;
        }
        public tblStaffSecurity getPermissions(Guid ProviderUserKey)
        {
            //This class returns all of the current permissions for the currently logged in user

            tblStaffSecurity values = db.tblStaffSecurities.Where(p => p.UserKey.Equals(ProviderUserKey)).FirstOrDefault();
            if (values == null)
            {
                MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                var userdetails = db.tblStaffSecurities.Where(p => p.UserKey.Equals(user.ProviderUserKey)).Single();

                //No Values So Create Permissions Record
                tblStaffSecurity newSecurity = new tblStaffSecurity
                {
                    UserKey = ProviderUserKey,
                    DutyOfCare=0,
                    Reports = 0,
                    StudentManager=0,
                    SystemManager=0,
                    TripRegistration=0,
                    TransportBookings=0,
                    CaseLog=0,
                    KitSignInOut=0,
                    Disciplinary=0,
                    FirstAid=0,
                    RiskAssessment=0,
                    RoomRequests=0,
                    StudentsUnion=userdetails.StudentsUnion,
                };
                db.tblStaffSecurities.InsertOnSubmit(newSecurity);
                db.SubmitChanges();
                return newSecurity;
            }
            else
            {
                return values;
            }
        }

        public bool checkbannedfrombar(int StudentId)
        {
            var sanctions = from p in db.tblSanctions join x in db.tblSanctionArea1s on p.SanctionAreaId equals x.Id where p.StudentId == StudentId where x.NoBar == 1 where p.SanctionStartDate < DateTime.Now where p.SanctionEndDate > DateTime.Now where p.StrikenDate == null select p;

            if(sanctions.Count() >0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public IQueryable<tblStudent> returnBarredMembers()
        {
            var sanctions = from p in db.tblSanctions join x in db.tblSanctionArea1s on p.SanctionAreaId equals x.Id join q in db.tblStudents on p.StudentId equals q.Id where x.NoBar == 1 where p.SanctionStartDate < DateTime.Now where p.SanctionEndDate > DateTime.Now where x.SU==getSU().Id where p.StrikenDate == null select q;

           return sanctions;
        
        }

        public IQueryable<tblStudent> returnFirstAiders()
        {
            var sanctions = from q in db.tblStudents where q.FirstAidCertExpiry>DateTime.Now where q.StudentsUnion == getSU().Id orderby q.Surname ascending select q;

            return sanctions;

        }

        public IQueryable<tblStudent> returnExpiredFirstAiders()
        {
            var sanctions = from q in db.tblStudents where q.FirstAidCertExpiry !=null where q.FirstAidCertExpiry < DateTime.Now.AddMonths(1) where q.FirstAidCertExpiry > DateTime.Now.AddYears(-1) where q.StudentsUnion == getSU().Id orderby q.FirstAidCertExpiry descending select q;

            return sanctions;

        }

        public IQueryable<tblStudent> returnDrivers()
        {
            var sanctions = from q in db.tblStudents where q.MiniBusAssessment==1 where q.StudentsUnion == getSU().Id orderby q.Surname descending select q;

            return sanctions;

        }


        public IQueryable<tblMissedReason> returnMissedReasons()
        {
            var sanctions = from p in db.tblMissedReasons where p.SU == getSU().Id select p;

            return sanctions;

        }

        public IQueryable<tblStudent> returnUnder18()
        {
            var sanctions = from p in db.tblStudents where p.DateOfBirth > DateTime.Now.AddYears(-17) where p.StudentsUnion==getSU().Id select p;

            return sanctions;

        }

        public bool checkbanneduni(int StudentId)
        {
            var sanctions = from p in db.tblSanctions join x in db.tblSanctionArea1s on p.SanctionAreaId equals x.Id where p.StudentId == StudentId where x.NoUni == 1 where p.SanctionStartDate < DateTime.Now where p.SanctionEndDate > DateTime.Now where p.StrikenDate == null select p;

            if (sanctions.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkbannedfromsports(int StudentId)
        {
            var sanctions = from p in db.tblSanctions join x in db.tblSanctionArea1s on p.SanctionAreaId equals x.Id where p.StudentId == StudentId where x.NoSports == 1 where p.SanctionStartDate < DateTime.Now where p.SanctionEndDate > DateTime.Now where p.StrikenDate == null select p;

            if (sanctions.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public String checkAccess(String SystemName)
        {
            //This class returns a string stating whether the currently logged in user has access to the specified system.

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                var values = getPermissions((Guid)user.ProviderUserKey);
                int SecurityLevel = 0;

                if (values == null)
                {
                    return "No Access";
                }
                else
                {
                    try
                    {
                        switch (SystemName)
                        {
                            case "Bar":

                                SecurityLevel = (int)values.Bar;
                                break;
                            case "Elections":

                                SecurityLevel = (int)values.Elections;
                                break;
                            case "DutyOfCare":

                                SecurityLevel = (int)values.DutyOfCare;
                                break;

                            case "Reports":
                                SecurityLevel = (int)values.Reports;
                                break;
                            case "StudentManager":
                                SecurityLevel = (int)values.StudentManager;
                                break;
                            case "SystemManager":
                                SecurityLevel = (int)values.SystemManager;
                                break;
                            case "TripRegistration":
                                SecurityLevel = (int)values.TripRegistration;
                                break;
                            case "TransportBookings":
                                SecurityLevel = (int)values.TransportBookings;
                                break;
                            case "CaseLog":
                                SecurityLevel = (int)values.CaseLog;
                                break;
                            case "KitSignInOut":
                                SecurityLevel = (int)values.KitSignInOut;
                                break;
                            case "Disciplinary":
                                SecurityLevel = (int)values.Disciplinary;
                                break;
                            case "FirstAid":
                                SecurityLevel = (int)values.FirstAid;
                                break;
                            case "RiskAssessment":
                                SecurityLevel = (int)values.RiskAssessment;
                                break;
                            case "RoomRequests":
                                SecurityLevel = (int)values.RoomRequests;
                                break;
                            case "Committee":
                                if (Roles.IsUserInRole("ClubCaptain"))
                                {
                                    SecurityLevel = 2;
                                }
                                else
                                {
                                    SecurityLevel = 0;
                                }
                                break;
                        }
                    }
                    catch
                    {
                        SecurityLevel = 0;
                    }

                    if (SecurityLevel == 0)
                    {
                        return "No Access";
                    }
                    else if (SecurityLevel == 1)
                    {
                        return "Read Only";
                    }
                    else if (SecurityLevel == 2)
                    {
                        return "Full Access";
                    }
                    else
                    {
                        return "No Access";
                    }

                }
            }
            else
            {
                return "No Access";
            }
        }

        public void setCurrentElection(int electionId)
        {
            var security = getPermissions((Guid)Membership.GetUser().ProviderUserKey);

            security.EditElection = electionId;
            db.SubmitChanges();
        }

        public void setCurrentPosition(int positionId)
        {
            var security = getPermissions((Guid)Membership.GetUser().ProviderUserKey);

            security.currentElectionPosition = positionId;
            db.SubmitChanges();
        }

        public tblElection returnCurrentElection()
        {
            var security = getPermissions((Guid)Membership.GetUser().ProviderUserKey);

            var election = db.tblElections.Where(p => p.electionId == security.EditElection).Single();

            return election;
        }

        public tblPosition returnCurrentPosition()
        {
            var security = getPermissions((Guid)Membership.GetUser().ProviderUserKey);

            var position = db.tblPositions.Where(p => p.positionId == security.currentElectionPosition).Single();

            return position;
        }

        public IQueryable<tblPosition> returnElectionPositions()
        {
            var positions = from p in db.tblPositions where p.electionId == returnCurrentElection().electionId select p;
            return positions;
        }

        public IQueryable<tblCandidate> returnPositionCandidates()
        {
            var candidates = from p in db.tblCandidates where p.positionId == returnCurrentPosition().positionId select p;
            return candidates;
        }
        
        public IQueryable<tblElection> returnElectionsInDesign()
        {

            var elections = from p in db.tblElections where (p.StudentsUnion == getSU().Id) where p.StartDateTime > DateTime.Now select p;
            return elections;

        }


        public void createElection(string name, DateTime startdate, DateTime enddate)
        {
            tblElection newElection = new tblElection
            {
                ElectionName = name,
                StartDateTime = startdate,
                EndDateTime = enddate,
                StudentsUnion = getSU().Id,
            };
            db.tblElections.InsertOnSubmit(newElection);
            db.SubmitChanges();
        }

        public void createPosition(string name, string description, int seats, string votetype)
        {
            tblPosition newposition = new tblPosition
            {
                PositionName = name,
                positionDescription_ = description,
                numberSeats = seats,
                votingType = votetype,
                electionId=returnCurrentElection().electionId,
            };
           db.tblPositions.InsertOnSubmit(newposition);
            db.SubmitChanges();
        }

        public tblCandidate createCandidate(string name, string youtube, string manifestofilename, string photofilename)
        {
            tblCandidate newcandidate = new tblCandidate
            {
                CandidateName = name,
                YouTubeLink = youtube,
                positionId = returnCurrentPosition().positionId,
                ManifestoFileName = manifestofilename,
                PhotoFileName=photofilename
            };
            db.tblCandidates.InsertOnSubmit(newcandidate);

   

            db.SubmitChanges();

            return newcandidate;
        }

        public IQueryable<tblElection> returnElectionsInProgress()
        {

            var elections = from p in db.tblElections where (p.StudentsUnion == getSU().Id) where p.StartDateTime < DateTime.Now where p.EndDateTime > DateTime.Now select p;
            return elections;

        }

        public IQueryable<tblElection> returnElectionsCompleted()
        {

            var elections = from p in db.tblElections where (p.StudentsUnion == getSU().Id) where p.EndDateTime < DateTime.Now select p;
            return elections;

        }
        public void createSession(string cup, int tripId, int TeamId, int SportSocietyId, String name, String Location, int LinkedRoom, int LinkedTransport, DateTime StartDateTime, DateTime EndDateTime, int noWeeks, string postcode, string transport, DateTime depart, int numberteas, DateTime teasTime, int BUCS)
        {

              MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);

            for (int i = 0; i < noWeeks; i++)
            {



                tblSession newSession = new tblSession
                {
                    SportSocietyId = SportSocietyId,
                    SessionName = name,
                    Location = Location,
                    linkedRoomBookingRequest = LinkedRoom,
                    StartDateTime = StartDateTime.AddDays(7 * i),
                    EndDateTime = EndDateTime.AddDays(7 * i),
                    createdBy = (Guid)user.ProviderUserKey,
                    creationDate = DateTime.Now,
                    teamId = TeamId,
                    PostCode = postcode,
                    NoTeas = numberteas,
                    TeasTime = teasTime,
                    Depart = depart,
                    BUCS = BUCS,
                    RiskAssessmentId=0,
                    RegisterComplete=0,
                    clearMiss=0,
                    linkedTransportBookingRequest = LinkedTransport,
                    Transport = transport,
                    CupLeague=cup,
                };

                var sportsoc = returnSportSociety(SportSocietyId);
                activitylogger.logDOCActivity(sportsoc.Name, "Created a session called " + name);



                db.tblSessions.InsertOnSubmit(newSession);
                db.SubmitChanges();

                
                    if(tripId==0)
                    {
                        newSession.trip = 0;
                        db.SubmitChanges();
                    }
                    else
                    {
                      
                        
                        if(tripId==-1)
                        {
                            // need to create a new trip reg
                            var trip = createTrip(returnActiveSportSoc().Id);
                            newSession.TripRegId = trip.tripId;
                            db.SubmitChanges();
                        }
                        else
                        {
                         //No Trip Required
                            newSession.trip = 0;
                            db.SubmitChanges();
                        }
                    }
            }
        }



        public void updateRoomRequest(int requestId, string sessionDesc, string requestDetails, string unionresponse, int status)
        {


            var request = db.tblRoomRequests.Where(p => p.Id == requestId).Single();
            request.Headline = sessionDesc;
            request.RequestDetails = requestDetails;
            request.UnionResponse = unionresponse;
            request.RequestStatus = status;
            request.RequestDate = DateTime.Now;

            var sportsoc = returnSportSociety((int)request.SportSociety);
            activitylogger.logDOCActivity(sportsoc.Name, "Updated a room request");


            db.SubmitChanges();

        }

        public void updateTransportRequest(int requestId, string sessionDesc, string requestDetails, string unionresponse, int status)
        {


            var request = db.tblTransportRequests.Where(p => p.Id == requestId).Single();
            request.Headline = sessionDesc;
            request.RequestDetails = requestDetails;
            request.UnionResponse = unionresponse;
            request.RequestStatus = status;
            request.RequestDate = DateTime.Now;

            var sportsoc = returnSportSociety((int)request.SportSociety);
            activitylogger.logDOCActivity(sportsoc.Name, "Updated a transport request");

            db.SubmitChanges();

        }

        public void submitScore(int sessionId, int ourscore, int theirscore)
        {
            var session = db.tblSessions.Where(p => p.Id == sessionId).Single();

            session.ourScore = ourscore;
            session.theirScore = theirscore;

            db.SubmitChanges();

        }


            
        public void updateSession(int tripId, int sessionId, int TeamId, int SportSocietyId, String name, String Location, int LinkedRoom, int LinkedTransport, DateTime StartDateTime, DateTime EndDateTime, string postcode, string transport, DateTime depart, int numberteas, DateTime teasTime, int BUCS)
        {


            var session = db.tblSessions.Where(p => p.Id == sessionId).Single();

            if (tripId > 0)
            {
                session.trip = 1;
            }

            session.SessionName = name;
            session.Location = Location;
            session.linkedRoomBookingRequest = LinkedRoom;
            session.teamId = TeamId;
            session.StartDateTime = StartDateTime;
            session.EndDateTime = EndDateTime;
            session.TripRegId = tripId;
            session.PostCode = postcode;
            session.NoTeas = numberteas;
            session.TeasTime = teasTime;
            session.Depart = depart;
            session.Transport = transport;
            session.BUCS = BUCS;
            session.linkedTransportBookingRequest = LinkedTransport;

            var sportsoc = returnSportSociety((int)session.SportSocietyId);
            activitylogger.logDOCActivity(sportsoc.Name, "Updated a session called " + name);

            db.SubmitChanges();

           
        }


        public void addSessionNotes(int sessionId,String sessionNotes)
        {


            var session = db.tblSessions.Where(p => p.Id == sessionId).Single();

            session.SessionNotes = sessionNotes;
            db.SubmitChanges();


        }

        public void setRegisterComplete(int sessionId)
        {
            var session = db.tblSessions.Where(p => p.Id == sessionId).Single();
            session.RegisterComplete = 1;

            var sportsoc = returnSportSociety((int)session.SportSocietyId);
            activitylogger.logDOCActivity(sportsoc.Name, "Submitted a session register");


            db.SubmitChanges();
        }

        public void setRiskAssessmentComplete(int sessionId)
        {
            var session = db.tblSessions.Where(p => p.Id == sessionId).Single();
            session.RiskAssessmentId = 1;

            var sportsoc = returnSportSociety((int)session.SportSocietyId);
            activitylogger.logDOCActivity(sportsoc.Name, "Submitted a Health and Safety Checklist");


            db.SubmitChanges();
        }

        public tblTeamMember returnTeamStatus(int teamId, int Student)
        {
            
                var studentattenance = db.tblTeamMembers.Where(p => p.teamId == teamId).Where(p => p.StudentId == Student).Single();
                return studentattenance;
                  }

        public tblRegister returnAttendeeStatus(int Session, int Student)
        {

            var studentattenance = db.tblRegisters.Where(p => p.SessionId == Session).Where(p => p.StudentId == Student).Single();
            return studentattenance;
        }


        public tblTripAttendee returnTripAttendeeStatus(int tripId, int Student)
        {

            var studentattenance = db.tblTripAttendees.Where(p => p.TripId == tripId).Where(p => p.StudentId == Student).Single();
            return studentattenance;
        }


        public tblRiskChecklist returnRiskStatus(int Session, int riskId)
        {

            var studentattenance = db.tblRiskChecklists.Where(p => p.sessionId == Session).Where(p => p.RiskQuestionId == riskId).Single();
            return studentattenance;
        }

        public bool submitRegisterAttendee(int session, int Student, int attending)
        {
            //Check to see if student is already in attendance database for this session
            try
            {
                var studentattenance = db.tblRegisters.Where(p => p.SessionId == session).Where(p => p.StudentId == Student).Single();

                if (studentattenance != null)
                {
                    //student returned so update
                    studentattenance.AttendanceStatus = attending;
                    db.SubmitChanges();
                }
                else
                {
                    //student not returned so add
                    tblRegister newattendee = new tblRegister
                    {
                        StudentId = Student,
                        SessionId = session,
                        AttendanceStatus = attending
                    };
                    db.tblRegisters.InsertOnSubmit(newattendee);
                    db.SubmitChanges();
                }
            }
            catch
            {
                //student not returned so add
                tblRegister newattendee = new tblRegister
                {
                    StudentId = Student,
                    SessionId = session,
                    AttendanceStatus = attending
                };
                db.tblRegisters.InsertOnSubmit(newattendee);
                db.SubmitChanges();
            }
            return true;
        }




        public bool submitTripAttendee(int tripId, int Student, int attending, int modeOfTransport, int Driver)
        {
            //Check to see if student is already in attendance database for this session
            try
            {
                var studentattenance = db.tblTripAttendees.Where(p => p.TripId==tripId).Where(p => p.StudentId == Student).Single();

                if (studentattenance != null)
                {
                    //student returned so update
                    studentattenance.attendanceStatus = attending;
                    studentattenance.ModeofTransport = modeOfTransport;
                    studentattenance.Driver = Driver;
                    db.SubmitChanges();
                }
                else
                {
                    //student not returned so add
                    tblTripAttendee newattendee = new tblTripAttendee
                    {
                        StudentId = Student,
                        TripId = tripId,
                        attendanceStatus = attending,
                        ModeofTransport = modeOfTransport,
                        Driver = Driver
                    };
                    db.tblTripAttendees.InsertOnSubmit(newattendee);
                    db.SubmitChanges();
                }
            }
            catch
            {
                //student not returned so add
                tblTripAttendee newattendee = new tblTripAttendee
                {
                    StudentId = Student,
                    TripId = tripId,
                    attendanceStatus = attending,
                    ModeofTransport = modeOfTransport,
                    Driver = Driver
                };
                db.tblTripAttendees.InsertOnSubmit(newattendee);
                db.SubmitChanges();
            }
            return true;
        }



        public bool submitTeamMember(int teamId, int Student, int attending)
        {
            //Check to see if student is already in attendance database for this session
            try
            {
                var studentattenance = db.tblTeamMembers.Where(p => p.teamId == teamId).Where(p => p.StudentId == Student).Single();

                if (studentattenance != null)
                {
                    //student returned so update
                    if(attending==1)
                    {
                        //Do Nothing
                    }
                    else
                    {
                        db.tblTeamMembers.DeleteOnSubmit(studentattenance);
                        db.SubmitChanges();
                    }
                }
                else
                {
                    if (attending == 1)
                    {
                        tblTeamMember newmember = new tblTeamMember
                        {
                            teamId = teamId,
                            StudentId = Student
                        };
                        db.tblTeamMembers.InsertOnSubmit(newmember);
                    }
                    else
                    {
                        //Do Nothing
                    }
                   
                }
            }
            catch
            {
                if (attending == 1)
                {
                    tblTeamMember newmember = new tblTeamMember
                    {
                        teamId = teamId,
                        StudentId = Student
                    };
                    db.tblTeamMembers.InsertOnSubmit(newmember);
                    db.SubmitChanges();
                }
                else
                {
                    //Do Nothing
                }
            }
            return true;
        }


        public bool submitRisk(int session, int riskId, int riskvalue,Guid guid, String txtWho, String txtActionTaken, string unionAction, int UnionActionStatus)
        {
            //Check to see if student is already in attendance database for this session
            try
            {
                var studentattenance = db.tblRiskChecklists.Where(p => p.sessionId == session).Where(p => p.RiskQuestionId == riskId).Single();

                if (studentattenance != null)
                {
                    //student returned so update
                    studentattenance.Status = riskvalue;
                    studentattenance.WhoIsAtRisk = txtWho;
                    studentattenance.ActionTaken = txtActionTaken;

                    if (unionAction != "")
                    {
                        studentattenance.UnionAction = unionAction;
                        studentattenance.UnionActionStatus = UnionActionStatus;
                    }
                    db.SubmitChanges();
                }
                else
                {
                    //student not returned so add
                    tblRiskChecklist newattendee = new tblRiskChecklist
                    {
                        RiskQuestionId = riskId,
                        sessionId = session,
                        Status = riskvalue,
                        ActionTaken = txtActionTaken,
                        WhoIsAtRisk = txtWho,
                        UnionAction = unionAction,
                        UnionActionStatus = UnionActionStatus,
                        CompletedDateTime = DateTime.Now,
                        CompletedBy = guid,

                    };
                    db.tblRiskChecklists.InsertOnSubmit(newattendee);
                    db.SubmitChanges();
                }
            }
            catch
            {
                //student not returned so add
                tblRiskChecklist newattendee = new tblRiskChecklist
                {
                    RiskQuestionId = riskId,
                    sessionId = session,
                    Status = riskvalue,
                    ActionTaken = txtActionTaken,
                    WhoIsAtRisk = txtWho,
                    UnionAction = unionAction,
                    UnionActionStatus = UnionActionStatus,
                    CompletedDateTime = DateTime.Now,
                    CompletedBy = guid,
                };
                db.tblRiskChecklists.InsertOnSubmit(newattendee);
                db.SubmitChanges();
            }
            return true;
        }

        public IQueryable<tblSession> returnFutureSessions(int SportSocId)
        {

            var sessions = from p in db.tblSessions where (p.EndDateTime > DateTime.Now.AddDays(-1)) where (p.SportSocietyId == SportSocId) orderby (p.StartDateTime) ascending select p;
            return sessions;

        }

        public IQueryable<tblRoomRequest> returnActiveRoomRequests()
        {

            var rooms = from p in db.tblRoomRequests join x in db.tblSportsSocieties on p.SportSociety equals x.Id where x.SU == getSU().Id where (p.RequestStatus == 0) orderby (p.RequestDate) ascending select p;
            return rooms;

        }

        public IQueryable<tblTransportRequest> returnActiveTransportRequests()
        {

            var rooms = from p in db.tblTransportRequests join x in db.tblSportsSocieties on p.SportSociety equals x.Id where x.SU==getSU().Id where (p.RequestStatus == 0) orderby (p.RequestDate) ascending select p;
            return rooms;

        }

        public IQueryable<tblSession> returnFixtures(DateTime fixtureDate)
        {

            var sessions = from p in db.tblSessions where p.StartDateTime.Value.Date==fixtureDate.Date join t in db.tblSportsSocieties on p.SportSocietyId equals t.Id where t.SU== getSU().Id where p.BUCS==1 join f in db.tblSportsSocieties on p.SportSocietyId equals f.Id orderby (f.Name) ascending select p;
            return sessions;

        }

        public IQueryable<tblSession> returnPastSessions(int SportSocId)
        {

            var sessions = from p in db.tblSessions where (p.EndDateTime < DateTime.Now.AddDays(-1)) where (p.SportSocietyId == SportSocId) orderby (p.StartDateTime) ascending select p;
            return sessions;

        }
        public IQueryable CommitteeSportsSocieties()
        {
            if (checkAccess("DutyOfCare") == "Full Access" || checkAccess("DutyOfCare") == "Read Only")
            {
                //return all clubs and societies
                var sportssocs = from p in db.tblSportsSocieties where p.SU==getSU().Id where(p.Archived==0) orderby p.Name select p;
                return sportssocs;

            }
            else
            {
                var currentuser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                //Check which clubs and societies user has access to and return only those!
                var sportssocs = from p in db.tblSportsSocieties where p.SU == getSU().Id join q in db.tblSportSocietyAccesses on p.Id equals q.SportSocietyId where q.UserId == (Guid)currentuser.ProviderUserKey where (p.Archived == 0) select p;
                return sportssocs;
            }
        }

        public void updateTrip(int tripId, DateTime departure, DateTime returntime, string address,string accomodation)
        {
            var trip = db.tblTripRegistrations.Where(p => p.tripId == tripId).Single();

            trip.TripDestinationAddress = address;
            trip.DepartureTime = departure;
            trip.ReturnTime = returntime;
            trip.ApprovalStatus = 1;
            trip.Accomodation = accomodation;

            db.SubmitChanges();
        }

        public void approveTrip(int tripId,string comments)
        {
            var trip = db.tblTripRegistrations.Where(p => p.tripId == tripId).Single();
            MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);

            trip.ApprovalStatus = 2;
            trip.ApprovalComments = comments;
            trip.tripReviewedBy = (Guid)user.ProviderUserKey;
            trip.reviewdate = DateTime.Now;

            db.SubmitChanges();
        }

        public void rejectTrip(int tripId, string comments)
        {
            var trip = db.tblTripRegistrations.Where(p => p.tripId == tripId).Single();
            MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);

            trip.ApprovalStatus = 3;
            trip.ApprovalComments = comments;
            trip.tripReviewedBy = (Guid)user.ProviderUserKey;
            trip.reviewdate = DateTime.Now;
            db.SubmitChanges();
        }

        public void tripamendment(int tripId, string comments)
        {
            var trip = db.tblTripRegistrations.Where(p => p.tripId == tripId).Single();
            MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);

            trip.TripAmendment = comments;
            db.SubmitChanges();
        }
        public String checkAccessForUser(Guid userkey, String SystemName)
        {
            //This class returns a string stating whether the currently logged in user has access to the specified system.

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                MembershipUser user = Membership.GetUser(userkey);
                var values = getPermissions((Guid)user.ProviderUserKey);
                int SecurityLevel = 0;

                if (values == null)
                {
                    return "No Access";
                }
                else
                {

                    switch (SystemName)
                    {
                        case "DutyOfCare":

                            SecurityLevel = (int)values.DutyOfCare;
                            break;

                        case "Reports":
                            SecurityLevel = (int)values.Reports;
                            break;
                        case "StudentManager":
                            SecurityLevel = (int)values.StudentManager;
                            break;
                        case "SystemManager":
                            SecurityLevel = (int)values.SystemManager;
                            break;
                        case "TripRegistration":
                            SecurityLevel = (int)values.TripRegistration;
                            break;
                        case "TransportBookings":
                            SecurityLevel = (int)values.TransportBookings;
                            break;
                        case "CaseLog":
                            SecurityLevel = (int)values.CaseLog;
                            break;
                        case "KitSignInOut":
                            SecurityLevel = (int)values.KitSignInOut;
                            break;
                        case "Disciplinary":
                            SecurityLevel = (int)values.Disciplinary;
                            break;
                        case "FirstAid":
                            SecurityLevel = (int)values.FirstAid;
                            break;
                        case "RiskAssessment":
                            SecurityLevel = (int)values.RiskAssessment;
                            break;
                        case "RoomRequests":
                            SecurityLevel = (int)values.RoomRequests;
                            break;
                    }

                    if (SecurityLevel == 0)
                    {
                        return "No Access";
                    }
                    else if (SecurityLevel == 1)
                    {
                        return "Read Only";
                    }
                    else if (SecurityLevel == 2)
                    {
                        return "Full Access";
                    }
                    else
                    {
                        return "No Access";
                    }

                }
            }
            else
            {
                return "No Access";
            }
        }

        public bool updateAccess(Guid userkey, String SystemName,String accessLevel)
        {
            //This class returns a string stating whether the currently logged in user has access to the specified system.

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Common tools = new Common();
                MembershipUser user = Membership.GetUser(userkey);
                var values = getPermissions((Guid)user.ProviderUserKey);
                if(!tools.checkAccessForUser(userkey,SystemName).Equals(accessLevel))
                {
                    int SecurityLevel=0;
                    if(accessLevel=="No Access")
                        SecurityLevel=0;
                    else if(accessLevel=="Read Only")
                        SecurityLevel=1;
                    else if(accessLevel=="Full Access")
                        SecurityLevel=2;

                    ActivityLogClass activity = new ActivityLogClass();
                    MembershipUser affecteduser = Membership.GetUser(userkey);

                    switch (SystemName)
                    {
                        case "Bar":
                            values.Bar = SecurityLevel;
                            activity.logActivity("Changed User Permissions", "Set Duty of Care Access to " + accessLevel + " for " + affecteduser.Email + ".");
                            break;
                        case "DutyOfCare":
                            values.DutyOfCare = SecurityLevel;
                            activity.logActivity("Changed User Permissions", "Set Duty of Care Access to " + accessLevel + " for " + affecteduser.Email + ".");
                            break;
                        case "Elections":
                            values.Elections = SecurityLevel;
                            activity.logActivity("Changed User Permissions", "Set Elections Access to " + accessLevel + " for " + affecteduser.Email + ".");
                            break;

                        case "Reports":
                            values.Reports = SecurityLevel;
                            activity.logActivity("Changed User Permissions", "Set Reports Access to " + accessLevel + " for " + affecteduser.Email + ".");
                            break;
                        case "StudentManager":
                            values.StudentManager = SecurityLevel;
                            activity.logActivity("Changed User Permissions", "Set Student Manager Access to " + accessLevel + " for " + affecteduser.Email + ".");
                          
                            break;
                        case "SystemManager":
                            values.SystemManager = SecurityLevel;
                            activity.logActivity("Changed User Permissions", "Set System Manager Access to " + accessLevel + " for " + affecteduser.Email + ".");
                          
                            break;
                        case "TripRegistration":
                            values.TripRegistration = SecurityLevel;
                            activity.logActivity("Changed User Permissions", "Set Trip Registration Access to " + accessLevel + " for " + affecteduser.Email + ".");
                          
                            break;
                        case "TransportBookings":
                            values.TransportBookings = SecurityLevel;
                            activity.logActivity("Changed User Permissions", "Set Transport Bookings Access to " + accessLevel + " for " + affecteduser.Email + ".");
                          
                            break;
                        case "CaseLog":
                            values.CaseLog = SecurityLevel;
                            activity.logActivity("Changed User Permissions", "Set Case Log Access to " + accessLevel + " for " + affecteduser.Email + ".");
                          
                            break;
                        case "KitSignInOut":
                            values.KitSignInOut = SecurityLevel;
                            activity.logActivity("Changed User Permissions", "Set Kit Sign In / Out Access to " + accessLevel + " for " + affecteduser.Email + ".");
                          
                            break;
                        case "Disciplinary":
                            values.Disciplinary = SecurityLevel;
                            activity.logActivity("Changed User Permissions", "Set Disciplinary Access to " + accessLevel + " for " + affecteduser.Email + ".");
                          
                            break;
                        case "FirstAid":
                            values.FirstAid = SecurityLevel;
                            activity.logActivity("Changed User Permissions", "Set First Aid Access to " + accessLevel + " for " + affecteduser.Email + ".");
                          
                            break;
                        case "RiskAssessment":
                            values.RiskAssessment = SecurityLevel;
                            activity.logActivity("Changed User Permissions", "Set Risk Assessment Access to " + accessLevel + " for " + affecteduser.Email + ".");
                          
                            break;
                        case "RoomRequests":
                            values.RoomRequests = SecurityLevel;
                            activity.logActivity("Changed User Permissions", "Set Room Bookings Access to " + accessLevel + " for " + affecteduser.Email + ".");
                          
                            break;
                    }
                    db.SubmitChanges();
                    return true;
                }
                else
                {

                    return false;
                }
            }
            else
            {
                return false;
            }
                
        }

        public bool setUserToEdit(Guid userkey)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                try
                {
                    var userdetails = getPermissions((Guid)user.ProviderUserKey);
                    userdetails.EditUser = userkey;
                    db.SubmitChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool setStudentToEdit(int StudentIdentifier)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                    MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);



                    var userdetails = getPermissions((Guid)user.ProviderUserKey); 
                userdetails.EditStudent = StudentIdentifier;
               
                    db.SubmitChanges();
                    return true;
               
            }
            else
            {
                return false;
            }
        }


        public bool setActiveSportSociety(int SportSocId)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);



                var userdetails = getPermissions((Guid)user.ProviderUserKey); 
                userdetails.ActiveSportSociety = SportSocId;

                db.SubmitChanges();
                return true;

            }
            else
            {
                return false;
            }
        }

  
        

        public Guid createUser(String email)
        {
            try
            {
                var newuser = Membership.CreateUser(email, "YG&&YYHJ*huhu76", email);
                Roles.AddUserToRole(newuser.UserName, "Staff");
                getPermissions((Guid)newuser.ProviderUserKey);

                string temppassword = newuser.ResetPassword();
                Membership.UpdateUser(newuser);

                EmailHelper newEmail = new EmailHelper();
                newEmail.Send(newuser.Email, "Welcome to the SU Database", "Dear " + newuser.Email + ",\n\n A new user account has been created for you to access the Students' Union Database. Your registered e-mail address is:\n"+newuser.Email+ " \nYour new password is:\n" + temppassword + "\n\nTo login to the system please visit http://www.sudb.co.uk.\n\n Kind Regards\nThe SU Database Team");


                return (Guid)newuser.ProviderUserKey;
            }
            catch
            {
                return Guid.Empty;

            }
        }

        public bool removeCommitteeMember(Guid userId, int SportSocId)
        {
            var access = db.tblSportSocietyAccesses.Where(p => p.UserId == userId).Where(p => p.SportSocietyId == SportSocId).Single();
           
            var student = Membership.GetUser((Guid)access.UserId);
            
            var sportsoc = returnSportSociety((int)access.SportSocietyId);
            activitylogger.logDOCActivity(sportsoc.Name, "Removed access from " + student.UserName);

            db.tblSportSocietyAccesses.DeleteOnSubmit(access);
            db.SubmitChanges();

            return true;

        }

        public tblSportsSociety createSportSoc(string sportsocname)
        {
            tblSportsSociety newsport = new tblSportsSociety
            {
                Name = sportsocname,
                SU = getSU().Id,
                AvailableToBuy = 0,
                Archived=0,
                Lock=0,
                VAT = 0,
                DefaultPrice=0,
                Type=1,
                EnforceRegister=1,
                EnableHSChecklist=1,
                EnableRegister=1,
                EnfoceHSChecklist=1
            };
            db.tblSportsSocieties.InsertOnSubmit(newsport);
            db.SubmitChanges();
            var sportsoc = returnSportSociety((int)newsport.Id);
            activitylogger.logDOCActivity(sportsoc.Name, "Created a new Sport / Society");


           
            return newsport;
        }

       
        public void lockSportSoc(int sport)
        {

            var sportsoc = returnSportSociety(sport);
            if (sportsoc.Lock == 1)
            {
                sportsoc.Lock = 0;

                var sportid = returnSportSociety((int)sportsoc.Id);
                activitylogger.logDOCActivity(sportid.Name, "Unlocked Sport / Society");

            }
            else
            {
                sportsoc.Lock = 1;
                var sportid = returnSportSociety((int)sportsoc.Id);
                activitylogger.logDOCActivity(sportid.Name, "Locked Sport / Society");

            }
            db.SubmitChanges();
        }
        public void archiveSportSoc(int sport)
        {

            var sportsoc = returnSportSociety(sport);
            if (sportsoc.Archived == 1)
            {
                sportsoc.Archived = 0;
            }
            else
            {
                sportsoc.Archived = 1;
            }
            db.SubmitChanges();
        }
        public void updateSportSoc(string name, string special, string nominal, int vat, int type, decimal price, int available,int register, int hschecklist, int enforceHSchecklist, int enforceregister)
        {
            
            var sportsoc = returnActiveSportSoc();

            sportsoc.Name = name;
            sportsoc.NominalCode = nominal;
            sportsoc.VAT = vat;
            sportsoc.Type = type;
            sportsoc.DefaultPrice = price;
            sportsoc.AvailableToBuy = available;
            sportsoc.SpecialEmailInstructions = special;
            sportsoc.EnableRegister = register;
            sportsoc.EnableHSChecklist = hschecklist;
            sportsoc.EnfoceHSChecklist = enforceHSchecklist;
            sportsoc.EnforceRegister = enforceregister;

            
            db.SubmitChanges();

            activitylogger.logDOCActivity(sportsoc.Name, "Updated Sport Society");

        }

        public bool canUserAccessSportSoc(Guid UserId, int SportSocietyId)
        {
            try
            {
                var access = db.tblSportSocietyAccesses.Where(p => p.UserId == UserId).Where(p => p.SportSocietyId == SportSocietyId).Single();

                if (access != null || checkAccess("DutyOfCare") == "Full Access" || checkAccess("DutyOfCare") == "Read Only")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                if (checkAccess("DutyOfCare") == "Full Access" || checkAccess("DutyOfCare") == "Read Only")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool createSportSocUser(String email,int SportSocId)
        {
            try
            {
                //Does User Exist?c
                var member  =  Membership.GetUser(email);

                if (member != null)
                {
                    //User Exists to just add user to this sport society
                    //Check if user has acccess
                    try
                    {
                        var access = db.tblSportSocietyAccesses.Where(p => p.UserId == (Guid)member.ProviderUserKey).Where(p => p.SportSocietyId == SportSocId).Single();
                    }
                    catch
                    {
                        //Add User to this sport Society
                        tblSportSocietyAccess newaccess = new tblSportSocietyAccess
                        {
                            SportSocietyId = SportSocId,
                            UserId = (Guid)member.ProviderUserKey,
                        };
                        db.tblSportSocietyAccesses.InsertOnSubmit(newaccess);
                        db.SubmitChanges();

                        Roles.AddUserToRole(member.UserName, "ClubCaptain");

                    }
                
                }
                else
                {

                    var newuser = Membership.CreateUser(email, "YG&&YYHJ*huhu76", email);
                    Roles.AddUserToRole(newuser.UserName, "ClubCaptain");
                    getPermissions((Guid)newuser.ProviderUserKey);

                    string temppassword = newuser.ResetPassword();
                    Membership.UpdateUser(newuser);

                    //Add User to this sport Society
                    tblSportSocietyAccess newaccess = new tblSportSocietyAccess
                    {
                        SportSocietyId = SportSocId,
                        UserId = (Guid)newuser.ProviderUserKey,
                    };
                    db.tblSportSocietyAccesses.InsertOnSubmit(newaccess);
                    db.SubmitChanges();

                    EmailHelper newEmail = new EmailHelper();
                    newEmail.Send(newuser.Email, "Welcome to the SU Database", "Dear " + newuser.Email + ",\n\n A new user account has been created for you to access the Students' Union Database. Your registered e-mail address is:\n" + newuser.Email + " \nYour new password is:\n" + temppassword + "\n\nTo login to the system please visit http://www.sudb.co.uk.\n\n Kind Regards\nThe SU Database Team");
                }

                return true;
            }
            catch
            {
                return false;

            }
        }

        public bool setUserToEditNull()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                try
                {
                    var userdetails = getPermissions((Guid)user.ProviderUserKey); 
                    userdetails.EditUser = null;
                    db.SubmitChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public tblStaffSecurity returnUserToEdit()
        {
            MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            var userdetails = getPermissions((Guid)user.ProviderUserKey);
            if (userdetails.EditUser != null)
            {
                var usertoedit = db.tblStaffSecurities.Where(p => p.UserKey.Equals(userdetails.EditUser)).Single();
                return usertoedit;
            }
            else
            {
                return null;
            }
        }

        public tblStudent returnStudentToEdit()
        {
            MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            var userdetails = getPermissions((Guid)user.ProviderUserKey);
            
            var usertoedit = db.tblStudents.Where(p => p.Id.Equals(userdetails.EditStudent)).Single();
            return usertoedit;
        
        }

        public tblSportsSociety returnActiveSportSoc()
        {
            MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);

            var userdetails = getPermissions((Guid)user.ProviderUserKey);
            try
            {
                var activesportsoc = db.tblSportsSocieties.Where(p => p.Id.Equals(userdetails.ActiveSportSociety)).Single();
                return activesportsoc;
            }
            catch
            {
                return null;
            }

        }

        public tblStudent returnStudent(int StudentId)
        {
         
                var usertoedit = db.tblStudents.Where(p => p.Id.Equals(StudentId)).FirstOrDefault();
                return usertoedit;
          
        }

        public tblSportsSociety returnSportSociety(int itemId)
        {

            var item = db.tblSportsSocieties.Where(p => p.Id.Equals(itemId)).FirstOrDefault();
            return item;

        }

        public int returnNoCases(int StudentId)
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }


            DateTime startreport = new DateTime(currentyear, (int)su.YearStartMonth, 1);
            DateTime endreport = new DateTime(currentyear +1, (int)su.YearStartMonth, 31);
            endreport = endreport.AddMonths(-1);
            

            var usertoedit = from p in db.tblCases where p.dateClosed==null where p.StudentId==StudentId where p.dateOpened>startreport where p.dateOpened<endreport select p;
            return usertoedit.Count();

        }

        public int returnNoStaffCases(Guid StaffId)
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }


            DateTime startreport = new DateTime(currentyear, (int)su.YearStartMonth, 1);
            DateTime endreport = new DateTime(currentyear + 1, (int)su.YearStartMonth, 31);
            endreport = endreport.AddMonths(-1);


            var usertoedit = from p in db.tblCases where p.dateClosed == null where p.caseAssignedTo.Equals(StaffId) select p;
            return usertoedit.Count();

        }

        public int returnNoCases()
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }


            DateTime startreport = new DateTime(currentyear, (int)su.YearStartMonth, 1);
            DateTime endreport = new DateTime(currentyear + 1, (int)su.YearStartMonth, 31);
            endreport = endreport.AddMonths(-1);


            var usertoedit = from p in db.tblCases join x in db.tblStudents on p.StudentId equals x.Id where (x.StudentsUnion==su.Id) where p.dateClosed == null where p.dateOpened > startreport where p.dateOpened < endreport select p;
            return usertoedit.Count();

        }


        public int returnNoStudents()
        {
            var su = getSU();

            
           
            var student = from p in db.tblStudents where (p.StudentsUnion==su.Id) where (p.MembershipType==su.studentmembertype) select p;


             return student.Count();
            
           
        }

        public int returnNoCasesType(int typeId)
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }


            DateTime startreport = new DateTime(currentyear, (int)su.YearStartMonth, 1);
            DateTime endreport = new DateTime(currentyear + 1, (int)su.YearStartMonth, 31);
            endreport = endreport.AddMonths(-1);


            var usertoedit = from p in db.tblCases where p.dateClosed == null where p.caseType==typeId where p.dateOpened > startreport where p.dateOpened < endreport select p;
            return usertoedit.Count();

        }

        public int returnNoCasesTypeClosed(int typeId)
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }


            DateTime startreport = new DateTime(currentyear, (int)su.YearStartMonth, 1);
            DateTime endreport = new DateTime(currentyear + 1, (int)su.YearStartMonth, 31);
            endreport = endreport.AddMonths(-1);


            var usertoedit = from p in db.tblCases where p.caseType == typeId where p.dateOpened > startreport where p.dateOpened < endreport select p;
            return usertoedit.Count();

        }

        public double returnNoCasesTypeAge(int typeId)
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }


            DateTime startreport = new DateTime(currentyear, (int)su.YearStartMonth, 1);
            DateTime endreport = new DateTime(currentyear + 1, (int)su.YearStartMonth, 31);
            endreport = endreport.AddMonths(-1);

            try
            {


                var usertoedit = from p in db.tblCases join x in db.tblStudents on p.StudentId equals x.Id where x.StudentsUnion==getSU().Id where p.dateClosed != null where p.caseType == typeId select p;

                double average1 = usertoedit.Average(p => Math.Ceiling((((DateTime)p.dateClosed.Value - (DateTime)p.dateOpened).TotalDays)));

                var usertoeditopen = from p in db.tblCases join x in db.tblStudents on p.StudentId equals x.Id where x.StudentsUnion == getSU().Id where p.dateClosed == null where p.caseType == typeId where p.dateOpened > startreport where p.dateOpened < endreport select p;
                double average2 = usertoeditopen.Average(p => Math.Ceiling((((DateTime)DateTime.Now.AddDays(1) - (DateTime)p.dateOpened).TotalDays)));


                return Math.Ceiling(average1 + average2)/2;
            }
            catch
            {
                return 0;
            }

        }


        public double returnNoCasesTypeTime(int typeId)
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }


            DateTime startreport = new DateTime(currentyear, (int)su.YearStartMonth, 1);
            DateTime endreport = new DateTime(currentyear + 1, (int)su.YearStartMonth, 31);
            endreport = endreport.AddMonths(-1);

            try
            {
                var usertoedit = from p in db.tblCases join d in db.tblCaseLogs on p.caseId equals d.caseId join q in db.tblTimeUnits on d.timeUnits equals q.Id where p.caseType == typeId where q.SU == getSU().Id where d.DateTime > startreport where d.DateTime < endreport select q;
                return (double)usertoedit.Sum(p => p.MinutesToAdd);
            }
            catch
            {
                return 0;
            }
        }



        public int returnNoCasesClosed()
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }


            DateTime startreport = new DateTime(currentyear, (int)su.YearStartMonth, 1);
            DateTime endreport = new DateTime(currentyear + 1, (int)su.YearStartMonth, 31);
            endreport = endreport.AddMonths(-1);


            var usertoedit = from p in db.tblCases join x in db.tblStudents on p.StudentId equals x.Id where (x.StudentsUnion == su.Id) where p.dateOpened > startreport where p.dateOpened < endreport select p;
            return usertoedit.Count();

        }


        public int returnNoStaffCasesClosed(Guid StaffId)
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }


            DateTime startreport = new DateTime(currentyear, (int)su.YearStartMonth, 1);
            DateTime endreport = new DateTime(currentyear + 1, (int)su.YearStartMonth, 31);
            endreport = endreport.AddMonths(-1);


            var usertoedit = from p in db.tblCases where p.caseAssignedTo.Equals(StaffId) where p.dateOpened > startreport where p.dateOpened < endreport select p;
            return usertoedit.Count();

        }

        public bool InsertType(string txtType)
        {
            var su = getSU();

            tblCaseType newtype = new tblCaseType
            {
                CaseType = txtType,
                SU = su.Id,
            };
            db.tblCaseTypes.InsertOnSubmit(newtype);
            db.SubmitChanges();
            return true;

        }

        public int returnNoCasesClosed(int StudentId)
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }


            DateTime startreport = new DateTime(currentyear, (int)su.YearStartMonth, 1);
            DateTime endreport = new DateTime(currentyear + 1, (int)su.YearStartMonth, 31);
            endreport = endreport.AddMonths(-1);


            var usertoedit = from p in db.tblCases where p.StudentId == StudentId where p.dateOpened > startreport where p.dateOpened < endreport select p;
            return usertoedit.Count();

        }

        public class level
        {
            public string levelis { get; set; }
        }

        public IQueryable<level> ReturnLevels()
        {
            var su = getSU();

              var students = (from p in db.tblStudents where p.StudentsUnion.Equals(su.Id) where p.Level != null where p.Level !="" join t in db.tblMembershipTypes on p.MembershipType equals t.Id where t.PartOfArchiveUpdate==1 select new level {levelis = p.Level }).Distinct();

              return students;
        }

        public IQueryable<tblCampuse> ReturnCapuses()
        {
            var su = getSU();

            var campuses = from p in db.tblCampuses where p.StudentUnion.Equals(su.Id) select p;

            return campuses;
        }

        public IQueryable<tblStudent> ReturnCampusStudents(int campusId)
        {
            var su = getSU();

            var students = from p in db.tblStudents where p.StudentsUnion.Equals(su.Id) join t in db.tblMembershipTypes on p.MembershipType equals t.Id where t.PartOfArchiveUpdate == 1 where p.Campus==campusId select p;

            return students;
        }

        public IQueryable<tblStudent> ReturnFacultyStudents(int facultyId)
        {
            var su = getSU();

            var students = from p in db.tblStudents where p.StudentsUnion.Equals(su.Id) join t in db.tblMembershipTypes on p.MembershipType equals t.Id where t.PartOfArchiveUpdate == 1 where p.Faculty == facultyId select p;

            return students;
        }

        public IQueryable<tblStudent> ReturnLevelStudents(string level)
        {
            var su = getSU();

            var students = from p in db.tblStudents where p.StudentsUnion.Equals(su.Id) join t in db.tblMembershipTypes on p.MembershipType equals t.Id where t.PartOfArchiveUpdate == 1 where p.Level == level select p;

            return students;
        }

        public int getTotalMembers(int SportSociety)
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }




            var members = returnSportSocietyMembers(SportSociety, currentyear);
            return members.Count();
        }

        public int getTotalSessions(int SportSociety)
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }

            var sessions = from p in db.tblSessions where (p.SportSocietyId==SportSociety) where (p.StartDateTime > new DateTime(currentyear, (int)su.YearStartMonth, 01)) where (p.StartDateTime < new DateTime(currentyear + 1,(int)su.YearStartMonth, 01)) select p; 

            return sessions.Count();
        }

        public int getTotalMissedRiskAssessments(int SportSociety)
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }

            var sessions = from p in db.tblSessions where(p.EndDateTime<DateTime.Now.AddDays(1)) where(p.RiskAssessmentId ==0)  where (p.SportSocietyId == SportSociety) where (p.StartDateTime > new DateTime(currentyear, (int)su.YearStartMonth, 01)) where (p.StartDateTime < new DateTime(currentyear + 1, (int)su.YearStartMonth, 01)) select p;

            return sessions.Count();
        }

        public int getTotalMissedReg(int SportSociety)
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }

            var sessions = from p in db.tblSessions where (p.EndDateTime < DateTime.Now.AddDays(1)) where (p.RegisterComplete ==0) where (p.SportSocietyId == SportSociety) where (p.StartDateTime > new DateTime(currentyear, (int)su.YearStartMonth, 01)) where (p.StartDateTime < new DateTime(currentyear + 1, (int)su.YearStartMonth, 01)) select p;

            return sessions.Count();
        }

        public bool addToCart(int item)
        {
            string sessionId = HttpContext.Current.Session.SessionID;

            var checkifin = from p in db.tblCarts where p.sessionId == sessionId where p.itemId == item select p;


            if (checkifin.Count() == 0)
            {

                tblCart newCartItem = new tblCart
                {
                    sessionId = sessionId,
                    itemId = item
                };
                db.tblCarts.InsertOnSubmit(newCartItem);
                db.SubmitChanges();
            }

            //Check if there is something else to add
            var sport = db.tblSportsSocieties.Where(p => p.Id == item).FirstOrDefault();
            if(sport.AlsoBuy!=null)
            {
                addToCart((int)sport.AlsoBuy);
            }

            return true;
        }

        public bool checkInCart(int item)
        {
            string sessionId = HttpContext.Current.Session.SessionID;

            try
            {
                var checkifin = db.tblCarts.Where(p => p.sessionId == sessionId).Where(p => p.itemId == item).Single();

                if (checkifin != null)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool removeFromCart(int item)
        {
            string sessionId = HttpContext.Current.Session.SessionID;

            var checkifin = from p in db.tblCarts where p.sessionId == sessionId where p.itemId == item select p;


            if (checkifin.Count() != 0)
            {

                db.tblCarts.DeleteAllOnSubmit(checkifin);
                db.SubmitChanges();
            }

            return true;
        }

        public decimal getPrice(int SportSociety)
        {
            var entity = db.tblSportsSocieties.Where(p => p.Id.Equals(SportSociety)).FirstOrDefault();

            //Check if there is an alternative price
            var altprice = from p in db.tblPriceSchedules where p.SportsSocID.Equals(SportSociety) where p.StartDate < DateTime.Now where p.EndDate > DateTime.Now select p;
        
            if(altprice.Count()>0)
            {
                altprice = altprice.OrderByDescending(p => p.StartDate);
                var priceis = altprice.FirstOrDefault();
                return (decimal)priceis.Price;
            }
            else
            {
                return (decimal)entity.DefaultPrice;
            }

           
        }

        public IQueryable<tblSportsSociety> ReturnSportsSocs()
        {
            var su = getSU();

            var sportssocs = from p in db.tblSportsSocieties where p.SU.Equals(su.Id) where p.Archived.Equals(0) orderby(p.Name) select p;

           

            return sportssocs;
        }

        public IQueryable<tblSportsSociety> ReturnAllSportsSocs()
        {
            var su = getSU();

            var sportssocs = from p in db.tblSportsSocieties where p.SU.Equals(su.Id) orderby (p.Name) select p;



            return sportssocs;
        }

        public IQueryable<tblTeam> ReturnTeams()
        {
            var su = getSU();

            var sportssocs = from p in db.tblTeams join x in db.tblSportsSocieties on p.SportSocietyId equals x.Id where x.SU.Equals(su.Id) where x.Archived.Equals(0) orderby (x.Name) select p;

            return sportssocs;
        }


        public IQueryable<tblSportsSociety> ReturnArchivedSportsSocs()
        {
            var su = getSU();

            var sportssocs = from p in db.tblSportsSocieties where p.SU.Equals(su.Id) where p.Archived.Equals(1) orderby (p.Name) select p;

            return sportssocs;
        }

        public IQueryable ReturnCart()
        {

            string sessionId = HttpContext.Current.Session.SessionID;

            var checkifin = from p in db.tblCarts where p.sessionId.Trim().Equals(sessionId) select p;

          
            return checkifin;
        }

        public bool clearCart()
        {

            string sessionId = HttpContext.Current.Session.SessionID;

            var checkifin = from p in db.tblCarts where p.sessionId.Trim().Equals(sessionId) select p;
            db.tblCarts.DeleteAllOnSubmit(checkifin);
            db.SubmitChanges();
  

            return true;
        }

        public tblDOCType returnDOCType(int typeId)
        {
            var type = db.tblDOCTypes.Where(p => p.Id == typeId).FirstOrDefault();
            return type;
        }

        public String returnGrossForSport(DateTime date, int SportSoc)
        {
            try
            {
                var members = from p in db.tblMemberships join t in db.tblTransactions on p.TransactionId equals t.transactionId where (t.status == 1 || t.status==3 || t.status == 4 || t.status==5) where (p.SportSocId == SportSoc) where p.DateJoined.Value.Date == date.Date select p;
                return Math.Round((decimal)members.Sum(p => p.Amount), 2).ToString();
            }
            catch
            {
               return "0.00";
            }
        }

        public String returnGrossForSportBetweenDates(DateTime startdate, DateTime enddate, int SportSoc)
        {
            try
            {
                var members = from t in db.tblTransactions join p in db.tblMemberships on t.transactionId equals p.TransactionId where (t.status == 1 || t.status == 3 || t.status == 4 || t.status == 5) where (p.SportSocId == SportSoc) where p.DateJoined.Value.Date >= startdate where p.DateJoined.Value.Date <= enddate select p;
                return Math.Round((decimal)members.Sum(p => p.Amount), 2).ToString();
            }
            catch
            {
                return "0.00";
            }
        }

        public String returnVATForSport(DateTime date, int SportSoc)
        {
            var members = from p in db.tblMemberships join t in db.tblTransactions on p.TransactionId equals t.transactionId where (t.status == 1 || t.status==3 || t.status == 4 || t.status==5) where (p.SportSocId == SportSoc) where p.DateJoined.Value.Date == date.Date select p;

            var sportsocdet = returnSportSociety(SportSoc);
            try
            {
                if (sportsocdet.VAT == 1)
                {
                    return Math.Round(((decimal)members.Sum(p => p.Amount) / 120 * 20), 2).ToString();
                }
                else
                {
                    return "0.00";
                }
            }
            catch
            {
                return "0.00";
            }

           
        }



        public String returnVATForSportBetweenDates(DateTime startdate,DateTime enddate, int SportSoc)
        {
            var members = from t in db.tblTransactions join p in db.tblMemberships on t.transactionId equals p.TransactionId where (t.status == 1 || t.status == 3 || t.status == 4 || t.status == 5) where (p.SportSocId == SportSoc) where p.DateJoined.Value.Date >= startdate where p.DateJoined.Value.Date <= enddate select p;

            var sportsocdet = returnSportSociety(SportSoc);
            try
            {
                if (sportsocdet.VAT == 1)
                {
                    return Math.Round(((decimal)members.Sum(p => p.Amount) / 120 * 20), 2).ToString();
                }
                else
                {
                    return "0.00";
                }
            }
            catch
            {
                return "0.00";
            }


        }


        public String returnNETForSport(DateTime date, int SportSoc)
        {
            var members = from p in db.tblMemberships join t in db.tblTransactions on p.TransactionId equals t.transactionId where (t.status==1 || t.status==3 || t.status == 4 || t.status==5) where (p.SportSocId == SportSoc) where p.DateJoined.Value.Date == date.Date select p;

            var sportsocdet = returnSportSociety(SportSoc);

            try
            {
                if (sportsocdet.VAT == 1)
                {
                    return (Math.Round((decimal)members.Sum(p => p.Amount), 2) - Math.Round(((decimal)members.Sum(p => p.Amount) / 120 * 20), 2)).ToString();
                }
                else
                {
                    return Math.Round((decimal)members.Sum(p => p.Amount), 2).ToString();
                }
            }
            catch
            {
                return "0.00";
            }


        }

        public String returnNETForSportBetweenDates(DateTime startdate,DateTime enddate, int SportSoc)
        {
            var members = from t in db.tblTransactions join p in db.tblMemberships on t.transactionId equals p.TransactionId where (t.status == 1 || t.status == 3 || t.status == 4 || t.status==5) where (p.SportSocId == SportSoc) where p.DateJoined.Value.Date >= startdate where p.DateJoined.Value.Date <= enddate select p;

            var sportsocdet = returnSportSociety(SportSoc);

            try
            {
                if (sportsocdet.VAT == 1)
                {
                    return (Math.Round((decimal)members.Sum(p => p.Amount), 2) - Math.Round(((decimal)members.Sum(p => p.Amount) / 120 * 20), 2)).ToString();
                }
                else
                {
                    return Math.Round((decimal)members.Sum(p => p.Amount), 2).ToString();
                }
            }
            catch
            {
                return "0.00";
            }


        }

        public IQueryable<tblCaseType> ReturnCaseTypes()
        {
            var su = getSU();

            var types = from p in db.tblCaseTypes where p.SU.Equals(su.Id) orderby (p.CaseType) select p;

            return types;
        }


        public IQueryable<tblReminderType> ReturnReminderTypes()
        {
            var su = getSU();

            var types = from p in db.tblReminderTypes where p.SU.Equals(su.Id) orderby (p.Type) select p;

            return types;
        }

        public int createNewCase(int studentId, int caseType, Guid assignedTo, Guid openedBy, string caseNotes, int TimeUnits)
        {
            try
            {
                tblCase newCase = new tblCase
                {
                    StudentId = studentId,
                    caseType = caseType,
                    caseAssignedTo = assignedTo,
                    caseOpenedBy = openedBy,
                    dateOpened = DateTime.Now,
                };
                db.tblCases.InsertOnSubmit(newCase);
                db.SubmitChanges();

                var currentuser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                tblCaseLog newLog = new tblCaseLog
                {
                    caseId = newCase.caseId,
                    notes = caseNotes.Replace("<", "(").Replace(">", ")").Replace(Environment.NewLine, "<br />"),
                    timeUnits = TimeUnits,
                    staffmember = (Guid)currentuser.ProviderUserKey,
                    DateTime = DateTime.Now
                };
                db.tblCaseLogs.InsertOnSubmit(newLog);
                db.SubmitChanges();
                  
                ActivityLogClass activitylog = new ActivityLogClass();
                activitylog.logActivity("Create Case", "Created Case Id " + newCase.caseId.ToString());


                //Send an Email to user who case is assigned to
                var permissions = getPermissions(assignedTo);
                if(permissions.caseEmails==1)
                {

                   
                        //Get user
                        var userassigned = Membership.GetUser(assignedTo);
                        var userwhocreated = Membership.GetUser(openedBy);

                        if (userassigned.UserName != userwhocreated.UserName)
                        {
                        ProfileBase profile = ProfileBase.Create(userassigned.UserName);

                        ProfileBase createdByProfile = ProfileBase.Create(userwhocreated.UserName);



                        EmailHelper newEmail = new EmailHelper();
                        newEmail.Send(userassigned.Email, "New Case Created", "Dear " + profile["FirstName"] + ",\n\n A new case has been created and assigned to you by " + createdByProfile["FirstName"] + ". You should login to www.sudb.co.uk to review your current open cases.\n\n Kind Regards\nThe Students' Union Management Hub");
                        }
                }


                return newCase.caseId;
            }
            catch
            {
                return 0;
            }
        }


        public int addReminder(int caseId,string notes, int reminderType, DateTime actionDate, Guid actionBy)
        {

            var currentuser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

            tblReminder newReminder = new tblReminder
            {
              reminderType=reminderType,
              createdBy = (Guid)currentuser.ProviderUserKey,
              actionDate=actionDate,
              
                creationDate = DateTime.Now,
                actionBy = actionBy,
                linkedCase=caseId,
                reminderNotes=notes
            };
            db.tblReminders.InsertOnSubmit(newReminder);
            db.SubmitChanges();

            ActivityLogClass activitylog = new ActivityLogClass();
            activitylog.logActivity("New Reminder", "A new remider has been created for case " + caseId);

            var caseis = ReturnCase(caseId);

            //Send an Email to user who case is assigned to
            var permissions = getPermissions(actionBy);
            if (permissions.caseEmails == 1)
            {
                //Get user
                var userassigned = Membership.GetUser(actionBy);
                var userwhocreated = Membership.GetUser((Guid)currentuser.ProviderUserKey);

                if (userassigned.UserName != userwhocreated.UserName)
                {
                    ProfileBase profile = ProfileBase.Create(userassigned.UserName);

                    ProfileBase createdByProfile = ProfileBase.Create(userwhocreated.UserName);



                    EmailHelper newEmail = new EmailHelper();
                    newEmail.Send(userassigned.Email, "New Reminder", "Dear " + profile["FirstName"] + ",\n\n A new case reminder has been created for you by " + createdByProfile["FirstName"] + ". You should login to www.sudb.co.uk to your open reminders.\n\n Kind Regards\nThe Students' Union Management Hub");




                }
            }


            return newReminder.reminderId;
        }


        public int newCaseLog(int caseId, string caseNotes, int TimeUnits)
        {

             var currentuser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                tblCaseLog newLog = new tblCaseLog
                {
                    caseId = caseId,
                    notes = caseNotes.Replace("<", "(").Replace(">", ")").Replace(Environment.NewLine, "<br />"),
                    timeUnits = TimeUnits,
                    staffmember = (Guid)currentuser.ProviderUserKey,
                    DateTime = DateTime.Now
                };
                db.tblCaseLogs.InsertOnSubmit(newLog);
                db.SubmitChanges();
                  
                ActivityLogClass activitylog = new ActivityLogClass();
                activitylog.logActivity("Create Log", "Created Log Id " + newLog.Id.ToString());

            var caseis = ReturnCase(caseId);

                //Send an Email to user who case is assigned to
                var permissions = getPermissions((Guid)caseis.caseAssignedTo);
                if (permissions.caseEmails == 1)
                {
                    //Get user
                    var userassigned = Membership.GetUser(caseis.caseAssignedTo);
                    var userwhocreated = Membership.GetUser((Guid)currentuser.ProviderUserKey);

                    if (userassigned.UserName != userwhocreated.UserName)
                    {
                        ProfileBase profile = ProfileBase.Create(userassigned.UserName);

                        ProfileBase createdByProfile = ProfileBase.Create(userwhocreated.UserName);



                        EmailHelper newEmail = new EmailHelper();
                        newEmail.Send(userassigned.Email, "New Case Notes", "Dear " + profile["FirstName"] + ",\n\n A new case note has been added to one of your cases by " + createdByProfile["FirstName"] + ". You should login to www.sudb.co.uk to review your current open cases.\n\n Kind Regards\nThe Students' Union Management Hub");


                    

                    }
                }


                return newLog.Id;
        }
         

        public bool updateCase(int caseId, Guid assignedTo, int caseType)
        {
            try
            {
                ActivityLogClass activitylog = new ActivityLogClass();
                activitylog.logActivity("Updated Case", "Saved changes to case number "+ caseId.ToString());


                var selectedcase = db.tblCases.Where(p => p.caseId.Equals(caseId)).FirstOrDefault();

                selectedcase.caseType = caseType;
                selectedcase.caseAssignedTo = assignedTo;

                db.SubmitChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool closeReminder(int reminderId)
        {
            var reminder = db.tblReminders.Where(p => p.reminderId == reminderId).Single();
            reminder.actionCompletedDate = DateTime.Now;
            db.SubmitChanges();
            return true;
        }
        public bool closeCase(int caseId, Guid assignedTo, int caseType)
        {
            try
            {
                var selectedcase = db.tblCases.Where(p => p.caseId.Equals(caseId)).FirstOrDefault();

                selectedcase.caseType = caseType;
                selectedcase.caseAssignedTo = assignedTo;
                selectedcase.dateClosed = DateTime.Now;

                db.SubmitChanges();

                ActivityLogClass activitylog = new ActivityLogClass();
                activitylog.logActivity("Closed Case", "Closed case number " + caseId.ToString());


                return true;
            }
            catch
            {
                return false;
            }
        }



        public bool reopenCase(int caseId)
        {
            try
            {
                var selectedcase = db.tblCases.Where(p => p.caseId.Equals(caseId)).FirstOrDefault();

                selectedcase.dateClosed = null;

                db.SubmitChanges();

                ActivityLogClass activitylog = new ActivityLogClass();
                activitylog.logActivity("Case Re-Opened", "Re-Opened case number " + caseId.ToString());


                return true;
            }
            catch
            {
                return false;
            }
        }



        public IQueryable ReturnTimeUnits()
        {
            var su = getSU();

            var units = from p in db.tblTimeUnits where p.SU.Equals(su.Id) orderby p.MinutesToAdd select p;

            return units;
        }

        public IQueryable<tblFaculty> ReturnFaculties()
        {
            var su = getSU();

            var faculties = from p in db.tblFaculties where p.StudentUnion.Equals(su.Id) select p;

            return faculties;
        }

        public IQueryable ReturnMembershipType()
        {
            var su = getSU();

            var type = from p in db.tblMembershipTypes where p.StudentUnion.Equals(su.Id) select p;

            return type;
        }

        public tblMembershipType ReturnStudentMembershipType(int Type)
        {
            

            var type = db.tblMembershipTypes.Where(p=>p.Id==Type).Single();

            return type;
        }


        public bool setStudentToEditNull()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                try
                {
                    var userdetails = getPermissions((Guid)user.ProviderUserKey);
                    userdetails.EditStudent = null;
                    db.SubmitChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public IQueryable studentSearch(string SearchTerm, int su)
        {

            var students = from p in db.tblStudents join q in db.tblFaculties on p.Faculty equals q.Id join s in db.tblCampuses on p.Campus equals s.Id join e in db.tblMembershipTypes on p.MembershipType equals e.Id where (s.Campus.Contains(SearchTerm) || q.FacultyName.Contains(SearchTerm) || e.MembershipType.Contains(SearchTerm) || p.StudentNumber.Contains(SearchTerm) || p.Surname.Contains(SearchTerm) || p.FirstName.Contains(SearchTerm) || p.Level.Contains(SearchTerm)) where (p.StudentsUnion==su) orderby p.Surname,p.FirstName select p;
            return students;

        }

        public IQueryable<tblMembership>returnSportsThatMemberOf(int studentId)
        {
            var su = getSU();

            int currentyear = DateTime.Now.Year;
            if (DateTime.Now < new DateTime(DateTime.Now.Year, (int)su.YearStartMonth, 01))
            {
                currentyear = currentyear - 1;
            }

            DateTime lowestDateJoined = new DateTime(currentyear, (int)su.YearStartMonth, 01);
            DateTime highestDateJoined = new DateTime(currentyear + 1, (int)su.YearStartMonth - 1, 31);

            var members = from p in db.tblMemberships join s in db.tblStudents on p.StudentId equals s.Id join t in db.tblMembershipTypes on s.MembershipType equals t.Id where t.CanJoinSports == 1 join r in db.tblTransactions on p.TransactionId equals r.transactionId where (p.DateJoined.Value < highestDateJoined) where (p.DateJoined.Value > lowestDateJoined) where (p.StudentId == studentId) where (r.status == 1 || r.status == 3 || r.status == 4 || r.status == 5) where p.Refunded == null select p;
            return members;
        }
        public tblStudent checkIfStudent(string SearchTerm)
        {
            try
            {
                var students = db.tblStudents.Where(p => p.StudentsUnion == 1).Where(p => p.StudentNumber == SearchTerm.Trim()).FirstOrDefault();

                var getType = db.tblMembershipTypes.Where(p => p.Id == students.MembershipType).Single();

                if (getType.CanJoinSports == 1)
                {

                    return students;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }

        }


        public tblStudent checkIfStudentUWSU(string SearchTerm)
        {
            try
            {
                var students = db.tblStudents.Where(p=>p.StudentsUnion==3).Where(p => p.StudentNumber == SearchTerm.Trim()).FirstOrDefault();

                var getType = db.tblMembershipTypes.Where(p => p.Id == students.MembershipType).Single();

                if (getType.CanJoinSports == 1)
                {

                    return students;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }

        }


        public tblStudent returnStudentByNumber(string SearchTerm)
        {
            try
            {
                var students = db.tblStudents.Where(p => p.StudentNumber == SearchTerm.Trim()).FirstOrDefault();


                return students;
            }
            catch
            {
                return null;
            }
          

        }

        public tblStudent returnRecord(string SearchTerm)
        {

            var students = db.tblStudents.Where(p => p.StudentNumber == SearchTerm.Trim()).FirstOrDefault();

            
            
                return students;
          

        }

        private Task longwork(int memtype)
        {
            var task = Task.Factory.StartNew(() =>
            {
                var students = from p in db.tblStudents where p.StudentsUnion == getSU().Id join q in db.tblMembershipTypes on p.MembershipType equals q.Id where q.PartOfArchiveUpdate == 1 select p;

                foreach (var student in students)
                {
                    student.MembershipType = memtype;
                    db.SubmitChanges();
                }
            });

            return task;
        }
        public async Task updateAllStudentsMemType(int memtype)
        {

            await longwork(memtype);


            //activitylogger.logActivity("Upadted Membership Type", "All Students were updated to Membership Type " + memtype.ToString());

        }

        public tblFaculty returnFaculty(int facultyId)
        {

            var faculty = db.tblFaculties.Where(p => p.Id == facultyId).Single();
            return faculty;

        }

        public tblCampuse returnCampus(int campusId)
        {

            var campus = db.tblCampuses.Where(p => p.Id == campusId).Single();
            return campus;

        }

        public tblTimeUnit returnTime(int timeId)
        {
                var time = db.tblTimeUnits.Where(p=>p.Id==timeId).Single();
                    return time;
        }

        public IQueryable<tblCaseLog> ReturnCaseNotes(int caseId)
        {
              var su = getSU();

              var notes = from p in db.tblCaseLogs where p.caseId.Equals(caseId) select p;
              notes = notes.OrderByDescending(p => p.DateTime);
              return notes;

        }


        public IQueryable ReturnOpenStudentCases(int studentId)
        {
            var su = getSU();

            var notes = from p in db.tblCases 
                        join q in db.tblStudents on p.StudentId equals q.Id 
                        where p.StudentId.Equals(studentId)
                        where q.StudentsUnion==su.Id
                        where p.dateClosed == null select p;
            notes = notes.OrderByDescending(p => p.dateOpened);
            return notes;

        }

        public IQueryable ReturnOpenStaffCases(Guid StaffGuid)
        {
            var su = getSU();

            var notes = from p in db.tblCases
                        join q in db.tblStudents on p.StudentId equals q.Id
                        where p.caseAssignedTo.Equals(StaffGuid)
                        where q.StudentsUnion == su.Id
                        where p.dateClosed == null
                        select p;
            notes = notes.OrderByDescending(p => p.dateOpened);
            return notes;

        }


        public IQueryable<tblReminder> ReturnOpenStaffReminders(Guid StaffGuid)
        {
            var su = getSU();

            var notes = from p in db.tblReminders
                        where p.actionBy.Equals(StaffGuid)
                        where p.actionDate <= DateTime.Now.AddDays(7)
                        where p.actionCompletedDate == null
                        select p;
            notes = notes.OrderBy(p => p.actionDate);
            return notes;

        }


        public IQueryable ReturnClosedStaffCases(Guid StaffGuid)
        {
            var su = getSU();

            var notes = from p in db.tblCases
                        join q in db.tblStudents on p.StudentId equals q.Id
                        where p.caseAssignedTo.Equals(StaffGuid)
                        where q.StudentsUnion == su.Id
                        where p.dateClosed != null
                        select p;
            notes = notes.OrderByDescending(p => p.dateOpened);
            return notes;

        }

        public tblCaseType returnCaseType(int typeId)
        {
          var casetype = db.tblCaseTypes.Where(p => p.Id.Equals(typeId)).FirstOrDefault();
            return casetype;
        }

        public IQueryable ReturnClosedStudentCases(int studentId)
        {
            var su = getSU();

            var notes = from p in db.tblCases
                        join q in db.tblStudents on p.StudentId equals q.Id
                        where p.StudentId.Equals(studentId)
                        where q.StudentsUnion == su.Id
                        where p.dateClosed != null
                        select p;
            notes = notes.OrderByDescending(p => p.dateClosed);
            return notes;

        }

        public tblCase ReturnCase(int caseId)
        {
            var su = getSU();

            //Check if user can read / write cases or if this case is assigned to this user
           
            var notes = db.tblCases.Where(p => p.caseId.Equals(caseId)).FirstOrDefault();

           
           
                //Check that this case belongs to this SU
                var student = db.tblStudents.Where(p => p.Id == notes.StudentId).Single();

                if (student.StudentsUnion == su.Id)
                {
                    ActivityLogClass activitylog = new ActivityLogClass();
                    activitylog.logActivity("Viewed Case", "Viewed case number " + caseId.ToString());



                    return notes;
                }
                else
                {
                    return null;
                }

           

        }
    }
}