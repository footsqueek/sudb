﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SUDB.Classes
{
    public class Points
    {

        DataClasses1DataContext db = new DataClasses1DataContext();
        ActivityLogClass activitylogger = new ActivityLogClass();

        public int returnPointsBalance(int StudentId)
        {
            try
            {
                var points = from p in db.tblPointsTransactions where (p.StudentId == StudentId) select p;
                int total = (int)points.Sum(p => p.NumberOfPoints);


                return total;
            }
            catch
            {
                return 0;
            }
        }

        public int returnPointsExposure()
        {
            var points = from p in db.tblPointsTransactions select p;
            int total = (int)points.Sum(p => p.NumberOfPoints);
            return total;
        }
    }
}