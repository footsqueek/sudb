﻿<%@ Page Title="Create Session" MaintainScrollPositionOnPostback="true" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="CreateSession.aspx.cs" Inherits="SUDatabase.Committee.Sessions.CreateSession" %>
<%@ Register src="../../UserControls/CommitteeMenu.ascx" tagname="CommitteeMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

   
<script>

    $(document).ready(

  /* This is the function that will get executed after the DOM is fully loaded */
  function () {

     $('#txtStartDate').datetimepicker({
          format: 'd M Y H:i',
          lang: 'en',
          minDate: '0'
        
     });

     $('#txtEndDate').datetimepicker({
         format: 'd M Y H:i',
         lang: 'en',
         minDate: '0',
        
     });
  }

);


    
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CommitteeMenu ID="CommitteeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



     <h1><asp:Label ID="lblSportSocietyName" runat="server" Text="Label"></asp:Label></h1>

    <h2>Create Session</h2>

    <p>Please enter the details of your session below.</p>

        <asp:Literal ID="litError" Visible="false" runat="server"></asp:Literal>

      <p><asp:Label CssClass="label" ID="Label6" runat="server" Text="Session Name"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtName" Text="" Enabled="true" runat="server"></asp:TextBox><br /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtName" CssClass="error" runat="server" ErrorMessage="You must enter a session name"></asp:RequiredFieldValidator></p>

       <p><asp:Label CssClass="label" ID="Label1" runat="server" Text="Session Location"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtLocation" Text="" Enabled="true" runat="server"></asp:TextBox><br /><asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtLocation" CssClass="error" runat="server" ErrorMessage="You must enter a session location"></asp:RequiredFieldValidator></p>

     <p><asp:Label CssClass="label" ID="Label9" runat="server" Text="Post Code"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtPostCode" Text="" Enabled="true" runat="server"></asp:TextBox><br /><asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="txtPostcode" CssClass="error" runat="server" ErrorMessage="You must enter a session location"></asp:RequiredFieldValidator></p>



       <p><asp:Label CssClass="label" ID="Label5" runat="server" Text="Linked Room Request"></asp:Label><br />
           <asp:DropDownList ID="cboRoomRequest" CssClass="combobox" runat="server">
               <asp:ListItem Value="0">No Room Request</asp:ListItem>
               <asp:ListItem Value="-1">Create Room Request</asp:ListItem>
           </asp:DropDownList></p>


     <p><asp:Label CssClass="label" ID="Label14" runat="server" Text="Linked Transport Request"></asp:Label><br />
           <asp:DropDownList ID="cboTransportRequest" CssClass="combobox" runat="server">
               <asp:ListItem Value="0">No Transport Required</asp:ListItem>
               <asp:ListItem Value="-1">Create Transport Request</asp:ListItem>
           </asp:DropDownList>  </p>


          <p><asp:Label CssClass="label" ID="Label7" runat="server" Text="Team"></asp:Label><br />
           <asp:DropDownList ID="cboTeam" CssClass="combobox" runat="server">
               <asp:ListItem Value="0">All Members</asp:ListItem>
           </asp:DropDownList>  </p>

         <p><asp:Label CssClass="label" ID="Label4" runat="server" Text="Is this session part of a trip?"></asp:Label><br />
           <asp:DropDownList ID="cboTrip" CssClass="combobox" runat="server">
               <asp:ListItem Value="0">No</asp:ListItem>
                <asp:ListItem Value="-1" Selected="True">Create New Trip Registration</asp:ListItem>
           </asp:DropDownList>  </p>



      <p><asp:Label CssClass="label" ID="Label2" runat="server" Text="Session Start Date / Time"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtStartDate" Text="" Enabled="true" runat="server"></asp:TextBox><br /><asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtStartDate" CssClass="error" runat="server" ErrorMessage="You must enter a session start date and time"></asp:RequiredFieldValidator></p>

     <p><asp:Label CssClass="label" ID="Label3" runat="server" Text="Session End Date / Time"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtEndDate" Text="" Enabled="true" runat="server"></asp:TextBox><br /><asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtEndDate" CssClass="error" runat="server" ErrorMessage="You must enter a session end date and time"></asp:RequiredFieldValidator></p>

    <p><asp:Label CssClass="label" ID="lblweeks" runat="server" Text="Number of weeks"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtRepeat" Text="1" Enabled="true" runat="server"></asp:TextBox><br />
        <asp:RangeValidator ID="RangeValidator1" Type="Integer" CssClass="error" runat="server" ErrorMessage="This value must be between 1 and 40" ControlToValidate="txtRepeat" MaximumValue="40" MinimumValue="1"></asp:RangeValidator> <br /><asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="txtRepeat" CssClass="error" runat="server" ErrorMessage="You must enter the number of weeks that this session will repeat for"></asp:RequiredFieldValidator></p>



    <h2>Information provided by the Students' Union</h2>

    <p>The information below is provided by the students' union if it is relevant to the activity.</p>


             <p><asp:Label CssClass="label" ID="Label13" runat="server" Text="BUCS Fixture"></asp:Label><br />
           <asp:DropDownList ID="cboBucs" CssClass="combobox" runat="server">
               <asp:ListItem Value="0">No</asp:ListItem>
                <asp:ListItem Value="1">Yes</asp:ListItem>
           </asp:DropDownList>  </p>

      <p><asp:Label CssClass="label" ID="Label8" runat="server" Text="Transport"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtTransport" Text="" Enabled="true" runat="server"></asp:TextBox></p>

         <p><asp:Label CssClass="label" ID="Label12" runat="server" Text="Departure Time"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtDepart" Text="" Enabled="true" runat="server"></asp:TextBox></p>

     <p><asp:Label CssClass="label" ID="Label10" runat="server" Text="Number of Team Teas"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtTeas" Text="0" Enabled="true" runat="server"></asp:TextBox></p>

     <p><asp:Label CssClass="label" ID="Label11" runat="server" Text="Team Teas Time"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtTeamTeasTime" Text="" Enabled="true" runat="server"></asp:TextBox></p>





    <asp:Button ID="btnCreate" CssClass="submitbutton" runat="server" Text="Create Session" OnClick="btnCreate_Click" />

</asp:Content>
