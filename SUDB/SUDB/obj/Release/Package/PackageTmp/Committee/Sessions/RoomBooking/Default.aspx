﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Committee.Sessions.RoomBooking.Default" %>
<%@ Register src="../../../UserControls/CommitteeMenu.ascx" tagname="CommitteeMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CommitteeMenu ID="CommitteeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Room Booking Request</h1>

    <p>Please provide details of your room booking request below. Please give as much information you can as to what is required.</p>

    <h2>Sessions this request is currently linked to:</h2>

    <table class="nicetable">
        <tr>

            <th>Session Name</th>
            <th>Start Date</th>
            <th>End Date</th>

        </tr>

    <asp:Repeater ID="rptsessions" runat="server">

        <ItemTemplate>
            <tr>

                <td>
                    <asp:Label ID="lblSessionName" runat="server" Text="Label"></asp:Label></td>
                <td>
                    <asp:Label ID="lblStart" runat="server" Text="Label"></asp:Label></td>
                <td>
                    <asp:Label ID="lblEnd" runat="server" Text="Label"></asp:Label></td>
            </tr>



        </ItemTemplate>



    </asp:Repeater>
        </table>

  

     <p><asp:Label ID="Label2" CssClass="label" runat="server" Text="Request Description"></asp:Label><br />
    <asp:TextBox ID="txtHeadline" CssClass="txtbox" runat="server"></asp:TextBox></p>

    <p><asp:Label ID="lblDetails" CssClass="label" runat="server" Text="Details of Room Request"></asp:Label><br />
    <asp:TextBox ID="txtDetails" placeholder="Please enter your request here and include approximate number of people, any required equipment etc." Rows="5" TextMode="MultiLine" CssClass="txtbox" runat="server"></asp:TextBox></p>

     <p> <asp:Label ID="Label1" CssClass="label" runat="server" Text="Response from Students' Union" Width="284px"></asp:Label><br />
    <asp:TextBox ID="txtResponse" Rows="5" Enabled="false" TextMode="MultiLine" CssClass="txtbox" runat="server"></asp:TextBox></p>

    <p>
        <asp:Label ID="Label3" CssClass="label" runat="server" Text="Request Status" Width="284px"></asp:Label><br />
        <asp:DropDownList Enabled="false" CssClass="combobox" ID="cboStatus" runat="server">
        <asp:ListItem Value="0">Waiting Response</asp:ListItem>
        <asp:ListItem Value="1">Room Booked</asp:ListItem>
        <asp:ListItem Value="2">Waiting Response from Sport / Society</asp:ListItem>
            </asp:DropDownList>
            </p>


     <asp:Button CssClass="submitbutton" ID="btnSubmit" runat="server" Text="Update Request" OnClick="btnSubmit_Click" />

</asp:Content>
