﻿<%@ Page Title="Trip Registration" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Committee.Sessions.TripRegistration.Default" %>
<%@ Register src="../../../UserControls/CommitteeMenu.ascx" tagname="CommitteeMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    
   
<script>

    $(document).ready(

  /* This is the function that will get executed after the DOM is fully loaded */
  function () {

     $('#txtDepartureTime').datetimepicker({
          format: 'd M Y H:i',
          lang: 'en',
          minDate: '0'
        
     });

     $('#txtReturnDateTime').datetimepicker({
         format: 'd M Y H:i',
         lang: 'en',
         minDate: '0',
        
     });
  }

);


    
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CommitteeMenu ID="CommitteeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Trip Registration</h1>

    <p>Every activity session that takes places away from University campus must have an approved trip registration prior to the activity taking place.</p>

   


    <h2>Trip Details</h2>

    <p>The current status of this trip is <strong><asp:Label ID="lblStatus" runat="server" Text="Not Yet Reviewed"></asp:Label></strong>. If you make changes to the trip it will be need to be reviewed by the Students' Union.</p>

     <p>
        <asp:Label ID="Label5" CssClass="label" runat="server" Text="Sport / Society"></asp:Label>
         <asp:TextBox ID="txtSportSocoety" Enabled="false" Text="Sport or Society" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>
     <p>
        <asp:Label ID="lblDepartureTime" CssClass="label" runat="server" Text="Departure Date / Time"></asp:Label>
         <asp:TextBox ID="txtDepartureTime" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>

    
     <p>
        <asp:Label ID="Label1" CssClass="label" runat="server" Text="Return Date / Time"></asp:Label>
         <asp:TextBox ID="txtReturnDateTime"  CssClass="txtbox" runat="server"></asp:TextBox>

    </p>

    
     <p>
        <asp:Label ID="Label2" CssClass="label" runat="server" Text="Address of Destination"></asp:Label>
         <asp:TextBox ID="txtDestinationAddress"  CssClass="txtbox" runat="server"></asp:TextBox>

    </p>

     <p>
        <asp:Label ID="Label6" CssClass="label" runat="server" Text="Accomodation (if applicable)"></asp:Label>
         <asp:TextBox ID="txtAccomodation"  CssClass="txtbox" runat="server"></asp:TextBox>

    </p>

   

    <table class="nicetable">
        <tr>
            <th>Member Name</th>
            <th>Trip Attendee</th>
            <th>Driver</th>
            <th>1st Aider</th>
            <th>Mode of Transport</th>

        </tr>
    <asp:Repeater ID="rptRegister" runat="server">

        <ItemTemplate>

            <tr>
                <asp:Label ID="lblId" Visible="false" runat="server" Text=""></asp:Label> 
                <td><asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>  </td>
                <td><div class="onoffswitch">
          
    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" runat="server" id="chkAttendee">
    <label class="onoffswitch-label" runat="server" id="lblattendee">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</div>                    
</td>

                   <td><div class="onoffswitch">
                 
    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" runat="server" id="chkDriver">
    <label class="onoffswitch-label" runat="server" id="lblDriver">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</div>                    
</td>

                   <td>
                       
                       <asp:Image Height="25px" ID="imgFirstAid" ImageUrl="~/Images/Icons/first-aid.jpg" runat="server" />
                                        
</td>

                <td><asp:DropDownList ID="cboModeofTransport" runat="server">
                    <asp:ListItem Text="Coach" Value="1"></asp:ListItem>
                       <asp:ListItem Text="Car" Value="2"></asp:ListItem>
                       <asp:ListItem Text="Bike" Value="3"></asp:ListItem>
                       <asp:ListItem Text="Minibus" Value="4"></asp:ListItem>
                       <asp:ListItem Text="Public Transport" Value="6"></asp:ListItem>
                       <asp:ListItem Text="Walk" Value="7"></asp:ListItem>
                       <asp:ListItem Text="SU Vehicle" Value="8"></asp:ListItem>
                    </asp:DropDownList>  </td>

            </tr>

        </ItemTemplate>

    </asp:Repeater>
    </table>

      <asp:Button ID="btnSubmitTripReg" CssClass="submitbutton" runat="server" Text="Submit Trip Registration" OnClick="btnSubmitTripReg_Click" />

    <span id="amendment" runat="server" visible="false">
    <h2>Trip Amendment</h2>
    <p>If there are any changes to your trip please tell us about them in the box below:</p>
     <p>
        <asp:Label ID="Label7" CssClass="label" runat="server" Text="Trip Amendment"></asp:Label>
         <asp:TextBox ID="txtAmendment" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>
     <asp:Button ID="btnSubmitAmendment" CssClass="submitbutton" runat="server" Text="Submit Amendment" OnClick="btnSubmitAmendment_Click"/>
    </span>


    <h2>Trip Approval</h2>
    <p>Before you can go on a trip your trip registration must be approved. Details of your trip approval are shown below.</p>
        <p>
        <asp:Label ID="Label4" CssClass="label" runat="server" Text="Staff Member"></asp:Label>
            <asp:Label ID="lblStaffMember" runat="server" Text="No staff member assigned"></asp:Label>

    </p>

     <p>
        <asp:Label ID="Label3" CssClass="label" runat="server" Text="Staff Comments"></asp:Label>
         <asp:TextBox ID="txtComments" Enabled="false" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>


    <asp:Button Visible="false" ID="btnApprove" CssClass="submitbutton" runat="server" Text="Approve Trip Registration" OnClick="btnApprove_Click" />
      <asp:Button ID="btnReject" Visible="false" CssClass="submitbutton" runat="server" Text="Reject Trip Registration" OnClick="btnReject_Click" />
  

</asp:Content>
