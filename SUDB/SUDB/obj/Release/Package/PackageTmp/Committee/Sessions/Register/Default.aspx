﻿<%@ Page Title="Session Register" EnableEventValidation="false" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Committee.Sessions.Register.Default" %>
<%@ Register src="../../../UserControls/CommitteeMenu.ascx" tagname="CommitteeMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CommitteeMenu ID="CommitteeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Session Register</h1>

    <h2>
        <asp:Label ID="lblSessionName" runat="server" Text="Label"></asp:Label></h2>

    <p>The members listed below are all of the members eligable to attend the session. Please complete the register at the start of the session.</p>



    <table class="nicetable">
        <tr>
            <th>Member Name</th>
            <th></th>

        </tr>
    <asp:Repeater ID="rptRegister" runat="server">

        <ItemTemplate>
            <asp:Label ID="lblId" Visible="false" runat="server" Text="Label"></asp:Label> 
            <tr>

                <td><asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>  </td>
                <td><div class="onoffswitch">

    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" runat="server" id="myonoffswitch">
    <label class="onoffswitch-label" runat="server" id="lblBut">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</div>                    
</td>

            </tr>

        </ItemTemplate>

    </asp:Repeater>
    </table>

    <asp:Button ID="btnAll" runat="server" CssClass="submitbutton" Text="All Attended" OnClick="btnAll_Click"/>

    <h2>Anything else that we need to know?</h2>
    <asp:TextBox ID="txtOther" TextMode="MultiLine" CssClass="txtbox" runat="server"></asp:TextBox>


    <br />
    <asp:Button ID="btnSubmit" runat="server" CssClass="submitbutton" Text="Submit Register" OnClick="btnSubmit_Click" />
</asp:Content>
