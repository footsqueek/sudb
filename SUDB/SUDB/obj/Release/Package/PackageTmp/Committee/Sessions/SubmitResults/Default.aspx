﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Committee.Sessions.SubmitResults.Default" %>
<%@ Register src="../../../UserControls/CommitteeMenu.ascx" tagname="CommitteeMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CommitteeMenu ID="CommitteeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Submit Results</h1>

    <p>Please use the form below to submit results from your Fixture.</p>

        <p><asp:Label CssClass="label" ID="Label11" runat="server" Text="Session Name"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtSessionName" Text="" Enabled="true" runat="server"></asp:TextBox></p>


     <p><asp:Label CssClass="label" ID="Label1" runat="server" Text="Our Score"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtOurs" Text="" Enabled="true" runat="server"></asp:TextBox></p>

    
     <p><asp:Label CssClass="label" ID="Label2" runat="server" Text="Their Score"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtTheirs" Text="" Enabled="true" runat="server"></asp:TextBox></p>


    <asp:Button CssClass="submitbutton" ID="btnSubmit" runat="server" Text="Submit Scores" OnClick="btnSubmit_Click" />

</asp:Content>
