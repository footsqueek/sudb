﻿<%@ Page Title="Committee Portal" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Committee.Default" %>
<%@ Register src="../UserControls/CommitteeMenu.ascx" tagname="CommitteeMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CommitteeMenu ID="CommitteeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Committee Portal</h1>

    <p>Welcome to the Committee Portal. This system allows you to administer Sports or Societies including submitting Risk Assessments, Trip Registration and session information.</p>

    <p>You might also find it useful to administer some aspects of your Sport or Society using the SUDB Committee App. This is available for download from the Apple App Store and Android Market.</p>

    <div class="error" id="LockedOut" runat="server">

        <h2>LOCKED OUT</h2>

        <p>The active sport or society is has been locked out by your administrator. The most likely cause is that you have not submitted a required submission within the allocated time frame. Please contact the Students' Union to resolve this issue. Until you have spoken to the Students' Union you are required to suspend all activity within this sport or society.</p>


    </div>
    <!--
    <h2>Announcements and Reminders</h2>

<table class="nicetable">

    <tr>
        <th>Sport or Society</th>
        <th>Details</th>
        <th></th>

    </tr>

    <tr>

        <td>Mens Football</td>
        <td>You have not yet submitted a risk assessment for your session on 12th August 2014.</td>
        <td></td>

    </tr>

    
    <tr>

        <td>Mens Football</td>
        <td>Tickets for your end of season meal can now be purchased via the clubs and societies shop on the CSU website.</td>
        <td></td>

    </tr>

      <tr>

        <td>All Sports and Societies</td>
        <td>Please ask all of your members to attend the CSU AGM on 3rd March at 7pm in Binks.</td>
        <td></td>

    </tr>



</table>--></asp:Content>
