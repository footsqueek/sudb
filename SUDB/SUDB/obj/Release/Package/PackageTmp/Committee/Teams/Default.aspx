﻿<%@ Page Title="Team Management" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Committee.Teams.Default" %>
<%@ Register src="../../UserControls/CommitteeMenu.ascx" tagname="CommitteeMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CommitteeMenu ID="CommitteeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <h1><asp:Label ID="lblSportSocietyName" runat="server" Text="Label"></asp:Label></h1>

      <p>Please select the year that you would like to view: <asp:DropDownList ID="cboYears" runat="server"></asp:DropDownList></p>

    <h2>Teams</h2>

    <table class="nicetable">

        <tr>

            <th>Team / Group Name</th>
            <th></th>

        </tr>

    <asp:Repeater ID="rptTeams" runat="server">

        <ItemTemplate>

            <tr>

                <td>
                    <asp:Label ID="lblTeamName" runat="server" Text="Label"></asp:Label></td>

                <td>  <asp:Button id="btnView"
           Text="View Team Sheet"
                    CssClass="blackbutton"
           CommandName="View"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnView_Clickbutton" 
           runat="server"/> <asp:Button id="btnEdit"
           Text="Edit Membership"
                    CssClass="blackbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnEdit_Clickbutton" 
           runat="server"/> <asp:Button id="btnDelete"
           Text="Delete Team"
                    CssClass="blackbutton"
           CommandName="Delete"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnDelete_Clickbutton" 
           runat="server"/></td>

            </tr>

        </ItemTemplate>


    </asp:Repeater>

        </table>

    <asp:Button ID="btnCreateTeam" runat="server" CssClass="submitbutton" Text="Create Team" OnClick="btnCreateTeam_Click" />
</asp:Content>
