﻿<%@ Page Title="Create Team" EnableEventValidation="false" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="CreateTeam.aspx.cs" Inherits="SUDatabase.Committee.Teams.CreateTeam" %>
<%@ Register src="../../UserControls/CommitteeMenu.ascx" tagname="CommitteeMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CommitteeMenu ID="CommitteeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <h1><asp:Label ID="lblSportSocietyName" runat="server" Text="Label"></asp:Label></h1>

    <h2>Create Team / Group</h2>

       <p><asp:Label CssClass="label" ID="Label6" runat="server" Text="Team Name"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtName" Text="" Enabled="true" runat="server"></asp:TextBox><br /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtName" CssClass="error" runat="server" ErrorMessage="You must enter a team name"></asp:RequiredFieldValidator></p>

    <asp:Button ID="btnCreateTeam" CssClass="submitbutton" runat="server" Text="Create Team" OnClick="btnCreateTeam_Click" />

</asp:Content>
