﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Students' Union Database</title>

    <link rel="stylesheet" href="Styles/LoginPage.css" />
    <meta name="viewport" content="width=device-width, user-scalable=no">
</head>
<body>
    <form id="form1" runat="server">
    <div id="Wrapper">

          <div id="SiteContainer">

              <div id="ContentArea">

                  <h1>Students' Union Management Hub</h1>

                 <p class="center"><img src="Images/fsu.png" alt="Footsqueek for Students' Unions" /></p>
                  
                  <p class="center">Welcome to the Students' Union Management Hub. To login please enter your e-mail address and password below. </p>

                  <div id="loginbox">
                  <asp:Login ID="Login1" runat="server" OnLoggedIn="Login1_LoggedIn">
                      <LayoutTemplate>
                         
                                                  <p"><asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Email Address:</asp:Label><br />
                                              
                                                  <asp:TextBox CssClass="txtbox" ID="UserName" runat="server"></asp:TextBox>


                                                  <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator></p>
                                             
                                                  <p><strong><asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label></strong><br />
                                             
                                                  <asp:TextBox CssClass="txtbox" ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                                                  <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator></p>
                                            
                                                  <p><asp:CheckBox ID="RememberMe" runat="server" Text="Remember me next time." /> <a style="float:right" href="ForgottenPassword.aspx">Forgotten Password</a></p>
                                               <p><asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal></p>
                                               <p class="center"><asp:Button ID="LoginButton" runat="server" CssClass="submitbutton" CommandName="Login" Text="Log In" ValidationGroup="Login1" /></p>
                                             
                      </LayoutTemplate>
                      </asp:Login>
                  
                  </div>


              </div>

          </div>
    
    </div>


            <div id="Footer">

                <div id="FooterContent">
           
                    </div>

        </div>

               <div id="BottomMenu">

                <div id="BottomMenuContent">
                    <p class="FootsqueekCredit">System Developed By <a href="http://www.footsqueek.co.uk">Footsqueek</a></p>
            
                    
                    </div>

        </div>
    </form>
</body>
</html>
