﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="SUDatabase.Error" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Students' Union Database</title>

    <link rel="stylesheet" href="Styles/LoginPage.css" />
    <meta name="viewport" content="width=device-width, user-scalable=no">
</head>
<body>
    <form id="form1" runat="server">
    <div id="Wrapper">

          <div id="SiteContainer">

              <div id="ContentArea">

                  <h1>....OOOPS<br />It seems we have a few gremlins!</h1>

                 <p class="center"><img src="Images/fsu.png" alt="Footsqueek for Students' Unions" /></p>
                  
                  <p class="center">We're really sorry but it seems there are a few gremlins at work which has resulted in an unexpected error! Don't worry though, the error has been reported and we're working really hard to fix it as quickly as we can. In the mean time try clicking the button below to return to the system which might just resolve your problem.</p>


                   <p class="center"><asp:Button ID="Button1" CssClass="submitbutton" runat="server" Text="Return to System" OnClick="Button1_Click" /></p>
                  


              </div>

          </div>
    
    </div>


            <div id="Footer">

                <div id="FooterContent">
           
                    </div>

        </div>

               <div id="BottomMenu">

                <div id="BottomMenuContent">
                    <p class="FootsqueekCredit">System Developed By <a href="http://www.footsqueek.co.uk">Footsqueek</a></p>
            
                    
                    </div>

        </div>
    </form>
</body>
</html>
