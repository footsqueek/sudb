﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ElectionsMenu.ascx.cs" Inherits="SUDatabase.UserControls.ElectionsMenu" %>
 <div id="RightMenu">
<div class="RightMenuUL">

    <h2>Elections</h2>

    <ul>
        <li  runat="server" id="liManageElections"><a href="/Management/Elections/ManageElections/">Manage Elections</a></li>
        <li  runat="server" id="litCreateElection"><a href="/Management/Elections/CreateNew/">Create New Election</a></li>
       
          <li><a href="/Management/Elections/Clerk/">Clerk</a></li>
           

    </ul>

</div>
         </div>