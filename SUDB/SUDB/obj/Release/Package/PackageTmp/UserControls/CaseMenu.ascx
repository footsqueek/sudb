﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CaseMenu.ascx.cs" Inherits="SUDatabase.UserControls.CaseMenu" %>
    <div id="RightMenu">
<div class="RightMenuUL">

    <h2>Cases</h2>

    <ul>
        <li><a href="/Management/StudentManager/Default.aspx">Create new case</a></li>
         <li runat="server" visible="false" id="casesbytype"><a href="/Management/Cases/CasesByType.aspx">View cases by type</a></li>
         <li><a href="/Management/Cases/StaffCases.aspx">View Open Cases by staff member</a></li>
         <li><a href="/Management/Cases/StaffCases.aspx?closed=true">View Closed Cases by staff member</a></li>
    </ul>

</div>
         </div>