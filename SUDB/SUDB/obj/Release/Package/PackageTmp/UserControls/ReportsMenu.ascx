﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportsMenu.ascx.cs" Inherits="SUDatabase.UserControls.ReportsMenu" %>
     <div id="RightMenu">
<div class="RightMenuUL">

    <h2>Reports</h2>

    <ul>
       <li><a href="/Management/Reports/DisabledWomen.aspx">Participation of Disabled Women</a></li>
       <li><a href="/Management/Reports/DisabledMen.aspx">Participation of Disabled Men</a></li>
           <li><a href="/Management/Reports/DisabledWomanSports.aspx">Participation of Disabled Woman (Sports)</a></li>
            <li><a href="/Management/Reports/DisabledWomenSocieties.aspx">Participation of Disabled Woman (Societies)</a></li>
             <li><a href="/Management/Reports/DisabledMenSports.aspx">Participation of Disabled Men (Sports)</a></li>
            <li><a href="/Management/Reports/DisabledMenSocieties.aspx">Participation of Disabled Men (Societies)</a></li>



          <li><a href="/Management/Reports/Participation.aspx?who=AllWomen">Participation of Women</a></li>
       <li><a href="/Management/Reports/Participation.aspx?who=AllMen">Participation of Men</a></li>
           <li><a href="/Management/Reports/Participation.aspx?who=AllWomenSport">Participation of Woman (Sports)</a></li>
            <li><a href="/Management/Reports/Participation.aspx?who=AllWomenSociety">Participation of Woman (Societies)</a></li>
             <li><a href="/Management/Reports/Participation.aspx?who=AllMenSport">Participation of Men (Sports)</a></li>
            <li><a href="/Management/Reports/Participation.aspx?who=AllMenSociety">Participation of Men (Societies)</a></li>
    </ul>

</div>
         </div>