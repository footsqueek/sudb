﻿<%@ Page Title="View Open Cases By Staff Member" EnableEventValidation="false" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="StaffCases.aspx.cs" Inherits="SUDatabase.Management.Cases.StaffCases" %>
<%@ Register src="../../UserControls/CaseMenu.ascx" tagname="CaseMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CaseMenu ID="CaseMenu1" runat="server" />

     <div class="RightMenuUL">

        <h2>Total Cases</h2>

        <p><asp:Label ID="lblTotalCases" runat="server" Text="0"></asp:Label> (<asp:Label ID="lblTotalCasesClosed" runat="server" Text="0"></asp:Label>)</p>

    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1><asp:Label ID="lblPageHeading" runat="server" Text="View Open Cases By Staff Member"></asp:Label></h1>

        <p><asp:Label CssClass="label" ID="Label6" runat="server" Text="Staff Member"></asp:Label><br />
            <asp:DropDownList CssClass="combobox" ID="cboStaff" runat="server" AutoPostBack="True"></asp:DropDownList>
        </p>

    <table class="nicetable">
        <tr>
            <th>Case Type</th>
            <th>Student Name</th>
            <th>Date Opened</th>
            <th>Last Activity</th>
            <th></th>

        </tr>

    <asp:Repeater ID="rptOpenCases" runat="server">

        <ItemTemplate>

            <tr>

                <td><asp:Label ID="lblCaseType" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblStudentName" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblDateOpened" runat="server" Text="Label"></asp:Label></td>
                   <td><asp:Label ID="lblLastActivity" runat="server" Text="Label"></asp:Label></td>
             
                <td><asp:Button id="btnView"
           Text="View Case"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnView_Clickbutton" 
           runat="server"/></td>
            </tr>

        </ItemTemplate>

    </asp:Repeater>

        </table>

    <asp:Literal ID="litNoOpen" Visible="false" runat="server"><p class="error">This staff member has no open cases assigned to them.</p></asp:Literal>


</asp:Content>
