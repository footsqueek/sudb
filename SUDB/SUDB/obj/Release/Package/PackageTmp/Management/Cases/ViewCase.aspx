﻿<%@ Page Title="View Case" ValidateRequest="false" MaintainScrollPositionOnPostback="true" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ViewCase.aspx.cs" Inherits="SUDatabase.Management.Cases.ViewCase" %>
<%@ Register src="../../UserControls/CaseMenu.ascx" tagname="CaseMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CaseMenu ID="CaseMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>View Case</h1>
    <div id="casecontent">
  <p><asp:Label CssClass="label" ID="Label6" runat="server" Text="Student Name"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtName" Text="" Enabled="false" runat="server"></asp:TextBox></p>

        <h2>Student Details</h2>

        <table class="lefttable nicetable">

                <tr><td><strong>Date of Birth: </strong></td><td><asp:Label ID="lblDOB" runat="server" Text="Label"></asp:Label></td></tr>
        <tr><td><strong>Faculty: </strong></td><td><asp:Label ID="lblFaculty" runat="server" Text="Label"></asp:Label></td></tr>
               <tr><td><strong>Course: </strong></td><td><asp:Label ID="lblCourse" runat="server" Text=""></asp:Label></td></tr>
 
              <tr><td><strong>Level: </strong></td><td><asp:Label ID="lblLevel" runat="server" Text="Label"></asp:Label></td></tr>
               <tr><td><strong>Campus: </strong></td><td><asp:Label ID="lblcampus" runat="server" Text="Label"></asp:Label></td></tr>
             <tr><td><strong>Mobile Number: </strong></td><td><asp:Label ID="lblMobile" runat="server" Text="Label"></asp:Label></td></tr>
  <tr><td><strong>Email Address: </strong></td><td><asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label></td></tr>
 
            </table>

        <h2>Case Details</h2>


          <p><asp:Label  CssClass="label" ID="Label3" runat="server" Text="Case Type"></asp:Label><br /><asp:DropDownList CssClass="combobox" ID="cboCaseType" runat="server"></asp:DropDownList></p>


     <p class="leftpara"><asp:Label  CssClass="label" ID="Label4" runat="server" Text="Date Created"></asp:Label><br />
         <asp:TextBox ID="txtDateCreated" CssClass="txtbox" runat="server"></asp:TextBox></p>

     <p class="rightpara"><asp:Label  CssClass="label" Enabled="false" Visible="false" ID="lblClosed" runat="server" Text="Date Closed"></asp:Label><br />
         <asp:TextBox ID="txtClosed" Enabled="false"  CssClass="txtbox" Visible="false" runat="server"></asp:TextBox></p>

   
      <div class="clear"></div>
      <p class="leftpara"><asp:Label  CssClass="label" ID="Label1" runat="server" Text="Created By"></asp:Label><br /><asp:DropDownList CssClass="combobox" ID="cboCreatedBy" runat="server"></asp:DropDownList></p>


      <p class="rightpara"><asp:Label  CssClass="label" ID="Label2" runat="server" Text="Assigned To"></asp:Label><br /><asp:DropDownList CssClass="combobox" ID="cboAssignedTo" runat="server"></asp:DropDownList></p>


    <p class="leftpara"><asp:Label  CssClass="labelsmall" ID="Label5" runat="server" Text="Total Time: "></asp:Label><asp:Label ID="lblTime" runat="server" Text="15 minutes"></asp:Label></p>
        <p class="rightpara"><asp:Label  CssClass="labelsmall" ID="Label8" runat="server" Text="Case Age: "></asp:Label><asp:Label ID="lblAge" runat="server" Text="5 days"></asp:Label></p>


                     

        </div>

    <div class="clear"></div>
    <table class="lefttable nicetable">

        <tr>

            <th>Date / Time</th>
            <th>Staff Member</th>
            <th>Time</th>

        </tr>
        
        <tr id="createnotes1" runat="server">
            <td><asp:Label ID="lblDateTime" runat="server" Text="Label"></asp:Label></td>
                 <td><asp:Label ID="lblStaffMember" runat="server" Text="Label"></asp:Label></td>
            <td><asp:DropDownList ID="cboTime" runat="server"></asp:DropDownList></td>

        </tr>

        <tr  id="createnotes2" runat="server">

            <td class="notes" colspan="3">

                <asp:TextBox Rows="2" CssClass="txtbox" TextMode="MultiLine" ID="txtNotes" runat="server"></asp:TextBox>

                 <asp:RequiredFieldValidator ValidationGroup="notesgroup" ControlToValidate="txtNotes" ID="RequiredFieldValidator1" runat="server" CssClass="error" ErrorMessage="You must type some case notes"></asp:RequiredFieldValidator>
                <br />

                 <asp:Button ValidationGroup="notesgroup" CausesValidation="true"  CssClass="submitbutton" ID="btnSaveClose" runat="server" Text="Add Notes" OnClick="btnSaveClose_Click" /> 


            </td>

        </tr>
    <asp:Repeater ID="rptCaseNotes" runat="server">

        <ItemTemplate>

            <tr>
                 <td><asp:Label ID="lblDateTime" runat="server" Text="Label"></asp:Label></td>
                 <td><asp:Label ID="lblStaffMember" runat="server" Text="Label"></asp:Label></td>
                 <td>
                     <asp:DropDownList ID="cboTime" runat="server"></asp:DropDownList></td>
                </tr>
            <tr>
                 <td class="notes" colspan="3"><asp:Label ID="lblNotes" runat="server" Text="Label"></asp:Label></td>
            </tr>


        </ItemTemplate>

    </asp:Repeater>

        </table>

        <p><asp:Button  CssClass="submitbutton" ID="Button1" runat="server" Text="Save Changes" OnClick="Button1_Click" /> 
                         <asp:Button  CssClass="submitbutton" ID="Button2" runat="server" Text="Save and Close Case" OnClick="Button2_Click" />

  <asp:Button  CssClass="submitbutton" ID="Button3" runat="server" Text="View Students' Cases" OnClick="Button3_Click" /> </p>

</asp:Content>
