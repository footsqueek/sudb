﻿<%@ Page Title="Students Cases" EnableEventValidation="false" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="StudentsCases.aspx.cs" Inherits="SUDatabase.Management.Cases.StudentsCases" %>
<%@ Register src="../../UserControls/CaseMenu.ascx" tagname="CaseMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:CaseMenu ID="CaseMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>View Students' Cases</h1>



     <p><asp:Label CssClass="label" ID="Label6" runat="server" Text="Student Name"></asp:Label><br />
    <asp:TextBox CssClass="txtbox" ID="txtName" Text="" Enabled="false" runat="server"></asp:TextBox></p>


       <table class="nicetable">

                <tr><td><strong>Date of Birth: </strong></td><td><asp:Label ID="lblDOB" runat="server" Text="Label"></asp:Label></td></tr>
        <tr><td><strong>Faculty: </strong></td><td><asp:Label ID="lblFaculty" runat="server" Text="Label"></asp:Label></td></tr>
           <tr><td><strong>Course: </strong></td><td><asp:Label ID="lblCourse" runat="server" Text="Label"></asp:Label></td></tr>
        
               <tr><td><strong>Level: </strong></td><td><asp:Label ID="lblLevel" runat="server" Text="Label"></asp:Label></td></tr>
               <tr><td><strong>Campus: </strong></td><td><asp:Label ID="lblcampus" runat="server" Text="Label"></asp:Label></td></tr>
             <tr><td><strong>Mobile Number: </strong></td><td>
                 <asp:TextBox ID="txtMobile" CssClass="txtbox" runat="server"></asp:TextBox>  </td></tr>
               <tr><td><strong>Email Address: </strong></td><td>
                 <asp:TextBox ID="txtEmail" CssClass="txtbox" runat="server"></asp:TextBox>  </td></tr>
            </table>

    <asp:Button ID="btnSave" runat="server" CssClass="submitbutton" Text="Save Details" OnClick="btnSave_Click" />

        <h2>Student Cases</h2>


    <table class="lefttable nicetable">
        <tr>
            <th>Case Type</th>
            <th>Assigned To</th>
            <th>Date Opened</th>
            <th></th>

        </tr>

    <asp:Repeater ID="rptOpenCases" runat="server">

        <ItemTemplate>

            <tr>

                <td><asp:Label ID="lblCaseType" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblAssignedTo" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblDateOpened" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Button id="btnView"
           Text="View Case"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnView_Clickbutton" 
           runat="server"/></td>
            </tr>

        </ItemTemplate>

    </asp:Repeater>

        </table>

    <asp:Literal ID="litNoOpen" Visible="false" runat="server"><p class="error">This student has no open cases.</p></asp:Literal>

    <h2>Closed Cases</h2>

     <table class="lefttable nicetable">
        <tr>
            <th>Case Type</th>
            <th>Assigned To</th>
            <th>Date Opened</th>
             <th>Date Closed</th>
            <th></th>

        </tr>


    <asp:Repeater ID="rptClosedCases" runat="server">

        <ItemTemplate>

            <tr>

                <td><asp:Label ID="lblCaseType" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblAssignedTo" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblDateOpened" runat="server" Text="Label"></asp:Label></td>
                   <td><asp:Label ID="lblDateClosed" runat="server" Text="Label"></asp:Label></td>
             
                <td><asp:Button id="btnView"
           Text="View Case"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnView_Clickbutton" 
           runat="server"/></td>
            </tr>

        </ItemTemplate>

    </asp:Repeater>

        </table>
     <asp:Literal ID="litnoclosed" Visible="false" runat="server"><p class="error">This student has no closed cases.</p></asp:Literal>


 <asp:Button CssClass="submitbutton" ID="Button1" runat="server" Text="Create Case" OnClick="Button1_Click" />
    <asp:Button CssClass="submitbutton" ID="btnDone" runat="server" Text="Back to Student Search" OnClick="btnDone_Click" />

</asp:Content>
