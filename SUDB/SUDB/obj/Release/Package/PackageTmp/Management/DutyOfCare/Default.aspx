﻿<%@ Page Title="Duty of Care" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Management.DutyOfCare.Default" %>
<%@ Register src="../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

      <script>
    $(document).ready(

  /* This is the function that will get executed after the DOM is fully loaded */
  function () {

     $('#txtDate').datetimepicker({
     timepicker:false,
          format: 'd M Y',
          lang: 'en',
          maxDate: '0'
        
     });

    
  }

);
          </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />

       <div class="RightMenuUL">

        <h2>Logins Today</h2>

        <p><asp:Label ID="lblCommitteeLoggedIn" runat="server" Text="0"></asp:Label></p>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Duty of Care</h1>
    
    <p>Welcome to the Duty of Care Management System. This system allows you to manage Sports and Societies within the Students' Union.</p>

    
       <p><strong>Activity Log Date: </strong> <asp:TextBox ID="txtDate" runat="server"></asp:TextBox> <asp:Button CssClass="blackbutton" ID="btnUpdate" runat="server" Text="Update" /> </p>


    <h2>Latest Activity</h2>

    <table class="nicetable">

        <asp:Repeater ID="rptActivityLog" runat="server">

            <ItemTemplate>

                 <tr>

            <td>
                <asp:Label ID="lblActivityType" runat="server" Text=""></asp:Label></td>
                      <td> <asp:Label ID="lblUser" runat="server" Text=""></asp:Label></td>
             <td> <asp:Label ID="lblDate" runat="server" Text=""></asp:Label></td>
            <td> <asp:Label ID="lblActivity" runat="server" Text=""></asp:Label></td>
        </tr>

            </ItemTemplate>

        </asp:Repeater>

        <asp:Literal ID="litNone" runat="server"><p class="error">There has been no activity on this date</p></asp:Literal>

    </table>
        
     
     


</asp:Content>
