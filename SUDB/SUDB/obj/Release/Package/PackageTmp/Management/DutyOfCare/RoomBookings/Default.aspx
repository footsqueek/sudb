﻿<%@ Page Title="Room Booking Management" Language="C#" EnableEventValidation="false" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Management.DutyOfCare.RoomBookings.Default" %>
<%@ Register src="../../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Room Bookings</h1>

    <table class="nicetable">

        <tr>

            <th>Sport / Society</th>
            <th>Request Description</th>
            <th>First Needed</th>
            <th></th>

        </tr>

    <asp:Repeater ID="rptRoomBookings" runat="server">

        <ItemTemplate>

            <tr>

                <td><asp:Label ID="lblSportSociety" runat="server" Text=""></asp:Label> </td>
                  <td><asp:Label ID="lblRequestHeadline" runat="server" Text=""></asp:Label></td>
                <td><asp:Label ID="lblRequestOpened" runat="server" Text=""></asp:Label></td>
                <td><asp:Button id="btnView"
           Text="Reply to Request"
                    CssClass="standardbutton"
           CommandName="Reply"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnRoom_Click" 
           runat="server"/></td>
            </tr>




        </ItemTemplate>


    </asp:Repeater>
    </table>

       <asp:Literal ID="litNone" runat="server"><p class="error">There are no outstanding room booking requests.</p></asp:Literal>

</asp:Content>
