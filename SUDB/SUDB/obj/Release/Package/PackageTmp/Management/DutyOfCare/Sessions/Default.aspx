﻿<%@ Page Title="Sessions" EnableEventValidation="false" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Management.DutyOfCare.RiskAssessment.Default" %>
<%@ Register src="../../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
        <uc1:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <h1>Session Management</h1>

    <p>Any session being organised by a Sport or Society should be logged in the system. Details of past and upcoming sessions are shown below.</p>

       <h2>Sessions that have taken place in the last 14 days</h2>

    <table class="nicetable">

        <tr>

            <th>Sport or Society</th>
            <th>Session Name</th>
            <th>Date Time</th>
            <th>Location</th>
            <th>Required Submissions</th>


        </tr>

    <asp:Repeater ID="rptSessionsLast14" runat="server">

        <ItemTemplate>

            
            <tr>

                   <td> <asp:Label ID="lblSportSociety" runat="server" Text="Label"></asp:Label>  </td>
                   <td> <asp:Label ID="SessionName" runat="server" Text="Label"></asp:Label> </td>
                   <td> <asp:Label ID="DateTime" runat="server" Text="Label"></asp:Label> </td>
                   <td> <asp:Label ID="Location" runat="server" Text="Label"></asp:Label> </td>
                   <td><asp:Button id="btnRegister"
           Text="Register"
                    CssClass="standardbutton"
           CommandName="Register"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnRegister_Clickbutton" 
           runat="server"/>  <asp:Button id="btnRiskAssessment"
           Text="Risk Assessment"
                    CssClass="standardorange"
           CommandName="Register"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnRiskAssessment_Click" 
           runat="server"/> <asp:Button id="btnTripReg"
           Text="Trip Registration"
                    CssClass="standardbutton"
           CommandName="Trip Registration"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnTripReg_Click" 
           runat="server"/> <asp:Button id="btnRoom"
           Text="Room Request"
                    CssClass="standardbutton"
           CommandName="Room Request"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnRoom_Click" 
           runat="server"/>  </td>

            </tr>


        </ItemTemplate>


    </asp:Repeater>
    </table>


       <h2>Scheduled Sessions in the next 14 days</h2>

    <table class="nicetable">

        <tr>

            <th>Sport or Society</th>
            <th>Session Name</th>
            <th>Date Time</th>
            <th>Location</th>
            <th>Required Submissions</th>


        </tr>

    <asp:Repeater ID="rptSessionsNext14" runat="server">

        <ItemTemplate>

            <tr>

                   <td> <asp:Label ID="lblSportSociety" runat="server" Text="Label"></asp:Label>  </td>
                   <td> <asp:Label ID="SessionName" runat="server" Text="Label"></asp:Label> </td>
                   <td> <asp:Label ID="DateTime" runat="server" Text="Label"></asp:Label> </td>
                   <td> <asp:Label ID="Location" runat="server" Text="Label"></asp:Label> </td>
                   <td><asp:Button id="btnRegister"
           Text="Register"
                    CssClass="standardbutton"
           CommandName="Register"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnRegister_Clickbutton" 
           runat="server"/>  <asp:Button id="btnRiskAssessment"
           Text="Risk Assessment"
                    CssClass="standardorange"
           CommandName="Register"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnRiskAssessment_Click" 
           runat="server"/> <asp:Button id="btnTripReg"
           Text="Trip Registration"
                    CssClass="standardbutton"
           CommandName="Trip Registration"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnTripReg_Click" 
           runat="server"/> <asp:Button id="btnRoom"
           Text="Room Request"
                    CssClass="standardbutton"
           CommandName="Room Request"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnRoom_Click" 
           runat="server"/>  </td>

            </tr>


        </ItemTemplate>


    </asp:Repeater>
    </table>

</asp:Content>
