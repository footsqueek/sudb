﻿<%@ Page Title="View Transactions" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Transactions.aspx.cs" Inherits="SUDatabase.Management.DutyOfCare.Transactions" %>
<%@ Register src="../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <h1>Transactions</h1>
    <p>The transactions processed through the sports and societies shop are shown below.</p>

       <p><strong>Please enter date: </strong> <asp:TextBox ID="txtDate" runat="server"></asp:TextBox> <asp:Button ID="btnUpdate" runat="server" Text="Update" /> </p>

    <table class="nicetable">

        <tr>

            <th>Order No.</th>
            <th>Student Number</th>
            <th>Name</th>
            <th>Date</th>
            <th>Gross</th>
            <th>Status</th>

        </tr>

    <asp:Repeater ID="rptTransactions" runat="server">

        <ItemTemplate>
            <tr>
           <td> <asp:Label ID="lblOrderNumber" runat="server" Text="0"></asp:Label></td>
           <td> <asp:Label ID="lblStudentNumber" runat="server" Text="0"></asp:Label></td>
           <td> <asp:Label ID="lblStudentName" runat="server" Text="0"></asp:Label></td>
            <td> <asp:Label ID="lblTransactionDate" runat="server" Text="0"></asp:Label></td>
                    <td> <asp:Label ID="lblTransactionAmount" runat="server" Text="0"></asp:Label></td>
            <td> <asp:Label ID="lblTransactionStatus" runat="server" Text="0"></asp:Label></td>
                </tr>
        </ItemTemplate>


    </asp:Repeater>
    </table>
    <br /><br />
    <p><strong>Total Approved: </strong><asp:Label ID="lblTotalApproved" runat="server" Text="Label"></asp:Label></p>
     <p><strong>Total Paid By Cash: </strong><asp:Label ID="lblCash" runat="server" Text="Label"></asp:Label></p>

</asp:Content>
