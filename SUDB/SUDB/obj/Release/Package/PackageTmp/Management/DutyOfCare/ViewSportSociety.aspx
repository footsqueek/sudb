﻿<%@ Page Title="View Sport / Society" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ViewSportSociety.aspx.cs" Inherits="SUDatabase.Management.DutyOfCare.ViewSportSociety" %>
<%@ Register src="../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1><asp:Label ID="lblSportSocietyName" runat="server" Text="Label"></asp:Label></h1>

    <p>This page allows you to administer a sport or society.</p>

    <p>Please select the year that you would like to view: <asp:DropDownList ID="cboYears" runat="server"></asp:DropDownList></p>


      <ul class="shopmenu">

        <li><a href="Default.aspx">Membership</a></li>
        <li><a href="/Committee/Sessions.aspx">Sessions</a></li>
        <li><a href="SocietyControls/Transport.aspx">Transport</a></li>
        <li><a href="SocietyControls/Disciplinary.aspx">Disciplinary</a></li>
            <li><a href="SocietyControls/Settings.aspx">Settings</a></li>



    </ul>


    <h2>Membership</h2>

    <table class="nicetable">

         <tr>

            <th>Student Number</th>
              <th>Name</th>
              <th>Date Joined</th>
              <th>Net</th>
              <th>VAT</th>
              <th>Gross</th>

        </tr>

    <asp:Repeater ID="rptMembers" runat="server">

        <ItemTemplate>

            <tr>
                <td> <asp:Label ID="lblStudentNumber" runat="server" Text="Label"></asp:Label></td>
                <td> <asp:Label ID="lblStudentName" runat="server" Text="Label"></asp:Label></td>
                <td> <asp:Label ID="lblDateJoined" runat="server" Text="Label"></asp:Label></td>
                      <td> <asp:Label ID="lblNetAmount" runat="server" Text="Label"></asp:Label></td>
                    <td> <asp:Label ID="lblVAT" runat="server" Text="Label"></asp:Label></td>
                    <td> <asp:Label ID="lblGross" runat="server" Text="Label"></asp:Label></td>

            </tr>

        </ItemTemplate>

    </asp:Repeater>
        
        <tr>

            <th colspan="1">Total</th>
              <th colspan="2">  <asp:Label ID="lblTotalMembers" runat="server" Text="Label"></asp:Label></th>
            <th>
                <asp:Label ID="lblNet" runat="server" Text="Label"></asp:Label></th>
                 <th>
                <asp:Label ID="lblVAT" runat="server" Text="Label"></asp:Label></th>

                 <th>
                <asp:Label ID="lblGross" runat="server" Text="Label"></asp:Label></th>

        </tr>
        </table>


</asp:Content>
