﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="CreateSportSoc.aspx.cs" Inherits="SUDatabase.Management.DutyOfCare.SocietyControls.CreateSportSoc" %>
<%@ Register src="../../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Create Sport / Society</h1>

    <p>Enter the name of your new sport / society below.</p>

     <p><asp:Label  CssClass="label" ID="Label3" runat="server" Text="Sport / Society Name"></asp:Label><br />  <asp:TextBox CssClass="txtbox" ID="txtName" runat="server"></asp:TextBox>
</p>

         <p><asp:Button  CssClass="submitbutton" ID="Button1" runat="server" Text="Save Changes" OnClick="Button1_Click" /> 
               

</asp:Content>
