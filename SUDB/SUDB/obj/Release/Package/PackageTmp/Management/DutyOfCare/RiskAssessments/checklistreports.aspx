﻿<%@ Page Title="Health and Safety Checklist Reports" EnableEventValidation="false" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="checklistreports.aspx.cs" Inherits="SUDatabase.Management.DutyOfCare.RiskAssessments.checklistreports" %>
<%@ Register src="../../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <h1>H&amp;S Checklist Reports</h1>

    <p>Below are all of the Health and Safety questions with unresolved actions that CSU must deal with.</p>

    <table class="nicetable">

        <tr>

            <th>Session Name</th>
            <th>Session Location</th>
            <th>Risk Question</th>
            <th>Who is at Risk</th>
            <th></th>

        </tr>

    <asp:Repeater ID="rptRiskAssessments" runat="server">

        <ItemTemplate>
            <asp:Label ID="lblRiskId" Visible="false" runat="server" Text=""></asp:Label>
            <tr>

                <td>
                    <asp:Label ID="lblSessionName" runat="server" Text=""></asp:Label></td>
                <td>
                    <asp:Label ID="lblSessionLocation" runat="server" Text=""></asp:Label></td>
                   <td>
                    <asp:Label ID="lblRiskQuestion" runat="server" Text=""></asp:Label></td>
                <td>
                    <asp:Label ID="lblWhoAtRisk" runat="server" Text=""></asp:Label></td>
                <td> <asp:Button id="btnRiskAssessment"
           Text="H&S Checklist"
                    CssClass="standardorange"
           CommandName="Register"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnRiskAssessment_Click" 
           runat="server"/>
                    <asp:Button id="btnClearRisk"
           Text="Clear Risk"
                    CssClass="standardbutton"
           CommandName="ClearRisk"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnClearRisk_Click" 
           runat="server"/>

                </td>
            </tr>
            <tr>

                <td colspan="5">
                    <asp:TextBox ID="txtAction" CssClass="txtbox" TextMode="MultiLine" placeholder="Please enter the action taken by the Students' Union" runat="server"></asp:TextBox></td>
                  </tr>

        </ItemTemplate>


    </asp:Repeater>

        </table>

    <asp:Literal ID="litNone" runat="server"><p class="error">There are no outstanding health and safety issues.</p></asp:Literal>

</asp:Content>
