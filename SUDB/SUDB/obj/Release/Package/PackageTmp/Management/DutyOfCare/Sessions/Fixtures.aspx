﻿<%@ Page EnableEventValidation="false" MaintainScrollPositionOnPostback="true" Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Fixtures.aspx.cs" Inherits="SUDatabase.Management.DutyOfCare.Sessions.Fixtures" %>
<%@ Register src="../../../UserControls/DutyOfCareMenu.ascx" tagname="DutyOfCareMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:DutyOfCareMenu ID="DutyOfCareMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Fixtures</h1>

    <p>What date would you like to view fixtures for? <asp:TextBox CssClass="txtbox" ID="txtFixturesDate" runat="server"></asp:TextBox> <asp:Button ID="btnRefresh" runat="server" CssClass="standardbutton" Text="Update Report" OnClick="btnRefresh_Click" />  </p>

    <h2>BUCS - <asp:Label ID="lblFixturesDate" runat="server" Text="Label"></asp:Label></h2>

    <table class="nicetable">

        <tr>
            <th>Club</th>
            <th>Team</th>
            <th>H/A</th>
            <th>v</th>
            <th>Time</th>
            <th>Venue</th>
            <!--<th>Postcode</th>-->
            <th>Transport</th>
          <!--  <th>Depart</th>-->
            <!--<th>Meet</th>-->
          
            <!--<th>Teas</th>-->
           <!-- <th>Teas Time</th>-->
               <th>Our Score</th>
               <th>Their Score</th>
            <th></th>

        </tr>

        <asp:Repeater ID="rptFixtures" runat="server">

            <ItemTemplate>

                <tr>

                     <td><asp:Label ID="lblTeam" runat="server" Text="Label"></asp:Label></td>
                      <td><asp:Label ID="lblActualTeam" runat="server" Text=""></asp:Label></td>
                     <td><asp:Label ID="lblHomeAway" runat="server" Text="Label"></asp:Label></td>
                     <td><asp:Label ID="lblv" runat="server" Text="Label"></asp:Label></td>
                     <td><asp:Label ID="lblTime" runat="server" Text="Label"></asp:Label></td>
                     <td><asp:Label ID="lblVenue" runat="server" Text="Label"></asp:Label></td>
                   <!--  <td><asp:Label ID="lblPostCode" runat="server" Text="Label"></asp:Label></td>-->
                     <td><asp:Label ID="lblTransport" runat="server" Text="Label"></asp:Label></td>
                    <!-- <td><asp:Label ID="lblDepart" runat="server" Text="Label"></asp:Label></td>-->
                   <!--  <td><asp:Label ID="lblMeet" runat="server" Text="Label"></asp:Label></td>-->
                   
                     <!-- <td><asp:Label ID="lblTeas" runat="server" Text="Label"></asp:Label></td>
                     <td><asp:Label ID="lblTeasTime" runat="server" Text="Label"></asp:Label></td>-->
                <td><asp:Label ID="lblOurScore" runat="server" Text=""></asp:Label></td>
                                   <td><asp:Label ID="lblTheirScore" runat="server" Text=""></asp:Label></td>
               
                         <td><asp:Button id="btnEdit"
           Text="Edit"
                    CssClass="standardbutton1"
           CommandName="Edit Fixture"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnEdit_Click" 
           runat="server"/> <asp:Button id="btnCancel"
           Text="Cancel"
                    CssClass="standardbutton1"
           CommandName="Cancel Session"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnCancel_Click" 
           runat="server"/></td>

                </tr>

            </ItemTemplate>


        </asp:Repeater>
    </table>

    <h2>Upload Fixtures</h2>
    <asp:FileUpload ID="FileUpload1" runat="server" /><asp:Button ID="btnUpload" CssClass="standardbutton" runat="server" Text="Upload" OnClick="btnUpload_Click" />
    <br /><br />
    <asp:Button ID="btnCreateFixture" runat="server" CssClass="submitbutton" Text="Create Fixture" OnClick="btnCreateFixture_Click" />
</asp:Content>
