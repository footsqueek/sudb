﻿<%@ Page Title="Manage Elections" EnableEventValidation="false" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Management.Elections.ManageElections.Default" %>
<%@ Register src="../../../UserControls/ElectionsMenu.ascx" tagname="ElectionsMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:ElectionsMenu ID="ElectionsMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Manage Elections</h1>

    <p>Listed below are all elections within the students' union. You should use this page to manage your elections.</p>

   <h2>Elections in Progress</h2>
     <table class="nicetable">

        <tr>
            <th>Election Name</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th></th>
        </tr>

        <asp:Repeater ID="rptInProgress" runat="server">

            <ItemTemplate>

                <tr>

                    <td><asp:Label ID="lblName" runat="server" Text="Label"></asp:Label></td>
                    <td><asp:Label ID="lblStartDate" runat="server" Text="Label"></asp:Label></td>
                    <td><asp:Label ID="lblEndDate" runat="server" Text="Label"></asp:Label></td>
                    <td><asp:Button id="btnReports"
           Text="Reports"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnInProgressReports_Clickbutton" 
           runat="server"/> </td>


                </tr>


            </ItemTemplate>

        </asp:Repeater>
          </table>
        <asp:Literal ID="litInProgressNoItems" Visible="false" runat="server"><p class="error">There are currently no elections in progress.</p></asp:Literal>

   


    <h2>Elections in Design Mode</h2>

    <table class="nicetable">

        <tr>
            <th>Election Name</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th></th>
        </tr>

        <asp:Repeater ID="rptDesignMode" runat="server">

             <ItemTemplate>

                <tr>

                    <td><asp:Label ID="lblName" runat="server" Text="Label"></asp:Label></td>
                    <td><asp:Label ID="lblStartDate" runat="server" Text="Label"></asp:Label></td>
                    <td><asp:Label ID="lblEndDate" runat="server" Text="Label"></asp:Label></td>
                    <td><asp:Button id="btnModify"
           Text="Modify Election"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnModify_Clickbutton" 
           runat="server"/> <asp:Button id="btnCandidates"
           Text="Manage Positions"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnCandidates_Clickbutton" 
           runat="server"/> <asp:Button id="btnDelete"
           Text="Delete"
                    CssClass="standardbutton"
           CommandName="Delete"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnDelete_Clickbutton" 
           runat="server"/></td>


                </tr>


            </ItemTemplate>


        </asp:Repeater>
            </table>
        <asp:Literal ID="litDesignModeNoItems" Visible="false" runat="server"><p class="error">There are currently no elections in design mode.</p></asp:Literal>




    <h2>Completed Elections</h2>
     <table class="nicetable">

        <tr>
            <th>Election Name</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th></th>
        </tr>

        <asp:Repeater ID="rptCompletedElections" runat="server">

             <ItemTemplate>

                <tr>

                    <td><asp:Label ID="lblName" runat="server" Text="Label"></asp:Label></td>
                    <td><asp:Label ID="lblStartDate" runat="server" Text="Label"></asp:Label></td>
                    <td><asp:Label ID="lblEndDate" runat="server" Text="Label"></asp:Label></td>
                    <td><asp:Button id="btnReports"
           Text="Reports"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnCompletedReports_Clickbutton" 
           runat="server"/></td>


                </tr>


            </ItemTemplate>

        </asp:Repeater>
         </table>
        <asp:Literal ID="litCompletedNoItems" Visible="false" runat="server"><p class="error">There are currently no elections that have concluded.</p></asp:Literal>

    

</asp:Content>
