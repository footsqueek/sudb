﻿<%@ Page Title="Manage Positions" EnableEventValidation="false" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Management.Elections.ManageElections.Positions.Default" %>
<%@ Register src="../../../../UserControls/ElectionsMenu.ascx" tagname="ElectionsMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:ElectionsMenu ID="ElectionsMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Election Positions</h1>

    <h2><asp:Label ID="litElectionName" runat="server" Text="Label"></asp:Label></h2>

    <p>The positions listed below have been created in this election.</p>

    <table class="nicetable">

        <tr>

            <th>Position Name</th>
            <th>Seats</th>
            <th>Voting System</th>
            <th></th>

        </tr>

        <asp:Repeater ID="rptPositions" runat="server">


            <ItemTemplate>


                <tr>

                    <td>
                        <asp:Label ID="lblPositionName" runat="server" Text="Label"></asp:Label>  </td>
                     <td>
                        <asp:Label ID="lblSeats" runat="server" Text="Label"></asp:Label>  </td>
                     <td>
                        <asp:Label ID="lblSystem" runat="server" Text="Label"></asp:Label>  </td>

                    <td><asp:Button id="btnModify"
           Text="Modify Position"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnModify_Clickbutton" 
           runat="server"/> <asp:Button id="btnCandidates"
           Text="Manage Candidates"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnCandidates_Clickbutton" 
           runat="server"/> <asp:Button id="btnDelete"
           Text="Delete"
                    CssClass="standardbutton"
           CommandName="Delete"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnDelete_Clickbutton" 
           runat="server"/></td>

                </tr>

            </ItemTemplate>


        </asp:Repeater>

    </table>

      <asp:Literal ID="litNoItems" Visible="false" runat="server"><p class="error">There are currently no positions for this election.</p></asp:Literal>

    <asp:Button ID="btnCreate" CssClass="submitbutton" runat="server" Text="Create Position" OnClick="btnCreate_Click" />

</asp:Content>
