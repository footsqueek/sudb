﻿<%@ Page Title="Student Manager" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="SearchResults.aspx.cs" Inherits="SUDatabase.Management.StudentManager.SearchResults" %>
<%@ Register src="../../UserControls/StudentManagerMenu.ascx" tagname="StudentManagerMenu" tagprefix="uc1" %>
<%@ Register src="../../UserControls/CaseMenu.ascx" tagname="CaseMenu" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:StudentManagerMenu ID="StudentManagerMenu1" runat="server" />

       <uc2:CaseMenu ID="CaseMenu1" runat="server" />

       <div class="RightMenuUL">

        <h2>Total Cases</h2>

        <p><asp:Label ID="lblTotalCases" runat="server" Text="0"></asp:Label> (<asp:Label ID="lblTotalCasesClosed" runat="server" Text="0"></asp:Label>)</p>

    </div>

    

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Search Results</h1>

    <p>Your search query <strong>
        <asp:Label ID="lblQuery" runat="server" Text="Label"></asp:Label></strong> has returned <strong><asp:Label ID="lblresults" runat="server" Text="Label"></asp:Label></strong> results.</p>

    <table class="nicetable">
        <tr>
            <th>Student Number</th>
            <th>Student Name</th>
            <th>Membership Type</th>
            <th>Flags</th>
            <th></th>

        </tr>
    <asp:Repeater ID="rptSearchResults" runat="server">

        <ItemTemplate>
            <tr>

                <td rowspan="1"><asp:Label ID="lblStudentNumber" runat="server" Text="Label"></asp:Label></td>
                <td><asp:Label ID="lblStudentName" runat="server" Text="Label"></asp:Label>
                   </td>
                 <td><asp:Label ID="lblType" runat="server" Text="Label"></asp:Label></td>
                 <td colspan="1"><asp:Image ID="imgFirstAid" ImageUrl="~/Images/Icons/Firstaid_g.png" height="25" runat="server" /> <asp:Image ID="imgDriver" ImageUrl="~/Images/Icons/Driver_g.png" height="25" runat="server" /> <asp:Image ID="imgWelfare" ImageUrl="~/Images/Icons/Welfare_g.png" height="25" runat="server" /> <asp:Image ID="imgInSport" ImageUrl="~/Images/Icons/insport_g.png" height="25" runat="server" /> <br /><asp:Image ID="imgUnder18" ImageUrl="~/Images/Icons/18_g.png" height="25" runat="server" /> <asp:Image ID="imgBeer" ImageUrl="~/Images/Icons/Beer_g.png" height="25" runat="server" /> <asp:Image ID="imgSports" ImageUrl="~/Images/Icons/Sports_g.png" height="25" runat="server" /> <asp:Image ID="imgUni" ImageUrl="~/Images/Icons/Uni_g.png" height="25" runat="server" /></td>
                <td rowspan="1">
                    <asp:Button id="btnNewCase"
           Text="Create Case"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnCreateCase_Clickbutton" 
           runat="server"/><br />
                    <asp:Button id="btnViewCases"
           Text="View Cases"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnViewCases_Clickbutton" 
           runat="server"/><br /> <asp:Button id="btnEdit"
           Text="View Student"
                    CssClass="standardbutton"
           CommandName="Edit"
           CommandArgument="<%# Container.ItemIndex %>"
           OnCommand="btnEdit_Clickbutton" 
           runat="server"/>

                </td>

               
            </tr>

        </ItemTemplate>

    </asp:Repeater>

        </table>
    <asp:Literal Visible="false" ID="litNoResults" runat="server"><br /><p class="error">Your search term returned no results, please <a href="Default.aspx">Click Here</a> to search again.</p></asp:Literal>

</asp:Content>
