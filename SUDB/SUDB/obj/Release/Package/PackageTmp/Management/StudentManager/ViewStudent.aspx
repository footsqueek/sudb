﻿<%@ Page Title="View Student" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="ViewStudent.aspx.cs" Inherits="SUDatabase.Management.StudentManager.ViewStudent" %>
<%@ Register src="../../UserControls/StudentManagerMenu.ascx" tagname="StudentManagerMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script>

    $(document).ready(

  /* This is the function that will get executed after the DOM is fully loaded */
  function () {

      $('#txtStartDate').datetimepicker({
          timepicker: false,
          format: 'd M Y',
          lang: 'en',
        
     });

      $('#txtEndDate').datetimepicker({
          timepicker: false,
         format: 'd M Y',
         lang: 'en',
        
      });

      $('#txtDOB').datetimepicker({
          timepicker: false,
          format: 'd M Y',
          lang: 'en',

      });

      $('#txtFirstAid').datetimepicker({
          timepicker: false,
          format: 'd M Y',
          lang: 'en',

      });
  }

);


    
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:StudentManagerMenu ID="StudentManagerMenu1" runat="server" />

    <div class="RightMenuUL">

        <h2>Total Cases</h2>

        <p>30</p>

    </div>

      <div class="RightMenuUL">

        <h2>Sports &amp; Societies</h2>

        <p>3</p>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>View Student Record</h1>

    <p>
        <asp:Label ID="Label1" CssClass="label" runat="server" Text="Student Name"></asp:Label>
        <asp:Label ID="lblName" runat="server" Text=""></asp:Label>

    </p>

    <p>
        <asp:Label ID="Label4" CssClass="label" runat="server" Text="Date of Birth"></asp:Label>
        <asp:TextBox ID="txtDOB" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>

     <p>
        <asp:Label ID="Label15" CssClass="label" runat="server" Text="Gender"></asp:Label>
            <asp:DropDownList ID="cboGender" CssClass="combobox" runat="server">
                <asp:ListItem Value="M">Male</asp:ListItem>
                <asp:ListItem Value="F">Female</asp:ListItem>
        </asp:DropDownList>

    </p>

        <p>
        <asp:Label ID="Label2" CssClass="label" runat="server" Text="Campus"></asp:Label>
            <asp:DropDownList ID="cboCampus" CssClass="combobox" runat="server"></asp:DropDownList>

    </p>

     <p>
        <asp:Label ID="Label5" CssClass="label" runat="server" Text="Faculty"></asp:Label>
            <asp:DropDownList ID="cboFaculty" CssClass="combobox" runat="server">
                <asp:ListItem Value="0">Please Select</asp:ListItem>
         </asp:DropDownList>

    </p>

      <p>
        <asp:Label ID="Label12" CssClass="label" runat="server" Text="Course"></asp:Label>
                <asp:TextBox ID="txtCourse" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>

        <p>
        <asp:Label ID="Label6" CssClass="label" runat="server" Text="Level"></asp:Label>
                <asp:TextBox ID="txtLevel" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>

        <p>
        <asp:Label ID="Label3" CssClass="label" runat="server" Text="Membership Type"></asp:Label>
            <asp:DropDownList ID="cboMembershipType" CssClass="combobox"  runat="server"></asp:DropDownList>

    </p>

    <p>
        <asp:Label ID="Label7" CssClass="label" runat="server" Text="Term Time Address"></asp:Label>
        <asp:TextBox ID="txtTermTime" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>

        <p>
        <asp:Label ID="Label8" CssClass="label" runat="server" Text="Personal Email"></asp:Label>
        <asp:TextBox ID="txtPeronalEmail" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>

        <p>
        <asp:Label ID="Label9" CssClass="label" runat="server" Text="Mobile Number"></asp:Label>
        <asp:TextBox ID="txtMobileNumber" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>

        <p>
        <asp:Label ID="Label10" CssClass="label" runat="server" Text="Medical Information"></asp:Label>
        <asp:TextBox ID="txtMedicalInfo" TextMode="MultiLine" Rows="5" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>

      <p>
        <asp:Label ID="Label16" CssClass="label" runat="server" Text="Share Medical with Committee"></asp:Label>
            <asp:DropDownList ID="cboShare" CssClass="combobox" runat="server">
                <asp:ListItem Value="0">No</asp:ListItem>
                <asp:ListItem Value="1">Yes</asp:ListItem>
        </asp:DropDownList>

    </p>

        <p>
        <asp:Label ID="Label11" CssClass="label" runat="server" Text="Notes"></asp:Label>
        <asp:TextBox ID="txtNotes" TextMode="MultiLine" Rows="5" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>

    <p>
        <asp:Label ID="Label13" CssClass="label" runat="server" Text="Driver Approved"></asp:Label>
            <asp:DropDownList ID="cboDriver" CssClass="combobox" runat="server">
                <asp:ListItem Value="0">No</asp:ListItem>
                <asp:ListItem Value="1">Yes</asp:ListItem>
        </asp:DropDownList>

    </p>

     <p>
        <asp:Label ID="Label14" CssClass="label" runat="server" Text="First Aid Certificate Expiry Date"></asp:Label>
           <asp:TextBox ID="txtFirstAid" CssClass="txtbox" runat="server"></asp:TextBox>


    </p>

    <h2>Sports and Societies</h2>
    <p>This student is a member of the folllowing sports and societies</p>

    <table class="lefttable nicetable">
        <tr>
            <th>Sport / Society Name</th>
            <th>Date Joined</th>
            <th>Amount Paid</th>
            <th>Order Number</th>
            
        </tr>

        <asp:Repeater ID="rptSportsSocs" runat="server">
            <ItemTemplate>

                <tr>

                    <td><asp:Label ID="lblSportSociety" runat="server" Text="Label"></asp:Label></td>
                      <td><asp:Label ID="lblDateJoined" runat="server" Text="Label"></asp:Label></td>
                      <td><asp:Label ID="lblAmount" runat="server" Text="Label"></asp:Label></td>
                      <td><asp:Label ID="lblOrderNumber" runat="server" Text="Label"></asp:Label></td>
                </tr>

            </ItemTemplate>


        </asp:Repeater>


    </table>

    <asp:Literal ID="litError" Visible="false" runat="server"><p class="error">This student is not a member of any sports or societies</p></asp:Literal>


    <span id="Disciplinary" runat="server">

      <h2>Disciplinary Record</h2>
    <p>The disciplinary record for this student is shown below.</p>

    <table class="lefttable nicetable">
        <tr>
            <th>Sanction Area</th>
           <th>Start Date</th>
             <th>End Date</th>
            <th></th>
        </tr>

        <asp:Repeater ID="rptSanctions" runat="server">
            <ItemTemplate>

                <tr id="createnotes1">

                    <td><asp:Label ID="lblSanctionArea" runat="server" Text="Label"></asp:Label></td>
                      <td><asp:Label ID="lblSanctionStart" runat="server" Text="Label"></asp:Label></td>
                      <td><asp:Label ID="lblSanctionEnd" runat="server" Text="Label"></asp:Label></td>
                    <td>
                  <asp:Button CssClass="standardbutton" ID="btnStrike" OnClick="btnDelete_Click" runat="server" Text="Strike Record" /><asp:Label ID="lblStrikeInfo" runat="server" Visible="false" Text=""></asp:Label></td>
                </tr>

                <tr id="createnotes2">

                    <td class="notes" colspan="4"><asp:Literal ID="litSanctionNotes" runat="server"></asp:Literal></td>

                </tr>

            </ItemTemplate>


        </asp:Repeater>

        

      
       

    </table>

    <asp:Literal ID="litNoDisciplinary" Visible="false" runat="server"><p class="error">This student does not have a disciplinary record.</p></asp:Literal>

    <br /><br />
    <table class="lefttable nicetable">
          <tr id="createnotes1" runat="server">
                <td><strong>Area: </strong><asp:DropDownList ID="cboSanctionType" runat="server"></asp:DropDownList></td>
                <td><strong>Start: </strong><asp:TextBox ID="txtStartDate" runat="server"></asp:TextBox></td>
              <td><strong>End: </strong><asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox></td>
             
          
          
          </tr>
       
          <tr  id="createnotes2" runat="server">

            <td colspan="3">

                <asp:TextBox Rows="2" CssClass="txtbox" TextMode="MultiLine" placeholder="Add Notes related to this sanction" ID="txtSanctionNotes" runat="server"></asp:TextBox><br />

                 <asp:RequiredFieldValidator ValidationGroup="notesgroup" ControlToValidate="txtSanctionNotes" ID="RequiredFieldValidator1" runat="server" CssClass="error" ErrorMessage="You must type some sanction notes"></asp:RequiredFieldValidator>
                <br />

                 <asp:Button ValidationGroup="notesgroup" CausesValidation="true"  CssClass="submitbutton" ID="btnSaveClose" runat="server" Text="Add Sanction" OnClick="btnSaveClose_Click" /> 


            </td>

        </tr>

    </table>
    </span>


       <asp:Button ID="btnSave" CssClass="submitbutton" runat="server" Text="Save" ValidationGroup="details" OnClick="btnSave_Click" />


</asp:Content>
