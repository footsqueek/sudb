﻿<%@ Page Title="Student Manager" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Management.StudentManager.Default" %>
<%@ Register src="../../UserControls/StudentManagerMenu.ascx" tagname="StudentManagerMenu" tagprefix="uc1" %>
<%@ Register Src="~/UserControls/CaseMenu.ascx" TagPrefix="uc1" TagName="CaseMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
       <uc1:StudentManagerMenu ID="StudentManagerMenu1" runat="server" />
    <uc1:CaseMenu runat="server" ID="CaseMenu" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="studentdiv">
    <h1>Student Manager</h1>

        <p>The student manager enables you to find information about students and how each individual has interacted with CSU. To begin please search using the box below.</p>

  <p class="center">  <asp:TextBox CssClass="searchbox" Placeholder="Please enter a student number or surname" ID="txtSearch" runat="server"></asp:TextBox></p>

        <p class="center"><asp:Button ID="btnSearch" CssClass="submitbutton" runat="server" Text="Search" OnClick="btnSearch_Click" /></p>

   

        </div>
</asp:Content>
