﻿<%@ Page Title="System Manager" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Management.SystemManager.Default" %>
<%@ Register src="../../UserControls/SystemManagerMenu.ascx" tagname="SystemManagerMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:SystemManagerMenu ID="SystemManagerMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>System Manager</h1>

    <p>The system manager allows administrators to make global changes to the system that will effect the entire organisation. Please make changes in this area of the system with caution and if in doubt contact one of the friendly people at <a href="mailto:hello@footsqueek.co.uk">Footsqueek</a> who will be able to offer advice.</p>

</asp:Content>
