﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Management.SystemManager.ActivityLog.Default" %>
<%@ Register src="../../../UserControls/SystemManagerMenu.ascx" tagname="SystemManagerMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:SystemManagerMenu ID="SystemManagerMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Activity Log</h1>

    <p>The activity log tracks user activity in the SU Database. Activity is displayed in reverse date order and can be filtered to only show specific users. The last 500 log entries are displayed on this page for each user.</p>
    <p class="filter">
        <asp:Label ID="lbltitle" CssClass="label" runat="server" Text="Filter Log by User"></asp:Label><br />
        <asp:DropDownList AutoPostBack="true" CssClass="txtbox" ID="cboUserFilter" runat="server" OnSelectedIndexChanged="cboUserFilter_SelectedIndexChanged">
        </asp:DropDownList>
    </p>
   
    <asp:Repeater ID="rptActivityLog" runat="server">

        <ItemTemplate>

            <div id="activityLogDiv" class="activityLogDiv">

               <p class="logusername"><asp:Label ID="lblUser" runat="server" Text="Label"></asp:Label> <asp:Label ID="lblActivityType" runat="server" Text="Label"></asp:Label></p>
                 <p class="logactivitydate"><asp:Label ID="lblDate" runat="server" Text="Label"></asp:Label></p>
                <p class="logdetails">
                    <asp:Literal ID="litActivityDesc" runat="server"></asp:Literal></p>




            </div>


        </ItemTemplate>

    </asp:Repeater>

    <asp:Literal Visible="false" ID="litError" runat="server"><p class="error">There are no logs for the selected user.</p></asp:Literal>

</asp:Content>
