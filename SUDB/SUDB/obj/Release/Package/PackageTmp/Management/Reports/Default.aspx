﻿<%@ Page Title="Reports" Language="C#" MasterPageFile="~/Masters/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Management.Reports.Default" %>
<%@ Register src="../../UserControls/ReportsMenu.ascx" tagname="ReportsMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
    <uc1:ReportsMenu ID="ReportsMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Reports</h1>

    <p>The reports system outputs a set of predetermined information based upon the data collected in the system. In addition Key Statistics are shown below:</p>

    <table class="nicetable">

        <tr>

            <th>Statistic</th>
            <th>#</th>
            <th>%</th>

        </tr>

        <tr>
        <td>Total Students</td>
        <td><asp:Label ID="lblTotalStudents" runat="server" Text="0"></asp:Label></td>
            <td></td>
        </tr>

        <tr>
        <td>Total Sports &amp; Society Members</td>
        <td><asp:Label ID="lblTotMembers" runat="server" Text="0"></asp:Label></td>
            <td><asp:Label ID="lblTotalMembersPercent" runat="server" Text="0"></asp:Label></td>
        </tr>
         
         <tr>
        <td>Total Unique Sports &amp; Societies Members</td>
              <td><asp:Label ID="lblIndividualMembers" runat="server" Text="0"></asp:Label></td>
            <td><asp:Label ID="lblUniqueMembersPercent" runat="server" Text="0"></asp:Label></td>
        </tr>

         <tr>
        <td>Total Sports &amp; Societies Revenue</td>
              <td><asp:Label ID="lblTotalRevenue" runat="server" Text="0"></asp:Label></td>
            <td></td>
        </tr>
 
    </table>

</asp:Content>
