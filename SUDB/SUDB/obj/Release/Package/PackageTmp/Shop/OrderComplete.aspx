﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Shop1.Master" AutoEventWireup="true" CodeBehind="OrderComplete.aspx.cs" Inherits="SUDatabase.Shop.OrderComplete" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Welcome to the Sports and Societies Shop</h1>

    <asp:Literal ID="litOrderOutcome" runat="server">  <p>Thank you, your order is now complete and you have been e-mailed a summary of your order. If you have any queries please don't hesitate to let us know.</p></asp:Literal>
   <p class="center"><asp:Button CssClass="submitbutton" ID="btnBacktoShop" runat="server" Text="Back to Shop" OnClick="btnBacktoShop_Click" /></p> 

</asp:Content>
