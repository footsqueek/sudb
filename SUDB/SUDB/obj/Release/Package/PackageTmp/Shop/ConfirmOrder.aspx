﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfirmOrder.aspx.cs" Inherits="SUDatabase.Shop.ConfirmOrder" %>

<!DOCTYPE html>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <link rel="stylesheet" href="../Styles/StyleSheet.css" />

    <title>Confirm Order</title>

     <meta name="viewport" content="width=device-width, user-scalable=no">
</head>
<body>
    <form id="form1" action="https://payments.epdq.co.uk/ncol/prod/orderstandard.asp" method="post" runat="server">
  <div id="Wrapper">

    <div id="SiteContainer">

        <div id="HeaderMenu">

            <a href="../Default.aspx"><asp:Image CssClass="topLogo" ID="imgLogo" ImageUrl="~/Images/Logos/SU1Logo_Large.png" runat="server" /></a>


            <div id="TopHeaderLinks">

              <br />
                <br />
                <br />
            </div>

            <div id="SiteName">
            <img src="/Images/fsusmall.png" alt="Footsqueek for Students' Unions Small" />
                </div>

                 </div>

            <div id="SystemMenu">

                <ul>

                    <li runat="server" id="menuStudentManager" class="StudentManagerLI"><a href="http://www.chestersu.com/you-union">Your Union</a></li>
                    <li runat="server" id="Li1" class="StudentManagerLI"><a href="http://www.chestersu.com/academic">Academic</a></li>
                    <li runat="server" id="Li2" class="StudentManagerLI"><a href="http://www.chestersu.com/advice-support">Advice &amp; Support</a></li>
                    <li runat="server" id="Li3" class="StudentManagerLI"><a href="http://www.chestersu.com/accomodation">Accomodation</a></li>
                                       <li runat="server" id="Li4" class="StudentManagerLI"><a href="http://www.chestersu.com/sports-societies">Sports &amp; Societies</a></li>
                                                       <li runat="server" id="Li5" class="StudentManagerLI"><a href="http://www.chestersu.com/events">What's On</a></li>
                                                                           <li runat="server" id="Li6" class="StudentManagerLI"><a href="http://www.chestersu.com/ch1">CH1</a></li>
                                                                           <li runat="server" id="Li7" class="StudentManagerLI"><a href="http://www.chestersu.com/campaigns">Campaigns</a></li>
                </ul>

            </div>

   

        <div id="ContentArea">



         
            <div id="container">

         <h1>Welcome to the Sports and Societies Shop</h1>

     <p>You have identied yourself as <strong><asp:Label ID="lblName" runat="server" Text="No Name"></asp:Label></strong>. If this is not you please <asp:LinkButton ID="btnSignOut" runat="server" OnClick="btnSignOut_Click">click here</asp:LinkButton>.</p>

    

    <p>You have added the items below to your shopping cart. Please click "Proceed To Payment" in order to complete your order.</p>


    <!--
        Create Submission form



        -->


    <ul class="cart">

        <asp:Repeater ID="rptCart" runat="server">

            <ItemTemplate>

                <li>
                    
                  
                    <asp:Label CssClass="cartlabel" ID="lblItem" runat="server" Text="Label"></asp:Label>
                 <asp:Label CssClass="cartlabel" ID="lblPrice" runat="server" Text="Label"></asp:Label> </li>

            </ItemTemplate>

        </asp:Repeater>
        <li class="highlighttotal">
        
                    <asp:Label  CssClass="cartlabel" ID="lblItem" runat="server" Text="Total"></asp:Label> 
                 <asp:Label CssClass="cartlabel" ID="lblTotal" runat="server" Text="Label"></asp:Label> </li>


    </ul>

    <div>
        <asp:HiddenField ID="AMOUNT" runat="server" />
        <asp:HiddenField ID="CN" runat="server" />
        <asp:HiddenField ID="COM" runat="server" />
        <asp:HiddenField ID="CURRENCY" runat="server" />
        <asp:HiddenField ID="EMAIL" runat="server" />
        <asp:HiddenField ID="FONTTYPE" runat="server" />
        <asp:HiddenField ID="LANGUAGE" runat="server" />
        <asp:HiddenField ID="LOGO" runat="server" />
        <asp:HiddenField ID="ORDERID" runat="server" />
        <asp:HiddenField ID="OWNERADDRESS" runat="server" />
        <asp:HiddenField ID="OWNERCTY" runat="server" />
        <asp:HiddenField ID="OWNERTELNO" runat="server" />
        <asp:HiddenField ID="OWNERTOWN" runat="server" />
        <asp:HiddenField ID="OWNERZIP" runat="server" />
        <asp:HiddenField ID="PMLISTTYPE" runat="server" />
        <asp:HiddenField ID="PSPID" runat="server" />
        <asp:HiddenField ID="BGCOLOR" runat="server" />
        <asp:HiddenField ID="BUTTONBGCOLOR" runat="server" />
        <asp:HiddenField ID="BUTTONTXTCOLOR" runat="server" />
        <asp:HiddenField ID="TBLBGCOLOR" runat="server" />
        <asp:HiddenField ID="TBLTXTCOLOR" runat="server" />
        <asp:HiddenField ID="TITLE" runat="server" />
        <asp:HiddenField ID="TXTCOLOR" runat="server" />
        
        <asp:HiddenField ID="SHASign" runat="server" />


        <p class="center">  <input id="Submit1" onclick="btnPayment_Click" class="submitbutton" type="submit" value="Proceed to Payment" /></p>

         <div class="clear"></div>

            </div>

    </div>
            </div>

            <div id="Footer">

                <div id="FooterContent">
           
                    </div>

        </div>

               <div id="BottomMenu">

                <div id="BottomMenuContent">
                    <p class="FootsqueekCredit">System Developed By <a href="http://www.footsqueek.co.uk">Footsqueek</a></p>
            <p>&copy; Chester Students' Union 2014</p>
                    
                    </div>

        </div>


    </form>
</body>
</html>
