﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Shop1.Master" AutoEventWireup="true" CodeBehind="Declined.aspx.cs" Inherits="SUDatabase.Shop.Declined" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h1>Welcome to the Sports and Societies Shop</h1>

    <asp:Literal ID="litOrderOutcome" runat="server"><p>Unfortunately your transaction has been declined. Please return the Sports and Societies Shop and try again or contact the Students' Union</p></asp:Literal>
   <p class="center"><asp:Button CssClass="submitbutton" ID="btnBacktoShop" runat="server" Text="Back to Shop" OnClick="btnBacktoShop_Click" /></p> 

</asp:Content>
