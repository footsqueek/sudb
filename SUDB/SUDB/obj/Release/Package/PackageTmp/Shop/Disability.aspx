﻿<%@ Page Title="Sports and Society Shop" Language="C#" MasterPageFile="~/Masters/Shop1.Master" AutoEventWireup="true" CodeBehind="Disability.aspx.cs" Inherits="SUDatabase.Shop.Disability" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <h1>Welcome to the Sports and Societies Shop</h1>

      <p>You have identied yourself as <strong><asp:Label ID="lblName" runat="server" Text="No Name"></asp:Label></strong>. If this is not you please <asp:LinkButton ID="btnSignOut" runat="server" OnClick="btnSignOut_Click">click here</asp:LinkButton>.</p>


  
    <p>A Disabled person is defined in the Disability Discrimination Act as someone with a physical or mental impairment that has a substantial and long-term impact on their ability to carry out day-to-day activities. This excludes situations where sight can be corrected by glasses or contact lenses. Having read this do you consider yourself to be covered by the definition?</p>

    <div class="center">
    <asp:Button CssClass="submitbutton" ID="btnYes" runat="server" Text="Yes" OnClick="btnYes_Click" />

     <asp:Button CssClass="submitbutton" ID="btnNo" runat="server" Text="No" OnClick="btnNo_Click" />
 </div>

</asp:Content>
