﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDB.Shop.UWSU
{
    public partial class Uncertain : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Common tools = new Common();
                int orderId = int.Parse(Request.QueryString["orderId"]);
                var transaction = tools.returnTransaction(orderId);
                if(transaction.status==1 || transaction.status==3 || transaction.status==4 || transaction.status==5)
                {
                    Response.Redirect("Approved.aspx?orderId=" + orderId.ToString());

                }
            }
            catch
            {

            }
        }

        protected void btnBacktoShop_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");

        }
    }
}