﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SUDatabase.Classes;
using System.Web.UI.HtmlControls;
using SUDB;

namespace SUDatabase.Shop
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bindToBuy();
        }

        protected void bindToBuy()
        {
            int type=1;
            try
            {
            type = int.Parse(Request.QueryString["Type"]);
            }
            catch
            {
            }
               Common tools = new Common();
               var thingstobuy = tools.returnThingsToBuy(type,1);

            if(thingstobuy.Count()==0)
            {
                litNone.Visible = true;
            }
            else
            {
                litNone.Visible = false ;
            }
              
               rptToBuy.DataSource = thingstobuy;
               rptToBuy.DataBind();


               int i = 0;

            foreach(tblSportsSociety sport in thingstobuy)
            {

                //((Label)rptToBuy.Items[i].FindControl("lblCheck")).Text = sport.Name;
                ((HtmlInputCheckBox)rptToBuy.Items[i].FindControl("myonoffswitch")).Value = sport.Id.ToString();

                if(tools.checkInCart(sport.Id))
                {
                    ((HtmlInputCheckBox)rptToBuy.Items[i].FindControl("myonoffswitch")).Attributes.Add("checked", "true");
               
                }

                ((HtmlInputCheckBox)rptToBuy.Items[i].FindControl("myonoffswitch")).ID = sport.Id.ToString();
                ((HtmlGenericControl)rptToBuy.Items[i].FindControl("lblBut")).Attributes.Add("for", sport.Id.ToString());
                ((Label)rptToBuy.Items[i].FindControl("lblName")).Text = sport.Name;
                   ((Label)rptToBuy.Items[i].FindControl("lblPrice")).Text = "&pound;" + Math.Round(tools.getPrice(sport.Id),2).ToString();

                   ((Label)rptToBuy.Items[i].FindControl("lblId")).Text = sport.Id.ToString();



              
                i++;
            }

        }

        protected void saveSessionState()
        {
            Common tools = new Common();

            //Add all selected items to the cart if they are not already added
            foreach (RepeaterItem sport in rptToBuy.Items)
            {

                String ID = ((Label)sport.FindControl("lblId")).Text;

                if (((HtmlInputCheckBox)sport.FindControl(ID)).Checked == true)
                {
                    tools.addToCart(int.Parse(((HtmlInputCheckBox)sport.FindControl(ID)).Value));
                }
                else
                {
                    tools.removeFromCart(int.Parse(((HtmlInputCheckBox)sport.FindControl(ID)).Value));
                }


            }
        }

        protected void btnCheckout_Click(object sender, EventArgs e)
        {

            Common tools = new Common();
              
            //Add all selected items to the cart if they are not already added
            foreach(RepeaterItem sport in rptToBuy.Items)
            {

                String ID = ((Label)sport.FindControl("lblId")).Text;

                if(((HtmlInputCheckBox)sport.FindControl(ID)).Checked==true)
                {
                    tools.addToCart(int.Parse(((HtmlInputCheckBox)sport.FindControl(ID)).Value));
                }
                else
                {
                    tools.removeFromCart(int.Parse(((HtmlInputCheckBox)sport.FindControl(ID)).Value));
                }
               

            }

            Response.Redirect("StudentNumber.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Common tools = new Common();

            //Add all selected items to the cart if they are not already added
            foreach (RepeaterItem sport in rptToBuy.Items)
            {

                String ID = ((Label)sport.FindControl("lblId")).Text;

                if (((HtmlInputCheckBox)sport.FindControl(ID)).Checked == true)
                {
                    tools.addToCart(int.Parse(((HtmlInputCheckBox)sport.FindControl(ID)).Value));
                }
                else
                {
                    tools.removeFromCart(int.Parse(((HtmlInputCheckBox)sport.FindControl(ID)).Value));
                }


            }

            Response.Redirect("Default.aspx?Type=1");
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            Common tools = new Common();

            //Add all selected items to the cart if they are not already added
            foreach (RepeaterItem sport in rptToBuy.Items)
            {

                String ID = ((Label)sport.FindControl("lblId")).Text;

                if (((HtmlInputCheckBox)sport.FindControl(ID)).Checked == true)
                {
                    tools.addToCart(int.Parse(((HtmlInputCheckBox)sport.FindControl(ID)).Value));
                }
                else
                {
                    tools.removeFromCart(int.Parse(((HtmlInputCheckBox)sport.FindControl(ID)).Value));
                }


            }

            Response.Redirect("Default.aspx?Type=2");
        }

        protected void LinkButton3_Click(object sender, EventArgs e)
        {
            Common tools = new Common();

            //Add all selected items to the cart if they are not already added
            foreach (RepeaterItem sport in rptToBuy.Items)
            {

                String ID = ((Label)sport.FindControl("lblId")).Text;

                if (((HtmlInputCheckBox)sport.FindControl(ID)).Checked == true)
                {
                    tools.addToCart(int.Parse(((HtmlInputCheckBox)sport.FindControl(ID)).Value));
                }
                else
                {
                    tools.removeFromCart(int.Parse(((HtmlInputCheckBox)sport.FindControl(ID)).Value));
                }


            }

            Response.Redirect("Default.aspx?Type=3");
        }

        protected void LinkButton4_Click(object sender, EventArgs e)
        {
            Common tools = new Common();

            //Add all selected items to the cart if they are not already added
            foreach (RepeaterItem sport in rptToBuy.Items)
            {

                String ID = ((Label)sport.FindControl("lblId")).Text;

                if (((HtmlInputCheckBox)sport.FindControl(ID)).Checked == true)
                {
                    tools.addToCart(int.Parse(((HtmlInputCheckBox)sport.FindControl(ID)).Value));
                }
                else
                {
                    tools.removeFromCart(int.Parse(((HtmlInputCheckBox)sport.FindControl(ID)).Value));
                }


            }

            Response.Redirect("Default.aspx?Type=4");
        }

    }
}