﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Shop1.Master" AutoEventWireup="true" CodeBehind="StudentNumber.aspx.cs" Inherits="SUDatabase.Shop.StudentNumber" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Welcome to the Sports and Societies Shop</h1>

    <p>In order to complete your purchase we need to know who you are. Please enter your student number below and click continue. If you do not know your student number please contact the Students' Union for assistance.</p>
    <br />
    <p class="center"> <asp:TextBox CssClass="searchbox" Placeholder="Place enter your student number" ID="txtSearch" runat="server"></asp:TextBox></p>

    <asp:Literal Visible="false" ID="litError" runat="server"><p class="center error">The student number you have entered cannot be found. Please try again. If this error persists then you may not have opted into Students' Union membership during enrollment. You should contact the Students' Union for assistance.</p></asp:Literal>

        <p class="center"><asp:Button ID="btnSearch" CssClass="submitbutton" runat="server" Text="Continue to Checkout" OnClick="btnSearch_Click" /></p>

   
     

</asp:Content>
