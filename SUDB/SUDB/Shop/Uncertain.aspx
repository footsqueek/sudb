﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Shop1.Master" AutoEventWireup="true" CodeBehind="Uncertain.aspx.cs" Inherits="SUDB.Shop.UWSU.Uncertain" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="systemmenuright" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <h1>Welcome to the Sports and Societies Shop</h1>

    <asp:Literal ID="litOrderOutcome" runat="server">  <p>Thank you for your order. Unfortunately we havn't been able to determine the status of your payment. This issue is usually automatically resolved but if you do not receive a confirmation e-mail from us please check with your sport or society committee to confirm your order has been sucessfully processed.</p></asp:Literal>
   <p class="center"><asp:Button CssClass="submitbutton" ID="btnBacktoShop" runat="server" Text="Back to Shop" OnClick="btnBacktoShop_Click" /></p> 

</asp:Content>
