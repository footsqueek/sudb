﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SUDatabase.Classes;
using System.Xml;
using System.Net;

namespace SUDB.Shop
{
    public partial class ConfirmAspire : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();
            //Clear Cart
            tools.clearCart();
            Session["buyasstudent"] = null;

            using (var client = new WebClient())
            {
               
                XmlDocument xdoc = new XmlDocument();//xml doc used for xml parsing

                string token = Request.QueryString["token_id"];

                xdoc.Load(
                    "https://mws.chester.ac.uk/bcpsecure/call_back.php?token_id=" + token
                );//loading XML in xml doc

            litFeed.Text = "start read";
            //XmlNode xNodelst = xdoc.DocumentElement.SelectSingleNode("/bookshopcallback_request/data/verify");//reading node so that we can traverse thorugh the XML

            foreach (XmlNode node in xdoc.DocumentElement.ChildNodes)
            {
                string text = node.InnerText; //or loop through its children as well
                if(text == "yes")
                    {
                        //Now update transaction
                        string status = Request.QueryString["action"];
                        if (status == "2")
                        {
                            //Successful
                            int newstatus = 4; //Paid with aspire;
                            tools.updateTransactionStatus(int.Parse(token), newstatus);
                            HttpContext.Current.Session.Clear();
                            HttpContext.Current.Session.Abandon();

                            var transaction = tools.returnTransaction(int.Parse(token));
                            var student = tools.returnStudent((int)transaction.studentId);

                            var transdetail = tools.returnTransactionDetail(int.Parse(token));

                            string joinedstring = "You have joined\n\n";
                            string email = "";
                            if (student.PersonalEmail != null)
                            {
                                email = student.PersonalEmail;
                            }
                            else
                            {
                                email = student.StudentNumber + "@chester.ac.uk";
                            }



                            foreach (var trans in transdetail)
                            {

                                var sport = tools.returnSportSociety((int)trans.SportSocId);
                                joinedstring = joinedstring + sport.Name + "\n" + sport.SpecialEmailInstructions + ",\n\n";
                            }


                            EmailHelper newEmail = new EmailHelper();
                            newEmail.Send(email, "Sports and Societies Order Number " + token, "Dear " + student.FirstName + ",\n\n You are now a duty of care member of your chosen sport or society. Chester Students’ Union (CSU) is a student-led charity, representing the views and interests of our members. Many of our groups and sports teams have won many awards, and we hold our own CSU Awards every year to recognise, reward and celebrate the achievements of all our members engaged with student groups.  All CSU officers and staff are passionate about helping you get the most out of your time at the University of Chester, wherever you’re based. We’re always happy to welcome students in to our offices in Chester and both Warrington sites – come and say hello! \n\n\n Duty of Care is the student membership to your sport society. The duty of care proves you have paid membership fee and all the money goes toward your chosen sport or society. All students who hold Duty of Care membership and participating within CSU activities are insured at platinum level against serious injury while participating in CSU activities. \n\n\n All our sports and societies are student run and co-ordinated by responsible elected students within that Sports Club or Society, who knows it could be you next! If you have any questions or queries please contact your club committee alternatively please come and visit the student activities team. \n\n\n Make sure you check out the online PUMA store for all of your kit needs and if you have any questions or enquiries just come and see us in the office! \n\n\n\n" + joinedstring + " \n\n#TeamChester");


                            Response.Redirect("Approved.aspx?orderId=" + token);
                        }
                        else
                        {
                            //Fail
                            int newstatus = 2;
                            tools.updateTransactionStatus(int.Parse(token), newstatus);
                            HttpContext.Current.Session.Clear();
                            HttpContext.Current.Session.Abandon();


                            var transaction = tools.returnTransaction(int.Parse(token));
                            var student = tools.returnStudent(transaction.transactionId);


                            string email = "";
                            if (student.PersonalEmail != null)
                            {
                                email = student.PersonalEmail;
                            }
                            else
                            {
                                email = student.StudentNumber + "@chester.ac.uk";
                            }



                            EmailHelper newEmail = new EmailHelper();
                            newEmail.Send(email, "Sports and Societies Order Number " + token, "Dear " + student.FirstName + ",\n\n Your recent transaction in the Students' Union Sports and Societies Online shop has been declined. Please try again or contact the Students' Union for assistance. \n\n Kind Regards\nChester Students' Union");


                            Response.Redirect("Declined.aspx");
                        }
                    }
                    
                }
                Response.Redirect("Declined.aspx");


            }




        }
    }
}