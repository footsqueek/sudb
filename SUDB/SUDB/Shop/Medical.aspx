﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Shop1.Master" AutoEventWireup="true" CodeBehind="Medical.aspx.cs" Inherits="SUDatabase.Shop.Medical" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Welcome to the Sports and Societies Shop</h1>


    <p>You have identied yourself as <strong><asp:Label ID="lblName" runat="server" Text="No Name"></asp:Label></strong>. If this is not you please <asp:LinkButton ID="btnSignOut" runat="server" OnClick="btnSignOut_Click">click here</asp:LinkButton>.</p>
        <h2>Medical Information</h2>
    <p>In order to participate in clubs and societies you must disclose to us of any medical conditions that you have that may be relevant to your participation. Please enter any details below.</p>

    <p>If you know about or are concerned that that you may have a medical condition which might interfere with undertaking activity safely you should seek advice from a relevant medical professional and follow that advice.</p>

    <p>You should not undertake activity which is beyond your own ability or you have been told is not suitable.</p>

    <p>Be aware that sporting activity carries a risk of injury. Activity is completely voluntary; by participating you accept the risk normally associated with sporting activity.</p>

    <asp:TextBox Rows="2" placeholder="Please enter any relevant medical information here." CssClass="txtboxmedicalshop" TextMode="MultiLine" ID="txtMedical" runat="server"></asp:TextBox>

    <h2>Confirm Contact Details</h2>

    <p>Please update your mobile number in the box below:</p>

     <p>
        <asp:Label ID="Label9" CssClass="label" runat="server" Text="Mobile Number"></asp:Label>
        <asp:TextBox ID="txtMobileNumber" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>

     <h2>Code of Conduct</h2>

    <p>The code of conduct covers all Chester Students’ Union (CSU) Activities authorised by and arranged with CSU. All members participating in activities are required to comply with the terms of the code conduct.</p>

    <ul class="codeofconduct">
<li>Members will conduct themselves at all times according to the best standards of behaviour and recognise that they are representing the University of Chester and the Chester Students’ Union whose policies apply to all SU activities.</li>
<li>Members should not organise or arrange activities or events (including training) without the clearance of CSU: ensuring suitable supervision is in place and completion of risk assessment. The CSU will not take responsibility for any activity not agreed with the SU office.</li>
<li>Consumption of alcohol is forbidden while partaking in activities including any travel undertaken.</li>
<li>Members are required to ensure that any use of alcohol at any time during a social activity is within the bounds of social responsibility as outlined in CSU Student Activities Policy.</li>
<li>Members will refrain from the use and involvement with any illegal substances at all times. </li>
<li>Members whose behaviour fall outside of what which is expected, including members engaging in any forms of bullying or harassment, will be dealt with under CSU disciplinary procedures and may be reported to other authorities as appropriate.</li>
<li>Members should report any inappropriate or offensive behaviour by other participants to the CSU office.</li>
<li>All activity transactions should be made directly to CSU, members are unauthorised to make payments to any committee members at any time and if instructed to do so will notify the CSU immediately.</li>
<li>Initiation ceremonies are not permitted by any sport or society. Members are to report any sport/society that undertakes initiation ceremonies.</li>
<li>Members act as an ambassador for the University, CSU and your club whilst either hosting other University clubs or when travelling to away sites.</li>
<li>Members should act within the law, and to be aware of offending others and members of the public through anti-social behaviour.</li>

        </ul>

   <p><strong>By clicking continue to checkout below I declare that as a participant of an SU activity:</strong></p>
<ul class="codeofconduct">
<li>I am over the age of 18</li>
<li>I understand and will up hold the above code of conduct and I understand that if I do not uphold these standards, disciplinary action may be taken against me under Chester Students’ Union Sport and Society, by-laws or policies. </li>
</ul>

    <p><strong>Chester Students Union will only use and share your personal information with CSU authorised staff and BUCS (British University & Colleges Sport) staff as appropriate. This will only be in connection with my involvement with any SU Activities. No third parties. </strong></p>

       <p class="center"><asp:Button ID="btnContinue" CssClass="submitbutton" runat="server" Text="Continue to Checkout" OnClick="btnContinue_Click"/></p>


</asp:Content>
