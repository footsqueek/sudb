﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Shop
{
    public partial class Medical : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["buyasstudent"] !=null)
            {
                Common tools = new Common();
                var student = tools.checkIfStudent(Session["buyasstudent"].ToString().Trim());

                lblName.Text = student.FirstName + " " + student.Surname;
                txtMobileNumber.Text = student.MobileNumber;
            }
            else
            {
                       Response.Redirect("StudentNumber.aspx");
            }
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
     

            if (Session["buyasstudent"] != null)
            {
                Common tools = new Common();
                var student = tools.checkIfStudent(Session["buyasstudent"].ToString().Trim());

                tools.updateMedical(student.Id, txtMedical.Text);
                tools.updateMobile(student.Id, txtMobileNumber.Text);
                

                Response.Redirect("OktoShare.aspx");
            }
            else
            {
                Response.Redirect("StudentNumber.aspx");
            }

          
        }

        protected void btnSignOut_Click(object sender, EventArgs e)
        {
            Session["buyasstudent"] = null;
            Response.Redirect("StudentNumber.aspx");
        }
    }
}