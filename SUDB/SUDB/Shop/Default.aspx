﻿<%@ Page Title="Sports and Societies Shop" Language="C#" MasterPageFile="~/Masters/Shop1.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SUDatabase.Shop.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Welcome to the Sports and Societies Shop</h1>

    <p>The sports and societies shop has been created to allow you to purchase your Sports and Societies Membership and other related products. To use this shop you must be a student, life member or be about to start a course.</p>

    <div id="Shopmenudiv">
    <ul class="shopmenu">

        <li><asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Chester Sports</asp:LinkButton></li>
       <li><asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click">Chester Societies</asp:LinkButton></li>
       <li><asp:LinkButton ID="LinkButton3" runat="server" OnClick="LinkButton3_Click">Warrington</asp:LinkButton></li>
      <li><asp:LinkButton ID="LinkButton4" runat="server" OnClick="LinkButton4_Click">Extras</asp:LinkButton></li>
       


    </ul>
        </div>

    <div id="thingstobuy">
    <ul>


        <asp:Repeater ID="rptToBuy" runat="server">

            <ItemTemplate>

                <li>
                    <div id="name">
                     <asp:Label ID="lblName" runat="server" Text="Mens Football"></asp:Label>
                        </div>
                           <asp:Label CssClass="price" ID="lblPrice" runat="server" Text="£49.00"></asp:Label>
<div class="onoffswitch">

    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" runat="server" id="myonoffswitch">
    <label class="onoffswitch-label" runat="server" id="lblBut">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</div>                    

                 
                    <asp:Label Visible="false" ID="lblId" runat="server" Text="0"></asp:Label>
                           

 </li>
            </ItemTemplate>


        </asp:Repeater>

        </ul>

        </div>

    </ul>

    <asp:Literal ID="litNone" Visible="false" runat="server"><p class="error">There are no items available to buy in this category.</p></asp:Literal>

    <p class="center">
    <asp:Button ID="btnCheckout" CssClass="submitbutton" runat="server" Text="Go To Check Out" OnClick="btnCheckout_Click" />
        </p>


</asp:Content>
