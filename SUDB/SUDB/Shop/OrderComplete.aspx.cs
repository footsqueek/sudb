﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Shop
{
    public partial class OrderComplete : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Common tools = new Common();
                //Clear Cart
                tools.clearCart();
                Session["buyasstudent"] = null;



                int orderId = int.Parse(Request.QueryString["orderID"]);
                int status = 0;

                if (Request.QueryString["STATUS"] == "9" || Request.QueryString["STATUS"] == "5")
                {
                    status = 1;
                    tools.updateTransactionStatus(orderId, status);
                    tools.updateTransactionPoints(orderId, status);

                    HttpContext.Current.Session.Clear();
                    HttpContext.Current.Session.Abandon();

                    var transaction = tools.returnTransaction(orderId);
                    var student = tools.returnStudent((int)transaction.studentId);

                    var transdetail = tools.returnTransactionDetail(orderId);

                    string joinedstring = "You have joined\n\n";
                    string email = "";
                    /*if (student.PersonalEmail != null)
                    {
                        email = student.PersonalEmail;
                    }
                    else
                    {*/
                        email = student.StudentNumber + "@chester.ac.uk";
                    //}



                    foreach (var trans in transdetail)
                    {

                        var sport = tools.returnSportSociety((int)trans.SportSocId);
                        joinedstring = joinedstring + sport.Name + "\n" + sport.SpecialEmailInstructions + ",\n\n";
                    }


                    EmailHelper newEmail = new EmailHelper();
                    newEmail.Send(email, "Sports and Societies Order Number " + orderId.ToString(), "Dear " + student.FirstName + ",\n\n You are now a duty of care member of your chosen sport or society. Chester Students’ Union (CSU) is a student-led charity, representing the views and interests of our members. Many of our groups and sports teams have won many awards, and we hold our own CSU Awards every year to recognise, reward and celebrate the achievements of all our members engaged with student groups.  All CSU officers and staff are passionate about helping you get the most out of your time at the University of Chester, wherever you’re based. We’re always happy to welcome students in to our offices in Chester and both Warrington sites – come and say hello! \n\n\n Duty of Care is the student membership to your sport society. The duty of care proves you have paid membership fee and all the money goes toward your chosen sport or society. All students who hold Duty of Care membership and participating within CSU activities are insured at platinum level against serious injury while participating in CSU activities. \n\n\n All our sports and societies are student run and co-ordinated by responsible elected students within that Sports Club or Society, who knows it could be you next! If you have any questions or queries please contact your club committee alternatively please come and visit the student activities team. \n\n\n Make sure you check out the online PUMA store for all of your kit needs and if you have any questions or enquiries just come and see us in the office! \n\n\n\n" + joinedstring + " \n\n#TeamChester");


                    Response.Redirect("Approved.aspx?orderId=" + orderId.ToString());
                }
                else
                {
                    status = 2;
                    tools.updateTransactionStatus(orderId, status);
                    HttpContext.Current.Session.Clear();
                    HttpContext.Current.Session.Abandon();


                    var transaction = tools.returnTransaction(orderId);
                    var student = tools.returnStudent(transaction.transactionId);


                    string email = "";
                    if (student.PersonalEmail != null)
                    {
                        email = student.PersonalEmail;
                    }
                    else
                    {
                        email = student.StudentNumber + "@chester.ac.uk";
                    }



                    EmailHelper newEmail = new EmailHelper();
                    newEmail.Send(email, "Sports and Societies Order Number " + orderId.ToString(), "Dear " + student.FirstName + ",\n\n Your recent transaction in the Students' Union Sports and Societies Online shop has been declined. Please try again or contact the Students' Union for assistance. \n\n Kind Regards\nChester Students' Union");


                    Response.Redirect("Declined.aspx");
                }


            }
            catch
            {
                try
                {
                    int orderId = int.Parse(Request.QueryString["orderID"]);

                    Response.Redirect("Uncertain.aspx?orderId=" + orderId.ToString());
                }
                catch
                {
                    Response.Redirect("Uncertain.aspx");

                }
            }
           
        }

        protected void btnBacktoShop_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}