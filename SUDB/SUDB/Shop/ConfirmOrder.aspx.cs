﻿using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Net;

namespace SUDatabase.Shop
{

    
    public partial class ConfirmOrder : System.Web.UI.Page
    {

        String orderNumber = "0";
        String orderDesc = "CSU Sports and Societies";
        protected void Page_Load(object sender, EventArgs e)
        {


            if (Session["buyasstudent"] != null)
            {
                Common tools = new Common();
                var student = tools.checkIfStudent(Session["buyasstudent"].ToString().Trim());

                lblName.Text = student.FirstName + " " + student.Surname;

                var cart = tools.ReturnCart();




                double totalprice = 0;

                foreach (tblCart item in cart)
                {
                    //Check if this person is already a member
                    if (!tools.checkIfMember((int)item.itemId, student.Id))
                    {


                        var sport = tools.returnSportSociety(int.Parse(item.itemId.ToString()));
                        totalprice = totalprice + double.Parse(tools.getPrice(int.Parse(item.itemId.ToString())).ToString());
                    }
                    }
                //Create a transaction

                var transaction = tools.createTransaction(totalprice, student.Id);

                //Add each transaction item to memberships

                foreach (tblCart item in cart)
                {
                    if (!tools.checkIfMember((int)item.itemId, student.Id))
                    {
                        var sport = tools.returnSportSociety(int.Parse(item.itemId.ToString()));
                        tools.createMembership(student.Id, sport.Id, transaction.transactionId);
                    }
                }

                orderNumber = transaction.transactionId.ToString();

            }
            else
            {
                Response.Redirect("StudentNumber.aspx");
            }






            bindCart();



         
        }


        private string SHA1HashData(string data) // encryptor
        {
            SHA1 Hasher = SHA1.Create();                                        // Create instance of Hasher
            byte[] NCodedtxt = Encoding.Default.GetBytes(data);                 // Encodes characters in string to a sequence of bytes
            byte[] HashedDataBytes = Hasher.ComputeHash(NCodedtxt);             // Encodes byte data with SHA1


            StringBuilder HashedDataStringBldr = new StringBuilder();           // Create new instance of StringBuilder to save hashed data back into (convert byte stream to string)
            for (int i = 0; i < HashedDataBytes.Length; i++)                    // Loop through each encoded byte and add it to the returnValue string
            {
                HashedDataStringBldr.Append(HashedDataBytes[i].ToString("X2")); // Returns a string of 2-digit hexadecimal values
            }

            return HashedDataStringBldr.ToString();                             // Send the sha1 hex encoded string back 
        }


        protected void bindCart()
        {
            Common tools = new Common();

            var cart = tools.ReturnCart();

            rptCart.DataSource = cart;
            rptCart.DataBind();

            int i = 0;

            double totalprice = 0;

            var student = tools.checkIfStudent(Session["buyasstudent"].ToString().Trim());


            foreach (tblCart item in cart)
            {
                //Check if this person is already a member
                if (!tools.checkIfMember((int)item.itemId, student.Id))
                {
                    var sport = tools.returnSportSociety(int.Parse(item.itemId.ToString()));

                    ((Label)rptCart.Items[i].FindControl("lblItem")).Text = sport.Name;
                    //((Button)rptCart.Items[i].FindControl("btnremove")).CommandArgument = item.itemId.ToString();
                    ((Label)rptCart.Items[i].FindControl("lblprice")).Text = Math.Round(tools.getPrice(int.Parse(item.itemId.ToString())), 2).ToString();
                    totalprice = totalprice + double.Parse(tools.getPrice(int.Parse(item.itemId.ToString())).ToString());

                }
                else
                {
                    rptCart.Items[i].Visible = false;
                }
                i++;
            }

            lblTotal.Text = "&pound;" + Math.Round(totalprice, 2);


                            
              



            //-- Set Values (these would be pulled from DB or a previous page). -- //

            //- Customer/Order Details - //
                                 // Customer details
            string strUserFirstname = student.FirstName;
            string strUserSurname = student.Surname;
                          // Address Details
            string strAd1 = student.Billing1;
            string strAd2 = student.Billing2;
            string strBillTown = student.BillingCity;               // Bill Town
            string strBillCountry = "UK";                  // Bill Country
            string strPcde = student.BillingPostCode;                         // Postcode
            string strContactTel = student.BillingPhoneNumber;             // Contact Telephone number
            string strShopperEmail = student.StudentNumber + "@chester.ac.uk";  // shopper Email
            string strShopperLocale = "en_GB";                  // shopper locale
            string strCurrencyCode = "GBP";                     // CurrecncyCode

            string strAddressline1n2 = strAd1 + ", " + strAd2;           // Concatenated Address eg 123 Penny Lane Central Areas
            string strCustomerName = strUserFirstname + " " + strUserSurname;  // Concatenated Customer Name eg Mr Edward Shopper

            string strPaymentAmount = Math.Round((totalprice*100),0).ToString();                    // This is 1 pound (100p)
            string strOrderDataRaw = orderDesc;         // Order description
            string strOrderID = orderNumber;                  // Order Id 	- **needs to be unique**

            //- integration user details - //
            string strPW = "myonlineshoporder2586";               // Update with the details you entered into back office
            string strPSPID = "epdq68068";                      // update with the details of the PSPID you were supplied with


            //- payment design options - '//
            string strTXTCOLOR = "#005588";                             // Page Text Colour
            string strTBLTXTCOLOR = "#005588";                          // Table Text Colour
            string strFONTTYPE = "Helvetica, Arial";                    // fonttype
            string strBUTTONTXTCOLOR = "#005588";                       // Button Text Colour
            string strBGCOLOR = "#d1ecf3";                              // Page Background Colour
            string strTBLBGCOLOR = "#ffffff";                           // Table BG Colour
            string strBUTTONBGCOLOR = "#cccccc";                        // Button Colour
            string strTITLE = "Chester Students' Union Sports and Societies Shop";    // Title
            string strLOGO = "https://www.sudb.co.uk/Images/Logos/SU1Logo_Large.png";    // logo location
            string strPMLISTTYPE = "1";                                 // Payment Method List type

            //= create string to hash (digest) using values of options/details above. MUST be in field alphabetical order!
            string plainDigest =
            "AMOUNT=" + strPaymentAmount + strPW +
            "BGCOLOR=" + strBGCOLOR + strPW +
            "BUTTONBGCOLOR=" + strBUTTONBGCOLOR + strPW +
            "BUTTONTXTCOLOR=" + strBUTTONTXTCOLOR + strPW +
            "CN=" + strCustomerName + strPW +
            "COM=" + strOrderDataRaw + strPW +
            "CURRENCY=" + strCurrencyCode + strPW +
            "EMAIL=" + strShopperEmail + strPW +
            "FONTTYPE=" + strFONTTYPE + strPW +
            "LANGUAGE=" + strShopperLocale + strPW +
            "LOGO=" + strLOGO + strPW +
            "ORDERID=" + strOrderID + strPW +
            "OWNERADDRESS=" + strAddressline1n2 + strPW +
            "OWNERCTY=" + strBillCountry + strPW +
            "OWNERTELNO=" + strContactTel + strPW +
            "OWNERTOWN=" + strBillTown + strPW +
            "OWNERZIP=" + strPcde + strPW +
            "PMLISTTYPE=" + strPMLISTTYPE + strPW +
            "PSPID=" + strPSPID + strPW +
            "TBLBGCOLOR=" + strTBLBGCOLOR + strPW +
            "TBLTXTCOLOR=" + strTBLTXTCOLOR + strPW +
            "TITLE=" + strTITLE + strPW +
            "TXTCOLOR=" + strTXTCOLOR + strPW +
            "";

            //-- insert payment details into hidden fields -- //
            AMOUNT.Value = strPaymentAmount;            // PaymentAmmount : (100 pence)
            ASPIREAMOUNT.Value = totalprice.ToString();      // PaymentAmmount : (100 pence)
            CN.Value = strCustomerName;                 // Customer Name
            COM.Value = strOrderDataRaw;                // OrderDataRaw (order description)
            CURRENCY.Value = strCurrencyCode;           // CurrecncyCode
            EMAIL.Value = strShopperEmail;              // shopper Email
            FONTTYPE.Value = strFONTTYPE;               // fonttype
            LANGUAGE.Value = strShopperLocale;          // shopper locale
            LOGO.Value = strLOGO;                       // logo location
            ORDERID.Value = strOrderID;                 // *this ORDER ID*
            OWNERADDRESS.Value = strAddressline1n2;     // AddressLine2
            OWNERCTY.Value = strBillCountry;            // Bill Country
            OWNERTELNO.Value = strContactTel;           // Contact Telephone number
            OWNERTOWN.Value = strBillTown;              // Bill Town
            OWNERZIP.Value = strPcde;                   // Postcode
            PMLISTTYPE.Value = strPMLISTTYPE;           // Payment Method List type
            PSPID.Value = strPSPID;                     // *Your PSPID*
            BGCOLOR.Value = strBGCOLOR;                 // Page Background Colour
            BUTTONBGCOLOR.Value = strBUTTONBGCOLOR;     // Button Colour
            BUTTONTXTCOLOR.Value = strBUTTONTXTCOLOR;   // Button Text Colour
            TBLBGCOLOR.Value = strTBLBGCOLOR;           // Table BG Colour
            TBLTXTCOLOR.Value = strTBLTXTCOLOR;         // Table Text Colour
            TITLE.Value = strTITLE;                     // Title
            TXTCOLOR.Value = strTXTCOLOR;               // Page Text Colour

            retURL.Value = "http://sumanagementhub-development.azurewebsites.net/Shop";
            retFN.Value = "ConfirmAspire.aspx";
            retCBF.Value = "call_back.php";
            merchant.Value = "Chester Students' Union Sports Shop";
            token_id.Value = orderNumber;
            user_id.Value = student.StudentNumber;

            SHASign.Value = SHA1HashData(plainDigest);  // Hashed String of plain digest put into sha sign using SHA1HashData function





        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void btnPayment_Click(object sender, EventArgs e)
        {

        }

        protected void btnSignOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("StudentNumber.aspx");
        }

        protected void btnAspire_Click(object sender, EventArgs e)
        {
            /*
            string token = "2d5889961b51d57ca762e43b45b9b238";
            string sid = "1422992";
            string amount = "2.10";

            WebRequest req = WebRequest.Create("https://mws.chester.ac.uk/bcpsecure");
            string postData = "retURL=http://www.chester.ac.uk&retFN=Default.aspx&retCBF=Default.aspx&Merchant=SportsSocsShop&token_id="+token+"&user_id="+sid+"&amount="+amount;

            byte[] send = Encoding.Default.GetBytes(postData);
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = send.Length;

            Stream sout = req.GetRequestStream();
            sout.Write(send, 0, send.Length);
            sout.Flush();
            sout.Close();

            WebResponse res = req.GetResponse();
            StreamReader sr = new StreamReader(res.GetResponseStream());
            string returnvalue = sr.ReadToEnd();

            Response.Write(returnvalue);
            */
            form1.Action = "https://mws.chester.ac.uk/bcpsecure";
        }
    }
}