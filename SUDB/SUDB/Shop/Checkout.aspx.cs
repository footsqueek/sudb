﻿using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Shop
{
    public partial class Checkout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            if (Session["buyasstudent"] != null)
            {
                Common tools = new Common();
                var student = tools.checkIfStudent(Session["buyasstudent"].ToString().Trim());

                lblName.Text = student.FirstName + " " + student.Surname;
            }
            else
            {
                Response.Redirect("StudentNumber.aspx");
            }
            bindCart();
        }

        protected void bindCart()
        {
            Common tools = new Common();

            var cart = tools.ReturnCart();

            rptCart.DataSource = cart;
            rptCart.DataBind();

            int i = 0;

            double totalprice = 0;

            foreach (tblCart item in cart)
            {
              var sport = tools.returnSportSociety(int.Parse(item.itemId.ToString()));

              ((Label)rptCart.Items[i].FindControl("lblItem")).Text = sport.Name;
              //((Button)rptCart.Items[i].FindControl("btnremove")).CommandArgument = item.itemId.ToString();
              ((Label)rptCart.Items[i].FindControl("lblprice")).Text = Math.Round(tools.getPrice(int.Parse(item.itemId.ToString())),2).ToString();
              totalprice = totalprice + double.Parse(tools.getPrice(int.Parse(item.itemId.ToString())).ToString());


                i++;
            }

            lblTotal.Text = "&pound;" + Math.Round(totalprice,2);

        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void btnPayment_Click(object sender, EventArgs e)
        {



            Common tools = new Common();
            var student = tools.checkIfStudent(Session["buyasstudent"].ToString().Trim());

            var cart = tools.ReturnCart();

           
          

            double totalprice = 0;

            foreach (tblCart item in cart)
            {
              var sport = tools.returnSportSociety(int.Parse(item.itemId.ToString()));
              totalprice = totalprice + double.Parse(tools.getPrice(int.Parse(item.itemId.ToString())).ToString());
            }
                        //Create a transaction

            var transaction = tools.createTransaction(totalprice,student.Id);

            //Add each transaction item to memberships

            foreach (tblCart item in cart)
            {
                var sport = tools.returnSportSociety(int.Parse(item.itemId.ToString()));
                tools.createMembership(student.Id, sport.Id, transaction.transactionId);
            }

            //This is where we now pass off to payment gateway!!!
            Response.Redirect("OrderComplete.aspx?orderId="+transaction.transactionId);
        }

        protected void btnSignOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("StudentNumber.aspx");
        }
    }
}