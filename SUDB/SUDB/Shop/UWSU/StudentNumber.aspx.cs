﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Shop.UWSU
{
    public partial class StudentNumber : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["buyasstudent"] != null)
            {
                Response.Redirect("Medical.aspx");
            }
           
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

            Common tools = new Common();

            if(txtSearch.Text.Substring(0,1).ToLower()=="w")
            {
                txtSearch.Text = txtSearch.Text.Substring(1, txtSearch.Text.Length-1);
            }

            if(txtSearch.Text.Length>8)
            {
                txtSearch.Text = txtSearch.Text.Substring(0, 8);
            }

            var student = tools.checkIfStudentUWSU(txtSearch.Text.Trim());

            if(student !=null)
            {
                Session["buyasstudent"] = student.StudentNumber;
                Response.Redirect("Medical.aspx");
            }
            else
            {
                litError.Visible = true;
            }

           //Response.Redirect("Checkout.aspx");
        }
    }
}