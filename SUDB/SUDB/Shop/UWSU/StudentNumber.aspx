﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/UWSUShop.Master" AutoEventWireup="true" CodeBehind="StudentNumber.aspx.cs" Inherits="SUDatabase.Shop.UWSU.StudentNumber" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Welcome to the Sports and Societies Shop</h1>

    <p>In order to complete your purchase we need to know who you are. Please enter your student number below and click continue. If you do not know your student number please contact the Students' Union for assistance.</p>
    <br />
    <p class="center"> <asp:TextBox CssClass="searchbox" Placeholder="Place enter your student number" ID="txtSearch" runat="server"></asp:TextBox></p>

    <p>You can find your student number at the bottom of your student card.  Please use the first 8 digits. Please do not use the W that you would log on to university computers with. If you have problems logging on please contact <a href="mailto:web@su.westminster.ac.uk">web@su.westminster.ac.uk</a></p>

    <asp:Literal Visible="false" ID="litError" runat="server"><p class="center error">The student number you have entered cannot be found. Please try again. If this error persists then you may not have opted into Students' Union membership during enrollment. You should contact the Students' Union for assistance.</p></asp:Literal>

        <p class="center"><asp:Button ID="btnSearch" CssClass="submitbutton" runat="server" Text="Continue to Checkout" OnClick="btnSearch_Click" /></p>

   
     

</asp:Content>
