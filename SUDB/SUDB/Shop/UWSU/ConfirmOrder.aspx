﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfirmOrder.aspx.cs" Inherits="SUDatabase.Shop.UWSU.ConfirmOrder" %>

<!DOCTYPE html>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

     <link rel="stylesheet" href="/Scripts/Menu/rmm-css/responsivemobilemenu.css" type="text/css"/>
<script type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript" src="/Scripts/Menu/rmm-js/responsivemobilemenu.js"></script>

     <link rel="stylesheet" href="../../Styles/StyleSheet.css" />


    <title>Confirm Order</title>

  <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <link rel="stylesheet" type="text/css" href="/Scripts/DatePicker/jquery.datetimepicker.css" />

<script src="/Scripts/DatePicker/jquery.datetimepicker.js"></script>
</head>
<body>
    <form id="form1" action="https://secure.worldpay.com/wcc/purchase" method="post" runat="server">
  <div id="Wrapper">

    <div id="SiteContainer">

        <div id="HeaderMenu">

            <a href="../Default.aspx"><asp:Image CssClass="topLogo" ID="imgLogo" Width="200px" ImageUrl="~/Images/Logos/UWSULogo_Large.png" runat="server" /></a>


            <div id="TopHeaderLinks">

              <br />
                <br />
                <br />
            </div>

            <div id="SiteName">
            <img src="/Images/fsusmall.png" alt="Footsqueek for Students' Unions Small" />
                </div>

                 </div>

           <div id="SystemMenu">
                  <div class="rmm">
                <ul>
                      <li runat="server" id="Li1" class="StudentManagerLI"><a href="http://www.uwsu.com/">UWSU Website</a></li>
                                       
                    <li runat="server" id="menuStudentManager" class="StudentManagerLI"><a href="http://www.uwsu.com/About">About the Students' Union</a></li>
                                       <li runat="server" id="Li4" class="StudentManagerLI"><a href="http://www.uwsu.com/sports/">Sports</a></li>
                                    
                       <li runat="server" id="Li2" class="StudentManagerLI"><a href="http://www.uwsu.com/societieslist/">Societies</a></li>
                                     <li runat="server" id="Li3" class="StudentManagerLI"><a href="http://www.uwsu.com/contact-us/">Contact Us</a></li>
                            
                                            </ul>
                      </div>
            </div>


   

        <div id="ContentArea">



         
            <div id="container">

         <h1>Welcome to the Sports and Societies Shop</h1>

     <p>You have identied yourself as <strong><asp:Label ID="lblName" runat="server" Text="No Name"></asp:Label></strong>. If this is not you please <asp:LinkButton ID="btnSignOut" runat="server" OnClick="btnSignOut_Click">click here</asp:LinkButton>.</p>

    

    <p>You have added the items below to your shopping cart. Please click "Proceed To Payment" in order to complete your order.</p>


    <!--
        Create Submission form



        -->


    <ul class="cart">

        <asp:Repeater ID="rptCart" runat="server">

            <ItemTemplate>

                <li>
                    
                  
                    <asp:Label CssClass="cartlabel" ID="lblItem" runat="server" Text="Label"></asp:Label>
                 <asp:Label CssClass="cartlabel" ID="lblPrice" runat="server" Text="Label"></asp:Label> </li>

            </ItemTemplate>

        </asp:Repeater>
        <li class="highlighttotal">
        
                    <asp:Label  CssClass="cartlabel" ID="lblItem" runat="server" Text="Total"></asp:Label> 
                 <asp:Label CssClass="cartlabel" ID="lblTotal" runat="server" Text="Label"></asp:Label> </li>


    </ul>

    <div>
        <asp:HiddenField ID="amount" runat="server" />
        <asp:HiddenField ID="instId" runat="server" />
        <asp:HiddenField ID="cartId" runat="server" />
        <asp:HiddenField ID="currency" runat="server" />
       
        <asp:HiddenField ID="desc" runat="server" />

         <asp:HiddenField ID="M_recipient" runat="server" />
       <asp:HiddenField ID="M_Subject" runat="server" />
        <asp:HiddenField ID="M_sname" runat="server" />
        <asp:HiddenField ID="M_stel" runat="server" />
        <asp:HiddenField ID="M_saddress" runat="server" />
        <asp:HiddenField ID="M_spostcode" runat="server" />

        <p class="center">  <input id="Submit1" onclick="btnPayment_Click" class="submitbutton" type="submit" value="Proceed to Payment" /></p>

         <div class="clear"></div>

        <p class="center">To refund or cancel this order after payment please contact the Sports and Societies Office on (0207) 9115000 or e-mail <a href="j.uddin5@westminster.ac.uk">j.uddin5@westminster.ac.uk</a>.</p>

        <p class="center"><img src="../../Images/powered-by-worldpay.gif" alt="Powered By WorldPay" /></p>

            </div>

    </div>
            </div>

            <div id="Footer">

                <div id="FooterContent">
           
                    </div>

        </div>

               <div id="BottomMenu">

                <div id="BottomMenuContent">
                    <p class="FootsqueekCredit">System Developed By <a href="http://www.footsqueek.co.uk">Footsqueek</a></p>
            <p>&copy; Chester Students' Union 2014</p>
                    
                    </div>

        </div>
        </div>
      </div>

    </form>
</body>
</html>
