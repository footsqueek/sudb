﻿<%@ Page Title="Sports and Society Shop" Language="C#" MasterPageFile="~/Masters/UWSUShop.Master" AutoEventWireup="true" CodeBehind="OktoShare.aspx.cs" Inherits="SUDatabase.Shop.UWSU.OktoShare" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Welcome to the Sports and Societies Shop</h1>

       <p>You have identied yourself as <strong><asp:Label ID="lblName" runat="server" Text="No Name"></asp:Label></strong>. If this is not you please <asp:LinkButton ID="btnSignOut" runat="server" OnClick="btnSignOut_Click">click here</asp:LinkButton>.</p>


    <p>You have indicated that you do have medical conditions that may affect your participation in Sports and Societies. For your safety we would like to share this information with your Sport / Society committee members but we need your permisssion to do this.</p>
    <div class="center">
    <asp:Button CssClass="submitbutton" ID="btnoktoshare" runat="server" Text="I am happy to share" OnClick="btnoktoshare_Click" />
     <asp:Button CssClass="submitbutton" ID="btndont" runat="server" Text="No, do not share" OnClick="btndont_Click" />
        </div>
</asp:Content>
