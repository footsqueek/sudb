﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/UWSUShop.Master" AutoEventWireup="true" CodeBehind="Medical.aspx.cs" Inherits="SUDatabase.Shop.UWSU.Medical" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Welcome to the Sports and Societies Shop</h1>


    <p>You have identied yourself as <strong><asp:Label ID="lblName" runat="server" Text="No Name"></asp:Label></strong>. If this is not you please <asp:LinkButton ID="btnSignOut" runat="server" OnClick="btnSignOut_Click">click here</asp:LinkButton>.</p>
        <h2>Medical Information</h2>
    <p>In order to participate in clubs and societies you must disclose to us of any medical conditions that you have that may be relevant to your participation. Please enter any details below.</p>

    <p>If you know about or are concerned that that you may have a medical condition which might interfere with undertaking activity safely you should seek advice from a relevant medical professional and follow that advice.</p>

    <p>You should not undertake activity which is beyond your own ability or you have been told is not suitable.</p>

    <p>Be aware that sporting activity carries a risk of injury. Activity is completely voluntary; by participating you accept the risk normally associated with sporting activity.</p>

    <asp:TextBox Rows="2" placeholder="Please enter any relevant medical information here." CssClass="txtboxmedicalshop" TextMode="MultiLine" ID="txtMedical" runat="server"></asp:TextBox>

    <h2>Confirm Contact Details</h2>

    <p>Please update your mobile number in the box below:</p>

     <p>
        <asp:Label ID="Label9" CssClass="label" runat="server" Text="Mobile Number"></asp:Label>
        <asp:TextBox ID="txtMobileNumber" CssClass="txtbox" runat="server"></asp:TextBox>

    </p>  

    <p>By clicking continue you confirm your acceptance of the student groups <a href="http://www.uwsu.com/activities">code of conduct.</a></p>
     
       <p class="center"><asp:Button ID="btnContinue" CssClass="submitbutton" runat="server" Text="Continue to Checkout" OnClick="btnContinue_Click"/></p>


</asp:Content>
