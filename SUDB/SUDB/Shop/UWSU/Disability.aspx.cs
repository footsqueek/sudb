﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Shop.UWSU
{
    public partial class Disability : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["buyasstudent"] != null)
            {
                Common tools = new Common();
                var student = tools.checkIfStudentUWSU(Session["buyasstudent"].ToString().Trim());

                lblName.Text = student.FirstName + " " + student.Surname;
            }
            else
            {
                Response.Redirect("StudentNumber.aspx");
            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            if (Session["buyasstudent"] != null)
            {
                Common tools = new Common();
                var student = tools.checkIfStudentUWSU(Session["buyasstudent"].ToString().Trim());

                tools.setHasDisability(student.Id);
                Response.Redirect("BillingAddress.aspx");
            }
            else
            {
                Response.Redirect("StudentNumber.aspx");
            }

        }
        protected void btnSignOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("StudentNumber.aspx");
        }
        protected void btnNo_Click(object sender, EventArgs e)
        {
            Response.Redirect("BillingAddress.aspx");
        }
    }
}