﻿<%@ Page Title="Checkout" EnableEventValidation="false" Language="C#" MasterPageFile="~/Masters/UWSUShop.Master" AutoEventWireup="true" CodeBehind="Checkout.aspx.cs" Inherits="SUDatabase.Shop.UWSU.Checkout" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Welcome to the Sports and Societies Shop</h1>

     <p>You have identied yourself as <strong><asp:Label ID="lblName" runat="server" Text="No Name"></asp:Label></strong>. If this is not you please <asp:LinkButton ID="btnSignOut" runat="server" OnClick="btnSignOut_Click">click here</asp:LinkButton>.</p>

    

    <p>You have added the items below to your shopping cart. "Please click Proceed To Payment" or "Continue Shopping". If you would like to remove items from your cart please click "Continue Shopping" and remove then deselect the items you do not wish to purchase.</p>


    <!--
        Create Submission form



        -->


    <ul class="cart">

        <asp:Repeater ID="rptCart" runat="server">

            <ItemTemplate>

                <li>
                    
                  
                    <asp:Label CssClass="cartlabel" ID="lblItem" runat="server" Text="Label"></asp:Label>
                 <asp:Label CssClass="cartlabel" ID="lblPrice" runat="server" Text="Label"></asp:Label> </li>

            </ItemTemplate>

        </asp:Repeater>
        <li class="highlighttotal">
        
                    <asp:Label  CssClass="cartlabel" ID="lblItem" runat="server" Text="Total"></asp:Label> 
                 <asp:Label CssClass="cartlabel" ID="lblTotal" runat="server" Text="Label"></asp:Label> </li>


    </ul>

    <div class="center">
    <asp:Button ID="btnContinue" CssClass="submitbutton" runat="server" Text="Continue Shopping" OnClick="btnContinue_Click" />
      <asp:Button ID="btnPayment" CssClass="submitbutton" runat="server" Text="Proceed to Payment" OnClick="btnPayment_Click" />

        </div>
</asp:Content>
