﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/UWSUShop.Master" AutoEventWireup="true" CodeBehind="BillingAddress.aspx.cs" Inherits="SUDatabase.Shop.UWSU.BillingAddress" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <h1>Welcome to the Sports and Societies Shop</h1>

     <p>You have identied yourself as <strong><asp:Label ID="lblName" runat="server" Text="No Name"></asp:Label></strong>. If this is not you please <asp:LinkButton ID="btnSignOut" runat="server" OnClick="btnSignOut_Click">click here</asp:LinkButton>.</p>

    <p>Please enter your billing address below and then click continue. You will finally confirm your order on the next page:</p>

      <p><asp:Label  CssClass="label" ID="Label1" runat="server" Text="Address Line 1"></asp:Label><br />  <asp:TextBox CssClass="txtboxline" ID="txtAddress1" runat="server"></asp:TextBox><br />
          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="error" ControlToValidate="txtAddress1" runat="server" ErrorMessage="Your must enter the first line of your address"></asp:RequiredFieldValidator>
</p>

     <p><asp:Label  CssClass="label" ID="Label2" runat="server" Text="Address Line 2"></asp:Label><br />  <asp:TextBox CssClass="txtboxline" ID="txtAddress2" runat="server"></asp:TextBox><br />
          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="error" ControlToValidate="txtAddress2" runat="server" ErrorMessage="Your must enter the second of your address"></asp:RequiredFieldValidator>
</p>

     <p><asp:Label  CssClass="label" ID="Label3" runat="server" Text="City"></asp:Label><br />  <asp:TextBox CssClass="txtboxline" ID="txtCity" runat="server"></asp:TextBox><br />
          <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="error" ControlToValidate="txtCity" runat="server" ErrorMessage="Your must enter your billing city"></asp:RequiredFieldValidator>
</p>

     <p><asp:Label  CssClass="label" ID="Label4" runat="server" Text="Postcode"></asp:Label><br />  <asp:TextBox CssClass="txtboxline" ID="txtPostcode" runat="server"></asp:TextBox><br />
          <asp:RequiredFieldValidator ID="RequiredFieldValidator4" CssClass="error" ControlToValidate="txtPostcode" runat="server" ErrorMessage="Your must enter your billing postcode"></asp:RequiredFieldValidator>
</p>

      <p><asp:Label  CssClass="label" ID="Label5" runat="server" Text="Telephone Number"></asp:Label><br />  <asp:TextBox CssClass="txtboxline" ID="txtTelephone" runat="server"></asp:TextBox><br />
          <asp:RequiredFieldValidator ID="RequiredFieldValidator5" CssClass="error" ControlToValidate="txtTelephone" runat="server" ErrorMessage="Your must enter your telephone number"></asp:RequiredFieldValidator>
</p>

    <asp:Button CssClass="submitbutton" ID="btnContinue" runat="server" Text="Continue" OnClick="btnContinue_Click" />

</asp:Content>
