﻿using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Shop.UWSU
{

    
    public partial class ConfirmOrder : System.Web.UI.Page
    {

        String orderNumber = "0";
        String orderDesc = "UWSU Sports and Societies";
        protected void Page_Load(object sender, EventArgs e)
        {


            if (Session["buyasstudent"] != null)
            {
                Common tools = new Common();
                var student = tools.checkIfStudentUWSU(Session["buyasstudent"].ToString().Trim());

                lblName.Text = student.FirstName + " " + student.Surname;

                var cart = tools.ReturnCart();




                double totalprice = 0;

                foreach (tblCart item in cart)
                {
                    //Check if this person is already a member
                    //var member = tools.


                    var sport = tools.returnSportSociety(int.Parse(item.itemId.ToString()));
                    totalprice = totalprice + double.Parse(tools.getPrice(int.Parse(item.itemId.ToString())).ToString());
                }
                //Create a transaction

                var transaction = tools.createTransactionUWSU(totalprice, student.Id);

                orderNumber = transaction.transactionId.ToString();

                //Add each transaction item to memberships

                foreach (tblCart item in cart)
                {
                    var sport = tools.returnSportSociety(int.Parse(item.itemId.ToString()));
                    tools.createMembership(student.Id, sport.Id, transaction.transactionId);
                }

                

            }
            else
            {
                Response.Redirect("StudentNumber.aspx");
            }






            bindCart();



         
        }



        protected void bindCart()
        {
            Common tools = new Common();

            var cart = tools.ReturnCart();

            rptCart.DataSource = cart;
            rptCart.DataBind();

            int i = 0;

            double totalprice = 0;

            foreach (tblCart item in cart)
            {
                var sport = tools.returnSportSociety(int.Parse(item.itemId.ToString()));

                ((Label)rptCart.Items[i].FindControl("lblItem")).Text = sport.Name;
                //((Button)rptCart.Items[i].FindControl("btnremove")).CommandArgument = item.itemId.ToString();
                ((Label)rptCart.Items[i].FindControl("lblprice")).Text = Math.Round(tools.getPrice(int.Parse(item.itemId.ToString())), 2).ToString();
                totalprice = totalprice + double.Parse(tools.getPrice(int.Parse(item.itemId.ToString())).ToString());


                i++;
            }

            lblTotal.Text = "&pound;" + Math.Round(totalprice, 2);


                            
                var student = tools.checkIfStudentUWSU(Session["buyasstudent"].ToString().Trim());




            //-- Set Values (these would be pulled from DB or a previous page). -- //

            //- Customer/Order Details - //
                                 // Customer details
            string strUserFirstname = student.FirstName;
            string strUserSurname = student.Surname;
                          // Address Details
            string strAd1 = student.Billing1;
            string strAd2 = student.Billing2;
            string strBillTown = student.BillingCity;               // Bill Town
                         // Bill Country
            string strPcde = student.BillingPostCode;                         // Postcode
            string strContactTel = student.BillingPhoneNumber;             // Contact Telephone number
            string strShopperEmail = student.StudentNumber + "@chester.ac.uk";  // shopper Email
           
            string strAddressline1n2 = strAd1 + ", " + strAd2;           // Concatenated Address eg 123 Penny Lane Central Areas
            string strCustomerName = strUserFirstname + " " + strUserSurname;  // Concatenated Customer Name eg Mr Edward Shopper

            string strPaymentAmount = Math.Round((totalprice*100),0).ToString();                    // This is 1 pound (100p)
            string strOrderDataRaw = orderDesc;         // Order description
            string strOrderID = orderNumber;                  // Order Id 	- **needs to be unique**

            

          
            
            //-- insert payment details into hidden fields -- //
            amount.Value = strPaymentAmount;            // PaymentAmmount : (100 pence)
            instId.Value = "1054840";
            cartId.Value = orderNumber;
            currency.Value = "GBP";
            amount.Value = totalprice.ToString();
            desc.Value = "UWSU Sports and Societies Order";
            M_recipient.Value = strShopperEmail;
            M_sname.Value = strUserFirstname + " " + strUserSurname;
            M_stel.Value = student.BillingPhoneNumber;
            M_saddress.Value = strAd1 + " " + strAd2 + " " + strBillTown;
            M_spostcode.Value = strPcde;

            int orderId = int.Parse(orderNumber);
            int status = 0;
            if (totalprice==0)
            {
                status = 5;
                tools.updateTransactionStatus(orderId, status);
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session.Abandon();

                var transaction = tools.returnTransaction(orderId);
                
                var transdetail = tools.returnTransactionDetail(orderId);

                string joinedstring = "You have joined\n\n";
                string email = "";
                if (student.PersonalEmail != null)
                {
                    email = student.PersonalEmail;
                }
                else
                {
                    email = student.UniversityEmail;
                }



                foreach (var trans in transdetail)
                {

                    var sport = tools.returnSportSociety((int)trans.SportSocId);
                    joinedstring = joinedstring + sport.Name + "\n" + sport.SpecialEmailInstructions + ",\n\n";
                }


                EmailHelper newEmail = new EmailHelper();
                newEmail.Send(email, "Sports and Societies Order Number " + orderId.ToString(), "Dear " + student.FirstName + ",\n\n You are now a duty of care member of your chosen sport or society. University of Westminster Students' Union is a student-led charity, representing the views and interests of our members. All officers and staff are passionate about helping you get the most out of your time at University, wherever you’re based. We’re always happy to welcome students in to our offices – come and say hello! \n\n\n Duty of Care is the student membership to your sport society. The duty of care proves you have paid membership fee and all the money goes toward your chosen sport or society. \n\n\n All our sports and societies are student run and co-ordinated by responsible elected students within that Sports Club or Society, who knows it could be you next! If you have any questions or queries please contact your club committee alternatively please come and visit the student activities team. \n\n\n \n\n\n\n" + joinedstring + " \n\nUWSU");


                Response.Redirect("Approved.aspx?orderId=" + orderId.ToString());
           
            }
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void btnPayment_Click(object sender, EventArgs e)
        {






        }

        protected void btnSignOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("StudentNumber.aspx");
        }

    }
}