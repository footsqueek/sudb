﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Shop
{
    public partial class StudentNumber : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["buyasstudent"] != null)
            {
                Response.Redirect("Medical.aspx");
            }
           
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

            Common tools = new Common();

            var student = tools.checkIfStudent(txtSearch.Text.Trim());

            if(student !=null)
            {
                Session["buyasstudent"] = student.StudentNumber;
                Response.Redirect("Medical.aspx");
            }
            else
            {
                litError.Visible = true;
            }

           //Response.Redirect("Checkout.aspx");
        }
    }
}