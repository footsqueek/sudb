﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.Shop
{
    public partial class OktoShare : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
  
            if (Session["buyasstudent"] != null)
            {
                Common tools = new Common();
                var student = tools.checkIfStudent(Session["buyasstudent"].ToString().Trim());

                if (student.MedicalInformation == "" || student.MedicalInformation == null)
                {
                    Response.Redirect("Disability.aspx");
                }
                else
                {
                    lblName.Text = student.FirstName + " " + student.Surname;
                }
            }
            else
            {
                Response.Redirect("StudentNumber.aspx");
            }
           
        }

        protected void btnoktoshare_Click(object sender, EventArgs e)
        {

            if (Session["buyasstudent"] != null)
            {
                Common tools = new Common();
                var student = tools.checkIfStudent(Session["buyasstudent"].ToString().Trim());
              
                tools.setoktoshare(student.Id);
                Response.Redirect("Disability.aspx");
            }
            else
            {
                Response.Redirect("StudentNumber.aspx");
            }

           
        }
        protected void btnSignOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("StudentNumber.aspx");
        }
        protected void btndont_Click(object sender, EventArgs e)
        {
            Response.Redirect("Disability.aspx");
        }
    }
}