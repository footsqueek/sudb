﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.UserControls
{
    public partial class DutyOfCareMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();
            if (tools.checkAccess("DutyOfCare") == "Full Access" || (tools.checkAccess("DutyOfCare") == "Read Only"))
            {

                var su = tools.getSU();
                publicfix.HRef = su.FixturesURL;
            }
            else
            {
                Response.Redirect("/Default.aspx");
            }
        }
    }
}