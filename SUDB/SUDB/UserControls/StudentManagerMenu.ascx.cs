﻿using SUDatabase.Classes;
using System.Web.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.UserControls
{
    public partial class StudentManagerMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();
            if (tools.checkAccess("StudentManager") == "Full Access" || (tools.checkAccess("StudentManager") == "Read Only"))
            {
                createnew.Visible = true;
            }
            else
            {
                Response.Redirect("/");
            }

            switch (tools.getSU().Id)
            {
                case 1:
                    lnkTelephoneEnquiry.Visible = true;
                    break;

                default:
                    lnkTelephoneEnquiry.Visible = false;
                    break;
            }
        }

        protected void lnlTelephoneEnquiry_Click(object sender, EventArgs e)
        {
            //Add 5 minute telephone enquiry
            Common tools = new Common();
            var currentuser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

            switch(tools.getSU().Id)
            {
                case 1:
                    int newcase = tools.createNewCase(1029789, 13, (Guid)currentuser.ProviderUserKey, (Guid)currentuser.ProviderUserKey, "Telephone Enquiry", 25);
                    tools.closeCase(newcase, (Guid)currentuser.ProviderUserKey, 13);
                    break;

                default:
                    break;
            }

           
        }
    }
}