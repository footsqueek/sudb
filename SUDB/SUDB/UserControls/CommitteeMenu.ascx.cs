﻿using SUDatabase.Classes;
using SUDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.UserControls
{
    public partial class CommitteeMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();
            if (tools.checkAccess("Committee") == "Full Access" || tools.checkAccess("DutyOfCare") == "Full Access" || tools.checkAccess("DutyOfCare") == "Read Only")
            {

                if(tools.isSportSockLockedOut())
                {
                    CommitteeMenuDiv.Visible = false;
                }
                else
                {
                    CommitteeMenuDiv.Visible = true;
                }

                if (!IsPostBack)
                {
                    var availsportssocs = tools.CommitteeSportsSocieties();

                    foreach (tblSportsSociety sport in availsportssocs)
                    {

                        cboSportsAndSocieties.Items.Add(new ListItem(sport.Name, sport.Id.ToString()));
                    }

                    //set active sport soc
                    try
                    {
                        var activesportsoc = tools.returnActiveSportSoc();
                        cboSportsAndSocieties.SelectedValue = activesportsoc.Id.ToString();

                        if(activesportsoc.Type==4)
                        {
                            linkActivity.Visible = false;
                            linkTeams.Visible = false;
                            linkFixtures.Visible = false;
                        }
                    }
                    catch
                    {

                    }

                    //Set active sport soc as selected sport soc
                    try
                    {
                        tools.setActiveSportSociety(int.Parse(cboSportsAndSocieties.SelectedValue));
                    }
                    catch
                    {
                        //There is no sport or society available
                        Response.Redirect("/Default.aspx?action=logout");
                    }
                }
            }
            else
            {
                Response.Redirect("/Default.aspx");
            }

            if (tools.checkAccess("DutyOfCare") == "Full Access" || tools.checkAccess("DutyOfCare") == "Read Only")
            {
                docmenu.Visible = true;
            }
            else
            {
                docmenu.Visible = false;
            }

            var su = tools.getSU();
            fixlink.HRef = su.FixturesURL;
           
        }

        protected void cboSportsAndSocieties_SelectedIndexChanged(object sender, EventArgs e)
        {
            Common tools = new Common();
           
            //Set active sport soc as selected sport soc
            tools.setActiveSportSociety(int.Parse(cboSportsAndSocieties.SelectedValue));
            Response.Redirect(Request.RawUrl);
        }
    }
}