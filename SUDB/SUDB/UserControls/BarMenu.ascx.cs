﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDB.UserControls
{
    public partial class BarMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();
            if (tools.checkAccess("Bar") == "Full Access" || (tools.checkAccess("CaseLog") == "RBar"))
            {
                
            }
            else
            {
                Response.Redirect("/");
            }
        }
    }
}