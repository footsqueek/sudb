﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DutyOfCareMenu.ascx.cs" Inherits="SUDatabase.UserControls.DutyOfCareMenu" %>
     <div id="RightMenu">
<div class="RightMenuUL">

    <h2>Duty of Care</h2>

    <ul>
        <li><a href="/Management/DutyOfCare/SportsSocieties.aspx">View Sports &amp; Societies</a></li>
         <li><a href="/Management/DutyOfCare/Transactions.aspx">View Transactions</a></li>
            <li><a href="/Management/DutyOfCare/DailyBanking.aspx">DailyBanking</a></li>

                   <li><a href="/Management/DutyOfCare/FullTransactionDetail.aspx">Full Transaction Breakdown</a></li>
              <li><a href="/Management/DutyOfCare/SocietyControls/CreateSportSoc.aspx">Create New Sport or Society</a></li>
              <li><a href="/Management/DutyOfCare/Sessions/">Sessions</a></li>
        <li><a href="/Management/DutyOfCare/RiskAssessments/MissedRiskAssessments.aspx">Missing Submissions</a></li>
         <li><a href="/Management/DutyOfCare/RiskAssessments/checklistreports.aspx">H&amp;S Checklist Reports</a></li>
                   <li><a href="/Management/DutyOfCare/Sessions/Fixtures.aspx">View Fixtures</a></li>
         <li><a href="/Management/DutyOfCare/FirstAiders.aspx">First Aiders</a></li>
           <li><a href="/Management/DutyOfCare/Drivers.aspx">Drivers</a></li>
                 <li><a href="/Management/DutyOfCare/ExpiredFirstAid.aspx">Expired First Aiders</a></li>
             <li><a href="/Management/DutyOfCare/Sessions/ManageFixtures.aspx">Add Fixtures</a></li>
            <li><a runat="server" id="publicfix" href="/Public/FixturesList.aspx">Public Fixtures List</a></li>
                    <li><a href="/Management/DutyOfCare/Sessions/TeamList.aspx">Team List</a></li>
                  
            <li><a href="/Management/DutyOfCare/TripRegistration/">Trip Registration</a></li>
                      <li><a href="/Management/DutyOfCare/TransportBookings/">Transport Bookings</a></li>
                      <li><a href="/Management/DutyOfCare/RoomBookings/">Room Bookings</a></li>
         <li><a href="/Committee/">Committee Portal</a></li>

    </ul>

</div>
         </div>