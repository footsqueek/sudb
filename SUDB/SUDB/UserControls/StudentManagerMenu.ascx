﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StudentManagerMenu.ascx.cs" Inherits="SUDatabase.UserControls.StudentManagerMenu" %>
     <div id="RightMenu">
<div class="RightMenuUL">

    <h2>Student Manager</h2>

    <ul>
         <li><a href="Default.aspx">Search Students</a></li>
        <li runat="server" visible="false" id="createnew"><a href="CreateStudent.aspx">Create New Student</a></li>
        <li><asp:LinkButton ID="lnkTelephoneEnquiry" runat="server" OnClick="lnlTelephoneEnquiry_Click">Telephone Enquiry</asp:LinkButton></li>

    </ul>

</div>
         </div>