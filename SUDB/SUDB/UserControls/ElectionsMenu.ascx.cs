﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.UserControls
{
    public partial class ElectionsMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();
            if (tools.checkAccess("Elections") == "Full Access")
            {

            }
            else if  (tools.checkAccess("DutyOfCare") == "Read Only")
            {
                //Only Display Clerking Options
                liManageElections.Visible = false;
                litCreateElection.Visible = false;
            }
            else
            {
                Response.Redirect("/Default.aspx");
            }
        }
    }
}