﻿using SUDatabase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SUDatabase.UserControls
{
    public partial class ReportsMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Common tools = new Common();
            if (tools.checkAccess("Reports") == "Full Access" || (tools.checkAccess("Reports") == "Read Only"))
            {

            }
            else
            {
                Response.Redirect("/Default.aspx");
            }
        }
    }
}