﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommitteeMenu.ascx.cs" Inherits="SUDatabase.UserControls.CommitteeMenu" %>
   <div id="RightMenu">

       <div class="RightMenuUL">
           <h2>Sport or Society</h2>
         <asp:DropDownList AutoPostBack="true" CssClass="comboboxrightmenu" ID="cboSportsAndSocieties" runat="server" OnSelectedIndexChanged="cboSportsAndSocieties_SelectedIndexChanged"></asp:DropDownList>
           </div>

   

<div id="CommitteeMenuDiv" runat="server" class="RightMenuUL">

  
    <h2>Committee</h2>

    <ul>
         <li><a href="/Committee/Default.aspx">Committee Home</a></li>
         <li><a href="/Committee/Sessions/CreateSession.aspx">Create Session</a></li>
        
        <li><a href="/Committee/Membership.aspx">View Membership</a></li>
         <li runat="server" id="linkActivity"><a href="/Committee/Sessions/Default.aspx">Activity Sessions</a></li>
          <li runat="server" id="linkTeams"><a href="/Committee/Teams/Default.aspx">Team Management</a></li>
                    <li runat="server" id="linkFixtures"><a target="_blank" runat="server" id="fixlink" href="/Public/FixturesList.aspx">BUCS Fixtures</a></li>
    </ul>
    </div>
           <div id="docmenu" runat="server">
       <div class="RightMenuUL">

      <h2>Duty of Care</h2>

      <ul>
        <li><a href="/Management/DutyOfCare/SportsSocieties.aspx">View Sports &amp; Societies</a></li>
         <li><a href="/Management/DutyOfCare/Transactions.aspx">View Transactions</a></li>
            <li><a href="/Management/DutyOfCare/DailyBanking.aspx">DailyBanking</a></li>
              <li><a href="/Management/DutyOfCare/SocietyControls/CreateSportSoc.aspx">Create New Sport or Society</a></li>
              <li><a href="/Management/DutyOfCare/Sessions/">Sessions</a></li>
        <li><a href="/Management/DutyOfCare/RiskAssessments/MissedRiskAssessments.aspx">Missing Submissions</a></li>
         <li><a href="/Management/DutyOfCare/RiskAssessments/checklistreports.aspx">H&amp;S Checklist Reports</a></li>
                   <li><a href="/Management/DutyOfCare/Sessions/Fixtures.aspx">Fixtures</a></li>
                      <li><a href="/Management/DutyOfCare/TripRegistration/">Trip Registration</a></li>
                      <li><a href="/Management/DutyOfCare/TransportBookings/">Transport Bookings</a></li>
                      <li><a href="/Management/DutyOfCare/RoomBookings/">Room Bookings</a></li>
         <li><a href="/Committee/">Committee Portal</a></li>

    </ul>
    </div>
</div>
         </div>