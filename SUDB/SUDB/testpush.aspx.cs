﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Net;
using Newtonsoft.Json.Linq;

namespace SUDB
{
    public partial class testpush : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            var student = db.tblStudents.Where(p => p.Id == 1030382).Single();

            Send("2bd206f1af62d1ab871057928d761cce6bb869d114ebd7165c490ddc869b5057");

        }

        public bool Send(string devices)
        {
            string pwAuth = "6goNTHBnNCTmhhldk2nMjlhpK+FiQzI2aJEdEc/YfrriiwZS8jt32cKQNlb5A+RRbBAzz1NlBbz3I87G+0yB";
            string pwApplication = "559F5-358C6";
            JObject json = new JObject(
                new JProperty("application", pwApplication),
                new JProperty("auth", pwAuth),
                new JProperty("notifications",
                    new JArray(
                        new JObject(
                            new JProperty("send_date", "now"),
                            new JProperty("content", "test"),
                            new JProperty("devices", devices)
                            ))));
            PWCall("createMessage", json);

            return true;
        }

        private void PWCall(string action, JObject data)
        {
            Uri url = new Uri("https://cp.pushwoosh.com/json/1.3/" + action);
            JObject json = new JObject(new JProperty("request", data));
            DoPostRequest(url, json);
        }
        private void DoPostRequest(Uri url, JObject data)
        {
            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(url);
            req.ContentType = "text/json";
            req.Method = "POST";
            using (var streamWriter = new StreamWriter(req.GetRequestStream()))
            {
                streamWriter.Write(data.ToString());
            }
            HttpWebResponse httpResponse;
            try
            {
                httpResponse = (HttpWebResponse)req.GetResponse();
            }
            catch (Exception exc)
            {
                throw new Exception(string.Format("Problem with {0}, {1}", url, exc.Message));
            }
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var responseText = streamReader.ReadToEnd();
                Page.Response.Write(responseText);
            }
        }
    }
}